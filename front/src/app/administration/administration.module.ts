import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AdministrationRoutingModule} from './administration-routing.module';
import {SharedModule} from '../shared/shared.module';
import {HtmlPageListComponent} from './pages/html-page-list/html-page-list.component';
import {HtmlPageComponent} from './pages/html-page/html-page.component';


@NgModule({
    declarations: [HtmlPageListComponent, HtmlPageComponent],
    imports: [
        CommonModule,
        AdministrationRoutingModule,
        SharedModule
    ]
})
export class AdministrationModule {
}
