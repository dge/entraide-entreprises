import {Component, OnInit} from '@angular/core';
import {AdministrationService} from '../../../store/services/administration.service';
import {take} from 'rxjs/operators';
import {HtmlPage} from '../../shared/models/html-page';
import {MatDialog} from '@angular/material/dialog';
import {HtmlModalComponent} from '../../../shared/modal/html-modal/html-modal.component';

@Component({
    selector: 'app-html-page-list',
    templateUrl: './html-page-list.component.html',
    styleUrls: ['./html-page-list.component.scss'],
})
export class HtmlPageListComponent implements OnInit {

    list: HtmlPage[];

    constructor(private administrationService: AdministrationService,
                private dialog: MatDialog) {
    }

    ngOnInit() {
    }

    ionViewDidEnter() {
        this.administrationService.getAllHtmlPages().pipe(take(1)).subscribe(list => {
            this.list = list;
        });
    }

    see(code: string) {
        this.dialog.open(HtmlModalComponent, {
            width: '70%',
            data: code,
            panelClass: 'design-system-modal'
        });
    }
}
