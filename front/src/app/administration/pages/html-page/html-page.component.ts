import {Component, OnInit} from '@angular/core';
import {AdministrationService} from '../../../store/services/administration.service';
import {ActivatedRoute} from '@angular/router';
import {map, switchMap, take} from 'rxjs/operators';
import {HtmlPage} from '../../shared/models/html-page';
import {FieldConfig} from '../../../shared/form/models/field-config';
import {Validators} from '@angular/forms';

@Component({
    selector: 'app-html-page',
    templateUrl: './html-page.component.html',
    styleUrls: ['./html-page.component.scss'],
})
export class HtmlPageComponent implements OnInit {

    item: HtmlPage;
    formConfig: FieldConfig[];

    constructor(private administrationService: AdministrationService,
                private route: ActivatedRoute) {
    }

    ngOnInit() {
    }

    ionViewDidEnter() {
        this.route.params.pipe(
            take(1),
            map(params => params.code),
            switchMap(code => this.administrationService.getHtmlPage(code))
        ).subscribe(page => {
            this.item = page[0];
            if (this.item) {
                this.formConfig = [
                    {
                        type: 'input',
                        name: 'titre',
                        label: 'Titre',
                        value: this.item.titre,
                        validations: [
                            {
                                name: 'required',
                                validator: Validators.required,
                                message: 'Le titre est obligatoire'
                            }
                        ]
                    },
                    {
                        type: 'editor',
                        name: 'contenu',
                        label: 'Contenu',
                        value: this.item.contenu,
                        validations: [
                            {
                                name: 'required',
                                validator: Validators.required,
                                message: 'Le contenu est obligatoire'
                            }
                        ]
                    }
                ];
            }
        });
    }

    save(values: { [p: string]: any }) {
        const form = {
            id: this.item.id,
            code: this.item.code,
            titre: values.titre,
            contenu: values.contenu
        };
        this.administrationService.updateHtmlPage(this.item.id, form).pipe(take(1)).subscribe();
    }
}
