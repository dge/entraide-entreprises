export interface HtmlPage {
    id: string;
    code: string;
    titre: string;
    contenu: string;
}
