import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HtmlPageListComponent} from './pages/html-page-list/html-page-list.component';
import {HtmlPageComponent} from './pages/html-page/html-page.component';

const routes: Routes = [
  {path: 'html-pages', component: HtmlPageListComponent},
  {path: 'html-pages/:code', component: HtmlPageComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdministrationRoutingModule { }
