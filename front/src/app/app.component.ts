import {Component, OnDestroy, OnInit} from '@angular/core';
import {NavigationService} from './core/navigation.service';
import {JsonipService} from './core/jsonip.service';
import {Config} from '@ionic/angular';
import {select, Store} from '@ngrx/store';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {isAdmin} from './store/user/user.selectors';
import {countModerationAction} from './store/user/user.actions';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
    unsusbcribe$: Subject<void> = new Subject<void>();

    constructor(private navigationService: NavigationService,
                private ip: JsonipService,
                private config: Config,
                private store: Store) {
        // this.config.set('navAnimation', null);
    }

    ngOnInit() {
        this.store.pipe(select(isAdmin), takeUntil(this.unsusbcribe$)).subscribe(admin => {
            if (admin) {
                this.store.dispatch(countModerationAction());
            }
        });
    }

    ngOnDestroy() {
        this.unsusbcribe$.next();
        this.unsusbcribe$.complete();
    }

}
