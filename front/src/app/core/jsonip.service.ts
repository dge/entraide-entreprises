import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {take} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {IpService} from './ip.service';

@Injectable({
    providedIn: 'root'
})
export class JsonipService {
    constructor(private http: HttpClient, private ip: IpService) {

        this.getIPAddress().pipe(take(1)).subscribe(x => {
            this.ip.ipAddress = x.ip;
        });
    }

    public getIPAddress(): Observable<{ ip: string }> {
        return this.http.get<{ ip: string }>('https://jsonip.com');
    }
}
