import { Component, OnInit, Inject } from '@angular/core';
import { MAT_SNACK_BAR_DATA, MatSnackBar } from '@angular/material/snack-bar';
import { SnackbarData } from './snackbar-data';
@Component({
  selector: 'app-snackbar',
  templateUrl: './snackbar.component.html',
  styleUrls: ['./snackbar.component.scss']
})
export class SnackbarComponent implements OnInit {
  public message: string;
  public messageList: string[];

  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: SnackbarData, private matSnackBar: MatSnackBar) {}

  ngOnInit() {
    if (this.data.message instanceof Array && this.checkIfString(this.data.message)) {
      const mess = this.data.message as Array<string>;
      this.messageList = mess.filter(this.onlyUnique);
    } else {
      this.message = this.data.message as string;
    }
  }

  close() {
    this.matSnackBar.dismiss();
  }

  onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }

  checkIfString(x) {
    return x.every(i => {
      return typeof i === 'string';
    });
  }
}
