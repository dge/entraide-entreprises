import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarRef } from '@angular/material/snack-bar';
import { SnackbarComponent } from './snackbar.component';
import { SnackbarData } from './snackbar-data';
import {LoadingState} from '../loading-state.enum';

@Injectable({
  providedIn: 'root'
})
export class SnackbarService {
  public static readonly WAITING = 'snackbar-waiting';
  public static readonly SUCCESS = 'snackbar-success';
  public static readonly ERROR = 'snackbar-error';
  public snackBar: MatSnackBarRef<any>;

  constructor(private matSnackBar: MatSnackBar) {}

  public openSnackBar(type: LoadingState, data: SnackbarData) {
    this.snackBar = this.matSnackBar.openFromComponent(SnackbarComponent, {
      data,
      duration: data.duration ? data.duration : type === LoadingState.ERROR ? -1 : 2500,
      panelClass: SnackbarService[LoadingState[type]],
      verticalPosition: 'top',
      horizontalPosition: 'right'
    });
  }

  public closeSnackBar() {
    if (this.snackBar) {
      this.snackBar.dismiss();
    }
  }
}
