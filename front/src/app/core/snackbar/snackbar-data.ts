export class SnackbarData {
  constructor(public titre: string, public message: string | string[], public duration?: number) {}
}
