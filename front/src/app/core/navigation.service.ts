import {Injectable} from '@angular/core';
import {Location} from '@angular/common';
import {NavigationEnd, NavigationStart, Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {

  private history: string[] = [];

  constructor(private router: Router, private location: Location) {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        this.history.push(event.url);
      }
    });
  }

  back(): void {
    this.history.pop();
    if (this.history.length > 0) {
      this.router.navigateByUrl(this.history[this.history.length - 1]);
    } else {
      this.router.navigateByUrl('/');
    }
  }
}
