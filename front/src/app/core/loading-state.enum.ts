export enum LoadingState {
  LOADING,
  WAITING,
  SUCCESS,
  ERROR
}
