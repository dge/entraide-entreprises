import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SirenResponse} from './models/siren-response';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SiretService {

  constructor(private httpClient: HttpClient) {
  }

  verifySiret(siret: string): Observable<SirenResponse> {
    return this.httpClient.get<SirenResponse>(`https://entreprise.data.gouv.fr/api/sirene/v3/etablissements/${siret}`);
  }

  getEntity(siren: SirenResponse) {
    const name = siren.etablissement.unite_legale.denomination ? siren.etablissement.unite_legale.denomination :
      siren.etablissement.unite_legale.nom + ' ' + siren.etablissement.unite_legale.prenom_usuel;
    return {
      id: null,
      name,
      siret: siren.etablissement.siret,
      address: siren.etablissement.numero_voie + ' ' + siren.etablissement.type_voie + ' ' + siren.etablissement.libelle_voie,
      addressBis: siren.etablissement.complement_adresse,
      codePostal: siren.etablissement.code_postal,
      city: siren.etablissement.libelle_commune,
      actif: false
    };
  }
}
