import {Injectable} from '@angular/core';
import {LoginModalComponent} from './login.component';
import {ModalController} from '@ionic/angular';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private modalController: ModalController,
              private router: Router) {
  }

  async openLoginForm(redirect?: boolean) {
    this.router.navigate(['/login']);

    const modal = await this.modalController.create({
      component: LoginModalComponent
    });
    await modal.present();
    const bool = await modal.onWillDismiss();
    if (bool.data.loggedIn && redirect) {
      this.router.navigate(['/']);
    }
    return bool;
  }
}
