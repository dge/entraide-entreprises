import {Component, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-login-modal',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginModalComponent implements OnInit {
  form: FormGroup;

  constructor(private modalController: ModalController) {
  }

  ngOnInit() {
  }

  dismissModal(value: boolean): void {
    this.modalController.dismiss({
      loggedIn: value
    });
  }
}
