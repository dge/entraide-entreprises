import {Thematique} from '../questions/shared/models/thematique';

export class Tools {

    public static sortThematicByQuestionCountDesc(a: Thematique, b: Thematique) {
        if (a.questionCount > b.questionCount) {
            return -1;
        } else if (a.questionCount < b.questionCount) {
            return 1;
        } else if (a.label < b.label) {
            return -1;
        } else if (a.label > b.label) {
            return 1;
        }
        return 0;
    }

    public static sortThematicByLabelAsc(a: Thematique, b: Thematique) {
        if (a.label < b.label) {
            return -1;
        } else if (a.label > b.label) {
            return 1;
        }
        return 0;
    }

    public static sortThematicByOrderAsc(a: Thematique, b: Thematique) {
        if (a.order && (!b.order || a.order < b.order)) {
            return -1;
        } else if (!a.order || a.order > b.order) {
            return 1;
        }
        return 0;
    }

    public static downloadFile(filename, contentType, data) {
        const fileAsArray = Tools.base64DecToArr(data);
        const blob = new Blob([fileAsArray], {type: contentType});


        const URL = window.URL || (window as any).webkitURL;
        const downloadUrl = URL.createObjectURL(blob);

        if (filename) {
            // use HTML5 a[download] attribute to specify filename
            const a = document.createElement('a');
            // safari doesn't support this yet
            if (typeof a.download === 'undefined') {
                window.location.href = downloadUrl;
            } else {
                a.href = downloadUrl;
                a.download = filename;
                document.body.appendChild(a);
                a.click();
            }
        } else {
            window.location.href = downloadUrl;
        }

        setTimeout(() => {
            URL.revokeObjectURL(downloadUrl);
        }, 100); // cleanup

    }

    public static b64ToUint6(nChr) {
        return nChr > 64 && nChr < 91
            ? nChr - 65
            : nChr > 96 && nChr < 123
                ? nChr - 71
                : nChr > 47 && nChr < 58
                    ? nChr + 4
                    : nChr === 43
                        ? 62
                        : nChr === 47
                            ? 63
                            : 0;
    }

    public static base64DecToArr(sBase64, nBlocksSize?) {
        /* tslint:disable:no-bitwise */
        const sB64Enc = sBase64.replace(/[^A-Za-z0-9\+\/]/g, '');
        const nInLen = sB64Enc.length;
        const nOutLen = nBlocksSize
            ? Math.ceil(((nInLen * 3 + 1) >> 2) / nBlocksSize) * nBlocksSize
            : (nInLen * 3 + 1) >> 2;
        const taBytes = new Uint8Array(nOutLen);

        for (let nMod3, nMod4, nUint24 = 0, nOutIdx = 0, nInIdx = 0; nInIdx < nInLen; nInIdx++) {
            nMod4 = nInIdx & 3;
            nUint24 |= Tools.b64ToUint6(sB64Enc.charCodeAt(nInIdx)) << (18 - 6 * nMod4);
            if (nMod4 === 3 || nInLen - nInIdx === 1) {
                for (nMod3 = 0; nMod3 < 3 && nOutIdx < nOutLen; nMod3++, nOutIdx++) {
                    taBytes[nOutIdx] = (nUint24 >>> ((16 >>> nMod3) & 24)) & 255;
                }
                nUint24 = 0;
            }
        }
        return taBytes;
    }

}
