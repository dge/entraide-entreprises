import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {Storage} from '@ionic/storage-angular';

@Injectable({
  providedIn: 'root'
})
export class UnlogguedGuard implements CanActivate {
  constructor(private storage: Storage, private router: Router) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.storage.get('user').then(x => {
      if (x) {
        this.router.navigate(['/']);
        return false;
      }
      return true;
    });
  }

}
