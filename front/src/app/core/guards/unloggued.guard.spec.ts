import { TestBed } from '@angular/core/testing';

import { UnlogguedGuard } from './unloggued.guard';

describe('UnlogguedGuard', () => {
  let guard: UnlogguedGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(UnlogguedGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
