import { TokenInterceptor } from './token-interceptor';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { TestBed } from '@angular/core/testing';

describe('TokenInterceptor', () => {
  let service: TokenInterceptor;
  let store: MockStore;
  const initialState = {
    token: null,
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TokenInterceptor, provideMockStore({ initialState })],
    });
    store = TestBed.inject(MockStore);
    service = TestBed.inject(TokenInterceptor);
  });

  it('should create an instance', () => {
    expect(service).toBeTruthy();
  });
});
