import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { ToastController } from '@ionic/angular';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {

    constructor(private toastController: ToastController) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(
            catchError((err: any) => {
                if (err instanceof HttpErrorResponse && err.status === 500) {
                    try {
                        this.showToast(err.error.data.message)
                    } catch (e) {
                        console.log(e)
                    }
                }
                return throwError(err);
            })
        );
    }

    async showToast(message: string) {
        const toast = await this.toastController.create({
            message,
            duration: 5000,
            color: 'danger'
        });

        toast.present();
    }
}
