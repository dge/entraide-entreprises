import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {first, mergeMap} from 'rxjs/operators';
import {Storage} from '@ionic/storage';
import {fromPromise} from 'rxjs/internal-compatibility';
import {IpService} from '../ip.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
    constructor(private storage: Storage, private ip: IpService) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const promise = this.storage.get('token');
        return fromPromise(promise).pipe(
            first(),
            mergeMap((token) => {
                let authReq = request;
                if (token != null && request.url !== 'https://jsonip.com') {
                    authReq = request.clone({
                        setHeaders: {
                            Authorization: 'Bearer ' + token,
                            'x-ip': 'IP ' + this.ip.ipAddress
                        }
                    });
                }
                return next.handle(authReq);
            }),
        );
    }
}
