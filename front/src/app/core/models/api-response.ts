export interface ApiResponse<T> {
  data: T;
  error: boolean;
  statusCode: number;
}
