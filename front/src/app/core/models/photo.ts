export interface Photo {
  base64: string;
  webviewPath: string;
}
