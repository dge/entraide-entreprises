import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { UserEffects } from './user.effects';
import { SharedModule } from '../../shared/shared.module';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('UserEffects', () => {
  const actions$: Observable<any> = null;
  let effects: UserEffects;
  let store: MockStore;
  const initialState = { loading: false };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, RouterTestingModule, HttpClientTestingModule],
      providers: [UserEffects, provideMockActions(() => actions$), provideMockStore({ initialState })],
    });

    store = TestBed.inject(MockStore);
    effects = TestBed.inject(UserEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
