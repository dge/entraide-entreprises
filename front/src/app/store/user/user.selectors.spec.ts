import * as fromUser from './user.reducer';
import { selectUserState } from './user.selectors';

describe('User Selectors', () => {
  it('should select the feature state', () => {
    const result = selectUserState({
      [fromUser.userFeatureKey]: {
        error: null,
        loading: false,
        infos: null,
        user: null,
        token: null,
      },
    });

    expect(result).toEqual({
      error: null,
      loading: false,
      infos: null,
      user: null,
      token: null,
    });
  });
});
