import {createFeatureSelector, createSelector} from '@ngrx/store';
import * as fromUser from './user.reducer';

export const selectUserState = createFeatureSelector<fromUser.State>(fromUser.userFeatureKey);

export const isAuthenticated = createSelector(selectUserState, (state: fromUser.State) => state.user != null);
export const getUser = createSelector(selectUserState, (state: fromUser.State) => state.user);
export const getMe = createSelector(selectUserState, (state: fromUser.State) => state.me);
export const getRoles = createSelector(selectUserState, (state: fromUser.State) => state.user ? state.user.roles : []);
export const isAdmin = createSelector(selectUserState, (state: fromUser.State) => state.user && state.user.roles ? state.user.roles.indexOf('ADMIN') !== -1 : false);
export const getToken = createSelector(selectUserState, (state: fromUser.State) => state.token);
export const getloginError = createSelector(selectUserState, (state: fromUser.State) => state.error);
export const askResetPasswordError = createSelector(selectUserState, (state: fromUser.State) => state.error);
export const resetPassword = createSelector(selectUserState, (state: fromUser.State) => state.error);
export const resetPasswordError = createSelector(selectUserState, (state: fromUser.State) => state.error);
export const getNotifications = createSelector(selectUserState, (state: fromUser.State) => state.notifications);
export const getNbNotifications = createSelector(selectUserState,
  (state: fromUser.State) => state.notifications ? state.notifications.length : 0);
export const getModerationCount = createSelector(selectUserState, (state: fromUser.State) => state.moderationCount);
