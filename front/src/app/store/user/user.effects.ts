import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {catchError, concatMap, mergeMap, tap, withLatestFrom} from 'rxjs/operators';

import * as UserActions from './user.actions';
import {UserService} from '../services/user.service';
import {UserInfos} from '../models/user-infos';
import {of} from 'rxjs';
import {ApiResponse} from '../../core/models/api-response';
import {StorageService} from '../services/storage.service';
import * as GlobalActions from '../global/global.actions';
import {getRouter} from '../router/router.state';
import {Store} from '@ngrx/store';
import {UserNotification} from '../models/user-notification';
import {User} from '../models/user';
import {Router} from '@angular/router';

@Injectable()
export class UserEffects {

    loginUser$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(UserActions.loginUser),
            concatMap((action) =>
                this.userService.login(action.username, action.password).pipe(
                    tap((resp: ApiResponse<UserInfos>) => {
                        this.storageService.set('token', resp.data.accessToken);
                        this.storageService.set('user', JSON.stringify(resp.data.user));
                    }),
                    mergeMap((resp: ApiResponse<UserInfos>) => [UserActions.loginUserSuccess({userInfos: resp.data})]),
                    catchError((errorResp) => of(UserActions.loginUserError({error: errorResp.error.data.message}))),
                ),
            ),
        );
    });

    signUpUser$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(UserActions.signUpUser),
            concatMap((action) =>
                this.userService.signUp(action.user).pipe(
                    tap((resp: ApiResponse<UserInfos>) => {
                        this.storageService.set('token', resp.data.accessToken);
                        this.storageService.set('user', JSON.stringify(resp.data.user));
                    }),
                    mergeMap((resp: ApiResponse<UserInfos>) => [UserActions.signUpUserSuccess({userInfos: resp.data})]),
                    catchError((errorResp) => of(UserActions.signUpUserError({error: errorResp.error.statusCode}))),
                ),
            ),
        );
    });

    askResetPassword$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(UserActions.askResetUserPasswordUser),
            concatMap((action) =>
                this.userService.askResetPassword(action.email).pipe(
                    mergeMap((resp: ApiResponse<boolean>) => [UserActions.askResetUserPasswordSuccess({success: resp.data})]),
                    catchError((errorResp) => of(UserActions.askResetUserPasswordError({error: errorResp.error.data.message})))
                )
            )
        );
    });

    resetPassword$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(UserActions.resetUserPasswordUser),
            withLatestFrom(this.store.select(getRouter), (action, route) => {
                return {
                    userId: route.state.queryParams.userId,
                    password: action.password,
                    token: route.state.queryParams.token
                };
            }),
            concatMap((action) =>
                this.userService.resetPassword(action.userId, action.password, action.token).pipe(
                    mergeMap((resp: ApiResponse<boolean>) => [UserActions.resetUserPasswordSuccess({success: resp.data})]),
                    catchError((errorResp) => of(UserActions.resetUserPasswordError({error: errorResp.error.data.message})))
                )
            )
        );
    });

    resendEmailVerification$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(UserActions.resendVerificationEmail),
            concatMap((action) =>
                this.userService.resendVerificationEmail(action.user).pipe(
                    mergeMap((resp: ApiResponse<boolean>) => [UserActions.resendVerificationEmailSuccess()]),
                    catchError((errorResp) => of(UserActions.resendVerificationEmailError({error: errorResp.error.data.message})))
                )
            )
        );
    });

    verifyAccount$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(UserActions.verifyAccount),
            tap((res) => this.store.dispatch(GlobalActions.startLoading())),
            withLatestFrom(this.store.select(getRouter), (action, route) => {
                return {userId: route.state.queryParams.userId, token: route.state.queryParams.token};
            }),
            concatMap((params: { userId: string, token; string }) =>
                this.userService.verifyAccount(params.userId, params.token).pipe(
                    mergeMap((resp: ApiResponse<boolean>) => [
                        UserActions.verifyAccountSuccess(),
                        GlobalActions.loadingSuccess(),
                    ]),
                    catchError((error) => {
                        return of(UserActions.verifyAccountError({message: error.error.data.code}));
                    }),
                ),
            ),
        );
    });

    verifyResetLink$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(UserActions.verifyResetLink),
            tap((res) => this.store.dispatch(GlobalActions.startLoading())),
            withLatestFrom(this.store.select(getRouter), (action, route) => {
                return {userId: route.state.queryParams.userId, token: route.state.queryParams.token};
            }),
            concatMap((params: { userId: string, token; string }) =>
                this.userService.verifyResetLink(params.userId, params.token).pipe(
                    mergeMap((resp: ApiResponse<boolean>) => [
                        UserActions.verifyResetLinkSuccess(),
                        GlobalActions.loadingSuccess(),
                    ]),
                    catchError((error) => {
                        return of(UserActions.verifyResetLinkError({message: error.error.data.code}));
                    }),
                ),
            ),
        );
    });

    logout$ = createEffect(
        () => {
            return this.actions$.pipe(
                ofType(UserActions.logoutUser),
                tap(() => {
                    this.storageService.clear();
                    this.router.navigate(['/']);
                }),
            );
        },
        {dispatch: false},
    );

    loadNotifications$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(UserActions.loadNotifications),
            concatMap((action) =>
                this.userService.loadNotifications().pipe(
                    mergeMap((resp: ApiResponse<UserNotification[]>) => [UserActions.loadNotificationsSuccess({notifications: resp.data})]),
                    catchError((error) => of(GlobalActions.addError({titre: 'Notifications', error})))
                )
            )
        );
    });

    deleteAllNotifications$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(UserActions.deleteAllNotifications),
            concatMap((action) =>
                this.userService.deleteAllNotifications().pipe(
                    mergeMap((resp: ApiResponse<boolean>) => [UserActions.deleteAllNotificationsSuccess()]),
                    catchError((error) => of(GlobalActions.addError({titre: 'Notifications', error})))
                )
            )
        );
    });

    deleteNotification$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(UserActions.deleteNotification),
            concatMap((action) =>
                this.userService.deleteNotification(action.id).pipe(
                    mergeMap((resp: ApiResponse<UserNotification>) => [UserActions.deleteNotificationSuccess({id: action.id})]),
                    catchError((error) => of(GlobalActions.addError({titre: 'Notifications', error})))
                )
            )
        );
    });

    loadMyProfile$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(UserActions.loadMyProfile),
            tap((res) => this.store.dispatch(GlobalActions.startLoading())),
            concatMap(() =>
                this.userService.getMe().pipe(
                    mergeMap((resp: ApiResponse<User>) => [
                        UserActions.loadMyProfileSuccess({data: resp.data}),
                        GlobalActions.loadingSuccess(),
                    ]),
                    catchError((error) => of(GlobalActions.addError({titre: 'Loading User', error}))),
                ),
            ),
        );
    });

    updateMyProfile$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(UserActions.updateMyProfile),
            tap((res) => this.store.dispatch(GlobalActions.startLoading())),
            concatMap((action) =>
                this.userService.updateMe(action.data).pipe(
                    mergeMap((resp: ApiResponse<User>) => [
                        UserActions.loadMyProfileSuccess({data: resp.data}),
                        GlobalActions.loadingSuccess(),
                    ]),
                    catchError((error) => of(GlobalActions.addError({titre: 'Loading User', error}))),
                ),
            ),
        );
    });

    changeEntity$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(UserActions.changeEntity),
            concatMap((action) =>
                this.userService.changeEntity(action.entity).pipe(
                    tap((resp: ApiResponse<UserInfos>) => {
                        this.storageService.set('token', resp.data.accessToken);
                        this.storageService.set('user', JSON.stringify(resp.data.user));
                    }),
                    mergeMap((resp: ApiResponse<UserInfos>) => [UserActions.loginUserSuccess({userInfos: resp.data})]),
                    catchError((error) => of(GlobalActions.addError({titre: 'Loading User', error})))
                )
            )
        );
    });

    deleteAcount$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(UserActions.deleteAccount),
            concatMap((action) =>
                this.userService.deleteAccount().pipe(
                    mergeMap((resp: ApiResponse<boolean>) => [UserActions.deleteAccountSuccess(), UserActions.logoutUser()]),
                    catchError((error) => of(GlobalActions.addError({titre: 'Suppression du compte', error})))
                )
            )
        );
    });

    countModerationAction$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(UserActions.countModerationAction),
            concatMap((action) =>
                this.userService.countModerationAction().pipe(
                    mergeMap((resp: ApiResponse<{
                        noValidQuestions: number;
                        noValidAnswers: number;
                        alertAnswers: number
                    }>) => [UserActions.countModerationActionSuccess({moderationCount: resp.data})]),
                    catchError((error) => of(GlobalActions.addError({titre: 'Modération', error})))
                )
            )
        );
    });

    constructor(
        private actions$: Actions,
        private userService: UserService,
        private storageService: StorageService,
        private store: Store,
        private router: Router
    ) {
    }
}
