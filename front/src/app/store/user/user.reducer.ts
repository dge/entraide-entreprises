import {createReducer, on} from '@ngrx/store';
import * as UserActions from './user.actions';
import {User} from '../models/user';
import {UserNotification} from '../models/user-notification';
import cloneDeep from 'lodash/cloneDeep';
import {CurrentUser} from '../models/current-user';

export const userFeatureKey = 'user';

export interface State {
    user: CurrentUser;
    token: string;
    error: string;
    notifications: UserNotification[];
    me: User;
    moderationCount: {
        noValidQuestions: number;
        noValidAnswers: number;
        alertAnswers: number
    };
}

export const initialState: State = {
    user: null,
    token: null,
    error: null,
    notifications: null,
    me: null,
    moderationCount: null
};

export const reducer = createReducer(
        initialState,

        on(UserActions.loginUserSuccess, (state, action) => ({
            ...state,
            user: action.userInfos.user,
            token: action.userInfos.accessToken,
        })),
        on(UserActions.storageLoginUserSuccess, (state, action) => ({
            ...state,
            user: action.userInfos.user,
            token: action.userInfos.accessToken,
        })),
        on(UserActions.loginUserError, (state, action) => ({...state, error: action.error})),
        on(UserActions.logoutUser, (state, action) => ({...state, user: null, token: null})),
        on(UserActions.loadNotificationsSuccess, (state, action) => ({...state, notifications: action.notifications})),
        on(UserActions.deleteAllNotificationsSuccess, (state, action) => ({...state, notifications: []})),
        on(UserActions.deleteNotificationSuccess, (state, action) => {
            const notifList = cloneDeep(state.notifications);
            const notif = notifList.filter(x => x.id === action.id)[0];
            if (notif) {
                notifList.splice(notifList.indexOf(notif), 1);
            }
            return {
                ...state,
                notifications: notifList
            };
        }),
        on(UserActions.loadMyProfileSuccess, (state, action) => ({...state, me: action.data})),
        on(UserActions.updateProfilePicture, (state, action) => ({
            ...state,
            user: {...state.user, profilePicture: action.profilePicture}
        })),

        on(UserActions.changeMode, (state, action) => ({...state, user: {...state.user, anonyme: action.anonyme}})),
        on(UserActions.changeEntitySuccess, (state, action) => {
            const entities = cloneDeep(state.user.entities);
            for (const e of entities) {
                e.actif = e.id === action.entity.id;
            }
            return {...state, user: {...state.user, entities}};
        }),
        on(UserActions.countModerationAction, (state, action) => ({...state, moderationCount: null})),
        on(UserActions.countModerationActionSuccess, (state, action) => ({
            ...state,
            moderationCount: action.moderationCount
        })),
    )
;
