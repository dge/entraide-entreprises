import {createAction, props} from '@ngrx/store';
import {UserInfos} from '../models/user-infos';
import {UserSignup} from '../models/user-signup';
import {UserNotification} from '../models/user-notification';
import {User} from '../models/user';
import {Entity} from '../models/entity';

export const loginUser = createAction('[User] login User', props<{ username: string, password: string }>());
export const loginUserSuccess = createAction('[User] login User Success', props<{ userInfos: UserInfos }>());
export const storageLoginUserSuccess = createAction('[User] storage login User Success', props<{ userInfos: UserInfos }>());
export const loginUserError = createAction('[User] login User Error', props<{ error: string }>());
export const updateProfilePicture = createAction('[User] updateProfilePicture', props<{ profilePicture: string }>());

export const logoutUser = createAction('[User] logout User');

export const loadMyProfile = createAction('[Profil] Load My Profile');
export const updateMyProfile = createAction('[Profil] Update My Profile', props<{ data: User }>());
export const loadMyProfileSuccess = createAction('[Profil] Load My Profile Success', props<{ data: User }>());

export const signUpUser = createAction('[User] sign up User', props<{ user: UserSignup }>());
export const signUpUserSuccess = createAction('[User] sign up User Success', props<{ userInfos: UserInfos }>());
export const signUpUserError = createAction('[User] sign up User Error', props<{ error: string }>());

export const askResetUserPasswordUser = createAction('[User] ask reset User password', props<{ email: string }>());
export const askResetUserPasswordSuccess = createAction('[User] ask reset User password Success', props<{ success: boolean }>());
export const askResetUserPasswordError = createAction('[User] ask reset User password Error', props<{ error: string }>());

export const resetUserPasswordUser = createAction('[User] reset User password', props<{ password: string }>());
export const resetUserPasswordSuccess = createAction('[User] reset User password Success', props<{ success: boolean }>());
export const resetUserPasswordError = createAction('[User] reset User password Error', props<{ error: string }>());

export const resendVerificationEmail = createAction('[User] resend email for account verification', props<{ user: any }>());
export const resendVerificationEmailSuccess = createAction('[User] resend email for account verification success');
export const resendVerificationEmailError = createAction('[User] resend email for account verification error', props<{ error: string }>());

export const verifyAccount = createAction('[Profil] Verify Account');
export const verifyAccountSuccess = createAction('[Profil] Verify Account Success');
export const verifyAccountError = createAction('[Profil] Verify Account Error', props<{ message: string }>());

export const verifyResetLink = createAction('[Profil] Verify Reset Link');
export const verifyResetLinkSuccess = createAction('[Profil] Verify Reset Link Success');
export const verifyResetLinkError = createAction('[Profil] Verify Reset Link Error', props<{ message: string }>());

export const loadNotifications = createAction('[Profil] Load Notification');
export const loadNotificationsSuccess = createAction('[User] Load Notification Success', props<{ notifications: UserNotification[] }>());

export const deleteAllNotifications = createAction('[Profil] Delete All Notifications');
export const deleteAllNotificationsSuccess = createAction('[User] Delete All Notifications Success');

export const deleteNotification = createAction('[Profil] Delete Notification', props<{ id: string }>());
export const deleteNotificationSuccess = createAction('[User] Delete Notification Success', props<{ id: string }>());


export const changeMode = createAction('[User] Change Mode', props<{ anonyme: boolean }>());

export const changeEntity = createAction('[Profil] Change Entity', props<{ entity: Entity }>());
export const changeEntitySuccess = createAction('[Profil] Change Entity Success', props<{ entity: Entity }>());
export const changeEntityError = createAction('[Profil] Change Entity Error', props<{ message: string }>());
export const deleteAccount = createAction('[User] Delete Account');
export const deleteAccountSuccess = createAction('[User] Delete Account Success');

export const countModerationAction = createAction('[Profil] countModerationAction');
export const countModerationActionSuccess = createAction('[Profil] countModerationAction Success', props<{
    moderationCount: {
        noValidQuestions: number;
        noValidAnswers: number;
        alertAnswers: number
    }
}>());
