import * as fromGlobal from './global.actions';

describe('loadGlobals', () => {
  it('should return an action', () => {
    expect(fromGlobal.startLoading().type).toBe('[Global] Start Loading');
  });
});
