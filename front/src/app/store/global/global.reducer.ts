import {createReducer, on} from '@ngrx/store';
import * as GlobalActions from './global.actions';

export const globalFeatureKey = 'global';

export interface State {
  error: string;
  loading: boolean;
}

export const initialState: State = {
  error: null,
  loading: false,
};

export const reducer = createReducer(
  initialState,

  on(GlobalActions.startLoading, (state) => ({...state, loading: true})),
  on(GlobalActions.loadingSuccess, (state) => ({...state, loading: false})),
  on(GlobalActions.addError, (state) => ({...state, loading: false}))
);
