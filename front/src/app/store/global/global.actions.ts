import {createAction, props} from '@ngrx/store';
import {HttpErrorResponse} from '@angular/common/http';

export const startLoading = createAction('[Global] Start Loading');
export const loadingSuccess = createAction('[Global] loading Success');
export const addError = createAction('[Global] Add Error', props<{ titre: string; error: HttpErrorResponse }>());
export const redirectLoginPage = createAction('[Global] Redirect Login Page');
export const redirectHomePage = createAction('[Global] Redirect Home Page');

