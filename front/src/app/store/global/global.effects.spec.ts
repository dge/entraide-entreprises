import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { GlobalEffects } from './global.effects';
import { SharedModule } from '../../shared/shared.module';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('GlobalEffects', () => {
  const actions$: Observable<any> = null;
  let effects: GlobalEffects;
  let store: MockStore;
  const initialState = { loading: false };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, RouterTestingModule, HttpClientTestingModule],
      providers: [GlobalEffects, provideMockActions(() => actions$), provideMockStore({ initialState })],
    });

    store = TestBed.inject(MockStore);
    effects = TestBed.inject(GlobalEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
