import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {tap} from 'rxjs/operators';

import * as GlobalActions from './global.actions';
import {HttpErrorResponse} from '@angular/common/http';
import {Router} from '@angular/router';
import {LoadingController} from '@ionic/angular';
import set = Reflect.set;

@Injectable()
export class GlobalEffects {
  /*loadGlobals$ = createEffect(() => {
    return this.actions$.pipe(

      ofType(GlobalActions.loadGlobals),
      concatMap(() =>
        /!** An EMPTY observable only emits completion. Replace with your own observable API request *!/
        EMPTY.pipe(
          map(data => GlobalActions.loadGlobalsSuccess({ data })),
          catchError(error => of(GlobalActions.loadGlobalsFailure({ error }))))
      )
    );
  });*/

  addError$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(GlobalActions.addError),
        tap((action: { titre: string; error: HttpErrorResponse }) => {
          this.loadingController.dismiss();
          /*if (action.error.status === 400) {
            this.snackBarService.openSnackBar(LoadingState.ERROR, {
              titre: action.titre,
              fieldErrors: action.error.error.fieldErrors,
            });
          } else {
            this.snackBarService.openSnackBar(LoadingState.ERROR, {
              titre: 'Erreur',
              message: 'Une erreur interne est survenue.',
            });
          }*/
        }),
      );
    },
    {dispatch: false},
  );

  redirectHomePage$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(GlobalActions.redirectHomePage),
        tap(() => {
          this.router.navigate(['/questions']);
        }),
      );
    },
    {dispatch: false},
  );

  startLoading$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(GlobalActions.startLoading),
        tap(() => {
          setTimeout(() => {
            this.presentLoading();
          }, 0);
        }),
      );
    },
    {dispatch: false},
  );

  loadingSuccess$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(GlobalActions.loadingSuccess),
        tap(() => {
          setTimeout(() => {
            this.stopLoading();
          }, 100);
        }),
      );
    },
    {dispatch: false},
  );

  async presentLoading() {
    const overlay = await this.loadingController.getTop();
    if (!overlay) {
      const loading = await this.loadingController.create({
        cssClass: 'loading-class',
        message: 'Veuillez patienter...',
      });
      await loading.present();
    }
  }

  async stopLoading() {
    const overlay = await this.loadingController.getTop();
    if (overlay) {
      this.loadingController.dismiss();
      setTimeout(() => {
        this.stopLoading();
      }, 100);
    }
  }

  constructor(
    private actions$: Actions,
    private router: Router,
    public loadingController: LoadingController
    /*private snackBarService: SnackBarService,
    private infosService: InfosService,
    private store: Store,*/
  ) {
  }
}
