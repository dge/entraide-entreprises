import * as fromGlobal from './global.reducer';
import { selectGlobalState } from './global.selectors';

describe('Global Selectors', () => {
  it('should select the feature state', () => {
    const result = selectGlobalState({
      [fromGlobal.globalFeatureKey]: {
        error: null,
        loading: false,
        infos: null,
        user: null,
        token: null,
      },
    });

    expect(result).toEqual({
      error: null,
      loading: false,
      infos: null,
      user: null,
      token: null,
    });
  });
});
