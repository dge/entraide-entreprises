import { routerReducer, RouterReducerState } from '@ngrx/router-store';
import { Data, Params } from '@angular/router';
import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';

export const routerFeatureKey = 'router';

// router state URL
export interface MyRouterStateSnapshot {
  oldUrl: string;
  url: string;
  params: Params;
  queryParams: Params;
  data: Data;
}

// State
export interface RouterState {
  routerReducerState: RouterReducerState<MyRouterStateSnapshot>;
}

// reducers = lié app module
export const routerReducers: ActionReducerMap<RouterState> = {
  routerReducerState: routerReducer,
};

// GetRouterState
export const getRouterState = createFeatureSelector<RouterReducerState<MyRouterStateSnapshot>>('router');
export const getRouter = createSelector(getRouterState, (routerState: any) => routerState.routerReducerState);
