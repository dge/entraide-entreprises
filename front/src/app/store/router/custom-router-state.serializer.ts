import { RouterStateSerializer } from '@ngrx/router-store';
import { MyRouterStateSnapshot } from './router.state';
import { ActivatedRouteSnapshot, Data, Params, RouterStateSnapshot } from '@angular/router';

export class CustomRouterStateSerializer implements RouterStateSerializer<MyRouterStateSnapshot> {
  serialize(routerState: RouterStateSnapshot): MyRouterStateSnapshot {
    const routerStateSnapshot: MyRouterStateSnapshot = {
      oldUrl: 'test',
      url: routerState.url,
      params: mergeRouteParams(routerState.root, (r) => r.params),
      queryParams: mergeRouteParams(routerState.root, (r) => r.queryParams),
      data: mergeRouteData(routerState.root),
    };
    return routerStateSnapshot;
  }
}

function mergeRouteParams(route: ActivatedRouteSnapshot, getter: (r: ActivatedRouteSnapshot) => Params): Params {
  if (!route) {
    return {};
  }
  const currentParams = getter(route);
  const primaryChild = route.children.find((c) => c.outlet === 'primary') || route.firstChild;
  return { ...currentParams, ...mergeRouteParams(primaryChild, getter) };
}

function mergeRouteData(route: ActivatedRouteSnapshot): Data {
  if (!route) {
    return {};
  }

  const currentData = route.data;
  const primaryChild = route.children.find((c) => c.outlet === 'primary') || route.firstChild;
  return { ...currentData, ...mergeRouteData(primaryChild) };
}
