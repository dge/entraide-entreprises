import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType, ROOT_EFFECTS_INIT} from '@ngrx/effects';
import {concatMap, delay, mergeMap} from 'rxjs/operators';
import {StorageService} from './services/storage.service';
import * as UserActions from './user/user.actions';
import {fromPromise} from 'rxjs/internal-compatibility';
import {UserService} from './services/user.service';

@Injectable()
export class RootEffects {
  init$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ROOT_EFFECTS_INIT),
      delay(1000),
      concatMap(() => {
          return fromPromise(Promise.all(
            [
              this.storageService.get('token'),
              this.storageService.get('user')
            ]
          )).pipe(
            mergeMap(values => {
              let userInfos = null;
              if (!this.userService.isTokenExpired(values[0])) {
                userInfos = {
                  user: values[1] ? JSON.parse(values[1]) : null,
                  accessToken: values[0]
                };
              } else {
                userInfos = {
                  user: null,
                  accessToken: null
                };
                this.storageService.clear();
              }
              return [
                UserActions.storageLoginUserSuccess({userInfos})
              ];
            })
          );
        }
      ),
    );
  });

  constructor(private actions$: Actions,
              private userService: UserService,
              private storageService: StorageService) {
  }
}
