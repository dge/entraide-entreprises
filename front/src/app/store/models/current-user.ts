import {Entity} from './entity';

export interface CurrentUser {
  id: string;
  roles?: string[];
  firstName: string;
  lastName: string;
  username: string;
  email: string;
  profilePicture: string;
  anonyme: boolean;
  entities: Entity[];
}
