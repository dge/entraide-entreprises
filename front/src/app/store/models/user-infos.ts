import {CurrentUser} from './current-user';

export interface UserInfos {
  user: CurrentUser;
  accessToken: string;
}
