export interface UserSignup {
  firstName: string;
  lastName: string;
  username: string;
  email: string;
  password: string;
  dateOfBirth: Date;
  confirmPassword?: string;
}
