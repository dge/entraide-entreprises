export interface UserNotification {
  id: string;
  user: string;
  type: string;
  message: string;
  question: string;
  createdAt: Date;
}
