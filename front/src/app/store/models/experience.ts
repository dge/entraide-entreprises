export interface Experience {
  name: string;
  startDate: string;
  endDate: string;
  url: string;
  description: string;
}
