import {Experience} from './experience';
import {Entity} from './entity';

export interface User {
  id: string;
  roles?: string[];
  firstName: string;
  lastName: string;
  username: string;
  email: string;
  password: string;
  dateOfBirth: Date;
  profilePicture: string;
  entities?: Entity[];
  job: string;
  responsability: any;
  followers: any;
  anonyme: boolean;
  questionCount?: number;
  answerCount?: number;
  profile?: string;
  experiences?: Experience[];
  training?: Experience[];
  networks?: {
    facebook?: string;
    twitter?: string;
    instagram?: string;
    linkedin?: string;
  };
  allowContact: boolean;

  notification?: {
    myquestion_answer_appli: boolean;
    myquestion_answer_email: boolean;
    myanswer_comment_appli: boolean;
    myanswer_comment_email: boolean;
    thematic_createquestion_appli: boolean;
    thematic_answerquestion_appli: boolean;
    thematic_commentanswer_appli: boolean;
    thematic_createquestion_email: boolean;
    thematic_answerquestion_email: boolean;
    thematic_commentanswer_email: boolean;
    user_createquestion_appli: boolean;
    user_answerquestion_appli: boolean;
    user_commentanswer_appli: boolean;
    user_createquestion_email: boolean;
    user_answerquestion_email: boolean;
    user_commentanswer_email: boolean;
  };
}
