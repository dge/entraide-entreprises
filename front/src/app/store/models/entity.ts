export interface Entity {
  id: string;
  name: string;
  siret: string;
  address: string;
  addressBis: string;
  codePostal: string;
  city: string;
  actif: boolean;
}
