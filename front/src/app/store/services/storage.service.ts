import {Injectable} from '@angular/core';
import {Storage} from '@ionic/storage-angular';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  private appStorage: Storage | null = null;

  constructor(private storage: Storage) {
    this.init();
  }

  async init() {
    this.appStorage = await this.storage.create();
  }

  set(key: string, value: any) {
    this.appStorage?.set(key, value);
  }

  async get(key: string) {
    return this.appStorage?.get(key);
  }

  clear() {
    this.appStorage?.clear();
  }
}
