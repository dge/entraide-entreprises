import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment as env} from '@environment/environment';
import {Observable} from 'rxjs';
import {HtmlPage} from '../../administration/shared/models/html-page';
import {map} from 'rxjs/operators';
import {ApiResponse} from '../../core/models/api-response';

@Injectable({
    providedIn: 'root'
})
export class AdministrationService {

    constructor(private http: HttpClient) {
    }

    getAllHtmlPages(): Observable<HtmlPage[]> {
        return this.http.get<ApiResponse<HtmlPage[]>>(`${env.apiUrl}/html-pages`).pipe(map(x => x.data));
    }

    getHtmlPage(code: string): Observable<HtmlPage> {
        return this.http.get<ApiResponse<HtmlPage>>(`${env.apiUrl}/html-pages/findbycode/${code}`).pipe(map(x => x.data));
    }

    updateHtmlPage(id: string, form: any) {
        return this.http.put(`${env.apiUrl}/html-pages/item/${id}`, form);
    }
}
