import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {UserInfos} from '../models/user-infos';
import {HttpClient} from '@angular/common/http';
import {ApiResponse} from '../../core/models/api-response';
import {environment, environment as env} from '@environment/environment';
import {JwtHelperService} from '@auth0/angular-jwt';
import {UserSignup} from '../models/user-signup';
import {UserNotification} from '../models/user-notification';
import {User} from '../models/user';
import {Entity} from '../models/entity';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private jwtHelper: JwtHelperService = new JwtHelperService();

  constructor(private httpClient: HttpClient) {
  }

  login(username: string, password: string): Observable<ApiResponse<UserInfos>> {
    return this.httpClient.post<ApiResponse<UserInfos>>(`${env.apiUrl}/user/signin`, {username, password});
  }

  signUp(user: UserSignup): Observable<ApiResponse<UserInfos>> {
    return this.httpClient.post<ApiResponse<UserInfos>>(`${env.apiUrl}/user/signup`, user);
  }

  isTokenExpired(token: string): boolean {
    return !token || this.jwtHelper.isTokenExpired(token);
  }

  verifyAccount(userId: string, token: string): Observable<ApiResponse<boolean>> {
    return this.httpClient.post<ApiResponse<boolean>>(`${env.apiUrl}/user/confirmation`, {userId, token});
  }

  verifyResetLink(userId: string, token: string): Observable<ApiResponse<boolean>> {
    return this.httpClient.post<ApiResponse<boolean>>(`${env.apiUrl}/user/verify-reset-link`, {userId, token});
  }

  askResetPassword(email: string): Observable<ApiResponse<boolean>> {
    return this.httpClient.post<ApiResponse<boolean>>(`${env.apiUrl}/user/askResetPassword`, {email});
  }

  resetPassword(userId: string, password: string, token: string): Observable<ApiResponse<boolean>> {
    return this.httpClient.post<ApiResponse<boolean>>(`${env.apiUrl}/user/resetPassword`, {userId, password, token});
  }

  resendVerificationEmail(user: UserSignup): Observable<ApiResponse<boolean>> {
    return this.httpClient.post<ApiResponse<boolean>>(`${env.apiUrl}/user/resendVerificationEmail`, user);
  }

  checkDuplicateUser(user: UserSignup): Observable<ApiResponse<boolean>> {
    return this.httpClient.post<ApiResponse<boolean>>(`${env.apiUrl}/user/checkDuplicateUser`, {usernameOrEmail: user});
  }

  loadNotifications(): Observable<ApiResponse<UserNotification[]>> {
    return this.httpClient.get<ApiResponse<UserNotification[]>>(`${env.apiUrl}/notifications`);
  }

  deleteAllNotifications(): Observable<ApiResponse<boolean>> {
    return this.httpClient.delete<ApiResponse<boolean>>(`${env.apiUrl}/notifications`);
  }

  deleteNotification(notifId: string): Observable<ApiResponse<UserNotification>> {
    return this.httpClient.delete<ApiResponse<UserNotification>>(`${env.apiUrl}/notifications/${notifId}`);
  }

  getMe(): Observable<ApiResponse<User>> {
    return this.httpClient.get<ApiResponse<User>>(`${environment.apiUrl}/user/me`);
  }

  updateMe(profil: User): Observable<ApiResponse<User>> {
    return this.httpClient.put<ApiResponse<User>>(`${env.apiUrl}/user/me`, profil);
  }

  completeInscription(id: string, form: any): Observable<ApiResponse<User>> {
    return this.httpClient.put<ApiResponse<User>>(`${env.apiUrl}/user/completeInscription/${id}`, form);
  }

  changeEntity(entity: Entity): Observable<ApiResponse<UserInfos>> {
    return this.httpClient.get<ApiResponse<UserInfos>>(`${env.apiUrl}/user/changeEntity/${entity.id}`);
  }

  deleteAccount(): Observable<ApiResponse<any>> {
    return this.httpClient.get<ApiResponse<any>>(`${env.apiUrl}/user/deleteAccount`);
  }

  countModerationAction(): Observable<ApiResponse<{
    noValidQuestions: number;
    noValidAnswers: number;
    alertAnswers: number
  }>> {
    return this.httpClient.get<ApiResponse<{
      noValidQuestions: number;
      noValidAnswers: number;
      alertAnswers: number
    }>>(`${env.apiUrl}/question/count-moderation-action`);
  }
}
