import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {StoreModule} from '@ngrx/store';
import {metaReducers, reducers} from './';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {environment} from '../../environments/environment';
import {StoreRouterConnectingModule} from '@ngrx/router-store';
import {CustomRouterStateSerializer} from './router/custom-router-state.serializer';
import {routerFeatureKey, routerReducers} from './router/router.state';
import {EffectsModule} from '@ngrx/effects';
import * as fromGlobal from './global/global.reducer';
import * as fromUser from './user/user.reducer';
import {GlobalEffects} from './global/global.effects';
import {RootEffects} from './root.effects';
import {UserEffects} from './user/user.effects';
import {IonicStorageModule} from '@ionic/storage-angular';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forRoot(reducers, { metaReducers }),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    EffectsModule.forRoot([RootEffects]),
    StoreRouterConnectingModule.forRoot({
      serializer: CustomRouterStateSerializer,
    }),
    StoreModule.forFeature(routerFeatureKey, routerReducers),
    StoreModule.forFeature(fromGlobal.globalFeatureKey, fromGlobal.reducer),
    StoreModule.forFeature(fromUser.userFeatureKey, fromUser.reducer),
    EffectsModule.forFeature([GlobalEffects, UserEffects]),
    IonicStorageModule.forRoot()
  ],
})
export class AppStoreModule {}
