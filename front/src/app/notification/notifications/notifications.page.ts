import {Component, OnDestroy, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {Observable, Subject} from 'rxjs';
import {UserNotification} from '../../store/models/user-notification';
import {getNbNotifications, getNotifications} from '../../store/user/user.selectors';
import {takeUntil} from 'rxjs/operators';
import {deleteAllNotifications, deleteNotification} from '../../store/user/user.actions';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.page.html',
  styleUrls: ['./notifications.page.scss'],
})
export class NotificationsPage implements OnInit, OnDestroy {

  notifications: UserNotification[];
  unsusbcribe$: Subject<void> = new Subject<void>();

  constructor(private store: Store) {
  }

  ngOnInit() {
    this.store.pipe(select(getNotifications), takeUntil(this.unsusbcribe$)).subscribe(notifications => {
      this.notifications = notifications || [];
    });
  }

  ngOnDestroy() {
    this.unsusbcribe$.next();
    this.unsusbcribe$.complete();
  }

  deleteAll() {
    this.store.dispatch(deleteAllNotifications());
  }

  delete(id: string) {
    this.store.dispatch(deleteNotification({id}));
  }
}
