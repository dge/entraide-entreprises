import {Component, OnInit} from '@angular/core';
import {MenuController} from '@ionic/angular';
import {select, Store} from '@ngrx/store';
import {isAuthenticated} from '../../store/user/user.selectors';
import {Observable} from 'rxjs';
import {logoutUser} from '../../store/user/user.actions';
import {LoginService} from '../../core/login/login.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {
  appPages = [
    {title: 'Accueil', url: '/questions'},
    {title: 'Rechercher une question', url: '/questions/rechercher'},
    {title: 'Parcourir les thématiques', url: '/questions/liste-des-thematiques'},
  ];
  isAuthenticated: Observable<boolean>;

  constructor(private menu: MenuController,
              private store: Store,
              private loginService: LoginService) {
  }

  ngOnInit() {
    this.isAuthenticated = this.store.pipe(select(isAuthenticated));
  }

  closeMenu(): void {
    this.menu.close('mainMenu');
  }

  async openLoginForm() {
    this.closeMenu();
    this.loginService.openLoginForm();
  }

  logout(): void {
    this.store.dispatch((logoutUser()));
    this.closeMenu();
  }
}
