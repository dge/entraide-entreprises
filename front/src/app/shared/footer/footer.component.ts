import { Component, OnInit } from '@angular/core';
import {HtmlModalComponent} from '../modal/html-modal/html-modal.component';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {

  constructor(private dialog: MatDialog) { }

  ngOnInit() {}

  seeContenu(code: string) {
    this.dialog.open(HtmlModalComponent, {
      width: '70%',
      data: code,
      panelClass: 'design-system-modal'
    });
  }

}
