import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {select, Store} from '@ngrx/store';
import {getModerationCount, isAdmin} from '../../store/user/user.selectors';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-footer-menu',
  templateUrl: './footer-menu.component.html',
  styleUrls: ['./footer-menu.component.scss'],
})
export class FooterMenuComponent implements OnInit, OnDestroy {

  isAdmin: boolean;
  nbModerations: number;
  unsusbcribe$: Subject<void> = new Subject<void>();
  constructor(private store: Store) { }

  ngOnInit() {
    this.store.pipe(select(isAdmin), takeUntil(this.unsusbcribe$)).subscribe(admin => {
      this.isAdmin = admin;
    });
    this.store.pipe(select(getModerationCount), takeUntil(this.unsusbcribe$)).subscribe(count => {
      this.nbModerations = count ? count.alertAnswers + count.noValidAnswers + count.noValidQuestions : 0;
    });
  }

  ngOnDestroy() {
    this.unsusbcribe$.next();
    this.unsusbcribe$.complete();
  }

}
