import {AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {FieldConfig} from '../form/models/field-config';
import {Validators} from '@angular/forms';
import {ProfilService} from '../../profil/shared/services/profil.service';
import {ValidateSiret} from '../../profil/shared/validators/siret.validator';
import {FormComponent} from '../form/form/form.component';
import {Subject} from 'rxjs';
import {take, takeUntil} from 'rxjs/operators';
import {SiretService} from '../../core/siret.service';
import {UserService} from '../../store/services/user.service';
import {Entity} from '../../store/models/entity';

@Component({
  selector: 'app-complete-inscription-form',
  templateUrl: './complete-inscription-form.component.html',
  styleUrls: ['./complete-inscription-form.component.scss'],
})
export class CompleteInscriptionFormComponent implements OnInit, AfterViewInit, OnDestroy {

  @Output() completeInscription: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('appForm') appForm: FormComponent;
  formConfig: FieldConfig[] = [
    {
      type: 'autocomplete',
      name: 'job',
      label: 'Votre poste',
      validations: [
        {
          name: 'required',
          validator: Validators.required,
          message: `Le poste est obligatoire`
        }
      ],
      options: this.profilService.getJobList()
    },
    {
      type: 'radio',
      name: 'businessCreation',
      label: 'Votre situation',
      validations: [
        {
          name: 'required',
          validator: Validators.required,
          message: `La situation est obligatoire`
        }
      ],
      options: [
        {label: 'Chef d\'entreprise ou salarié', value: false},
        {label: 'Création d\'entreprise', value: true}
      ]
    },
    {
      type: 'input',
      name: 'siret',
      label: 'SIRET',
      disabled: true,
      validations: [
        {
          name: 'required',
          validator: Validators.required,
          message: 'Le SIRET est obligatoire'
        },
        {
          name: 'siret',
          validator: ValidateSiret,
          message: `Le SIRET n'est pas valide`
        }
      ]
    }
  ];
  entity: Entity;
  unsubscribe$: Subject<boolean> = new Subject<boolean>();

  constructor(private profilService: ProfilService,
              private siretService: SiretService) {
  }

  ngOnInit() {
  }

  save(value: { [p: string]: any }) {
    const form = {
      businessCreation: value.businessCreation,
      job: value.job,
      entity: this.entity
    };
    this.completeInscription.emit(form);
  }

  ngAfterViewInit(): void {
    this.appForm.form.get('businessCreation').valueChanges.pipe(takeUntil(this.unsubscribe$)).subscribe((x) => {
      if (x) {
        this.appForm.form.get('siret').setValue(null, {emitEvent: false});
        this.appForm.form.get('siret').disable();
        this.appForm.form.get('siret').clearValidators();
      } else {
        this.appForm.form.get('siret').enable();
        this.appForm.form.get('siret').setValidators(Validators.required);
      }
    });
    this.appForm.form.get('siret').valueChanges.pipe(takeUntil(this.unsubscribe$)).subscribe((x) => {
      setTimeout(() => {
        if (this.appForm.form.get('siret').valid && x && x.length === 14) {
          this.checkSiret(x);
        }
      }, 0);
    });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next(true);
    this.unsubscribe$.complete();
  }

  checkSiret(value: string) {
    this.siretService.verifySiret(value).pipe(take(1)).subscribe(
      (siren) => {
        this.appForm.form.get('siret').setErrors(null);
        this.entity = this.siretService.getEntity(siren);
        this.entity.actif = true;
      }, (err) => {
        this.appForm.form.get('siret').setErrors({siret: true});
        console.log(err);
      }
    );
  }
}
