import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {Validators} from '@angular/forms';
import {Observable, Subject} from 'rxjs';
import {ModalController} from '@ionic/angular';
import {select, Store} from '@ngrx/store';
import {getloginError} from '../../store/user/user.selectors';
import {takeUntil} from 'rxjs/operators';
import {loginUser, loginUserSuccess, resendVerificationEmail} from '../../store/user/user.actions';
import {FieldConfig} from '../form/models/field-config';
import {Actions, ofType} from '@ngrx/effects';
import {getRouter} from '../../store/router/router.state';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
})
export class LoginFormComponent implements OnInit, OnDestroy {

  @Output() loginEvent: EventEmitter<void> = new EventEmitter<void>();
  @Output() closeEvent: EventEmitter<void> = new EventEmitter<void>();
  unsusbcribe$: Subject<void> = new Subject<void>();
  error$: Observable<string>;
  values: { username: string, password: string };

  formConfig: FieldConfig[] = [
    {
      type: 'input',
      name: 'username',
      label: 'Pseudo ou adresse mail',
      validations: [
        {
          name: 'required',
          validator: Validators.required,
          message: 'Le pseudo est obligatoire'
        }
      ]
    },
    {
      type: 'password',
      name: 'password',
      label: 'Mot de passe',
      validations: [
        {
          name: 'required',
          validator: Validators.required,
          message: 'Le mot de passe est obligatoire'
        },
        {
          name: 'pattern',
          validator: Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&\-])[0-9A-Za-z\d$@$!%*?&\-].{8,}'),
          message: `Le mot de passe n'est pas valide`
        }
      ]
    }
  ];

  constructor(private modalController: ModalController,
              private router: Router,
              private store: Store,
              private actions$: Actions) {
  }

  ngOnInit() {
    this.actions$.pipe(ofType(loginUserSuccess), takeUntil(this.unsusbcribe$)).subscribe((action) => {
      this.loginEvent.emit();
    });
    this.error$ = this.store.pipe(select(getloginError));
  }

  ngOnDestroy() {
    this.unsusbcribe$.next();
    this.unsusbcribe$.complete();
  }

  dismissModal(): void {
    this.closeEvent.emit();
  }

  login(values: any): void {
    this.store.dispatch(loginUser(values));
  }

  onChange(values: any) {
    this.values = values;
  }

  sendMail() {
    this.store.dispatch(resendVerificationEmail({user: {email: this.values.username}}));
  }

  cancel(event: void) {
    this.router.navigate(['/utilisateur/mot-de-passe-oublie']);
  }
}
