import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Field} from '../models/field';
import {FieldEditorConfig} from '../models/field-editor-config';
import * as Editor from '../field-editor/ckeditor.js';


@Component({
  selector: 'app-field-editor',
  templateUrl: './field-editor.component.html',
  styleUrls: ['./field-editor.component.scss'],
})
export class FieldEditorComponent implements Field, OnInit {

  @Input()
  field: FieldEditorConfig;
  @Input()
  group: FormGroup;
  public Editor = Editor;
  required: boolean;

  constructor() {
  }

  ngOnInit() {
    this.required = this.field.validations && this.field.validations.filter(x => x.name === 'required').length > 0;
  }

}
