import {Component, OnDestroy, OnInit} from '@angular/core';
import {Field} from '../models/field';
import {FormGroup} from '@angular/forms';
import {Observable, Subject} from 'rxjs';
import {map, startWith, switchMap} from 'rxjs/operators';
import {FieldAutocompleteConfig} from '../models/field-autocomplete-config';

@Component({
  selector: 'app-field-autocomplete',
  templateUrl: './field-autocomplete.component.html',
  styleUrls: ['./field-autocomplete.component.scss'],
})
export class FieldAutocompleteComponent implements Field, OnInit, OnDestroy {

  field: FieldAutocompleteConfig;
  group: FormGroup;
  required: boolean;
  options: string[] = ['One', 'Two', 'Three'];
  filteredOptions: Observable<string[]>;
  unsubscribe$: Subject<boolean> = new Subject<boolean>();

  constructor() {
  }

  ngOnInit() {
    this.required = this.field.validations && this.field.validations.filter(x => x.name === 'required').length > 0;
    this.filteredOptions = this.group.get(this.field.name).valueChanges.pipe(
      startWith(''),
      switchMap(value => this._filter(value))
    );
  }

  ngOnDestroy() {
    this.unsubscribe$.next(true);
    this.unsubscribe$.complete();
  }

  private _filter(value: string): Observable<string[]> {
    const filterValue = value ? value.toLowerCase() : '';
    return this.field.options.pipe(map(list => {
      return list.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
    }));
  }

}
