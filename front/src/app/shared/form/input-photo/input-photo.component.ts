import {Component, forwardRef, OnInit} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {PhotoService} from '../../../core/photo.service';

@Component({
  selector: 'app-input-photo',
  templateUrl: './input-photo.component.html',
  styleUrls: ['./input-photo.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputPhotoComponent),
      multi: true
    }
  ],
})
export class InputPhotoComponent implements OnInit, ControlValueAccessor {

  public disabled: boolean;
  public value: string;

  onChange = (value: string) => {
  }
  onTouched = () => {
  }

  constructor(private photoService: PhotoService) {
  }

  ngOnInit() {
  }

  writeValue(value: string): void {
    this.value = value;
    this.onChange(this.value);
  }

  registerOnChange(fn: (value: string) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  async takePhoto() {
    const photo = await this.photoService.takePhoto();
    this.writeValue(photo.base64);
  }

}
