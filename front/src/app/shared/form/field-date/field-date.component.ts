import {Component, Inject, OnInit} from '@angular/core';
import {Field} from '../models/field';
import {FormGroup} from '@angular/forms';
import {FieldDateConfig} from '../models/field-date-config';
import {MatDatepicker} from '@angular/material/datepicker';
import {Moment} from 'moment';
import {Platform} from '@ionic/angular';

@Component({
  selector: 'app-field-date',
  templateUrl: './field-date.component.html',
  styleUrls: ['./field-date.component.scss'],
})
export class FieldDateComponent implements Field, OnInit {

  field: FieldDateConfig;
  group: FormGroup;
  required: boolean;
  date: any;

  constructor(public platform: Platform, @Inject('moment') private moment) {
  }

  ngOnInit() {
    if (this.field && this.field.value) {
      this.group.get(this.field.name).setValue(this.moment(this.field.value).format('YYYY-MM-DD'));
    }
    this.required = this.field.validations && this.field.validations.filter(x => x.name === 'required').length > 0;
  }

  filterDate = (d: Moment | null): boolean => {
    return this.field.filter ? this.field.filter(d, this.field, this.group) : true;
  }

  onInputClick(picker: MatDatepicker<any>) {
    picker.open();
  }

  onChange() {
    this.group.get(this.field.name).setValue(this.moment(this.date).format('YYYY-MM-DD'));
  }

  onInput() {
    if (this.group.get(this.field.name).value) {
      const d = this.moment(this.group.get(this.field.name).value, 'YYYY-MM-DD');
      this.date = d.toDate();
    }
  }
}
