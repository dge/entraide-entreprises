import {Component, OnInit} from '@angular/core';
import {Field} from '../models/field';
import {FormGroup} from '@angular/forms';
import {FieldPhotoConfig} from '../models/field-photo-config';

@Component({
  selector: 'app-field-photo',
  templateUrl: './field-photo.component.html',
  styleUrls: ['./field-photo.component.scss'],
})
export class FieldPhotoComponent implements Field, OnInit {

  field: FieldPhotoConfig;
  group: FormGroup;

  constructor() {
  }

  ngOnInit() {
  }

}
