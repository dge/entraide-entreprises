import {Component, OnInit} from '@angular/core';
import {Field} from '../models/field';
import {FormGroup} from '@angular/forms';
import {FieldInputConfig} from '../models/field-input-config';

@Component({
  selector: 'app-field-input',
  templateUrl: './field-input.component.html',
  styleUrls: ['./field-input.component.scss'],
})
export class FieldInputComponent implements Field, OnInit {

  field: FieldInputConfig;
  group: FormGroup;
  required: boolean;

  constructor() {
  }

  ngOnInit() {
    this.required = this.field.validations && this.field.validations.filter(x => x.name === 'required').length > 0;
  }


}
