import {
  ComponentFactoryResolver,
  ComponentRef,
  Directive,
  Input,
  OnChanges,
  OnInit, Type,
  ViewContainerRef
} from '@angular/core';
import {FieldConfig} from './models/field-config';
import {Field} from './models/field';
import {FormGroup} from '@angular/forms';
import {FieldInputComponent} from './field-input/field-input.component';
import {FieldPasswordComponent} from './field-password/field-password.component';
import {FieldPhotoComponent} from './field-photo/field-photo.component';
import {FieldDateComponent} from './field-date/field-date.component';
import {FieldAutocompleteComponent} from './field-autocomplete/field-autocomplete.component';
import {FieldEditorComponent} from './field-editor/field-editor.component';
import {FieldRadioComponent} from './field-radio/field-radio.component';
import {FieldCheckboxComponent} from './field-checkbox/field-checkbox.component';
import {FieldTextareaComponent} from './field-textarea/field-textarea.component';

const components: { [type: string]: Type<Field> } = {
  input: FieldInputComponent,
  textarea: FieldTextareaComponent,
  password: FieldPasswordComponent,
  photo: FieldPhotoComponent,
  date: FieldDateComponent,
  autocomplete: FieldAutocompleteComponent,
  editor: FieldEditorComponent,
  radio: FieldRadioComponent,
  checkbox: FieldCheckboxComponent
};


@Directive({
  selector: '[appForm]'
})
export class FormDirective implements Field, OnChanges, OnInit {
  @Input()
  field: FieldConfig;

  @Input()
  group: FormGroup;

  component: ComponentRef<Field>;

  constructor(private resolver: ComponentFactoryResolver, private container: ViewContainerRef) {
  }

  ngOnChanges() {
    if (this.component) {
      this.component.instance.field = this.field;
      this.component.instance.group = this.group;
    }
  }

  ngOnInit() {
    if (!components[this.field.type]) {
      const supportedTypes = Object.keys(components).join(', ');
      throw new Error(
        `Trying to use an unsupported type (${this.field.type}).
        Supported types: ${supportedTypes}`
      );
    }
    const component = this.resolver.resolveComponentFactory<Field>(components[this.field.type]);
    this.component = this.container.createComponent(component);
    this.component.instance.field = this.field;
    this.component.instance.group = this.group;
  }
}
