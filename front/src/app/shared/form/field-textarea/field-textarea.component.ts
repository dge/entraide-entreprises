import { Component, OnInit } from '@angular/core';
import {Field} from '../models/field';
import {FormGroup} from '@angular/forms';
import {FieldTextareaConfig} from '../models/field-textarea-config';

@Component({
  selector: 'app-field-textarea',
  templateUrl: './field-textarea.component.html',
  styleUrls: ['./field-textarea.component.scss'],
})
export class FieldTextareaComponent implements Field, OnInit {

  field: FieldTextareaConfig;
  group: FormGroup;
  required: boolean;

  constructor() {
  }

  ngOnInit() {
    this.required = this.field.validations && this.field.validations.filter(x => x.name === 'required').length > 0;
  }

}
