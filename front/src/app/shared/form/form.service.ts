import { Injectable } from '@angular/core';
import {FieldConfig} from './models/field-config';
import {FormBuilder, FormGroup} from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class FormService {

  constructor(private fb: FormBuilder) { }

  createFormGroup(fields: FieldConfig[]): FormGroup {
    const formGroup = this.fb.group({});
    fields.forEach(control => {
      if (!control.name) {
        throw new Error(
          `this attribute "name" is required`
        );
      }
      formGroup.addControl(control.name, this.createControl(control));
    });
    return formGroup;
  }

  createControl(config: FieldConfig) {
    const validations = config.validations;
    return this.fb.control({disabled: config.disabled, value: config.value},
      validations ? validations.filter(x => x.validator).map(x => x.validator) : [],
      validations ? validations.filter(x => x.asyncValidator).map(x => x.asyncValidator) : []
    );
  }
}
