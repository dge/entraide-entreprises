import {Component, OnInit} from '@angular/core';
import {Field} from '../models/field';
import {FormGroup} from '@angular/forms';
import {FieldCheckboxConfig} from '../models/field-checkbox-config';
import {HtmlModalComponent} from '../../modal/html-modal/html-modal.component';
import {MatDialog} from '@angular/material/dialog';

@Component({
    selector: 'app-field-checkbox',
    templateUrl: './field-checkbox.component.html',
    styleUrls: ['./field-checkbox.component.scss'],
})
export class FieldCheckboxComponent implements Field, OnInit {

    field: FieldCheckboxConfig;
    group: FormGroup;
    required: boolean;

    constructor(private dialog: MatDialog) {
    }

    ngOnInit() {
        this.required = this.field.validations && this.field.validations.filter(x => x.name === 'required').length > 0;
    }

    see() {
        this.dialog.open(HtmlModalComponent, {
            width: '70%',
            data: this.field.contenuHtml,
            panelClass: 'design-system-modal'
        });
    }

}
