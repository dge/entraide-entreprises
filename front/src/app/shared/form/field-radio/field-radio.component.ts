import {Component, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {FieldRadioConfig} from '../models/field-radio-config';
import {Field} from '../models/field';

@Component({
  selector: 'app-field-radio',
  templateUrl: './field-radio.component.html',
  styleUrls: ['./field-radio.component.scss'],
})
export class FieldRadioComponent implements Field, OnInit {

  field: FieldRadioConfig;
  group: FormGroup;
  required: boolean;

  constructor() { }

  ngOnInit() {}

}
