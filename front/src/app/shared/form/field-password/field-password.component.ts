import {Component, OnInit} from '@angular/core';
import {Field} from '../models/field';
import {FormGroup} from '@angular/forms';
import {FieldPasswordConfig} from '../models/field-password-config';

@Component({
  selector: 'app-field-password',
  templateUrl: './field-password.component.html',
  styleUrls: ['./field-password.component.scss'],
})
export class FieldPasswordComponent implements Field, OnInit {

  field: FieldPasswordConfig;
  group: FormGroup;
  required: boolean;
  hide = true;
  hideMess = true;
  showOnFocus = true;
  validations = [
    {pattern: '(?=.*[0-9])', message: 'Le mot de passe doit contenir au moins un chiffre'},
    {pattern: '(?=.*[a-z])', message: 'Le mot de passe doit contenir au moins une lettre minuscule'},
    {pattern: '(?=.*[A-Z])', message: 'Le mot de passe doit contenir au moins une lettre majuscule'},
    {pattern: '(?=.*[$@$!%*?&\-])', message: 'Le mot de passe doit contenir au moins un caractère spécial (@!%*?&-)'},
    {pattern: '(?=.*[$@$!%*?&\-])[0-9A-Za-z\d$@$!%*?&\-].{8,}', message: 'Le mot de passe doit contenir au moins 8 caractères'},
  ];

  constructor() {
  }

  ngOnInit() {
    this.required = this.field.validations && this.field.validations.filter(x => x.name === 'required').length > 0;
    this.hideMess = !this.field.showMessage;
    this.showOnFocus = this.field.showOnFocus;
  }

  isValid(pattern: string): boolean {
    const value = this.group.get(this.field.name).value;
    return value && new RegExp(pattern).test(value);
  }

}
