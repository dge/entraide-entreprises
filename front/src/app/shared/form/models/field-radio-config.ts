import {Validator} from './validator';

export interface FieldRadioConfig {
  type: 'radio';
  name: string;
  label?: string;
  value?: any;
  disabled?: boolean;
  validations?: Validator[];
  className?: string | string[];
  description?: string;
  options: {label: string, value: any}[];
}
