import {FieldInputConfig} from './field-input-config';
import {FieldPasswordConfig} from './field-password-config';
import {FieldDateConfig} from './field-date-config';
import {FieldPhotoConfig} from './field-photo-config';
import {FieldAutocompleteConfig} from './field-autocomplete-config';
import {FieldEditorConfig} from './field-editor-config';
import {FieldRadioConfig} from './field-radio-config';
import {FieldCheckboxConfig} from './field-checkbox-config';
import {FieldTextareaConfig} from './field-textarea-config';

export type FieldConfig = FieldInputConfig
    | FieldPasswordConfig
    | FieldDateConfig
    | FieldPhotoConfig
    | FieldAutocompleteConfig
    | FieldEditorConfig
    | FieldCheckboxConfig
    | FieldRadioConfig
    | FieldTextareaConfig;
