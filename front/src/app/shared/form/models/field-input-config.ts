import {Validator} from './validator';


export interface FieldInputConfig {
  type: 'input';
  name: string;
  inputType?: 'email' | 'file' | 'number' | 'text' | 'url';
  label?: string;
  value?: any;
  disabled?: boolean;
  validations?: Validator[];
  className?: string | string[];
  placeholder?: string;
  description?: string;
  autocomplete?: string;
}
