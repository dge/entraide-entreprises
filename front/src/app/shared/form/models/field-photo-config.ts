import {Validator} from './validator';

export interface FieldPhotoConfig {
  type: 'photo';
  name: string;
  label?: string;
  disabled?: boolean;
  validations?: Validator[];
  value?: any;
}
