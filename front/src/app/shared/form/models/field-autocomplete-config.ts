import {Validator} from './validator';
import {Observable} from 'rxjs';

export interface FieldAutocompleteConfig {
  type: 'autocomplete';
  name: string;
  options: Observable<string[]>;
  label?: string;
  value?: any;
  disabled?: boolean;
  validations?: Validator[];
  className?: string | string[];
  placeholder?: string;
  description?: string;
}
