import {Validator} from './validator';
export interface FieldCheckboxConfig {
  type: 'checkbox';
  name: string;
  label?: string;
  value?: any;
  disabled?: boolean;
  validations?: Validator[];
  className?: string | string[];
  description?: string;
  contenuHtml?: string;
}
