import {AsyncValidatorFn, ValidatorFn} from '@angular/forms';

export interface Validator {
  name: string;
  validator?: ValidatorFn;
  asyncValidator?: AsyncValidatorFn;
  message: string;
}
