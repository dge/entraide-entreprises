import {Validator} from './validator';
import {FormGroup} from '@angular/forms';
import {Moment} from 'moment';

export interface FieldDateConfig {
  type: 'date';
  name?: string;
  label?: string;
  value?: any;
  disabled?: boolean;
  validations?: Validator[];
  className?: string | string[];
  placeholder?: string;
  filter?: (d: Moment | null, field?: FieldDateConfig, group?: FormGroup) => boolean;

  startView?: 'month' | 'year' | 'multi-year';
  startDate?: Date;
  description?: string;
}
