import {Validator} from './validator';

export interface FieldPasswordConfig {
  type: 'password';
  name?: string;
  label?: string;
  value?: any;
  disabled?: boolean;
  validations?: Validator[];
  className?: string | string[];
  placeholder?: string;
  autocomplete?: string;
  showMessage?: boolean;
  showOnFocus?: boolean;
}
