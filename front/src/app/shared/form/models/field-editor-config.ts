import {Validator} from './validator';
import {CKEditor5} from '@ckeditor/ckeditor5-angular';

export interface FieldEditorConfig {
  type: 'editor';
  name: string;
  label?: string;
  value?: any;
  disabled?: boolean;
  validations?: Validator[];
  className?: string | string[];
  placeholder?: string;
  description?: string;
  config?: CKEditor5.Config;
}
