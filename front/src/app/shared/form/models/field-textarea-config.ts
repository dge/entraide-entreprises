import {Validator} from './validator';


export interface FieldTextareaConfig {
  type: 'textarea';
  name: string;
  label?: string;
  value?: any;
  disabled?: boolean;
  validations?: Validator[];
  className?: string | string[];
  placeholder?: string;
  description?: string;
}
