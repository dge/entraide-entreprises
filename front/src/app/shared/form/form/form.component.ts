import {Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {FormService} from '../form.service';
import {FieldConfig} from '../models/field-config';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class FormComponent implements OnChanges, OnDestroy {

  @Input()
  fields: FieldConfig[];
  @Input()
  submitLabel = 'Enregistrer';
  @Input()
  cancelLabel: string;

  @Output() submitForm: EventEmitter<{ [key: string]: any }> = new EventEmitter<{ [key: string]: any }>();
  @Output() changeForm: EventEmitter<{ [key: string]: any }> = new EventEmitter<{ [key: string]: any }>();
  @Output() cancelForm: EventEmitter<void> = new EventEmitter<void>();

  form: FormGroup;
  unsuscribe$: Subject<boolean> = new Subject<boolean>();

  constructor(private formService: FormService) {
  }

  ngOnChanges() {
    this.form = this.formService.createFormGroup(this.fields);
    this.form.valueChanges.pipe(takeUntil(this.unsuscribe$)).subscribe(x => {
      this.changeForm.emit(this.form.value);
    });
  }

  ngOnDestroy() {
    this.unsuscribe$.next(true);
    this.unsuscribe$.complete();
  }

  handleSubmit() {
    this.submitForm.emit(this.form.value);
  }

  handleCancel() {
    this.cancelForm.emit();
  }
}
