import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-photo-profil',
  templateUrl: './photo-profil.component.html',
  styleUrls: ['./photo-profil.component.scss'],
})
export class PhotoProfilComponent implements OnInit {

  @Input() picture: string;
  @Input() size: '1x' | '2x' | '3x' = '2x';
  constructor() { }

  ngOnInit() {}

}
