import {FieldConfig} from '../../form/models/field-config';

export interface FormModalData {
    title: string;
    items: FieldConfig[];
    submitLabel?: string;
    cancelLabel?: string;
}
