import {Component, Inject, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormService} from '../../form/form.service';
import {FormModalData} from './form-modal-data';

@Component({
    selector: 'app-form-modal',
    templateUrl: './form-modal.component.html',
    styleUrls: ['./form-modal.component.scss'],
})
export class FormModalComponent implements OnInit {

    form: FormGroup;
    submitLabel: string;
    cancelLabel: string;

    constructor(public dialogRef: MatDialogRef<FormModalComponent>,
                @Inject(MAT_DIALOG_DATA) public data: FormModalData,
                private formService: FormService) {
    }

    ngOnInit() {
        this.form = this.formService.createFormGroup(this.data.items);
        this.submitLabel = this.data.submitLabel || 'Envoyer';
        this.cancelLabel = this.data.cancelLabel || 'Annuler';
    }

    handleSubmit() {
        this.dialogRef.close(this.form.value);
    }

    handleCancel() {
        this.dialogRef.close(null);
    }
}
