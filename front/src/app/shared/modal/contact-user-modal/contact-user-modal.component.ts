import {Component, Inject, OnInit} from '@angular/core';
import {FieldConfig} from '../../form/models/field-config';
import {FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormService} from '../../form/form.service';
import {select, Store} from '@ngrx/store';
import {getUser} from '../../../store/user/user.selectors';

@Component({
    selector: 'app-contact-user-modal',
    templateUrl: './contact-user-modal.component.html',
    styleUrls: ['./contact-user-modal.component.scss'],
})
export class ContactUserModalComponent implements OnInit {

    fields: FieldConfig[];
    form: FormGroup;
    submitLabel = 'Envoyer';
    cancelLabel = 'Annuler';

    constructor(public dialogRef: MatDialogRef<ContactUserModalComponent>,
                private store: Store,
                @Inject(MAT_DIALOG_DATA) private data: string,
                private formService: FormService) {
    }

    ngOnInit() {
        this.store.pipe(select(getUser)).subscribe(user => {
          this.fields = [
            {
              type: 'input',
              name: 'email',
              label: 'Mail',
              validations: [
                {
                  name: 'required',
                  validator: Validators.required,
                  message: 'L\'adresse mail est obligatoire'
                },
                {
                  name: 'required',
                  validator: Validators.email,
                  message: 'L\'adresse mail n\'est pas valide'
                }
              ],
              value: user.email
            },
            {
              type: 'input',
              name: 'name',
              label: 'Prénom Nom',
              validations: [
                {
                  name: 'required',
                  validator: Validators.required,
                  message: 'Le nom est obligatoire'
                }
              ],
              value: user.firstName + ' ' + user.lastName
            },
            {
              type: 'textarea',
              name: 'message',
              label: 'Précisez votre motivation',
              validations: [
                {
                  name: 'required',
                  validator: Validators.required,
                  message: 'Le message est obligatoire'
                }
              ]
            }
          ];
          this.form = this.formService.createFormGroup(this.fields);
        });
    }

    handleSubmit() {
        this.dialogRef.close(this.form.value);
    }

    handleCancel() {
        this.dialogRef.close(null);
    }

}
