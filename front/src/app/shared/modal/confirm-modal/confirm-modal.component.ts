import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {take} from 'rxjs/operators';
import {AdministrationService} from '../../../store/services/administration.service';
import {HtmlPage} from '../../../administration/shared/models/html-page';

@Component({
    selector: 'app-confirm-modal',
    templateUrl: './confirm-modal.component.html',
    styleUrls: ['./confirm-modal.component.css']
})
export class ConfirmModalComponent implements OnInit {
  public message: string;

  constructor(public dialogRef: MatDialogRef<ConfirmModalComponent>,
              @Inject(MAT_DIALOG_DATA) private data: string) {}

  ngOnInit() {
    this.message = this.data;
  }

  close(result: boolean) {
    this.dialogRef.close(result);
  }
}
