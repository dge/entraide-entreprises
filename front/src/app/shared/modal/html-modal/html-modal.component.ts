import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {HtmlPage} from '../../../administration/shared/models/html-page';
import {AdministrationService} from '../../../store/services/administration.service';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-html-modal',
  templateUrl: './html-modal.component.html',
  styleUrls: ['./html-modal.component.scss'],
})
export class HtmlModalComponent implements OnInit {
  public page: HtmlPage;

  constructor(public dialogRef: MatDialogRef<HtmlModalComponent>,
              @Inject(MAT_DIALOG_DATA) private code: string,
              private administrationService: AdministrationService) {
  }

  ngOnInit() {
    this.administrationService.getHtmlPage(this.code).pipe(take((1))).subscribe(x => this.page = x[0]);
  }

  close(result: boolean) {
    this.dialogRef.close(result);
  }
}
