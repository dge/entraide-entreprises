import {Inject, Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'customDate'
})
export class CustomDatePipe implements PipeTransform {

  constructor(@Inject('moment') private moment) {
  }

  transform(value: unknown, createdDate?: Date): unknown {
    let text = '';
    if (value) {
      const d = this.moment(value);
      const d2 = createdDate ? this.moment(createdDate) : null;
      if (!d2 && !d.isSame(d2)) {
        text = createdDate ? 'Mise à jour ' : 'Créé ';
        const diffHours = this.moment().diff(d, 'hour');
        const diffDay = this.moment().diff(d, 'day');
        const diffWeek = this.moment().diff(d, 'week');
        if (diffHours <= 72) {
          text += 'il y a ' + diffHours + 'h';
        } else if (diffDay < 7) {
          text += 'il y a ' + diffDay + ' jour' + (diffDay > 1 ? 's' : '');
        } else if (diffWeek < 4) {
          text += 'il y a ' + diffWeek + ' semaine' + (diffWeek > 1 ? 's' : '');
        } else {
          text += 'le ' + d.format('DD/MM/YYYY');
        }
      }
    }
    return text;
  }

}
