import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'profile'
})
export class ProfilePipe implements PipeTransform {

  transform(value: number): unknown {
    if (!value || value === 0) {
      return 'Contributeur novice';
    } else if (value < 5) {
      return 'Contributeur débutant';
    } else if (value < 10) {
      return 'Contributeur actif';
    } else {
      return 'Contributeur expert';
    }
  }

}
