import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MenuComponent} from './menu/menu.component';
import {IonicModule} from '@ionic/angular';
import {RouterModule} from '@angular/router';
import {HeaderComponent} from './header/header.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {PageTitleComponent} from './page-title/page-title.component';
import {PhotoProfilComponent} from './photo-profil/photo-profil.component';
import {FormDirective} from './form/form.directive';
import {FormComponent} from './form/form/form.component';
import {FieldInputComponent} from './form/field-input/field-input.component';
import {FieldDateComponent} from './form/field-date/field-date.component';
import {FieldPhotoComponent} from './form/field-photo/field-photo.component';
import {FieldPasswordComponent} from './form/field-password/field-password.component';
import {MaterialModule} from './material.module';
import {InputPhotoComponent} from './form/input-photo/input-photo.component';
import {FieldAutocompleteComponent} from './form/field-autocomplete/field-autocomplete.component';
import {CustomDatePipe} from './pipes/custom-date.pipe';
import {FooterMenuComponent} from './footer-menu/footer-menu.component';
import {LoginFormComponent} from './login-form/login-form.component';
import {ProfilePipe} from './pipes/profile.pipe';
import {FieldEditorComponent} from './form/field-editor/field-editor.component';
import {CompleteInscriptionFormComponent} from './complete-inscription-form/complete-inscription-form.component';
import {FieldRadioComponent} from './form/field-radio/field-radio.component';
import {CKEditorModule} from '@ckeditor/ckeditor5-angular';
import {FieldCheckboxComponent} from './form/field-checkbox/field-checkbox.component';
import {ConfirmModalComponent} from './modal/confirm-modal/confirm-modal.component';
import {FormModalComponent} from './modal/form-modal/form-modal.component';
import {FieldTextareaComponent} from './form/field-textarea/field-textarea.component';
import {ContactUserModalComponent} from './modal/contact-user-modal/contact-user-modal.component';
import {FooterComponent} from './footer/footer.component';


@NgModule({
    declarations: [
        MenuComponent,
        HeaderComponent,
        PageTitleComponent,
        PhotoProfilComponent,
        FormComponent,
        FieldInputComponent,
        FieldDateComponent,
        FieldPhotoComponent,
        FieldPasswordComponent,
        FieldAutocompleteComponent,
        FieldEditorComponent,
        FieldCheckboxComponent,
        FieldRadioComponent,
        FieldTextareaComponent,
        FormDirective,
        InputPhotoComponent,
        CustomDatePipe,
        FooterMenuComponent,
        LoginFormComponent,
        ProfilePipe,
        CompleteInscriptionFormComponent,
        ConfirmModalComponent,
        FormModalComponent,
        ContactUserModalComponent,
        FooterComponent
    ],
    exports: [
        IonicModule,
        MaterialModule,
        MenuComponent,
        HeaderComponent,
        PageTitleComponent,
        PhotoProfilComponent,
        FormComponent,
        FieldEditorComponent,
        CustomDatePipe,
        FooterMenuComponent,
        LoginFormComponent,
        ProfilePipe,
        CompleteInscriptionFormComponent,
        FormModalComponent,
        ConfirmModalComponent,
        FooterComponent
    ],
    imports: [
        CommonModule,
        IonicModule,
        RouterModule,
        ReactiveFormsModule,
        MaterialModule,
        FormsModule,
        CKEditorModule
    ]
})
export class SharedModule {
}
