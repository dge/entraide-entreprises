import {ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MenuController} from '@ionic/angular';
import {select, Store} from '@ngrx/store';
import {getRouter} from '../../store/router/router.state';
import {fromEvent, merge, Subject, Subscription, timer} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {
    getModerationCount,
    getNbNotifications,
    getUser,
    isAdmin,
    isAuthenticated
} from '../../store/user/user.selectors';
import {changeEntity, loadNotifications, logoutUser} from '../../store/user/user.actions';
import {environment} from '@environment/environment';
import {Router} from '@angular/router';
import {CurrentUser} from '../../store/models/current-user';
import {Entity} from '../../store/models/entity';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnDestroy {

    isHomePage: boolean;
    user: CurrentUser;
    isAuthenticated: boolean;
    nbNotifications: number;
    nbModerations: number;
    unsusbcribe$: Subject<void> = new Subject<void>();
    subscription: Subscription;
    appVersion: string;
    entity: Entity;
    isAdmin: boolean;
    @ViewChild('notifButton') notifButton: ElementRef;

    constructor(private menu: MenuController,
                private store: Store,
                private router: Router,
                private changeDetectorRef: ChangeDetectorRef) {
    }

    ngOnInit() {
        this.appVersion = environment.appVersion;
        this.store.pipe(select(isAuthenticated), takeUntil(this.unsusbcribe$)).subscribe(isAuth => {
            this.isAuthenticated = isAuth;
            this.loadNotifs();
        });
        this.store.pipe(select(getUser), takeUntil(this.unsusbcribe$)).subscribe(user => {
            this.user = user;
            if (user && user.entities) {
                const e = user.entities.filter(x => x && x.actif)[0];
                this.entity = e ? e : user.entities[0];
            }
        });
        this.store.pipe(select(getRouter), takeUntil(this.unsusbcribe$)).subscribe(route => {
            this.isHomePage = route.state.url === '/questions';
        });
        this.store.pipe(select(getNbNotifications), takeUntil(this.unsusbcribe$)).subscribe(nbNotifications => {
            this.nbNotifications = nbNotifications;
        });
        this.store.pipe(select(isAdmin), takeUntil(this.unsusbcribe$)).subscribe(admin => {
            this.isAdmin = admin;
        });
        this.store.pipe(select(getModerationCount), takeUntil(this.unsusbcribe$)).subscribe(count => {
            this.nbModerations = count ? count.alertAnswers + count.noValidAnswers + count.noValidQuestions : 0;
        });
    }

    openMenu() {
        this.menu.open('mainMenu');
    }

    ngOnDestroy() {
        this.unsusbcribe$.next();
        this.unsusbcribe$.complete();
        if (this.subscription) {
            this.subscription.unsubscribe();
            this.subscription = null;
        }
    }

    logout() {
        this.store.dispatch((logoutUser()));
        this.router.navigate(['/']);
    }

    loadNotifs() {
        if (this.isAuthenticated && !this.subscription) {
            this.changeDetectorRef.detectChanges();
            setTimeout(() => {
                if (this.notifButton) {
                    this.subscription = merge(
                        fromEvent(this.notifButton.nativeElement, 'click'),
                        timer(0, environment.timerNotification)
                    ).subscribe(x => this.store.dispatch(loadNotifications()));
                }
            }, 500);
        } else if (!this.isAuthenticated && this.subscription) {
            this.subscription.unsubscribe();
            this.subscription = null;
        }
    }

    changeEntity(entity) {
        this.store.dispatch(changeEntity({entity}));
    }
}
