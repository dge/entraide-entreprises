import {AbstractControl, ValidationErrors} from '@angular/forms';

export function ValidateConfirmPassword(control: AbstractControl): ValidationErrors | null {
  const password = control.parent && control.parent.get('password') ? control.parent.get('password').value : '';
  return password === control.value ? null : {confirmPassword: true};
}
