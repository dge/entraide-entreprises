
import { AbstractControl, AsyncValidatorFn, ValidationErrors } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ApiResponse } from 'src/app/core/models/api-response';
import { UserService } from 'src/app/store/services/user.service';

/**
 * Async Validator checking if the username or the email is already used
 */
export class UserValidator {
  static createValidator(userService: UserService): AsyncValidatorFn {
    return (control: AbstractControl): Observable<ValidationErrors> => {
      return userService.checkDuplicateUser(control.value).pipe(
        map((result: ApiResponse<boolean>) => !result.error ? null : { duplicateUser: true })
      );
    };
  }
}