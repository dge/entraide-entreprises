// Coming from https://github.com/jbpoujol/ng-siret-validator

import {AbstractControl, ValidationErrors} from '@angular/forms';

export function ValidateSiret(control: AbstractControl): ValidationErrors | null {
  let siret: any = control.value;
  if (siret == null) {
    return null;
  }
  siret = siret.replace(/\s/g, '');
  if (isNaN(siret) || siret.length !== 14) {
    return {siret: true};
  }

  // SIRET de la poste
  if (siret.startsWith(356)) {
    let tot = 0;
    for (let i = 0; i < 14; i++) {
      tot += parseInt(siret[i], 10);
    }
    return tot % 5 === 0 ? null : {siret: true};
  }

  // SIRET classique
  let bal = 0;
  let total = 0;
  for (let j = 14 - 1; j >= 0; j--) {
    const step = (siret.charCodeAt(j) - 48) * (bal + 1);
    total += step > 9 ? step - 9 : step;
    bal = 1 - bal;
  }
  return total % 10 === 0 ? null : {siret: true};
}
