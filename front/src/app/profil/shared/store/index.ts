import {createReducer, MetaReducer, on} from '@ngrx/store';
import {environment} from '@environment/environment';
import {User} from '../../../store/models/user';
import * as ProfilActions from './profil.actions';

export const stateFeatureKey = 'profil';

export interface State {
  profil: User;
}

export const initialState: State = {
  profil: null
};

export const reducer = createReducer(
  initialState,

  on(ProfilActions.loadProfil, (state) => ({...state, profil: null})),
  on(ProfilActions.loadProfilSuccess, (state, action) => ({...state, profil: action.data})),
  on(ProfilActions.loadProfilSuccess, (state) => ({...state})),
  on(ProfilActions.likeProfilSuccess, (state, action) => ({
    ...state,
    profil: {...state.profil, followers: action.data.followers}
  })),
  on(ProfilActions.changeMode, (state, action) => ({...state})),
  on(ProfilActions.changeModeSuccess, (state, action) => ({
    ...state,
    profil: {...state.profil, anonyme: !state.profil.anonyme}
  })),
);


export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];
