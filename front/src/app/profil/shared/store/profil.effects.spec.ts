import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { ProfilEffects } from './profil.effects';

describe('ProfilEffects', () => {
  let actions$: Observable<any>;
  let effects: ProfilEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ProfilEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.inject(ProfilEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
