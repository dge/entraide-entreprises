import * as fromProfil from './profil.actions';

describe('loadProfils', () => {
  it('should return an action', () => {
    expect(fromProfil.loadProfil().type).toBe('[Profil] Load Profils');
  });
});
