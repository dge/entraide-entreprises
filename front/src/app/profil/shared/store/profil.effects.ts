import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import * as ProfilActions from './profil.actions';
import {catchError, concatMap, mergeMap, tap, withLatestFrom} from 'rxjs/operators';
import * as GlobalActions from '../../../store/global/global.actions';
import {getRouter} from '../../../store/router/router.state';
import {ApiResponse} from '../../../core/models/api-response';
import {of} from 'rxjs';
import {Store} from '@ngrx/store';
import {ProfilService} from '../services/profil.service';
import {User} from '../../../store/models/user';
import * as UserActions from '../../../store/user/user.actions';


@Injectable()
export class ProfilEffects {

  loadProfil$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ProfilActions.loadProfil),
      tap((res) => this.store.dispatch(GlobalActions.startLoading())),
      withLatestFrom(this.store.select(getRouter), (action, route) => {
        return {
          myProfile: (route.state.url.indexOf('mon-profil') !== -1
            || route.state.url.indexOf('modifier-mon-compte') !== -1
            || route.state.url.indexOf('parametre-compte') !== -1),
          username: route.state.params.username
        };
      }),
      concatMap((route: { myProfile: boolean, username: string }) =>
        this.profilService.getProfile(route).pipe(
          mergeMap((resp: ApiResponse<User>) => [
            ProfilActions.loadProfilSuccess({data: resp.data}),
            GlobalActions.loadingSuccess(),
          ]),
          catchError((error) => of(GlobalActions.addError({titre: 'Loadind User', error}))),
        ),
      ),
    );
  });

  likeUser$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ProfilActions.likeProfil),
      tap((res) => this.store.dispatch(GlobalActions.startLoading())),
      concatMap((action) =>
        this.profilService.likeUser(action.data).pipe(
          mergeMap((resp: ApiResponse<User>) => [
            ProfilActions.likeProfilSuccess({data: resp.data}),
            GlobalActions.loadingSuccess(),
          ]),
          catchError((error) => of(GlobalActions.addError({titre: 'Suivre un utilisateur', error}))),
        ),
      ),
    );
  });

  changeMode$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ProfilActions.changeMode),
      tap((res) => this.store.dispatch(GlobalActions.startLoading())),
      concatMap((action) =>
        this.profilService.changeMode().pipe(
          mergeMap((resp: ApiResponse<User>) => [
            ProfilActions.changeModeSuccess(),
            GlobalActions.loadingSuccess(),
            UserActions.changeMode({anonyme: resp.data.anonyme})
          ]),
          catchError((error) => of(GlobalActions.addError({titre: 'Change Mode', error}))),
        ),
      ),
    );
  });

  constructor(private actions$: Actions,
              private store: Store,
              private profilService: ProfilService) {
  }

}
