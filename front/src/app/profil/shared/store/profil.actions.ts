import {createAction, props} from '@ngrx/store';
import {User} from '../../../store/models/user';

export const loadProfil = createAction(
  '[Profil] Load Profil'
);

export const loadProfilSuccess = createAction(
  '[Profil] Load Profil Success',
  props<{ data: User }>()
);

export const likeProfil = createAction('[Profil] like Profil', props<{ data: { id: string, userId: string } }>());
export const likeProfilSuccess = createAction('[Profil] like Profil Success', props<{ data: User }>());
export const changeMode = createAction('[Profil] Change Mode');
export const changeModeSuccess = createAction('[Profil] Change Mode Success');



