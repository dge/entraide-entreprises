import { createFeatureSelector, createSelector } from '@ngrx/store';

import * as fromProfil from './index';

export const selectReferentielState = createFeatureSelector<fromProfil.State>(fromProfil.stateFeatureKey);

export const getProfil = createSelector(selectReferentielState, (state: fromProfil.State) => state.profil);
