import {Inject, Injectable} from '@angular/core';
import {FieldConfig} from '../../../shared/form/models/field-config';
import {Validators} from '@angular/forms';
import {UserService} from '../../../store/services/user.service';
import {UserValidator} from '../validators/user-validator';
import {ValidateConfirmPassword} from '../validators/confirm-password.validator';
import {ValidateSiret} from '../validators/siret.validator';
import {ProfilService} from './profil.service';

@Injectable({
    providedIn: 'root'
})
export class CreerProfilService {

    step1Fields: FieldConfig[] = [
        {
            type: 'input',
            name: 'firstName',
            label: 'Prénom',
            validations: [
                {
                    name: 'required',
                    validator: Validators.required,
                    message: 'Le prénom est obligatoire'
                }
            ],
            autocomplete: 'given-name'
        },
        {
            type: 'input',
            name: 'lastName',
            label: 'Nom',
            validations: [
                {
                    name: 'required',
                    validator: Validators.required,
                    message: 'Le nom est obligatoire'
                }
            ],
            autocomplete: 'family-name'
        },
        {
            type: 'input',
            name: 'username',
            label: 'Pseudo',
            validations: [
                {
                    name: 'required',
                    validator: Validators.required,
                    message: 'Le pseudo est obligatoire'
                },
                {
                    name: 'duplicateUser',
                    asyncValidator: UserValidator.createValidator(this.userService),
                    message: `Le pseudo est déjà utilisé`
                }
            ],
            autocomplete: 'off'
        },
        {
            type: 'input',
            inputType: 'email',
            name: 'email',
            label: 'Adresse mail',
            validations: [
                {
                    name: 'required',
                    validator: Validators.required,
                    message: `L'adresse mail est obligatoire`
                },
                {
                    name: 'email',
                    validator: Validators.email,
                    message: `L'adresse mail n'est pas valide`
                },
                {
                    name: 'duplicateUser',
                    asyncValidator: UserValidator.createValidator(this.userService),
                    message: `L'adresse mail est déjà utilisée`
                }
            ],
            autocomplete: 'email'
        },
        {
            type: 'password',
            name: 'password',
            label: 'Mot de passe',
            showOnFocus: true,
            showMessage: false,
            validations: [
                {
                    name: 'required',
                    validator: Validators.required,
                    message: 'Le mot de passe est obligatoire'
                },
                {
                    name: 'pattern',
                    validator: Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&\-])[0-9A-Za-z\d$@$!%*?&\-].{8,}'),
                    message: `Le mot de passe n'est pas valide`
                }
            ],
            autocomplete: 'new-password'
        },
        {
            type: 'password',
            name: 'confirmPassword',
            label: 'Confirmation du mot de passe',
            showOnFocus: true,
            showMessage: false,
            validations: [
                {
                    name: 'required',
                    validator: Validators.required,
                    message: 'La confirmation du mot de passe est obligatoire'
                },
                {
                    name: 'pattern',
                    validator: Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&\-])[0-9A-Za-z\d$@$!%*?&\-].{8,}'),
                    message: `La confirmation du mot de passe n'est pas valide`
                },
                {
                    name: 'confirmPassword',
                    validator: ValidateConfirmPassword,
                    message: `Le mot de passe et sa confirmation ne sont pas identiques`
                }
            ],
            autocomplete: 'new-password'
        },
        {
            type: 'date',
            name: 'birthDate',
            label: 'Date de naissance',
            startView: 'multi-year',
            startDate: this.moment().add(-30, 'year').toDate()
        },
        {
            type: 'checkbox',
            name: 'anonyme',
            label: 'Mode anonyme',
            value: true,
            description: 'En mode anonyme, votre nom et votre entreprise n\'apparaissent pas sur vos contributions'
        },
        {
            type: 'checkbox',
            name: 'cgu',
            label: 'J\'accepte les conditions générales d\'utilisation' ,
            contenuHtml: 'cgu',
            validations: [
                {
                    name: 'required',
                    validator: Validators.required,
                    message: 'Le pseudo est obligatoire'
                }
            ]
        }
    ];

    step2Fields: FieldConfig[] = [
        {
            type: 'input',
            name: 'entityName',
            label: 'Raison sociale',
            validations: [
                {
                    name: 'required',
                    validator: Validators.required,
                    message: 'Le nom de la structure est obligatoire'
                }
            ]
        },
        {
            type: 'input',
            name: 'entitySiret',
            label: 'SIRET',
            validations: [
                {
                    name: 'required',
                    validator: Validators.required,
                    message: 'Le SIRET est obligatoire'
                },
                {
                    name: 'siret',
                    validator: ValidateSiret,
                    message: `Le SIRET mail n'est pas valide`
                }
            ]
        },
        {
            type: 'input',
            name: 'entityURL',
            label: 'URL'
        },
        {
            type: 'input',
            name: 'entityAddress',
            label: 'Adresse',
            validations: [
                {
                    name: 'required',
                    validator: Validators.required,
                    message: `L'URL est obligatoire`
                }
            ]
        },
        {
            type: 'input',
            name: 'entityAddressBis',
            label: 'Complément d\'adresse'
        },
        {
            type: 'input',
            name: 'entityCodePostal',
            label: 'Code Postal',
            validations: [
                {
                    name: 'required',
                    validator: Validators.required,
                    message: `Le code postl est obligatoire`
                }
            ]
        },
        {
            type: 'input',
            name: 'entityCity',
            label: 'Ville',
            validations: [
                {
                    name: 'required',
                    validator: Validators.required,
                    message: `La ville est obligatoire`
                }
            ]
        },
        {
            type: 'autocomplete',
            name: 'job',
            label: 'Votre poste',
            validations: [
                {
                    name: 'required',
                    validator: Validators.required,
                    message: `Le poste est obligatoire`
                }
            ],
            options: this.profilService.getJobList()
        }
    ];

    constructor(private userService: UserService,
                private profilService: ProfilService,
                @Inject('moment') private moment) {
    }
}
