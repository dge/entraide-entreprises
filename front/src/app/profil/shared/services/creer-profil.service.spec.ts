import { TestBed } from '@angular/core/testing';

import { CreerProfilService } from './creer-profil.service';

describe('CreerProfilService', () => {
  let service: CreerProfilService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CreerProfilService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
