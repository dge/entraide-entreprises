import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment as env, environment} from '@environment/environment';
import {ApiResponse} from '../../../core/models/api-response';
import {User} from '../../../store/models/user';
import {Observable} from 'rxjs';
import {Question} from '../../../questions/shared/models/question';
import {map, share} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProfilService {

  constructor(private http: HttpClient) {
  }

  getProfile(route: { myProfile: boolean, username: string }): Observable<ApiResponse<User>> {
    if (route.myProfile) {
      return this.http.get<ApiResponse<User>>(`${environment.apiUrl}/user/me`);
    } else {
      return this.http.get<ApiResponse<User>>(`${environment.apiUrl}/user/item/${route.username}`);
    }
  }

  getAskedQuestion(id: string): Observable<Question[]> {
    return this.http.get<ApiResponse<Question[]>>(`${env.apiUrl}/question/askedQuestions/${id}`)
      .pipe(map(resp => resp.data));
  }

  getAnsweredQuestion(id: string): Observable<Question[]> {
    return this.http.get<ApiResponse<Question[]>>(`${env.apiUrl}/question/answeredQuestions/${id}`)
      .pipe(map(resp => resp.data));
  }

  getFollowers(username: string): Observable<User[]> {
    return this.http.get<ApiResponse<User[]>>(`${env.apiUrl}/user/followed/${username}`)
      .pipe(map(resp => resp.data));
  }

  likeUser(form: { id: string; userId: string }): Observable<ApiResponse<User>> {
    return this.http.post<ApiResponse<User>>(`${env.apiUrl}/user/likeUser`, form);
  }

  changeMode(): Observable<ApiResponse<User>> {
    return this.http.post<ApiResponse<User>>(`${env.apiUrl}/user/changeMode`, {});
  }

  changeContact(): Observable<ApiResponse<User>> {
    return this.http.post<ApiResponse<User>>(`${env.apiUrl}/user/changeContact`, {});
  }

  updateProfile(profil: User): Observable<ApiResponse<User>> {
    return this.http.put<ApiResponse<User>>(`${env.apiUrl}/user/item/${profil.id}`, profil);
  }

  getAll(search: string, page: number, limit: number): Observable<ApiResponse<{ users: User[], total: number }>> {
    let params = new HttpParams();
    params = params.append('page', String(page));
    params = params.append('limit', String(limit));
    if (search) {
      params = params.append('search', search);
    }
    return this.http.get<ApiResponse<{ users: User[], total: number }>>(`${env.apiUrl}/user/all`, {params});
  }

  getJobList(): Observable<string[]> {
    return this.http.get<string[]>('assets/liste-metiers.json').pipe(share());
  }
}
