import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { ProfilRoutingModule } from './profil-routing.module';
import { SharedModule } from '../shared/shared.module';
import * as fromState from './shared/store';
import { ProfilEffects } from './shared/store/profil.effects';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ProfilRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    StoreModule.forFeature(fromState.stateFeatureKey, fromState.reducer, { metaReducers: fromState.metaReducers }),
    EffectsModule.forFeature([ProfilEffects])
  ]
})
export class ProfilModule { }
