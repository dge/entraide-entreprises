import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../../store/models/user';
import {CurrentUser} from '../../../store/models/current-user';

@Component({
  selector: 'app-profil-tabs',
  templateUrl: './profil-tabs.component.html',
  styleUrls: ['./profil-tabs.component.scss'],
})
export class ProfilTabsComponent implements OnInit {

  @Input() profil: User;
  @Input() currentUser: CurrentUser;
  constructor() { }

  ngOnInit() {}

}
