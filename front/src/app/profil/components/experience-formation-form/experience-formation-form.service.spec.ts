import { TestBed } from '@angular/core/testing';

import { ExperienceFormationFormService } from './experience-formation-form.service';

describe('ExperienceFormationFormService', () => {
  let service: ExperienceFormationFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ExperienceFormationFormService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
