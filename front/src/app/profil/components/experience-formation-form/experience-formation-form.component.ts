import {Component, Input, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {FormGroup} from '@angular/forms';
import {ExperienceFormationFormService} from './experience-formation-form.service';
import {Experience} from '../../../store/models/experience';
import {FieldConfig} from '../../../shared/form/models/field-config';

@Component({
  selector: 'app-experience-formation-form',
  templateUrl: './experience-formation-form.component.html',
  styleUrls: ['./experience-formation-form.component.scss'],
})
export class ExperienceFormationFormComponent implements OnInit {

  @Input() type: 'experience' | 'training';
  @Input() item: Experience;
  fields: FieldConfig[];

  constructor(private modalController: ModalController,
              private formService: ExperienceFormationFormService) {
  }

  ngOnInit() {
    this.fields = this.formService.createForm(this.item);
  }

  dismissModal(value: any): void {
    this.modalController.dismiss(value);
  }

  save(value: any) {
    this.dismissModal(value);
  }
}
