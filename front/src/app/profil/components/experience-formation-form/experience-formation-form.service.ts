import {Injectable} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {Experience} from '../../../store/models/experience';
import {Platform} from '@ionic/angular';
import {FieldConfig} from '../../../shared/form/models/field-config';
import {UserValidator} from '../../shared/validators/user-validator';
import {ValidateConfirmPassword} from '../../shared/validators/confirm-password.validator';

@Injectable({
  providedIn: 'root'
})
export class ExperienceFormationFormService {


  constructor() {
  }

  createForm(value: any): FieldConfig[] {
    return [
      {
        type: 'input',
        name: 'name',
        label: 'Libellé',
        value: value ? value.name : '',
        validations: [
          {
            name: 'required',
            validator: Validators.required,
            message: 'Le libellé est obligatoire'
          }
        ]
      },
      {
        type: 'date',
        name: 'startDate',
        label: 'Date de début',
        startView: 'multi-year',
        value: value ? new Date(value.startDate) : '',
        validations: [
          {
            name: 'required',
            validator: Validators.required,
            message: 'La date de début est obligatoire'
          }
        ]
      },
      {
        type: 'date',
        name: 'endDate',
        label: 'Date de fin',
        startView: 'multi-year',
        value: value ? new Date(value.endDate) : ''
      },
      {
        type: 'input',
        name: 'url',
        label: 'URL',
        value: value ? value.url : ''
      },
      {
        type: 'input',
        name: 'description',
        label: 'Description',
        value: value ? value.description : '',
      }
    ];
  }

}
