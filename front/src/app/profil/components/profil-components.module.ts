import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProfilEnteteComponent} from './profil-entete/profil-entete.component';
import {IonicModule} from '@ionic/angular';
import {ExpertiseComponent} from './expertise/expertise.component';
import {UserListComponent} from './user-list/user-list.component';
import {UserListItemComponent} from './user-list-item/user-list-item.component';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ModifierProfilFormComponent} from './modifier-profil-form/modifier-profil-form.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {ExperienceFormationFormComponent} from './experience-formation-form/experience-formation-form.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatFormFieldModule} from '@angular/material/form-field';
import {SharedModule} from '../../shared/shared.module';
import {ProfilTabsComponent} from './profil-tabs/profil-tabs.component';
import {ParametrageNotificationsComponent} from './parametrage-notifications/parametrage-notifications.component';


@NgModule({
    declarations: [
        ProfilEnteteComponent,
        ExpertiseComponent,
        UserListComponent,
        UserListItemComponent,
        ModifierProfilFormComponent,
        ExperienceFormationFormComponent,
        ProfilTabsComponent,
        ParametrageNotificationsComponent
    ],
    exports: [
        ProfilEnteteComponent,
        ExpertiseComponent,
        UserListComponent,
        UserListItemComponent,
        ModifierProfilFormComponent,
        ProfilTabsComponent,
        ParametrageNotificationsComponent
    ],
    imports: [
        IonicModule,
        CommonModule,
        RouterModule,
        FormsModule,
        MatExpansionModule,
        ReactiveFormsModule,
        MatDatepickerModule,
        MatFormFieldModule,
        SharedModule
    ]
})
export class ProfilComponentsModule {
}
