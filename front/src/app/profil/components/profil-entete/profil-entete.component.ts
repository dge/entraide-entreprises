import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {User} from '../../../store/models/user';
import {LoginService} from '../../../core/login/login.service';
import {Store} from '@ngrx/store';
import {changeMode, likeProfil} from '../../shared/store/profil.actions';
import {CurrentUser} from '../../../store/models/current-user';

@Component({
  selector: 'app-profil-entete',
  templateUrl: './profil-entete.component.html',
  styleUrls: ['./profil-entete.component.scss'],
})
export class ProfilEnteteComponent implements OnChanges {

  @Input() profil: User;
  @Input() currentUser: CurrentUser;
  liked: boolean;

  constructor(private loginService: LoginService,
              private store: Store) {
  }

  ngOnChanges(changes: SimpleChanges) {
    this.liked = this.currentUser && this.profil
      && this.profil.followers && this.profil.followers.indexOf(this.currentUser.id) !== -1;
  }

  like(): void {
    if (!this.currentUser) {
      this.loginService.openLoginForm();
    } else {
      const answer = {
        id: this.profil.id,
        userId: this.currentUser.id
      };
      this.store.dispatch(likeProfil({data: answer}));
    }
  }

  changeMode(): void {
    this.store.dispatch(changeMode());
  }
}
