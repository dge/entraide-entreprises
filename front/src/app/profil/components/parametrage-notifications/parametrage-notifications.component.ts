import {Component, OnDestroy, OnInit} from '@angular/core';
import {User} from '../../../store/models/user';
import {Subject} from 'rxjs';
import {select, Store} from '@ngrx/store';
import {loadMyProfile, updateMyProfile} from '../../../store/user/user.actions';
import {getMe} from '../../../store/user/user.selectors';
import {takeUntil} from 'rxjs/operators';
import cloneDeep from 'lodash/cloneDeep';

@Component({
  selector: 'app-parametrage-notifications',
  templateUrl: './parametrage-notifications.component.html',
  styleUrls: ['./parametrage-notifications.component.scss'],
})
export class ParametrageNotificationsComponent implements OnInit, OnDestroy {

  user: User;
  unsusbcribe$: Subject<void> = new Subject<void>();

  constructor(private store: Store) {
  }

  ngOnInit() {
    this.store.dispatch(loadMyProfile());
    this.store.pipe(select(getMe), takeUntil(this.unsusbcribe$)).subscribe(user => {
      this.user = cloneDeep(user);
    });
  }

  ngOnDestroy() {
    this.unsusbcribe$.next();
    this.unsusbcribe$.complete();
  }

  save() {
    this.store.dispatch(updateMyProfile({data: this.user}));
  }
}
