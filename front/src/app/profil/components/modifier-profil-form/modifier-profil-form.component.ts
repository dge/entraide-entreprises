import {Component, Input, OnChanges, SimpleChanges, ViewChild} from '@angular/core';
import {User} from '../../../store/models/user';
import {Photo} from '../../../core/models/photo';
import {PhotoService} from '../../../core/photo.service';
import {ModalController} from '@ionic/angular';
import {ExperienceFormationFormComponent} from '../experience-formation-form/experience-formation-form.component';
import {ProfilService} from '../../shared/services/profil.service';
import {take} from 'rxjs/operators';
import {Router} from '@angular/router';
import {Experience} from '../../../store/models/experience';
import cloneDeep from 'lodash/cloneDeep';
import {Store} from '@ngrx/store';
import {updateProfilePicture} from '../../../store/user/user.actions';
import {FieldConfig} from '../../../shared/form/models/field-config';
import {Validators} from '@angular/forms';
import {ValidateSiret} from '../../shared/validators/siret.validator';
import {SiretService} from '../../../core/siret.service';
import {FormComponent} from '../../../shared/form/form/form.component';
import {Entity} from '../../../store/models/entity';

@Component({
  selector: 'app-modifier-profil-form',
  templateUrl: './modifier-profil-form.component.html',
  styleUrls: ['./modifier-profil-form.component.scss'],
})
export class ModifierProfilFormComponent implements OnChanges {

  @Input() profil: User;
  photo: Photo;
  expandExp: boolean;
  expandForm: boolean;
  expandNet: boolean;
  expandEnt: boolean;
  @ViewChild('appForm') appForm: FormComponent;
  formConfig: FieldConfig[] = [
    {
      type: 'input',
      name: 'siret',
      label: 'SIRET',
      validations: [
        {
          name: 'required',
          validator: Validators.required,
          message: 'Le SIRET est obligatoire'
        },
        {
          name: 'siret',
          validator: ValidateSiret,
          message: `Le SIRET n'est pas valide`
        }
      ]
    }
  ];
  entity: Entity;
  openEntityForm: boolean;

  constructor(private photoService: PhotoService,
              private store: Store,
              private modalController: ModalController,
              private profilService: ProfilService,
              private siretService: SiretService,
              private router: Router) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.profil) {
      this.profil = cloneDeep(this.profil);
      if (!this.profil.networks) {
        this.profil.networks = {};
      }
    }
  }

  async takePhoto() {
    const photo = await this.photoService.takePhoto();
    this.profil.profilePicture = photo.base64;
  }

  async addItem(type: 'experience' | 'training') {
    const modal = await this.modalController.create({
      component: ExperienceFormationFormComponent,
      componentProps: {
        type,
        item: null
      }
    });
    await modal.present();
    const {data} = await modal.onWillDismiss();

    if (data) {
      if (type === 'experience') {
        if (!this.profil.experiences) {
          this.profil.experiences = [];
        }
        this.profil.experiences.push(data);
      } else {
        if (!this.profil.training) {
          this.profil.training = [];
        }
        this.profil.training.push(data);
      }
    }
  }

  async updateItem(type: 'experience' | 'training', item: Experience) {
    const modal = await this.modalController.create({
      component: ExperienceFormationFormComponent,
      componentProps: {
        type,
        item
      }
    });
    await modal.present();
    const {data} = await modal.onWillDismiss();
    Object.assign(item, data);
  }

  deleteItem(type: 'experience' | 'training', item: Experience): void {
    if (type === 'experience') {
      this.profil.experiences.splice(this.profil.experiences.indexOf(item), 1);
    } else {
      this.profil.training.splice(this.profil.training.indexOf(item), 1);
    }
  }

  save(): void {
    this.profilService.updateProfile(this.profil).pipe(take(1)).subscribe(() => {
      this.store.dispatch(updateProfilePicture({profilePicture: this.profil.profilePicture}));
      this.router.navigate(['/utilisateur/mon-profil']);
    });
  }

  addSiret(form: { [p: string]: any }) {
    this.siretService.verifySiret(form.siret).pipe(take(1)).subscribe(
      (siren) => {
        this.appForm.form.get('siret').setErrors(null);
        this.profil.entities.push(this.siretService.getEntity(siren));
        this.openEntityForm = false;
      }, (err) => {
        this.appForm.form.get('siret').setErrors({siret: true});
        console.log(err);
      }
    );
  }
}
