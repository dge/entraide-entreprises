import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../../store/models/user';
import {CurrentUser} from '../../../store/models/current-user';

@Component({
  selector: 'app-expertise',
  templateUrl: './expertise.component.html',
  styleUrls: ['./expertise.component.scss'],
})
export class ExpertiseComponent implements OnInit {

  @Input() profil: User;
  @Input() currentUser: CurrentUser;

  constructor() { }

  ngOnInit() {}

}
