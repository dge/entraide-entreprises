import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {Validators} from '@angular/forms';
import {select, Store} from '@ngrx/store';
import {ActivatedRoute} from '@angular/router';
import {Actions, ofType} from '@ngrx/effects';
import {askResetUserPasswordSuccess, askResetUserPasswordUser} from '../../../store/user/user.actions';
import {takeUntil} from 'rxjs/operators';
import {askResetPasswordError} from '../../../store/user/user.selectors';
import {FieldConfig} from '../../../shared/form/models/field-config';

@Component({
  selector: 'app-mot-de-passe-oublie',
  templateUrl: './mot-de-passe-oublie.page.html',
  styleUrls: ['./mot-de-passe-oublie.page.scss'],
})
export class MotDePasseOubliePage implements OnInit, OnDestroy {

  private unsubscribe$: Subject<void> = new Subject<void>();
  public error$: Observable<string>;
  public requestHasBeenSent = false;

  formConfig: FieldConfig[];

  constructor(
    private store: Store,
    private route: ActivatedRoute,
    updates$: Actions
  ) {
    updates$.pipe(
      ofType(askResetUserPasswordSuccess),
      takeUntil(this.unsubscribe$)
    ).subscribe(() => this.requestHasBeenSent = true);
  }

  ngOnInit() {
    this.route.queryParams.pipe(
      takeUntil(this.unsubscribe$)
    ).subscribe(params => {
      const email = params.email || '';
      this.formConfig = [
        {
          type: 'input',
          inputType: 'email',
          name: 'email',
          label: 'Adresse mail',
          value: email,
          validations: [
            {
              name: 'required',
              validator: Validators.required,
              message: 'L\'adresse mail est obligatoire'
            },
            {
              name: 'required',
              validator: Validators.email,
              message: 'L\'adresse mail n\'est pas valide'
            }
          ]
        }
      ];
    });
    this.error$ = this.store.pipe(select(askResetPasswordError));
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  send(value: any) {
    this.store.dispatch(askResetUserPasswordUser(value));
  }
}
