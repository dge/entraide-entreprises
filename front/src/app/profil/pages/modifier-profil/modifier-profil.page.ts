import {Component, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {getProfil} from '../../shared/store/profil.selectors';
import {Observable} from 'rxjs';
import {User} from '../../../store/models/user';
import {getUser} from '../../../store/user/user.selectors';
import {loadProfil} from '../../shared/store/profil.actions';

@Component({
  selector: 'app-modifier-profil',
  templateUrl: './modifier-profil.page.html',
  styleUrls: ['./modifier-profil.page.scss'],
})
export class ModifierProfilPage implements OnInit {

  profil$: Observable<User>;

  constructor(private store: Store) {
  }

  ngOnInit() {
    this.profil$ = this.store.pipe(select(getProfil));
  }

  ionViewDidEnter() {
    this.store.dispatch(loadProfil());
  }

}
