import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfilPage } from './profil.page';

const routes: Routes = [
  {
    path: '',
    component: ProfilPage,
    children: [
      {
        path: 'expertises',
        loadChildren: () => import('../profil-expertises/profil-expertises.module').then(m => m.ProfilExpertisesPageModule)
      },
      {
        path: 'contributeurs',
        loadChildren: () => import('../profil-contributeurs/profil-contributeurs.module').then(m => m.ProfilContributeursPageModule)
      },
      {
        path: 'contributions',
        loadChildren: () => import('../profil-contributions/profil-contributions.module').then( m => m.ProfilContributionsPageModule)
      },
      {
        path: '',
        redirectTo: 'expertises',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfilPageRoutingModule {}
