import {Component, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {loadProfil} from '../../shared/store/profil.actions';
import {getProfil} from '../../shared/store/profil.selectors';
import {Observable} from 'rxjs';
import {User} from '../../../store/models/user';
import {tap} from 'rxjs/operators';
import {getUser} from '../../../store/user/user.selectors';
import {CurrentUser} from '../../../store/models/current-user';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.page.html',
  styleUrls: ['./profil.page.scss'],
})
export class ProfilPage implements OnInit {

  profil$: Observable<User>;
  user$: Observable<CurrentUser>;

  constructor(private store: Store) {
  }

  ngOnInit() {
    this.user$ = this.store.pipe(select(getUser));
    this.profil$ = this.store.pipe(select(getProfil));
  }

  ionViewDidEnter() {
    this.store.dispatch(loadProfil());
  }
}
