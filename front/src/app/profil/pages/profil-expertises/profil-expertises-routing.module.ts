import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfilExpertisesPage } from './profil-expertises.page';

const routes: Routes = [
  {
    path: '',
    component: ProfilExpertisesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfilExpertisesPageRoutingModule {}
