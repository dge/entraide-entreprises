import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfilExpertisesPageRoutingModule } from './profil-expertises-routing.module';

import { ProfilExpertisesPage } from './profil-expertises.page';
import {ProfilComponentsModule} from '../../components/profil-components.module';
import {SharedModule} from '../../../shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ProfilExpertisesPageRoutingModule,
        ProfilComponentsModule,
        SharedModule
    ],
  declarations: [ProfilExpertisesPage]
})
export class ProfilExpertisesPageModule {}
