import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {User} from '../../../store/models/user';
import {select, Store} from '@ngrx/store';
import {getUser} from '../../../store/user/user.selectors';
import {getProfil} from '../../shared/store/profil.selectors';
import {CurrentUser} from '../../../store/models/current-user';

@Component({
  selector: 'app-profil-expertises',
  templateUrl: './profil-expertises.page.html',
  styleUrls: ['./profil-expertises.page.scss'],
})
export class ProfilExpertisesPage implements OnInit {

  profil$: Observable<User>;
  user$: Observable<CurrentUser>;

  constructor(private store: Store) {
  }

  ngOnInit() {
    this.user$ = this.store.pipe(select(getUser));
    this.profil$ = this.store.pipe(select(getProfil));
  }

}
