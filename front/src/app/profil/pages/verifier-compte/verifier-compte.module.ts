import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {VerifierComptePageRoutingModule} from './verifier-compte-routing.module';

import {VerifierComptePage} from './verifier-compte.page';
import {SharedModule} from '../../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VerifierComptePageRoutingModule,
    SharedModule
  ],
  declarations: [VerifierComptePage]
})
export class VerifierComptePageModule {
}
