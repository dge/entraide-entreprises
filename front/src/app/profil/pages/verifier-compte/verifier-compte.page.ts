import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {LoginService} from '../../../core/login/login.service';
import {select, Store} from '@ngrx/store';
import {
  resendVerificationEmail, resendVerificationEmailSuccess,
  verifyAccount,
  verifyAccountError,
  verifyAccountSuccess
} from '../../../store/user/user.actions';
import {Actions, ofType} from '@ngrx/effects';
import {switchMap, take, takeUntil} from 'rxjs/operators';
import {Validators} from '@angular/forms';
import {UserService} from '../../../store/services/user.service';
import {getRouter} from '../../../store/router/router.state';
import {FieldConfig} from '../../../shared/form/models/field-config';

@Component({
  selector: 'app-verifier-compte',
  templateUrl: './verifier-compte.page.html',
  styleUrls: ['./verifier-compte.page.scss'],
})
export class VerifierComptePage implements OnInit, OnDestroy {

  isOk: boolean;
  isComplete: boolean;
  sendMailOk: boolean;
  errorCode: string;
  formConfig: FieldConfig[] = [
    {
      type: 'input',
      inputType: 'email',
      name: 'email',
      label: 'Adresse mail',
      validations: [
        {
          name: 'required',
          validator: Validators.required,
          message: 'L\'adresse mail est obligatoire'
        },
        {
          name: 'required',
          validator: Validators.email,
          message: 'L\'adresse mail n\'est pas valide'
        }
      ]
    }
  ];
  private unsubscribe$ = new Subject<void>();

  constructor(
    private loginService: LoginService,
    private userService: UserService,
    private store: Store,
    private actions$: Actions
  ) {
  }

  ngOnInit() {
    this.store.dispatch(verifyAccount());
    this.actions$.pipe(ofType(verifyAccountError), takeUntil(this.unsubscribe$)).subscribe((action) => {
      this.errorCode = action.message;
    });
    this.actions$.pipe(ofType(verifyAccountSuccess), takeUntil(this.unsubscribe$)).subscribe(() => {
      this.isOk = true;
    });
    this.actions$.pipe(ofType(resendVerificationEmailSuccess), takeUntil(this.unsubscribe$)).subscribe(() => {
      this.sendMailOk = true;
    });
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  public openLoginForm() {
    this.loginService.openLoginForm(true);
  }

  sendEmail(value: any) {
    this.store.dispatch(resendVerificationEmail({user: {email: value.email}}));
  }

  completeInscription(form: any) {
    this.store.pipe(
      select(getRouter),
      switchMap((router) => {
        const id = router.state.queryParams.userId;
        return this.userService.completeInscription(id, form);
      }),
      take(1)
    ).subscribe(x => {
      this.isComplete = true;
    });
  }
}
