import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListeContributeursPageRoutingModule } from './liste-contributeurs-routing.module';

import { ListeContributeursPage } from './liste-contributeurs.page';
import {SharedModule} from '../../../shared/shared.module';
import {ProfilComponentsModule} from '../../components/profil-components.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ListeContributeursPageRoutingModule,
        SharedModule,
        ProfilComponentsModule
    ],
  declarations: [ListeContributeursPage]
})
export class ListeContributeursPageModule {}
