import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListeContributeursPage } from './liste-contributeurs.page';

const routes: Routes = [
  {
    path: '',
    component: ListeContributeursPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListeContributeursPageRoutingModule {}
