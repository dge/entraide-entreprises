import {Component, OnInit} from '@angular/core';
import {ProfilService} from '../../shared/services/profil.service';
import {take} from 'rxjs/operators';
import {User} from '../../../store/models/user';

@Component({
  selector: 'app-liste-contributeurs',
  templateUrl: './liste-contributeurs.page.html',
  styleUrls: ['./liste-contributeurs.page.scss'],
})
export class ListeContributeursPage implements OnInit {
  userList: User[];
  userCount: number;
  inputText: string;
  page = 0;
  limit = 20;

  constructor(private profilService: ProfilService) {
  }

  ngOnInit() {
    this.userList = [];
    this.loadQuestion();
  }

  search(input: HTMLInputElement): void {
    this.inputText = input.value;
    this.page = 0;
    this.userList = [];
    this.loadQuestion();
  }

  loadQuestion(event?: any) {
    this.profilService.getAll(this.inputText, this.page, this.limit).pipe(take(1)).subscribe(resp => {
      this.userList = this.userList.concat(resp.data.users);
      this.userCount = resp.data.total;
      if (event) {
        event.target.complete();
        if (this.userList.length === this.userCount) {
          event.target.disabled = true;
        }
      }
    });
  }

  loadData(event: any) {
    setTimeout(() => {
      this.page++;
      this.loadQuestion(event);
    }, 500);
  }

}
