import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreerProfilPageRoutingModule } from './creer-profil-routing.module';

import { CreerProfilPage } from './creer-profil.page';
import {SharedModule} from '../../../shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        IonicModule,
        CreerProfilPageRoutingModule,
        ReactiveFormsModule,
        SharedModule
    ],
  declarations: [CreerProfilPage]
})
export class CreerProfilPageModule { }
