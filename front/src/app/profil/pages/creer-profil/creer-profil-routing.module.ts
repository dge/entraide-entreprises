import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreerProfilPage } from './creer-profil.page';

const routes: Routes = [
  {
    path: '',
    component: CreerProfilPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreerProfilPageRoutingModule {}
