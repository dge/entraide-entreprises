import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MenuController} from '@ionic/angular';
import {select, Store} from '@ngrx/store';
import {Observable, Subject} from 'rxjs';
import {UserSignup} from 'src/app/store/models/user-signup';

import {resendVerificationEmail, signUpUser, signUpUserSuccess} from '../../../store/user/user.actions';
import {getloginError} from '../../../store/user/user.selectors';
import {CreerProfilService} from '../../shared/services/creer-profil.service';
import {FieldConfig} from '../../../shared/form/models/field-config';
import {Actions, ofType} from '@ngrx/effects';
import {takeUntil} from 'rxjs/operators';
import {FormComponent} from '../../../shared/form/form/form.component';
import {Validators} from '@angular/forms';

@Component({
  selector: 'app-creer-profil',
  templateUrl: './creer-profil.page.html',
  styleUrls: ['./creer-profil.page.scss'],
})
export class CreerProfilPage implements OnInit, OnDestroy, AfterViewInit {

  error$: Observable<string>;
  formData: UserSignup;
  step1Fields: FieldConfig[];
  seeConfirmation: boolean;
  unsubscribe$ = new Subject<boolean>();
  @ViewChild('appForm') appForm: FormComponent;

  constructor(
    private store: Store,
    private menu: MenuController,
    private service: CreerProfilService,
    private actions$: Actions
  ) {
  }

  ngOnInit() {
    this.step1Fields = this.service.step1Fields;
    this.error$ = this.store.pipe(select(getloginError));
    this.actions$.pipe(ofType(signUpUserSuccess), takeUntil(this.unsubscribe$)).subscribe((action) => {
      this.seeConfirmation = true;
    });
  }

  ngOnDestroy() {
    this.unsubscribe$.next(true);
    this.unsubscribe$.complete();
  }

  save(values: { [key: string]: any }) {
    this.formData = {...this.formData, ...values};
    delete this.formData.confirmPassword;
    this.store.dispatch(signUpUser({user: this.formData}));
  }

  resendEmail() {
    this.store.dispatch(resendVerificationEmail({user: this.formData}));
  }

  ngAfterViewInit(): void {
    this.appForm.form.get('password').valueChanges.pipe(takeUntil(this.unsubscribe$)).subscribe((x) => {
      if (x && this.appForm.form.get('confirmPassword').value) {
        this.appForm.form.get('confirmPassword').updateValueAndValidity();
      }
    });
  }

}
