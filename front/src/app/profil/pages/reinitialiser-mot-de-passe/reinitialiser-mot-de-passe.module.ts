import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReinitialiserMotDePassePageRoutingModule } from './reinitialiser-mot-de-passe-routing.module';

import { ReinitialiserMotDePassePage } from './reinitialiser-mot-de-passe.page';
import {SharedModule} from '../../../shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ReinitialiserMotDePassePageRoutingModule,
        SharedModule
    ],
  declarations: [ReinitialiserMotDePassePage]
})
export class ReinitialiserMotDePassePageModule {}
