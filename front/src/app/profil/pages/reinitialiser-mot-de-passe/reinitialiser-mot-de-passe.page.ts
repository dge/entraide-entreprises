import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {Store} from '@ngrx/store';
import {
  askResetUserPasswordSuccess,
  askResetUserPasswordUser, resetUserPasswordSuccess, resetUserPasswordUser,
  verifyAccount,
  verifyAccountError,
  verifyAccountSuccess,
  verifyResetLink, verifyResetLinkError, verifyResetLinkSuccess
} from '../../../store/user/user.actions';
import {Actions, ofType} from '@ngrx/effects';
import {Validators} from '@angular/forms';
import {FieldConfig} from '../../../shared/form/models/field-config';
import {ValidateConfirmPassword} from '../../shared/validators/confirm-password.validator';

@Component({
  selector: 'app-reinitialiser-mot-de-passe',
  templateUrl: './reinitialiser-mot-de-passe.page.html',
  styleUrls: ['./reinitialiser-mot-de-passe.page.scss'],
})
export class ReinitialiserMotDePassePage implements OnInit {

  isOk: boolean;
  errorCode: string;
  emailFormConfig: FieldConfig[] = [
    {
      type: 'input',
      inputType: 'email',
      name: 'email',
      label: 'Adresse mail',
      validations: [
        {
          name: 'required',
          validator: Validators.required,
          message: 'L\'adresse mail est obligatoire'
        },
        {
          name: 'required',
          validator: Validators.email,
          message: 'L\'adresse mail n\'est pas valide'
        }
      ]
    }
  ];
  resetFormConfig: FieldConfig[] = [
    {
      type: 'password',
      name: 'password',
      label: 'Mot de passe',
      validations: [
        {
          name: 'required',
          validator: Validators.required,
          message: 'Le mot de passe est obligatoire'
        },
        {
          name: 'pattern',
          validator: Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&\-])[0-9A-Za-z\d$@$!%*?&\-].{8,}'),
          message: `Le mot de passe n'est pas valide`
        }
      ]
    },
    {
      type: 'password',
      name: 'confirmPassword',
      label: 'Confirmation du mot de passe',
      validations: [
        {
          name: 'required',
          validator: Validators.required,
          message: 'La confirmation du mot de passe est obligatoire'
        },
        {
          name: 'pattern',
          validator: Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&\-])[0-9A-Za-z\d$@$!%*?&\-].{8,}'),
          message: `La confirmation du mot de passe n'est pas valide`
        },
        {
          name: 'confirmPassword',
          validator: ValidateConfirmPassword,
          message: `Le mot de passe et sa confirmation ne sont pas identiques`
        }
      ]
    }
  ];
  unsubscribe$ = new Subject<void>();
  sendMailOk: boolean;
  resetOk: boolean;

  constructor(private store: Store,
              private actions$: Actions) {
  }

  ngOnInit() {
    this.store.dispatch(verifyResetLink());
    this.actions$.pipe(ofType(verifyResetLinkError), takeUntil(this.unsubscribe$)).subscribe((action) => {
      this.errorCode = action.message;
    });
    this.actions$.pipe(ofType(verifyResetLinkSuccess), takeUntil(this.unsubscribe$)).subscribe(() => {
      this.isOk = true;
    });
    this.actions$.pipe(ofType(askResetUserPasswordSuccess), takeUntil(this.unsubscribe$)
    ).subscribe(() => this.sendMailOk = true);
    this.actions$.pipe(ofType(resetUserPasswordSuccess), takeUntil(this.unsubscribe$)
    ).subscribe(() => this.resetOk = true);
  }

  send(value: any) {
    this.store.dispatch(askResetUserPasswordUser(value));
  }

  resetPassword(value: any) {
    this.store.dispatch(resetUserPasswordUser({
      password: value.password
    }));
  }
}
