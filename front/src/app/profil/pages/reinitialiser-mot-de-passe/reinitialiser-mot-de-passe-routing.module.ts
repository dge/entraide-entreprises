import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReinitialiserMotDePassePage } from './reinitialiser-mot-de-passe.page';

const routes: Routes = [
  {
    path: '',
    component: ReinitialiserMotDePassePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReinitialiserMotDePassePageRoutingModule {}
