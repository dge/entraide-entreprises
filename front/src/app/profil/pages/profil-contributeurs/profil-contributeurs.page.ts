import {Component, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {getProfil} from '../../shared/store/profil.selectors';
import {map, switchMap} from 'rxjs/operators';
import {Observable, of} from 'rxjs';
import {User} from '../../../store/models/user';
import {ProfilService} from '../../shared/services/profil.service';

@Component({
  selector: 'app-profil-contributeurs',
  templateUrl: './profil-contributeurs.page.html',
  styleUrls: ['./profil-contributeurs.page.scss'],
})
export class ProfilContributeursPage implements OnInit {

  users$: Observable<User[]>;

  constructor(private store: Store,
              private profilService: ProfilService) {
  }

  ionViewWillEnter() {
    this.users$ = this.store.pipe(
      select(getProfil),
      map(profil => profil ? profil.username : ''),
      switchMap(username => username ? this.profilService.getFollowers(username) : of(null))
    );
  }

  ngOnInit() {
  }

}
