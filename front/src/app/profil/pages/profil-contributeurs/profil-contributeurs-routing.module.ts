import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfilContributeursPage } from './profil-contributeurs.page';

const routes: Routes = [
  {
    path: '',
    component: ProfilContributeursPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfilContributeursPageRoutingModule {}
