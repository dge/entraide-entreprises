import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfilContributeursPageRoutingModule } from './profil-contributeurs-routing.module';

import { ProfilContributeursPage } from './profil-contributeurs.page';
import {ProfilComponentsModule} from '../../components/profil-components.module';
import {SharedModule} from '../../../shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ProfilContributeursPageRoutingModule,
        ProfilComponentsModule,
        SharedModule
    ],
  declarations: [ProfilContributeursPage]
})
export class ProfilContributeursPageModule {}
