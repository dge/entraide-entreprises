import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfilContributionsPageRoutingModule } from './profil-contributions-routing.module';

import { ProfilContributionsPage } from './profil-contributions.page';
import {QuestionsComponentsModule} from '../../../questions/components/questions-components.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ProfilContributionsPageRoutingModule,
        QuestionsComponentsModule
    ],
  declarations: [ProfilContributionsPage]
})
export class ProfilContributionsPageModule {}
