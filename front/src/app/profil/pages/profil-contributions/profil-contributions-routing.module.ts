import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfilContributionsPage } from './profil-contributions.page';

const routes: Routes = [
  {
    path: '',
    component: ProfilContributionsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfilContributionsPageRoutingModule {}
