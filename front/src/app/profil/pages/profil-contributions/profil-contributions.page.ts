import {Component, OnInit} from '@angular/core';
import {Observable, of} from 'rxjs';
import {Question} from '../../../questions/shared/models/question';
import {ProfilService} from '../../shared/services/profil.service';
import {select, Store} from '@ngrx/store';
import {getProfil} from '../../shared/store/profil.selectors';
import {map, switchMap} from 'rxjs/operators';

@Component({
  selector: 'app-profil-contributions',
  templateUrl: './profil-contributions.page.html',
  styleUrls: ['./profil-contributions.page.scss'],
})
export class ProfilContributionsPage implements OnInit {

  tab: 'question' | 'response' = 'question';
  askedQuestions$: Observable<Question[]>;
  answeredQuestions$: Observable<Question[]>;

  constructor(private profilService: ProfilService,
              private store: Store) {
  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.askedQuestions$ = this.store.pipe(
      select(getProfil),
      map(profil => profil ? profil.id : ''),
      switchMap(id => id ? this.profilService.getAskedQuestion(id) : of(null))
    );
    this.answeredQuestions$ = this.store.pipe(
      select(getProfil),
      map(profil => profil ? profil.id : ''),
      switchMap(id => id ? this.profilService.getAnsweredQuestion(id) : of(null))
    );
  }

  segmentChanged($event: any) {
    this.tab = $event.detail.value;
  }
}
