import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParametreComptePage } from './parametre-compte.page';

const routes: Routes = [
  {
    path: '',
    component: ParametreComptePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParametreComptePageRoutingModule {}
