import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {ParametreComptePageRoutingModule} from './parametre-compte-routing.module';

import {ParametreComptePage} from './parametre-compte.page';
import {SharedModule} from '../../../shared/shared.module';
import {ProfilComponentsModule} from '../../components/profil-components.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ParametreComptePageRoutingModule,
        SharedModule,
        ProfilComponentsModule
    ],
    declarations: [ParametreComptePage]
})
export class ParametreComptePageModule {
}
