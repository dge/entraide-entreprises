import { Component, OnInit } from '@angular/core';
import {ConfirmModalComponent} from '../../../shared/modal/confirm-modal/confirm-modal.component';
import {take} from 'rxjs/operators';
import {moderateAnswer} from '../../../questions/shared/store/questions.actions';
import {MatDialog} from '@angular/material/dialog';
import {select, Store} from '@ngrx/store';
import {deleteAccount} from '../../../store/user/user.actions';
import {Observable} from 'rxjs';
import {User} from '../../../store/models/user';
import {getProfil} from '../../shared/store/profil.selectors';
import {loadProfil} from '../../shared/store/profil.actions';
import {ProfilService} from '../../shared/services/profil.service';

@Component({
  selector: 'app-parametre-compte',
  templateUrl: './parametre-compte.page.html',
  styleUrls: ['./parametre-compte.page.scss'],
})
export class ParametreComptePage implements OnInit {

  profil$: Observable<User>;
  constructor(private dialog: MatDialog,
              private store: Store,
              private profilService: ProfilService) { }

  ngOnInit() {
    this.profil$ = this.store.pipe(select(getProfil));
  }

  ionViewDidEnter() {
    this.store.dispatch(loadProfil());
  }

  deleteAccount() {
    const dialogRef = this.dialog.open(ConfirmModalComponent, {
      width: '70%',
      data: 'Confirmez-vous la suppression de votre compte ?'
    });
    dialogRef
      .afterClosed()
      .pipe(take(1))
      .subscribe(resp => {
        if (resp) {
          this.store.dispatch(deleteAccount());
        }
      });
  }

  changeContact() {
    this.profilService.changeContact().pipe(take(1)).subscribe();
  }
}
