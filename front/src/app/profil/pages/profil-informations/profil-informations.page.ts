import { Component, OnInit } from '@angular/core';
import {select, Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {User} from '../../../store/models/user';
import {getProfil} from '../../shared/store/profil.selectors';

@Component({
  selector: 'app-profil-informations',
  templateUrl: './profil-informations.page.html',
  styleUrls: ['./profil-informations.page.scss'],
})
export class ProfilInformationsPage implements OnInit {

  profil$: Observable<User>;
  constructor(private store: Store) { }

  ngOnInit() {
    this.profil$ = this.store.pipe(select(getProfil));
  }

}
