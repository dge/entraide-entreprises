import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfilInformationsPageRoutingModule } from './profil-informations-routing.module';

import { ProfilInformationsPage } from './profil-informations.page';
import {SharedModule} from '../../../shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ProfilInformationsPageRoutingModule,
        SharedModule
    ],
  declarations: [ProfilInformationsPage]
})
export class ProfilInformationsPageModule {}
