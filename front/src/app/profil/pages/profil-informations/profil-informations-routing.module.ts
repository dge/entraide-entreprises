import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfilInformationsPage } from './profil-informations.page';

const routes: Routes = [
  {
    path: '',
    component: ProfilInformationsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfilInformationsPageRoutingModule {}
