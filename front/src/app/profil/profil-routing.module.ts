import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UnlogguedGuard} from '../core/guards/unloggued.guard';
import {AuthGuard} from '../core/guards/auth.guard';

const routes: Routes = [
  {
    path: 'mon-profil',
    loadChildren: () => import('./pages/profil/profil.module').then(m => m.ProfilPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'creer-profil',
    loadChildren: () => import('./pages/creer-profil/creer-profil.module').then(m => m.CreerProfilPageModule),
    canActivate: [UnlogguedGuard]
  },
  {
    path: 'profil/:username',
    loadChildren: () => import('./pages/profil/profil.module').then(m => m.ProfilPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'modifier-mon-compte',
    loadChildren: () => import('./pages/modifier-profil/modifier-profil.module').then(m => m.ModifierProfilPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'verifier-compte',
    loadChildren: () => import('./pages/verifier-compte/verifier-compte.module').then(m => m.VerifierComptePageModule),
    canActivate: [UnlogguedGuard]
  },
  {
    path: 'mot-de-passe-oublie',
    loadChildren: () => import('./pages/mot-de-passe-oublie/mot-de-passe-oublie.module').then(m => m.MotDePasseOubliePageModule),
    canActivate: [UnlogguedGuard]
  },
  {
    path: 'reinitialiser-mot-de-passe',
    loadChildren: () => import('./pages/reinitialiser-mot-de-passe/reinitialiser-mot-de-passe.module')
      .then(m => m.ReinitialiserMotDePassePageModule),
    canActivate: [UnlogguedGuard]
  },
  {
    path: 'liste-contributeurs',
    loadChildren: () => import('./pages/liste-contributeurs/liste-contributeurs.module').then(m => m.ListeContributeursPageModule)
  },  {
    path: 'parametre-compte',
    loadChildren: () => import('./pages/parametre-compte/parametre-compte.module').then( m => m.ParametreComptePageModule)
  }



];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfilRoutingModule {
}
