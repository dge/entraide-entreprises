import {Injectable} from '@angular/core';
import {Thematique} from '../models/thematique';
import {Observable, of} from 'rxjs';
import {Question} from '../models/question';
import {HttpClient, HttpParams} from '@angular/common/http';
import {ApiResponse} from '../../../core/models/api-response';
import {AddQuestion} from '../models/add-question';
import {map} from 'rxjs/operators';
import {struct} from 'pb-util';
import {AddResponse} from '../models/add-response';
import {Answer} from '../models/answer';
import {environment as env} from '@environment/environment';

@Injectable({
  providedIn: 'root'
})
export class QuestionsService {

  constructor(private httpClient: HttpClient) {
  }

  getThematiqueList(): Observable<ApiResponse<Thematique[]>> {
    return this.httpClient.get<ApiResponse<Thematique[]>>(`${env.apiUrl}/thematic`);
  }

  getActiveThematiqueList(): Observable<ApiResponse<Thematique[]>> {
    return this.httpClient.get<ApiResponse<Thematique[]>>(`${env.apiUrl}/thematic/actives`);
  }

  getThematique(id: string): Observable<ApiResponse<Thematique>> {
    return this.httpClient.get<ApiResponse<Thematique>>(`${env.apiUrl}/thematic/item/${id}`);
  }

  getThematiqueQuestionList(id: string, page: number, limit: number,
                            faq: boolean, urgent: boolean): Observable<ApiResponse<{ questions: Question[], total: number }>> {
    let params = new HttpParams();
    params = params.append('page', String(page));
    params = params.append('limit', String(limit));
    if (faq) {
      params = params.append('faq', 'true');
    }
    if (urgent) {
      params = params.append('urgent', 'true');
    }
    return this.httpClient.get<ApiResponse<{ questions: Question[], total: number }>>(`${env.apiUrl}/question/thematic/${id}`, {params});
  }

  searchQuestionList(question: string): Observable<ApiResponse<{ dialogflow: Question[], mongo: Question[] }>> {
    return this.httpClient.post<ApiResponse<{ dialogflow: Question[], mongo: Question[] }>>(`${env.apiUrl}/question/similar`, {question});
  }

  getFirstQuestionList(): Observable<ApiResponse<Question[]>> {
    return this.httpClient.get<ApiResponse<Question[]>>(`${env.apiUrl}/question/favorite`);
  }

  getQuestion(id: string): Observable<ApiResponse<Question>> {
    return this.httpClient.get<ApiResponse<Question>>(`${env.apiUrl}/question/item/${id}`);
  }

  findQuestionThematic(title: string): Observable<Thematique[]> {
    const question = {question: title};

    return this.httpClient.post<any>(`${env.apiUrl}/thematic/findQuestionThematic`, question).pipe(
      map(resp => resp.data));
  }

  saveQuestion(question: AddQuestion): Observable<ApiResponse<{ item: Question, dialogflow: Question[], mongo: Question[] }>> {
    return this.httpClient.post<ApiResponse<{ item: Question, dialogflow: Question[], mongo: Question[] }>>
    (`${env.apiUrl}/question`, question);
  }

  updateQuestion(id: string, question: AddQuestion): Observable<ApiResponse<Question>> {
    return this.httpClient.put<ApiResponse<Question>>(`${env.apiUrl}/question/item/${id}`, question);
  }

  saveAnswer(answer: AddResponse) {
    return this.httpClient.post<ApiResponse<Answer>>(`${env.apiUrl}/answer`, answer);
  }

  updateAnswer(id: string, answer: AddResponse): Observable<ApiResponse<Question>> {
    return this.httpClient.put<ApiResponse<Question>>(`${env.apiUrl}/answer/item/${id}`, answer);
  }

  getResponseList(id: string): Observable<ApiResponse<Answer[]>> {
    return this.httpClient.get<ApiResponse<Answer[]>>(`${env.apiUrl}/answer/question/${id}`);
  }

  likeThematic(form: { id: string; userId: string }): Observable<ApiResponse<Thematique>> {
    return this.httpClient.post<ApiResponse<Thematique>>(`${env.apiUrl}/thematic/likeThematic`, form);
  }

  likeQuestion(form: { id: string; userId: string }): Observable<ApiResponse<Question>> {
    return this.httpClient.post<ApiResponse<Question>>(`${env.apiUrl}/question/likeQuestion`, form);
  }

  likeResponse(form: { id: string; userId: string }): Observable<ApiResponse<Answer>> {
    return this.httpClient.post<ApiResponse<Answer>>(`${env.apiUrl}/answer/likeAnswer`, form);
  }

  addThematic(form: Thematique): Observable<ApiResponse<boolean>> {
    return this.httpClient.post<ApiResponse<boolean>>(`${env.apiUrl}/thematic`, form);
  }

  updateThematic(form: Thematique): Observable<ApiResponse<boolean>> {
    return this.httpClient.put<ApiResponse<boolean>>(`${env.apiUrl}/thematic/item/${form.id}`, form);
  }

  importThematics(form: any): Observable<ApiResponse<boolean>> {
    return this.httpClient.post<ApiResponse<boolean>>(`${env.apiUrl}/thematic/insertThematicList`, form);
  }

  importQuestions(form: any): Observable<ApiResponse<boolean>> {
    return this.httpClient.post<ApiResponse<boolean>>(`${env.apiUrl}/question/insertQuestionList`, form);
  }

  notValidQuestions(): Observable<ApiResponse<{ questions: Question[], total: number }>> {
    return this.httpClient.get<ApiResponse<{ questions: Question[], total: number }>>(`${env.apiUrl}/question/notValidQuestions`);
  }

  validateQuestion(id: string): Observable<ApiResponse<Question[]>> {
    return this.httpClient.get<ApiResponse<Question[]>>(`${env.apiUrl}/question/validate/${id}`);
  }

  notValidAnswers(): Observable<ApiResponse<{ answers: Answer[], total: number }>> {
    return this.httpClient.get<ApiResponse<{ answers: Answer[], total: number }>>(`${env.apiUrl}/answer/notValidAnswers`);
  }

  alertAnswers(): Observable<ApiResponse<{ answers: Answer[], total: number }>> {
    return this.httpClient.get<ApiResponse<{ answers: Answer[], total: number }>>(`${env.apiUrl}/answer/alertAnswers`);
  }

  validateAnswer(id: string): Observable<ApiResponse<Question[]>> {
    return this.httpClient.get<ApiResponse<Question[]>>(`${env.apiUrl}/answer/validate/${id}`);
  }

  moderateAnswer(id: string): Observable<ApiResponse<Question[]>> {
    return this.httpClient.get<ApiResponse<Question[]>>(`${env.apiUrl}/answer/moderate/${id}`);
  }

  deleteQuestion(id: string): Observable<ApiResponse<void>> {
    return this.httpClient.delete<ApiResponse<void>>(`${env.apiUrl}/question/item/${id}`);
  }

  alertAnswer(id: string) {
    return this.httpClient.get<ApiResponse<Question[]>>(`${env.apiUrl}/answer/alert/${id}`);
  }

  deleteAnswser(id: string) {
    return this.httpClient.get<ApiResponse<Question[]>>(`${env.apiUrl}/answer/delete/${id}`);
  }

  sendQuestion(form: any) {
    return this.httpClient.post<ApiResponse<any>>(`${env.apiUrl}/question/send`, form);
  }

  contactUser(form: any) {
    return this.httpClient.post<ApiResponse<any>>(`${env.apiUrl}/user/contact`, form);
  }

  uploadQuestionFile(formData: any): Observable<ApiResponse<any>> {
    return this.httpClient.post<ApiResponse<any>>(`${env.apiUrl}/question/upload-file`, formData);
  }

  getImportFile() {
    return this.httpClient.get(`${env.apiUrl}/question/download-import-file`);
  }
}
