import {Answer} from './answer';
import {User} from '../../../store/models/user';
import {Thematique} from './thematique';

export interface Question {
  id?: string;
  label: string;
  message: string;
  likes?: string[];
  createdAt?: Date;
  updatedAt?: Date;
  thematic?: Thematique;
  author?: User;
  answerCount?: number;
  nbLikes?: number;
  answers?: Answer[];
  isUrgent?: boolean;
  isFaq?: boolean;
  anonyme?: boolean;
  valid?: boolean;
  moderate?: boolean;
  alert?: boolean;
  entity?: string;
}
