export interface AddResponse {
  author: string;
  message: any;
  question?: string;
  parent?: string;
  anonyme: boolean;
  entity: string;
  valid: boolean;
}
