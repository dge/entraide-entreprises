import {User} from '../../../store/models/user';

export interface Answer {
  id: string;
  message: string;
  createdAt: Date;
  updatedAt: Date;
  author: User;
  likes?: string[];
  answers?: Answer[];
  parent?: string;
  question?: string;
  anonyme?: boolean;
  valid?: boolean;
  moderate?: boolean;
  alert?: boolean;
  entity?: string;
}
