import {Question} from './question';

export interface Thematique {
  id: string;
  order: number;
  label: string;
  default?: boolean;
  active?: boolean;
  icon?: string;
  nbMembres?: number;
  questionCount?: number;
  questionList?: Question[];
  followers?: string[];
}
