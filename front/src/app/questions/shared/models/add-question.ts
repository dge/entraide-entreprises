export interface AddQuestion {
  author: string;
  label: any;
  message: any;
  thematic: string;
  isUrgent: boolean;
  anonyme: boolean;
  entity: string;
  valid: boolean;
}
