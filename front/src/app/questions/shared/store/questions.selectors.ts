import {createFeatureSelector, createSelector} from '@ngrx/store';

import * as fromQuestions from './index';

export const selectReferentielState = createFeatureSelector<fromQuestions.State>(fromQuestions.questionsFeatureKey);

export const getThematiqueList = createSelector(selectReferentielState, (state: fromQuestions.State) => state.thematiqueList);
export const getThematique = createSelector(selectReferentielState, (state: fromQuestions.State) => state.thematique);
export const getQuestionList = createSelector(selectReferentielState, (state: fromQuestions.State) => state.questionList);
export const getDialogflowQuestionList = createSelector(selectReferentielState, (state: fromQuestions.State) =>
  state.dialogflowQuestionList);
export const getResponseList = createSelector(selectReferentielState, (state: fromQuestions.State) => state.responseList);
export const getFavoriteQuestionList = createSelector(selectReferentielState, (state: fromQuestions.State) => state.favoriteQuestionList);
export const getQuestion = createSelector(selectReferentielState, (state: fromQuestions.State) => state.question);
export const getSearchQuestion = createSelector(selectReferentielState, (state: fromQuestions.State) => state.searchQuestion);
