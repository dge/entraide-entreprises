import {createReducer, MetaReducer, on} from '@ngrx/store';
import {Thematique} from '../models/thematique';
import * as QuestionsActions from './questions.actions';
import {Question} from '../models/question';
import {Answer} from '../models/answer';
import cloneDeep from 'lodash/cloneDeep';
import {environment} from '@environment/environment';
import {moderateAnswer, validateQuestion} from './questions.actions';

export const questionsFeatureKey = 'questions';

export interface State {
  thematiqueList: Thematique[];
  thematique: Thematique;
  questionList: Question[];
  dialogflowQuestionList: Question[];
  favoriteQuestionList: Question[];
  question: Question;
  searchQuestion: string;
  responseList: Answer[];
}

export const initialState: State = {
  thematiqueList: null,
  thematique: null,
  questionList: null,
  dialogflowQuestionList: null,
  favoriteQuestionList: null,
  question: null,
  searchQuestion: null,
  responseList: null
};

export const reducer = createReducer(
  initialState,

  on(QuestionsActions.loadThematiqueList, (state) => ({...state})),
  on(QuestionsActions.loadThematiqueListSuccess, (state, action) => ({...state, thematiqueList: action.data})),
  on(QuestionsActions.loadThematique, (state) => ({...state, thematique: null})),
  on(QuestionsActions.loadThematiqueSuccess, (state, action) => ({...state, thematique: action.data})),
  on(QuestionsActions.loadQuestionList, (state, action) => ({...state, searchQuestion: action.data})),
  on(QuestionsActions.loadQuestionListSuccess, (state, action) => ({
    ...state,
    dialogflowQuestionList: action.data.dialogflow,
    questionList: action.data.mongo
  })),
  on(QuestionsActions.loadResponseList, (state, action) => ({...state, responseList: []})),
  on(QuestionsActions.loadResponseListSuccess, (state, action) => ({...state, responseList: action.data})),
  on(QuestionsActions.loadFirstQuestionList, (state) => ({...state})),
  on(QuestionsActions.loadFirstQuestionListSuccess, (state, action) => ({...state, favoriteQuestionList: action.data})),
  on(QuestionsActions.loadQuestion, (state) => ({...state, question: null})),
  on(QuestionsActions.loadQuestionSuccess, (state, action) => ({...state, question: action.data})),
  on(QuestionsActions.saveResponse, (state) => ({...state})),
  on(QuestionsActions.saveResponseSuccess, (state, action) => ({
    ...state,
    question: {...state.question, isUrgent: false},
    responseList: [].concat(state.responseList).concat([action.data])
  })),
  on(QuestionsActions.saveSubResponse, (state) => ({...state})),
  on(QuestionsActions.saveSubResponseSuccess, (state, action) => {
    const respList = cloneDeep(state.responseList);
    if (!respList[action.data.index].answers) {
      respList[action.data.index].answers = [];
    }
    respList[action.data.index].answers.push(action.data.answer);
    return {
      ...state,
      responseList: respList
    };
  }),
  on(QuestionsActions.moderateAnswer, (state, action) => {
    const respList = cloneDeep(state.responseList);
    if (action.data.valid) {
      respList[action.data.index].valid = true;
      respList[action.data.index].alert = false;
    } else {
      respList[action.data.index].message = null;
      respList[action.data.index].valid = true;
      respList[action.data.index].moderate = true;
      respList[action.data.index].alert = false;
    }
    return {
      ...state,
      responseList: respList
    };
  }),
  on(QuestionsActions.deleteAnswer, (state, action) => {
    const respList = cloneDeep(state.responseList);
    respList[action.data.index].message = null;
    return {
      ...state,
      responseList: respList
    };
  }),
  on(QuestionsActions.likeQuestion, (state) => ({...state})),
  on(QuestionsActions.validateQuestion, (state) => ({...state, question: {...state.question, valid: true}})),
  on(QuestionsActions.likeQuestionSuccess, (state, action) => ({
    ...state,
    question: {...state.question, likes: action.data.likes}
  })),
  on(QuestionsActions.likeResponse, (state) => ({...state})),
  on(QuestionsActions.likeResponseSuccess, (state, action) => {
    const respList = cloneDeep(state.responseList);
    if (action.data.parent == null) {
      respList[action.data.index].likes = action.data.response.likes;
    } else {
      respList[action.data.parent].answers[action.data.index].likes = action.data.response.likes;
    }
    return {...state, responseList: respList};
  }),
  on(QuestionsActions.likeThematic, (state) => ({...state})),
  on(QuestionsActions.likeThematicSuccess, (state, action) => ({
    ...state,
    thematique: {...state.thematique, followers: action.data.followers}
  })),
);


export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];
