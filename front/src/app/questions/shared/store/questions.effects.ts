import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType, OnInitEffects} from '@ngrx/effects';
import * as QuestionsActions from './questions.actions';
import {catchError, concatMap, mergeMap, tap, withLatestFrom} from 'rxjs/operators';
import {Action, Store} from '@ngrx/store';
import {QuestionsService} from '../services/questions.service';
import {getRouter} from '../../../store/router/router.state';
import * as GlobalActions from '../../../store/global/global.actions';
import {Thematique} from '../models/thematique';
import {of} from 'rxjs';
import {Question} from '../models/question';
import {ApiResponse} from '../../../core/models/api-response';
import {getQuestion} from './questions.selectors';
import {getUser} from '../../../store/user/user.selectors';
import {AddResponse} from '../models/add-response';
import {Answer} from '../models/answer';
import {SnackbarService} from '../../../core/snackbar/snackbar.service';
import {LoadingState} from '../../../core/loading-state.enum';


@Injectable()
export class QuestionsEffects implements OnInitEffects {

    loadThematiqueList$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(QuestionsActions.loadThematiqueList),
            tap((res) => this.store.dispatch(GlobalActions.startLoading())),
            concatMap(() =>
                this.questionsService.getActiveThematiqueList().pipe(
                    mergeMap((resp: ApiResponse<Thematique[]>) => [
                        QuestionsActions.loadThematiqueListSuccess({data: resp.data}),
                        GlobalActions.loadingSuccess(),
                    ]),
                    catchError((error) => of(GlobalActions.addError({titre: 'Liste des thématiques', error}))),
                ),
            ),
        );
    });

    loadThematique$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(QuestionsActions.loadThematique),
            tap((res) => this.store.dispatch(GlobalActions.startLoading())),
            withLatestFrom(this.store.select(getRouter), (action, route) => {
                return route.state.params.id;
            }),
            concatMap((id: string) =>
                this.questionsService.getThematique(id).pipe(
                    mergeMap((resp: ApiResponse<Thematique>) => [
                        QuestionsActions.loadThematiqueSuccess({data: resp.data}),
                        GlobalActions.loadingSuccess(),
                    ]),
                    catchError((error) => of(GlobalActions.addError({titre: 'Liste des thématiques', error}))),
                ),
            ),
        );
    });

    loadQuestionList$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(QuestionsActions.loadQuestionList),
            tap((res) => this.store.dispatch(GlobalActions.startLoading())),
            concatMap((action) =>
                this.questionsService.searchQuestionList(action.data).pipe(
                    mergeMap((resp: ApiResponse<{ dialogflow: Question[], mongo: Question[] }>) => [
                        QuestionsActions.loadQuestionListSuccess({data: resp.data}),
                        GlobalActions.loadingSuccess(),
                    ]),
                    catchError((error) => of(GlobalActions.addError({titre: 'Liste des thématiques', error}))),
                ),
            ),
        );
    });

    loadFirstQuestionList = createEffect(() => {
        return this.actions$.pipe(
            ofType(QuestionsActions.loadFirstQuestionList),
            tap((res) => this.store.dispatch(GlobalActions.startLoading())),
            concatMap(() =>
                this.questionsService.getFirstQuestionList().pipe(
                    mergeMap((resp: ApiResponse<Question[]>) => [
                        QuestionsActions.loadFirstQuestionListSuccess({data: resp.data}),
                        GlobalActions.loadingSuccess(),
                    ]),
                    catchError((error) => of(GlobalActions.addError({titre: 'Liste des thématiques', error}))),
                ),
            ),
        );
    });

    loadQuestion$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(QuestionsActions.loadQuestion),
            tap((res) => this.store.dispatch(GlobalActions.startLoading())),
            withLatestFrom(this.store.select(getRouter), (action, route) => {
                return route.state.params.id;
            }),
            concatMap((id: string) =>
                this.questionsService.getQuestion(id).pipe(
                    mergeMap((resp: ApiResponse<Question>) => [
                        QuestionsActions.loadQuestionSuccess({data: resp.data}),
                        GlobalActions.loadingSuccess(),
                    ]),
                    catchError((error) => of(GlobalActions.addError({titre: 'Liste des thématiques', error}))),
                ),
            ),
        );
    });

    loadResponseList$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(QuestionsActions.loadResponseList),
            tap((res) => this.store.dispatch(GlobalActions.startLoading())),
            withLatestFrom(this.store.select(getRouter), (action, route) => {
                return route.state.params.id;
            }),
            concatMap((id: string) =>
                this.questionsService.getResponseList(id).pipe(
                    mergeMap((resp: ApiResponse<Answer[]>) => [
                        QuestionsActions.loadResponseListSuccess({data: resp.data}),
                        GlobalActions.loadingSuccess(),
                    ]),
                    catchError((error) => of(GlobalActions.addError({titre: 'Liste des réponses', error}))),
                ),
            ),
        );
    });

    saveAnswer$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(QuestionsActions.saveResponse),
            tap((res) => this.store.dispatch(GlobalActions.startLoading())),
            withLatestFrom(this.store.select(getQuestion), this.store.select(getUser), (action, question, user) => {
                let e = user.entities ? user.entities.filter(x => x && x.actif)[0] : null;
                if (e == null) {
                    e = user.entities ? user.entities[0] : null;
                }
                return {
                    author: user.id,
                    message: action.data,
                    question: question.id,
                    anonyme: user.anonyme,
                    entity: e != null ? e.id : null,
                    valid: user.roles.indexOf('ADMIN') !== -1
                } as AddResponse;
            }),
            concatMap((form: AddResponse) =>
                this.questionsService.saveAnswer(form).pipe(
                    mergeMap((resp: ApiResponse<Answer>) => [
                        QuestionsActions.saveResponseSuccess({data: resp.data}),
                        GlobalActions.loadingSuccess(),
                    ]),
                    tap((action: any) => {
                        this.snackBarService.openSnackBar(LoadingState.SUCCESS, {
                            titre: '',
                            message: 'Nous vous remercions pour votre réponse. Elle ne sera visible qu\'après validation du modérateur.',
                        });
                    }),
                    catchError((error) => of(GlobalActions.addError({titre: 'Répondre à la question', error}))),
                ),
            ),
        )
            ;
    });

    saveSubResponse$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(QuestionsActions.saveSubResponse),
            tap((res) => this.store.dispatch(GlobalActions.startLoading())),
            concatMap((action) =>
                this.questionsService.saveAnswer(action.data.answer).pipe(
                    mergeMap((resp: ApiResponse<Answer>) => [
                        QuestionsActions.saveSubResponseSuccess({data: {index: action.data.index, answer: resp.data}}),
                        GlobalActions.loadingSuccess(),
                    ]),
                    catchError((error) => of(GlobalActions.addError({titre: 'Répondre', error}))),
                ),
            ),
        );
    });

    likeThematic$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(QuestionsActions.likeThematic),
            tap((res) => this.store.dispatch(GlobalActions.startLoading())),
            concatMap((action) =>
                this.questionsService.likeThematic(action.data).pipe(
                    mergeMap((resp: ApiResponse<Thematique>) => [
                        QuestionsActions.likeThematicSuccess({data: resp.data}),
                        GlobalActions.loadingSuccess(),
                    ]),
                    catchError((error) => of(GlobalActions.addError({titre: 'J\'aime', error}))),
                ),
            ),
        );
    });

    likeQuestion$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(QuestionsActions.likeQuestion),
            tap((res) => this.store.dispatch(GlobalActions.startLoading())),
            concatMap((action) =>
                this.questionsService.likeQuestion(action.data).pipe(
                    mergeMap((resp: ApiResponse<Question>) => [
                        QuestionsActions.likeQuestionSuccess({data: resp.data}),
                        GlobalActions.loadingSuccess(),
                    ]),
                    catchError((error) => of(GlobalActions.addError({titre: 'J\'aime', error}))),
                ),
            ),
        );
    });

    likeResponse$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(QuestionsActions.likeResponse),
            tap((res) => this.store.dispatch(GlobalActions.startLoading())),
            concatMap((action) =>
                this.questionsService.likeResponse(action.data.form).pipe(
                    mergeMap((resp: ApiResponse<Answer>) => [
                        QuestionsActions.likeResponseSuccess({
                            data: {
                                parent: action.data.parent,
                                index: action.data.index,
                                response: resp.data
                            }
                        }),
                        GlobalActions.loadingSuccess(),
                    ]),
                    catchError((error) => of(GlobalActions.addError({titre: 'J\'aime', error}))),
                ),
            ),
        );
    });

    ngrxOnInitEffects(): Action {
        return {type: '[Questions] Load ThematiqueList'};
    }

    constructor(private actions$: Actions,
                private store: Store,
                private questionsService: QuestionsService,
                private snackBarService: SnackbarService) {
    }

}
