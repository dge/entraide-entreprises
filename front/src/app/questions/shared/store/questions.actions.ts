import {createAction, props} from '@ngrx/store';
import {Question} from '../models/question';
import {Thematique} from '../models/thematique';
import {Answer} from '../models/answer';
import {AddResponse} from '../models/add-response';

export const loadThematiqueList = createAction('[Questions] Load ThematiqueList');
export const loadThematiqueListSuccess = createAction('[Questions] Load ThematiqueList Success', props<{ data: Thematique[] }>());

export const loadThematique = createAction('[Questions] Load Thematique');
export const loadThematiqueSuccess = createAction('[Questions] Load Thematique Success', props<{ data: Thematique }>());

export const loadQuestionList = createAction('[Questions] Load QuestionList', props<{ data: string }>());
export const loadQuestionListSuccess = createAction('[Questions] Load QuestionList Success',
  props<{ data: { dialogflow: Question[], mongo: Question[] } }>());

export const loadFirstQuestionList = createAction('[Questions] Load FirstQuestionList');
export const loadFirstQuestionListSuccess = createAction('[Questions] Load FirstQuestionList Success', props<{ data: Question[] }>());

export const loadQuestion = createAction('[Questions] Load Question');
export const loadQuestionSuccess = createAction('[Questions] Load Question Success', props<{ data: Question }>());

export const loadResponseList = createAction('[Questions] Load ResponseList', props<{ data: string }>());
export const loadResponseListSuccess = createAction('[Questions] Load ResponseList Success', props<{ data: Answer[] }>());

export const saveResponse = createAction('[Questions] Save Response', props<{ data: string }>());
export const saveResponseSuccess = createAction('[Questions] Save Response Success', props<{ data: Answer }>());

export const moderateAnswer = createAction('[Questions] moderate Response', props<{ data: { index: number, valid: boolean } }>());
export const deleteAnswer = createAction('[Questions] delete Response', props<{ data: { index: number } }>());
export const saveSubResponse = createAction('[Questions] Save SubResponse', props<{ data: { index: number, answer: AddResponse } }>());
export const saveSubResponseSuccess = createAction('[Questions] Save SubResponse Success',
  props<{ data: { index: number, answer: Answer } }>());

export const likeThematic = createAction('[Questions] like Thematic', props<{ data: { id: string, userId: string } }>());
export const likeThematicSuccess = createAction('[Questions] like Thematic Success', props<{ data: Thematique }>());

export const likeQuestion = createAction('[Questions] like Question', props<{ data: { id: string, userId: string } }>());
export const likeQuestionSuccess = createAction('[Questions] like Question Success', props<{ data: Question }>());

export const likeResponse = createAction('[Questions] like Response',
  props<{ data: { parent: number; index: number; form: { id: string, userId: string } } }>());
export const likeResponseSuccess = createAction('[Questions] like Response Success',
  props<{ data: { parent: number; index: number; response: Answer } }>());

export const validateQuestion = createAction('[Questions] validate Question');
