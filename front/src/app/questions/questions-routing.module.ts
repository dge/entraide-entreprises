import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from '../core/guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./pages/accueil/accueil.module').then( m => m.AccueilPageModule)
  },
  {
    path: 'liste-des-thematiques',
    loadChildren: () => import('./pages/liste-thematiques/liste-thematiques.module').then( m => m.ListeThematiquesPageModule)
  },
  {
    path: 'nouvelle-question',
    loadChildren: () => import('./pages/poser-question/poser-question.module').then( m => m.PoserQuestionPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'rechercher',
    loadChildren: () => import('./pages/rechercher-question/rechercher-question.module').then( m => m.RechercherQuestionPageModule)
  },
  {
    path: 'question/:id',
    loadChildren: () => import('./pages/visualiser-question/visualiser-question.module').then( m => m.VisualiserQuestionPageModule)
  },
  {
    path: 'theme/:id',
    loadChildren: () => import('./pages/visualiser-thematique/visualiser-thematique.module').then( m => m.VisualiserThematiquePageModule)
  },
  {
    path: 'modifier/:id',
    loadChildren: () => import('./pages/modifier-question/modifier-question.module').then( m => m.ModifierQuestionPageModule)
  },
  {
    path: 'modifier-reponse/:id',
    loadChildren: () => import('./pages/modifier-reponse/modifier-reponse.module').then( m => m.ModifierReponsePageModule)
  },  {
    path: 'moderate-questions',
    loadChildren: () => import('./pages/moderate-questions/moderate-questions.module').then( m => m.ModerateQuestionsPageModule)
  },
  {
    path: 'import-question',
    loadChildren: () => import('./pages/import-question/import-question.module').then( m => m.ImportQuestionPageModule)
  },
  {
    path: 'import-thematique',
    loadChildren: () => import('./pages/import-thematique/import-thematique.module').then( m => m.ImportThematiquePageModule)
  },



];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QuestionsRoutingModule {
}
