import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {VisualiserQuestionPageRoutingModule} from './visualiser-question-routing.module';

import {VisualiserQuestionPage} from './visualiser-question.page';
import {QuestionsComponentsModule} from '../../components/questions-components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VisualiserQuestionPageRoutingModule,
    QuestionsComponentsModule
  ],
  declarations: [VisualiserQuestionPage]
})
export class VisualiserQuestionPageModule {}
