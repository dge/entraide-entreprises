import {Component, OnInit, ViewChild} from '@angular/core';
import {Observable} from 'rxjs';
import {select, Store} from '@ngrx/store';
import {loadQuestion, loadResponseList, saveResponse} from '../../shared/store/questions.actions';
import {getQuestion, getResponseList} from '../../shared/store/questions.selectors';
import {Question} from '../../shared/models/question';
import {getUser} from '../../../store/user/user.selectors';
import {FooterAnswerQuestionComponent} from '../../components/footer-answer-question/footer-answer-question.component';
import {Answer} from '../../shared/models/answer';
import {LoginService} from '../../../core/login/login.service';
import {take} from 'rxjs/operators';
import {CurrentUser} from '../../../store/models/current-user';
import {getLoading} from '../../../store/global/global.selectors';

@Component({
  selector: 'app-visualiser-question',
  templateUrl: './visualiser-question.page.html',
  styleUrls: ['./visualiser-question.page.scss'],
})
export class VisualiserQuestionPage implements OnInit {
  question$: Observable<Question>;
  loading$: Observable<boolean>;
  responseList$: Observable<Answer[]>;
  user$: Observable<CurrentUser>;
  @ViewChild('footer') footer: FooterAnswerQuestionComponent;
  @ViewChild('content') private content: any;


  constructor(private store: Store,
              private loginService: LoginService) {
  }

  ionViewWillEnter() {
    this.store.dispatch(loadQuestion());
    this.store.dispatch(loadResponseList({data: ''}));
  }

  ngOnInit() {
    this.loading$ = this.store.pipe(select(getLoading));
    this.responseList$ = this.store.pipe(select(getResponseList));
    this.question$ = this.store.pipe(select(getQuestion));
    this.user$ = this.store.pipe(select(getUser));
  }

  saveAnswer($event: string): void {
    this.user$.pipe(take(1)).subscribe(user => {
      if (user) {
        this.store.dispatch(saveResponse({data: $event}));
      } else {
        this.loginService.openLoginForm().then(x => {
          if (x.data.loggedIn) {
            setTimeout(() => {
              this.store.dispatch(saveResponse({data: $event}));
            }, 500);
          }
        });
      }
    });
  }

  onAnswerClick() {
    this.content.scrollToBottom(300);
    this.footer.setFocus();
  }
}
