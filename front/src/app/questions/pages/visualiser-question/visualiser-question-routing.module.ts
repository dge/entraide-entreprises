import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VisualiserQuestionPage } from './visualiser-question.page';

const routes: Routes = [
  {
    path: '',
    component: VisualiserQuestionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VisualiserQuestionPageRoutingModule {}
