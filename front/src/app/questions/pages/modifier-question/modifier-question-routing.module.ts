import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModifierQuestionPage } from './modifier-question.page';

const routes: Routes = [
  {
    path: '',
    component: ModifierQuestionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModifierQuestionPageRoutingModule {}
