import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModifierQuestionPageRoutingModule } from './modifier-question-routing.module';

import { ModifierQuestionPage } from './modifier-question.page';
import {SharedModule} from '../../../shared/shared.module';
import {QuestionsComponentsModule} from '../../components/questions-components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModifierQuestionPageRoutingModule,
    SharedModule,
    QuestionsComponentsModule
  ],
  declarations: [ModifierQuestionPage]
})
export class ModifierQuestionPageModule {}
