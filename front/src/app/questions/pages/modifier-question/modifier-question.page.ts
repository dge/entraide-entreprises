import {Component, OnDestroy, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {getQuestion} from '../../shared/store/questions.selectors';
import {getRouter} from '../../../store/router/router.state';
import {combineLatest, Subject} from 'rxjs';
import {map, switchMap, take, takeUntil} from 'rxjs/operators';
import {loadQuestion} from '../../shared/store/questions.actions';
import {Question} from '../../shared/models/question';
import {getUser} from '../../../store/user/user.selectors';
import {AddQuestion} from '../../shared/models/add-question';
import {ApiResponse} from '../../../core/models/api-response';
import {QuestionsService} from '../../shared/services/questions.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-modifier-question',
  templateUrl: './modifier-question.page.html',
  styleUrls: ['./modifier-question.page.scss'],
})
export class ModifierQuestionPage implements OnInit, OnDestroy {
  question: Question;
  unsubscribe$: Subject<void> = new Subject<void>();

  constructor(private store: Store,
              private questionService: QuestionsService,
              private router: Router) {
  }

  ionViewWillEnter(){
  }

  ngOnInit() {
    combineLatest([this.store.pipe(select(getRouter)), this.store.pipe(select(getQuestion))])
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(([route, question]) => {
        if (!question || question.id !== route.state.params.id) {
          this.store.dispatch(loadQuestion());
        } else {
          this.question = question;
        }
      });
    this.store.pipe(select(getQuestion)).subscribe(question => {

    });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  save(form: { label: string; message: string }) {
    this.store.pipe(
      select(getUser),
      map(x => {
        return {
          label: form.label,
          message: form.message,
          thematic: this.question.thematic.id,
          author: this.question.author.id
        } as AddQuestion;
      }),
      switchMap(question => this.questionService.updateQuestion(this.question.id, question)),
      take(1)
    ).subscribe((resp: ApiResponse<Question>) => {
      this.router.navigate(['/questions/question/', resp.data.id]);
    });
  }
}
