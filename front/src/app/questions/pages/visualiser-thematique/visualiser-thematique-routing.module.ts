import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VisualiserThematiquePage } from './visualiser-thematique.page';

const routes: Routes = [
  {
    path: '',
    component: VisualiserThematiquePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VisualiserThematiquePageRoutingModule {}
