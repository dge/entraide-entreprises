import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {VisualiserThematiquePageRoutingModule} from './visualiser-thematique-routing.module';

import {VisualiserThematiquePage} from './visualiser-thematique.page';
import {QuestionsComponentsModule} from '../../components/questions-components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VisualiserThematiquePageRoutingModule,
    QuestionsComponentsModule
  ],
  declarations: [VisualiserThematiquePage]
})
export class VisualiserThematiquePageModule {}
