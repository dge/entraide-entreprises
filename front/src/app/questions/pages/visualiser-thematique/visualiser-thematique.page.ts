import {Component, OnInit} from '@angular/core';
import {Thematique} from '../../shared/models/thematique';
import {Observable} from 'rxjs';
import {select, Store} from '@ngrx/store';
import {loadThematique} from '../../shared/store/questions.actions';
import {getThematique} from '../../shared/store/questions.selectors';
import {getUser} from '../../../store/user/user.selectors';
import {CurrentUser} from '../../../store/models/current-user';

@Component({
  selector: 'app-visualiser-thematique',
  templateUrl: './visualiser-thematique.page.html',
  styleUrls: ['./visualiser-thematique.page.scss'],
})
export class VisualiserThematiquePage implements OnInit {
  thematique$: Observable<Thematique>;
  user$: Observable<CurrentUser>;

  constructor(private store: Store) { }

  ngOnInit() {
    this.store.dispatch(loadThematique());
    this.thematique$ = this.store.pipe(select(getThematique));
    this.user$ = this.store.pipe(select(getUser));
  }

}
