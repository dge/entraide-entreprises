import {Component, OnDestroy, OnInit} from '@angular/core';
import {Thematique} from '../../shared/models/thematique';
import {Question} from '../../shared/models/question';
import {select, Store} from '@ngrx/store';
import {Observable, Subject} from 'rxjs';
import {getFavoriteQuestionList, getThematiqueList} from '../../shared/store/questions.selectors';
import {loadFirstQuestionList, loadQuestionList} from '../../shared/store/questions.actions';
import {map, takeUntil} from 'rxjs/operators';
import {Router} from '@angular/router';
import {Tools} from '../../../core/tools';
import {isAuthenticated} from '../../../store/user/user.selectors';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.page.html',
  styleUrls: ['./accueil.page.scss'],
})
export class AccueilPage implements OnInit, OnDestroy {
  themeList: Thematique[];
  allThemeList: Thematique[];
  questionList$: Observable<Question[]>;
  seeInfos = false;
  isAuthen: boolean;
  unsusbcribe$: Subject<void> = new Subject<void>();

  constructor(private store: Store, private router: Router) {
  }

  ngOnInit() {
    this.store.pipe(select(isAuthenticated), takeUntil(this.unsusbcribe$)).subscribe(isAuth => {
      this.isAuthen = isAuth;
    });
    this.store.dispatch(loadFirstQuestionList());
    this.store.pipe(select(getThematiqueList)).subscribe(list => {
      this.themeList = list ? list.slice().sort(Tools.sortThematicByOrderAsc).slice(0, 4) : [];
      this.allThemeList = list ? list.slice().sort(Tools.sortThematicByOrderAsc) : [];
    });
    this.questionList$ = this.store.pipe(select(getFavoriteQuestionList), map(list => {
      return list ? list.slice(0, 2) : [];
    }));
  }

  ngOnDestroy() {
    this.unsusbcribe$.next();
    this.unsusbcribe$.complete();
  }

  search(input: any): void {
    this.store.dispatch(loadQuestionList({ data: '' + input.value }));
    input.value = '';
    this.router.navigate(['/questions', 'rechercher']);
  }
}
