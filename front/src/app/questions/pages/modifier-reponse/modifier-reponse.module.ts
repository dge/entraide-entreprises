import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModifierReponsePageRoutingModule } from './modifier-reponse-routing.module';

import { ModifierReponsePage } from './modifier-reponse.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModifierReponsePageRoutingModule
  ],
  declarations: [ModifierReponsePage]
})
export class ModifierReponsePageModule {}
