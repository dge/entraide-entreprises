import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModifierReponsePage } from './modifier-reponse.page';

const routes: Routes = [
  {
    path: '',
    component: ModifierReponsePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModifierReponsePageRoutingModule {}
