import {Component, OnInit} from '@angular/core';
import {take} from 'rxjs/operators';
import {QuestionsService} from '../../shared/services/questions.service';
import {Store} from '@ngrx/store';
import {Thematique} from '../../shared/models/thematique';
import {MatDialog} from '@angular/material/dialog';
import {FormModalComponent} from '../../../shared/modal/form-modal/form-modal.component';
import {Validators} from '@angular/forms';
import {FormModalData} from '../../../shared/modal/form-modal/form-modal-data';
import {loadThematiqueList} from '../../shared/store/questions.actions';

@Component({
    selector: 'app-import-thematique',
    templateUrl: './import-thematique.page.html',
    styleUrls: ['./import-thematique.page.scss'],
})
export class ImportThematiquePage implements OnInit {

    list: Thematique[];

    constructor(
        private questionService: QuestionsService,
        private store: Store,
        private dialog: MatDialog) {
    }

    ngOnInit() {
    }

    ionViewDidEnter() {
        this.questionService.getThematiqueList().pipe(take(1)).subscribe(list => {
            this.list = list.data;
        });
    }

    add() {
        const dialogRef = this.dialog.open(FormModalComponent, {
            width: '70%',
            data: {
                title: 'Ajouter une thématique',
                items: this.getItems()
            } as FormModalData
        });
        dialogRef
            .afterClosed()
            .pipe(take(1))
            .subscribe(resp => {
                if (resp) {
                    this.questionService.addThematic(resp).pipe(take(1)).subscribe(x => {
                        this.ionViewDidEnter();
                        this.store.dispatch(loadThematiqueList());
                    });
                }
            });
    }

    update(item: Thematique) {
        const dialogRef = this.dialog.open(FormModalComponent, {
            width: '70%',
            data: {
                title: 'Modifier une thématique',
                items: this.getItems(item)
            } as FormModalData
        });
        dialogRef
            .afterClosed()
            .pipe(take(1))
            .subscribe(resp => {
                if (resp) {
                    resp.id = item.id;
                    this.questionService.updateThematic(resp).pipe(take(1)).subscribe(x => {
                        this.ionViewDidEnter();
                        this.store.dispatch(loadThematiqueList());
                    });
                }
            });
    }

    /*
    save() {
      try {
        this.questionService.importThematics(json).pipe(take(1)).subscribe(x => {
          this.store.dispatch(loadThematiqueList());
        });
      } catch (e) {
        console.error(e);
      }
    }

     */

    getItems(item?: Thematique) {
        return [
            {
                type: 'input',
                inputType: 'number',
                name: 'order',
                label: 'Ordre',
                validations: [
                    {
                        name: 'required',
                        validator: Validators.required,
                        message: 'L\'ordre est obligatoire'
                    }
                ],
                value: item ? item.order : null
            },
            {
                type: 'input',
                name: 'label',
                label: 'Libellé',
                validations: [
                    {
                        name: 'required',
                        validator: Validators.required,
                        message: 'Le libellé est obligatoire'
                    }
                ],
                value: item ? item.label : null
            },
            {
                type: 'input',
                name: 'icon',
                label: 'Icône',
                value: item ? item.icon : null
            },
            {
                type: 'checkbox',
                name: 'default',
                label: 'Thématique par défaut',
                value: item ? item.default : false
            },
            {
                type: 'checkbox',
                name: 'active',
                label: 'Thématique active',
                value: item ? item.active : true
            }
        ];
    }

}
