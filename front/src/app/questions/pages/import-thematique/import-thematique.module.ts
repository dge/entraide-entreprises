import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ImportThematiquePageRoutingModule } from './import-thematique-routing.module';

import { ImportThematiquePage } from './import-thematique.page';
import {SharedModule} from '../../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ImportThematiquePageRoutingModule,
    SharedModule
  ],
  declarations: [ImportThematiquePage]
})
export class ImportThematiquePageModule {}
