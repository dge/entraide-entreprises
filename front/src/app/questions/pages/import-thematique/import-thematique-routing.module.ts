import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ImportThematiquePage } from './import-thematique.page';

const routes: Routes = [
  {
    path: '',
    component: ImportThematiquePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ImportThematiquePageRoutingModule {}
