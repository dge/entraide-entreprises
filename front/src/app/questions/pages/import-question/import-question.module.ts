import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ImportQuestionPageRoutingModule } from './import-question-routing.module';

import { ImportQuestionPage } from './import-question.page';
import {SharedModule} from '../../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ImportQuestionPageRoutingModule,
    SharedModule
  ],
  declarations: [ImportQuestionPage]
})
export class ImportQuestionPageModule {}
