import {Component, OnInit} from '@angular/core';
import {QuestionsService} from '../../shared/services/questions.service';
import {take} from 'rxjs/operators';
import {Tools} from '../../../core/tools';

@Component({
    selector: 'app-import-question',
    templateUrl: './import-question.page.html',
    styleUrls: ['./import-question.page.scss'],
})
export class ImportQuestionPage implements OnInit {

    initialText = `[
    {
      "thematic": "test",
      "title": "test",
      "message": "test",
      "answer": "test"
    }
  ]`;
    text = this.initialText;
    fileName = '';
    requiredFileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
    errors: any[];
    success: boolean;

    constructor(
        private questionService: QuestionsService) {
    }

    ngOnInit() {
    }

    save() {
        try {
            const json = JSON.parse(this.text);
            this.questionService.importQuestions(json).pipe(take(1)).subscribe(x => {
                this.text = this.initialText;
            });
        } catch (e) {
            console.error(e);
        }
    }

    onFileSelected(event) {
        const file: File = event.target.files[0];
        event.target.value = null;
        if (file && file.type === this.requiredFileType) {
            this.fileName = file.name;
            const formData = new FormData();
            formData.append('file', file);
            this.success = false;
            this.questionService.uploadQuestionFile(formData).pipe(take(1)).subscribe(x => {
                console.log(x);
                if (x.error) {
                    this.errors = x.data;
                } else {
                    this.errors = null;
                    this.success = true;
                }
                this.fileName = null;
            });
        }
    }

    getImportFile() {
        this.questionService.getImportFile().pipe(take(1)).subscribe((x: any) => {
            Tools.downloadFile(x.name, x.type, x.content);
        });
    }
}
