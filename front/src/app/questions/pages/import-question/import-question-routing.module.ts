import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ImportQuestionPage } from './import-question.page';

const routes: Routes = [
  {
    path: '',
    component: ImportQuestionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ImportQuestionPageRoutingModule {}
