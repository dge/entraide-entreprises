import {Component, OnInit} from '@angular/core';
import {QuestionsService} from '../../shared/services/questions.service';
import {take} from 'rxjs/operators';
import {Question} from '../../shared/models/question';
import {Answer} from '../../shared/models/answer';
import {loadProfil} from '../../../profil/shared/store/profil.actions';

@Component({
  selector: 'app-moderate-questions',
  templateUrl: './moderate-questions.page.html',
  styleUrls: ['./moderate-questions.page.scss'],
})
export class ModerateQuestionsPage implements OnInit {

  questionList: Question[];
  totalQuestion: number;
  answerList: Answer[];
  totalAnswer: number;
  alertAnswerList: Answer[];
  totalAlertAnswer: number;
  expandQ: boolean;
  expandS: boolean;
  expandN: boolean;

  constructor(private questionsService: QuestionsService) {
  }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.questionsService.notValidQuestions().pipe(take(1)).subscribe(x => {
      this.questionList = x.data.questions;
      this.totalQuestion = x.data.total;
    });
    this.questionsService.notValidAnswers().pipe(take(1)).subscribe(x => {
      this.answerList = x.data.answers;
      this.totalAnswer = x.data.total;
    });
    this.questionsService.alertAnswers().pipe(take(1)).subscribe(x => {
      this.alertAnswerList = x.data.answers;
      this.totalAlertAnswer = x.data.total;
    });
  }
}
