import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModerateQuestionsPageRoutingModule } from './moderate-questions-routing.module';

import { ModerateQuestionsPage } from './moderate-questions.page';
import {SharedModule} from '../../../shared/shared.module';
import {QuestionsComponentsModule} from '../../components/questions-components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModerateQuestionsPageRoutingModule,
    SharedModule,
    QuestionsComponentsModule
  ],
  declarations: [ModerateQuestionsPage]
})
export class ModerateQuestionsPageModule {}
