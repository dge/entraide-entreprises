import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {PoserQuestionPageRoutingModule} from './poser-question-routing.module';

import {PoserQuestionPage} from './poser-question.page';
import {QuestionsComponentsModule} from '../../components/questions-components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PoserQuestionPageRoutingModule,
    QuestionsComponentsModule,
    ReactiveFormsModule
  ],
  declarations: [PoserQuestionPage]
})
export class PoserQuestionPageModule {}
