import {Component, OnDestroy, OnInit} from '@angular/core';
import {Thematique} from '../../shared/models/thematique';
import {SelectThematicComponent} from '../../components/select-thematic/select-thematic.component';
import {map, switchMap, take, takeUntil} from 'rxjs/operators';
import {select, Store} from '@ngrx/store';
import {getUser} from '../../../store/user/user.selectors';
import {AddQuestion} from '../../shared/models/add-question';
import {ApiResponse} from '../../../core/models/api-response';
import {Question} from '../../shared/models/question';
import {loadThematiqueList} from '../../shared/store/questions.actions';
import {ModalController} from '@ionic/angular';
import {QuestionsService} from '../../shared/services/questions.service';
import {Router} from '@angular/router';
import {getSearchQuestion} from '../../shared/store/questions.selectors';
import {of, Subject} from 'rxjs';
import {environment as env} from '@environment/environment';

@Component({
  selector: 'app-poser-question',
  templateUrl: './poser-question.page.html',
  styleUrls: ['./poser-question.page.scss'],
})
export class PoserQuestionPage implements OnInit, OnDestroy {
  form: { 'label': string; 'message': string; isUrgent: boolean };
  question: Question;
  unsusbcribe$: Subject<void> = new Subject<void>();
  addQuestionOk: boolean;
  questionList: Question[];
  dialogflowQuestionList: Question[];
  newQuestion: Question;

  constructor(private store: Store,
              private modalController: ModalController,
              private questionService: QuestionsService,
              private router: Router) {
  }

  ngOnInit() {
    this.store.pipe(select(getSearchQuestion), takeUntil(this.unsusbcribe$)).subscribe(searchQuestion => {
      this.question = {
        label: searchQuestion,
        message: ''
      };
    });
  }

  ngOnDestroy() {
    this.unsusbcribe$.next();
    this.unsusbcribe$.complete();
  }

  async openSelectThematic(thematicList: Thematique[]) {
    const modal = await this.modalController.create({
      component: SelectThematicComponent,
      componentProps: {thematicList}
    });
    await modal.present();
    const {data} = await modal.onWillDismiss();
    if (data) {
      this.saveQuestion(data);
    }
  }

  checkQuestion(form: { 'label': string; 'message': string; isUrgent: boolean }) {
    this.form = form;
    const req = env.useDialogflow
      ? this.questionService.findQuestionThematic(form.label).pipe(take(1))
      : of([]);
    req.pipe(take(1)).subscribe(resp => this.openSelectThematic(resp));
  }

  saveQuestion(thematicId: string) {
    if (thematicId) {
      this.store.pipe(
        select(getUser),
        map(user => {
          let e = user.entities ? user.entities.filter(y => y && y.actif)[0] : null;
          if (e == null) {
            e = user.entities ? user.entities[0] : null;
          }
          return {
            label: this.form.label,
            message: this.form.message,
            isUrgent: this.form.isUrgent,
            thematic: thematicId,
            author: user.id,
            anonyme: user.anonyme,
            entity: e != null ? e.id : null,
            valid: user.roles.indexOf('ADMIN') !== -1
          } as AddQuestion;
        }),
        switchMap(question => this.questionService.saveQuestion(question)),
        take(1)
      ).subscribe((resp: ApiResponse<{ item: Question, dialogflow: Question[], mongo: Question[] }>) => {
        this.store.dispatch(loadThematiqueList());
        this.addQuestionOk = true;
        this.newQuestion = resp.data.item;
        this.questionList = resp.data.mongo ? resp.data.mongo.slice(0, 4) : null;
        this.dialogflowQuestionList = resp.data.dialogflow;
      });
    }
  }


}
