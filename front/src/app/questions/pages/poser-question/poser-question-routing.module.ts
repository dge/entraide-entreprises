import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PoserQuestionPage } from './poser-question.page';
import {ReactiveFormsModule} from '@angular/forms';

const routes: Routes = [
  {
    path: '',
    component: PoserQuestionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes), ReactiveFormsModule],
  exports: [RouterModule],
})
export class PoserQuestionPageRoutingModule {}
