import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {RechercherQuestionPageRoutingModule} from './rechercher-question-routing.module';

import {RechercherQuestionPage} from './rechercher-question.page';
import {QuestionsComponentsModule} from '../../components/questions-components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RechercherQuestionPageRoutingModule,
    QuestionsComponentsModule
  ],
  declarations: [RechercherQuestionPage]
})
export class RechercherQuestionPageModule {}
