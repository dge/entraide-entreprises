import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {select, Store} from '@ngrx/store';
import {getDialogflowQuestionList, getQuestionList, getSearchQuestion} from '../../shared/store/questions.selectors';
import {Question} from '../../shared/models/question';
import {loadQuestionList} from '../../shared/store/questions.actions';
import {ModalController} from '@ionic/angular';
import {ImportFormComponent} from '../../components/import-form/import-form.component';
import {isAdmin} from '../../../store/user/user.selectors';

@Component({
  selector: 'app-rechercher-question',
  templateUrl: './rechercher-question.page.html',
  styleUrls: ['./rechercher-question.page.scss'],
})
export class RechercherQuestionPage implements OnInit, OnDestroy {
  questionList: Question[];
  dialogflowQuestionList: Question[];
  searchQuestion$: Observable<string>;
  isAdmin$: Observable<boolean>;
  unsubscribe$ = new Subject<boolean>();

  constructor(private store: Store,
              private modalController: ModalController) {
  }

  ngOnInit() {
    this.store.pipe(select(getQuestionList)).subscribe(list => {
      this.questionList = list;
    });
    this.store.pipe(select(getDialogflowQuestionList)).subscribe(list => {
      this.dialogflowQuestionList = list;
    });
    this.searchQuestion$ = this.store.pipe(select(getSearchQuestion));
    this.isAdmin$ = this.store.pipe(select(isAdmin));
  }

  ngOnDestroy() {
    this.unsubscribe$.next(true);
    this.unsubscribe$.complete();
  }

  search(input: HTMLInputElement): void {
    this.store.dispatch(loadQuestionList({data: '' + input.value}));
  }

  async import() {
    const modal = await this.modalController.create({
      component: ImportFormComponent,
      componentProps: {
        type: 'import_questions',
      }
    });
    await modal.present();
    await modal.onWillDismiss();
  }
}
