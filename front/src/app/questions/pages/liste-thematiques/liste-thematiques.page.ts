import {Component, OnInit} from '@angular/core';
import {Thematique} from '../../shared/models/thematique';
import {select, Store} from '@ngrx/store';
import {getThematiqueList} from '../../shared/store/questions.selectors';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {Tools} from '../../../core/tools';
import {ModalController} from '@ionic/angular';
import {ImportFormComponent} from '../../components/import-form/import-form.component';
import {isAdmin} from '../../../store/user/user.selectors';

@Component({
  selector: 'app-liste-thematiques',
  templateUrl: './liste-thematiques.page.html',
  styleUrls: ['./liste-thematiques.page.scss'],
})
export class ListeThematiquesPage implements OnInit {
  themeList$: Observable<Thematique[]>;
  isAdmin$: Observable<boolean>;

  constructor(private store: Store,
              private modalController: ModalController) { }

  ngOnInit() {
    this.themeList$ = this.store.pipe(select(getThematiqueList), map(list => {
      return list ? list.slice().sort(Tools.sortThematicByOrderAsc) : [];
    }));
    this.isAdmin$ = this.store.pipe(select(isAdmin));
  }

  async import() {
    const modal = await this.modalController.create({
      component: ImportFormComponent,
      componentProps: {
        type: 'import_thematics',
      }
    });
    await modal.present();
    await modal.onWillDismiss();
  }

}
