import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {ListeThematiquesPageRoutingModule} from './liste-thematiques-routing.module';

import {ListeThematiquesPage} from './liste-thematiques.page';
import {QuestionsComponentsModule} from '../../components/questions-components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListeThematiquesPageRoutingModule,
    QuestionsComponentsModule
  ],
  declarations: [ListeThematiquesPage]
})
export class ListeThematiquesPageModule {}
