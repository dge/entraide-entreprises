import {Component, Input, OnInit} from '@angular/core';
import {Question} from '../../shared/models/question';

@Component({
  selector: 'app-question-list-item',
  templateUrl: './question-list-item.component.html',
  styleUrls: ['./question-list-item.component.scss'],
})
export class QuestionListItemComponent implements OnInit {
  @Input() question: Question;
  @Input() mode: 'light' | 'full' | 'moderation' = 'light';
  @Input() className: 'odd' | 'even';
  @Input() index: number;
  @Input() showAnonyme: boolean;

  constructor() { }

  ngOnInit() {}

}
