import {Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import {Thematique} from '../../shared/models/thematique';
import {Question} from '../../shared/models/question';
import {take, takeUntil} from 'rxjs/operators';
import {QuestionsService} from '../../shared/services/questions.service';
import {User} from '../../../store/models/user';
import {likeQuestion, likeThematic} from '../../shared/store/questions.actions';
import {Store} from '@ngrx/store';
import {LoginService} from '../../../core/login/login.service';
import {FormControl} from '@angular/forms';
import {Subject} from 'rxjs';
import {CurrentUser} from '../../../store/models/current-user';

@Component({
  selector: 'app-theme-detail',
  templateUrl: './theme-detail.component.html',
  styleUrls: ['./theme-detail.component.scss'],
})
export class ThemeDetailComponent implements OnInit, OnChanges, OnDestroy {
  @Input() thematique: Thematique;
  @Input() user: CurrentUser;
  questionList: Question[];
  questionCount: number;
  page = 0;
  limit = 20;
  likeCount: number;
  liked: boolean;
  urgentControl: FormControl = new FormControl();
  faqControl: FormControl = new FormControl();
  unsubscribe$: Subject<boolean> = new Subject<boolean>();

  constructor(private questionService: QuestionsService,
              private loginService: LoginService,
              private store: Store) {
  }

  ngOnInit() {
    if (this.thematique) {
      this.questionList = [];
      this.loadQuestion();
    }
    this.urgentControl.valueChanges.pipe(takeUntil(this.unsubscribe$)).subscribe(x => {
      this.page = 0;
      this.loadQuestion();
    });
    this.faqControl.valueChanges.pipe(takeUntil(this.unsubscribe$)).subscribe(x => {
      this.page = 0;
      this.loadQuestion();
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    this.likeCount = this.thematique.followers ? this.thematique.followers.length : 0;
    this.liked = this.user && this.thematique.followers && this.thematique.followers.indexOf(this.user.id) !== -1;
  }

  ngOnDestroy() {
    this.unsubscribe$.next(true);
    this.unsubscribe$.complete();
  }

  loadQuestion(event?: any) {
    if (this.page === 0) {
      this.questionList = [];
    }
    this.questionService.getThematiqueQuestionList(this.thematique.id, this.page, this.limit,
      this.faqControl.value, this.urgentControl.value).pipe(take(1)).subscribe(resp => {
      this.questionList = this.questionList.concat(resp.data.questions);
      this.questionCount = resp.data.total;
      if (event) {
        event.target.complete();
        if (this.questionList.length === this.questionCount) {
          event.target.disabled = true;
        }
      }
    });
  }

  loadData(event: any) {
    setTimeout(() => {
      this.page++;
      this.loadQuestion(event);
    }, 500);
  }

  saveLikeThematic() {
    if (!this.user) {
      this.loginService.openLoginForm();
    } else {
      const answer = {
        id: this.thematique.id,
        userId: this.user.id
      };
      this.store.dispatch(likeThematic({data: answer}));
    }
  }
}
