import {Component, Input, OnInit} from '@angular/core';
import {Question} from '../../shared/models/question';

@Component({
  selector: 'app-question-list',
  templateUrl: './question-list.component.html',
  styleUrls: ['./question-list.component.scss'],
})
export class QuestionListComponent implements OnInit {
  @Input() questionList: Question[];
  @Input() mode: 'light' | 'full' | 'moderation' = 'light';
  @Input() emptyMessage: string;
  @Input() showAnonyme: any;

  constructor() { }

  ngOnInit() {}

}
