import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {isAuthenticated} from '../../../store/user/user.selectors';
import {take} from 'rxjs/operators';
import {Validators} from '@angular/forms';
import {Question} from '../../shared/models/question';
import {LoginService} from '../../../core/login/login.service';
import {FieldConfig} from '../../../shared/form/models/field-config';
import {saveResponse} from '../../shared/store/questions.actions';

@Component({
  selector: 'app-question-add-form',
  templateUrl: './question-add-form.component.html',
  styleUrls: ['./question-add-form.component.scss'],
})
export class QuestionAddFormComponent implements OnInit, OnChanges {
  @Input() question: Question;
  @Output() questionSave: EventEmitter<{ 'label': string; 'message': string; isUrgent: boolean }> = new EventEmitter<{ 'label': string; 'message': string; isUrgent: boolean }>();
  formConfig: FieldConfig[] = [
    {
      type: 'input',
      name: 'label',
      label: 'Posez votre question',
      className: 'poserquestion',
      validations: [
        {
          name: 'required',
          validator: Validators.required,
          message: 'Le titre est obligatoire'
        }
      ]
    },
    {
      type: 'editor',
      name: 'message',
      label: 'Précisez le contexte de la question',
      validations: [
        {
          name: 'required',
          validator: Validators.required,
          message: 'Le contexte est obligatoire'
        }
      ]
    },
    {
      type: 'checkbox',
      name: 'isUrgent',
      label: 'Urgent',
    }
  ];

  constructor(private store: Store,
              private loginService: LoginService) {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.question) {
      this.formConfig[0].value = this.question.label;
      this.formConfig[1].value = this.question.message;
      if (this.question && ((this.question.likes && this.question.likes.length > 0)
        || (this.question.answerCount > 0))) {
        this.formConfig[0].disabled = true;
      }
      this.formConfig = [...this.formConfig];
    }
  }

  save(values: any) {
    this.store.pipe(select(isAuthenticated), take(1)).subscribe((authOk) => {
      if (authOk) {
        this.questionSave.emit(values);
      } else {
        this.loginService.openLoginForm().then(x => {
          if (x.data.loggedIn) {
            setTimeout(() => {
              this.questionSave.emit(values);
            }, 500);
          }
        });
      }
    });
  }
}
