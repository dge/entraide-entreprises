import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {Question} from '../../shared/models/question';
import {Answer} from '../../shared/models/answer';
import {AddResponse} from '../../shared/models/add-response';
import {ModalController} from '@ionic/angular';
import {QuestionsService} from '../../shared/services/questions.service';
import {LoginService} from '../../../core/login/login.service';
import {Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {
    deleteAnswer,
    likeQuestion,
    likeResponse,
    loadResponseList,
    moderateAnswer,
    saveSubResponse
} from '../../shared/store/questions.actions';
import {ResponseAddFormModalComponent} from '../response-add-form-modal/response-add-form-modal.component';
import {take} from 'rxjs/operators';
import {CurrentUser} from '../../../store/models/current-user';
import {ConfirmModalComponent} from '../../../shared/modal/confirm-modal/confirm-modal.component';
import {MatDialog} from '@angular/material/dialog';
import {FormModalComponent} from '../../../shared/modal/form-modal/form-modal.component';
import {ContactUserModalComponent} from '../../../shared/modal/contact-user-modal/contact-user-modal.component';
import {SnackbarService} from '../../../core/snackbar/snackbar.service';
import {LoadingState} from '../../../core/loading-state.enum';
import {ApiResponse} from '../../../core/models/api-response';
import {countModerationAction} from '../../../store/user/user.actions';
import {FormModalData} from '../../../shared/modal/form-modal/form-modal-data';
import {Validators} from '@angular/forms';

@Component({
    selector: 'app-question-response-item',
    templateUrl: './question-response-item.component.html',
    styleUrls: ['./question-response-item.component.scss'],
})
export class QuestionResponseItemComponent implements OnInit, OnChanges {
    @Input() type: 'question' | 'answer' | 'subanswer';
    @Input() item: Question | Answer;
    @Input() user: CurrentUser;
    @Input() index: number;
    @Input() parentIndex: number;
    @Input() isQuestionAuthor: boolean;
    @Output() answerClick: EventEmitter<void> = new EventEmitter<void>();
    likeCount: number;
    liked: boolean;
    isMine: boolean;
    isAdmin: boolean;
    seeResponseZone = false;
    entityName: string;

    getLabel() {
        return this.item ? (this.item as Question).label : '';
    }

    constructor(private modalController: ModalController,
                private loginService: LoginService,
                private questionService: QuestionsService,
                private store: Store,
                private snackbarService: SnackbarService,
                private dialog: MatDialog,
                private router: Router) {
    }

    ngOnInit() {
    }

    ngOnChanges(changes: SimpleChanges) {
        if (this.item) {
            this.likeCount = this.item.likes ? this.item.likes.length : 0;
            this.liked = this.user && this.item.likes && this.item.likes.indexOf(this.user.id) !== -1;
            this.isMine = this.user && this.item.author && this.item.author.id === this.user.id;
            this.isAdmin = this.user && this.user.roles.indexOf('ADMIN') !== -1;
            const e = this.item.author && this.item.author.entities ? this.item.author.entities.filter(x => x && x.id === this.item.entity)[0] : null;
            this.entityName = e != null ? e.name : null;
        }
    }

    answer() {
        if (!this.user) {
            this.loginService.openLoginForm();
        } else if (this.type === 'question') {
            this.answerClick.emit();
        } else if (this.type === 'answer') {
            this.seeResponseZone = true;
        }
    }

    like(): void {
        if (!this.user) {
            this.loginService.openLoginForm();
        } else if (this.type === 'question') {
            this.saveLikeQuestion();
        } else {
            this.saveLikeResponse();
        }
    }

    saveLikeQuestion() {
        const answer = {
            id: this.item.id,
            userId: this.user.id
        };
        this.store.dispatch(likeQuestion({data: answer}));
    }

    saveLikeResponse() {
        const answer = {
            id: this.item.id,
            userId: this.user.id
        };
        this.store.dispatch(likeResponse({
            data: {
                parent: this.parentIndex,
                index: this.index,
                form: answer
            }
        }));
    }

    async update() {
        if (this.type === 'question') {
            this.router.navigate(['/questions/modifier', this.item.id]);
        } else {
            const modal = await this.modalController.create({
                component: ResponseAddFormModalComponent,
                componentProps: {
                    message: this.item.message,
                    type: this.type
                }
            });
            await modal.present();
            const resp = await modal.onWillDismiss();

            if (resp.data.message) {
                this.questionService.updateAnswer(this.item.id, {
                    author: this.item.author.id,
                    message: resp.data.message,
                    question: (this.item as Answer).question,
                    parent: (this.item as Answer).parent,
                    anonyme: this.item.anonyme,
                    entity: this.item.entity,
                    valid: this.user.roles.indexOf('ADMIN') !== -1
                }).pipe(take(1)).subscribe(x => this.store.dispatch(loadResponseList({data: ''})));
            }
        }
    }

    saveSubResponse($event: string): void {
        let e = this.user.entities ? this.user.entities.filter(x => x && x.actif)[0] : null;
        if (e == null) {
            e = this.user.entities ? this.user.entities[0] : null;
        }
        const answer: AddResponse = {
            author: this.user.id,
            message: $event,
            parent: this.item.id,
            anonyme: this.user.anonyme,
            entity: e != null ? e.id : null,
            valid: this.user.roles.indexOf('ADMIN') !== -1
        };
        this.store.dispatch(saveSubResponse({data: {index: this.index, answer}}));
        this.seeResponseZone = false;
    }

    cancelMessage(): void {
        this.seeResponseZone = false;
    }

    validate() {
        const dialogRef = this.dialog.open(ConfirmModalComponent, {
            width: '70%',
            data: 'Confirmez-vous la validation du message ?'
        });
        dialogRef
            .afterClosed()
            .pipe(take(1))
            .subscribe(resp => {
                if (resp) {
                    this.questionService.validateAnswer(this.item.id).pipe(take(1)).subscribe(() => {
                        this.store.dispatch(moderateAnswer({data: {index: this.index, valid: true}}));
                        this.store.dispatch(countModerationAction());
                    });
                }
            });
    }

    moderate() {
        const dialogRef = this.dialog.open(ConfirmModalComponent, {
            width: '70%',
            data: 'Confirmez-vous la suppression du message ?'
        });
        dialogRef
            .afterClosed()
            .pipe(take(1))
            .subscribe(resp => {
                if (resp) {
                    this.questionService.moderateAnswer(this.item.id).pipe(take(1)).subscribe(() => {
                        this.store.dispatch(moderateAnswer({data: {index: this.index, valid: false}}));
                        this.store.dispatch(countModerationAction());
                    });
                }
            });
    }

    alert() {
        const dialogRef = this.dialog.open(ConfirmModalComponent, {
            width: '70%',
            data: 'Confirmez-vous le signalement du message ?'
        });
        dialogRef
            .afterClosed()
            .pipe(take(1))
            .subscribe(resp => {
                if (resp) {
                    this.questionService.alertAnswer(this.item.id).pipe(take(1)).subscribe();
                    this.store.dispatch(countModerationAction());
                }
            });
    }

    delete() {
        if (this.type === 'question') {
            this.deleteQuestion();
        } else {
            this.deleteAnswer();
        }
    }

    deleteAnswer() {
        const dialogRef = this.dialog.open(ConfirmModalComponent, {
            width: '70%',
            data: 'Confirmez-vous la suppression du message ?'
        });
        dialogRef
            .afterClosed()
            .pipe(take(1))
            .subscribe(resp => {
                if (resp) {
                    this.questionService.deleteAnswser(this.item.id).pipe(take(1)).subscribe(() => {
                        this.store.dispatch(deleteAnswer({data: {index: this.index}}));
                        this.store.dispatch(countModerationAction());
                    });
                }
            });
    }

    deleteQuestion() {
        const dialogRef = this.dialog.open(ConfirmModalComponent, {
            width: '70%',
            data: 'Confirmez-vous la suppression de la question ?'
        });
        dialogRef
            .afterClosed()
            .pipe(take(1))
            .subscribe(resp => {
                if (resp) {
                    this.questionService.deleteQuestion(this.item.id).pipe(take(1)).subscribe(() => {
                        this.router.navigate(['/']);
                        this.store.dispatch(countModerationAction());
                    });
                }
            });
    }

    shareQuestion() {
        if (!this.user) {
            this.loginService.openLoginForm();
        } else {
            const dialogRef = this.dialog.open(FormModalComponent, {
                width: '70%',
                data: {
                    title: 'Partagez cette question',
                    items: [
                        {
                            type: 'input',
                            name: 'email',
                            label: 'Email',
                            inputType: 'email',
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'L\'adresse mail est obligatoire'
                                },
                                {
                                    name: 'email',
                                    validator: Validators.email,
                                    message: 'L\'adresse mail n\'est pas valide'
                                }
                            ]
                        },
                        {
                            type: 'textarea',
                            name: 'message',
                            label: 'Message'
                        }
                    ]
                } as FormModalData
            });
            dialogRef
                .afterClosed()
                .pipe(take(1))
                .subscribe(resp => {
                    if (resp) {
                        const form = {
                            question: this.item.id,
                            message: resp.message,
                            email: resp.email
                        };
                        this.questionService.sendQuestion(form).pipe(take(1)).subscribe();
                    }
                });
        }
    }

    contactUser(user: any) {
        if (!this.user) {
            this.loginService.openLoginForm();
        } else {
            const dialogRef = this.dialog.open(ContactUserModalComponent, {
                width: '70%',
                data: 'Confirmez-vous le partage de la question ?'
            });
            dialogRef
                .afterClosed()
                .pipe(take(1))
                .subscribe(resp => {
                    if (resp) {
                        const form = {
                            user: user.id,
                            message: resp.message,
                            email: resp.email,
                            name: resp.name,
                        };
                        this.questionService.contactUser(form).pipe(take(1)).subscribe((response: ApiResponse<any>) => {
                            if (response.error) {
                                this.snackbarService.openSnackBar(LoadingState.ERROR, {
                                    titre: 'Demande de contact',
                                    message: response.data.message
                                });
                            } else {
                                this.snackbarService.openSnackBar(LoadingState.ERROR, {
                                    titre: 'Demande de contact',
                                    message: 'Demande envoyée'
                                });
                            }
                        });
                    }
                });
        }
    }
}
