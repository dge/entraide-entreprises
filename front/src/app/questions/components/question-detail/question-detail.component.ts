import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Question} from '../../shared/models/question';
import {Answer} from '../../shared/models/answer';
import {QuestionsService} from '../../shared/services/questions.service';
import {take} from 'rxjs/operators';
import {Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {validateQuestion} from '../../shared/store/questions.actions';
import {CurrentUser} from '../../../store/models/current-user';
import {countModerationAction} from '../../../store/user/user.actions';

@Component({
    selector: 'app-question-detail',
    templateUrl: './question-detail.component.html',
    styleUrls: ['./question-detail.component.scss'],
})
export class QuestionDetailComponent implements OnInit {
    @Input() question: Question;
    @Input() responseList: Answer[];
    @Input() user: CurrentUser;
    @Output() answerClick: EventEmitter<void> = new EventEmitter<void>();
    liked: boolean;
    isMine: boolean;
    isAdmin: boolean;

    get nbReponse() {
        return this.responseList ? this.responseList.filter(x => x.valid).length : 0;
    }

    constructor(private questionService: QuestionsService,
                private store: Store,
                private router: Router) {
    }

    ngOnInit() {
        this.isAdmin = this.user && this.user.roles.indexOf('ADMIN') !== -1;
    }

    answerQuestion() {
        this.answerClick.emit();
    }

    validate() {
        this.questionService.validateQuestion(this.question.id).pipe(take(1)).subscribe(() => {
            this.store.dispatch(validateQuestion());
            this.store.dispatch(countModerationAction());
        });
    }

    moderate() {
        this.questionService.deleteQuestion(this.question.id).pipe(take(1)).subscribe(() => {
            this.router.navigate(['/questions/moderate-questions']);
            this.store.dispatch(countModerationAction());
        });
    }
}
