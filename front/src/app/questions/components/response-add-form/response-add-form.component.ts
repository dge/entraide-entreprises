import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {FieldEditorConfig} from '../../../shared/form/models/field-editor-config';

@Component({
  selector: 'app-response-add-form',
  templateUrl: './response-add-form.component.html',
  styleUrls: ['./response-add-form.component.scss'],
})
export class ResponseAddFormComponent implements OnInit {
  @Output() answerSave: EventEmitter<string> = new EventEmitter<string>();
  @Input() message: string;
  @Input() submitLabel: string;
  form: FormGroup;
  field: FieldEditorConfig = {
    type: 'editor',
    name: 'editor',
    config: {
      placeholder: 'Répondre'
    }
  };

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    this.form = this.fb.group({
      editor: [this.message, Validators.required],
    });
  }

  save() {
    this.answerSave.emit(this.form.value.editor);
  }
}
