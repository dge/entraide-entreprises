import {Component, Input, OnInit} from '@angular/core';
import {Thematique} from '../../shared/models/thematique';
import {select, Store} from '@ngrx/store';
import {take} from 'rxjs/operators';
import {getThematiqueList} from '../../shared/store/questions.selectors';
import {ModalController} from '@ionic/angular';
import {FormControl} from '@angular/forms';
import {Tools} from '../../../core/tools';

@Component({
  selector: 'app-select-thematic',
  templateUrl: './select-thematic.component.html',
  styleUrls: ['./select-thematic.component.scss'],
})
export class SelectThematicComponent implements OnInit {
  @Input() thematicList: Thematique[];
  otherThematicList: Thematique[];
  defaultThematic: Thematique;
  formControl: FormControl;

  constructor(private store: Store,
              public modalController: ModalController) {
  }

  ngOnInit() {
    this.formControl = new FormControl();
    this.store.pipe(select(getThematiqueList), take(1)).subscribe(list => {
      const ids = this.thematicList.map(x => x.id);
      this.otherThematicList = list && list.filter(x => ids.indexOf(x.id) === -1).sort(Tools.sortThematicByLabelAsc);
      this.defaultThematic = list.filter(x => x.default)[0] ? list.filter(x => x.default)[0] : list[0];
      if (ids.length > 0) {
        this.formControl.setValue(ids[0]);
      }
    });
  }

  save() {
    let value = this.formControl.value;
    if (value === 'NONE') {
      value = this.defaultThematic.id;
    }
    this.modalController.dismiss(value);
  }

  dismissModal() {
    this.modalController.dismiss(null);
  }
}
