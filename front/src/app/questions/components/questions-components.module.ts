import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ThemeListComponent} from './theme-list/theme-list.component';
import {ThemeListItemComponent} from './theme-list-item/theme-list-item.component';
import {QuestionListComponent} from './question-list/question-list.component';
import {QuestionListItemComponent} from './question-list-item/question-list-item.component';
import {SharedModule} from '../../shared/shared.module';
import {RouterModule} from '@angular/router';
import {ThemeDetailComponent} from './theme-detail/theme-detail.component';
import {QuestionDetailComponent} from './question-detail/question-detail.component';
import {SelectThematicComponent} from './select-thematic/select-thematic.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FooterAskQuestionComponent} from './footer-ask-question/footer-ask-question.component';
import {QuestionAddFormComponent} from './question-add-form/question-add-form.component';
import {ResponseAddFormModalComponent} from './response-add-form-modal/response-add-form-modal.component';
import {ResponseAddFormComponent} from './response-add-form/response-add-form.component';
import {QuestionResponseItemComponent} from './question-response-item/question-response-item.component';
import {FooterAnswerQuestionComponent} from './footer-answer-question/footer-answer-question.component';
import {ImportFormComponent} from './import-form/import-form.component';
import {AnswerListComponent} from './answer-list/answer-list.component';
import {AnswerListItemComponent} from './answer-list-item/answer-list-item.component';


@NgModule({
    declarations: [
        ThemeListComponent,
        ThemeListItemComponent,
        QuestionListComponent,
        QuestionListItemComponent,
        ThemeDetailComponent,
        QuestionDetailComponent,
        SelectThematicComponent,
        QuestionAddFormComponent,
        FooterAskQuestionComponent,
        ResponseAddFormModalComponent,
        ResponseAddFormComponent,
        QuestionResponseItemComponent,
        FooterAnswerQuestionComponent,
        ImportFormComponent,
        AnswerListComponent,
        AnswerListItemComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        RouterModule,
        ReactiveFormsModule,
        FormsModule
    ],
    exports: [
        ThemeListComponent,
        ThemeListItemComponent,
        QuestionListComponent,
        QuestionListItemComponent,
        ThemeDetailComponent,
        QuestionDetailComponent,
        SelectThematicComponent,
        FooterAskQuestionComponent,
        QuestionAddFormComponent,
        FooterAnswerQuestionComponent,
        ImportFormComponent,
        SharedModule,
        AnswerListComponent
    ]
})
export class QuestionsComponentsModule {
}
