import {Component, Input, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';

@Component({
  selector: 'app-response-add-form-modal',
  templateUrl: './response-add-form-modal.component.html',
  styleUrls: ['./response-add-form-modal.component.scss'],
})
export class ResponseAddFormModalComponent implements OnInit {

  @Input() message: string;
  @Input() type: 'answer' | 'subanswer';
  constructor(private modalController: ModalController) { }

  ngOnInit() {}

  dismissModal(value: string): void {
    this.modalController.dismiss({
      message: value
    });
  }

}
