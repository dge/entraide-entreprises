import {Component, Input, OnInit} from '@angular/core';
import {Thematique} from '../../shared/models/thematique';

@Component({
  selector: 'app-theme-list',
  templateUrl: './theme-list.component.html',
  styleUrls: ['./theme-list.component.scss'],
})
export class ThemeListComponent implements OnInit {
  @Input() themeList: Thematique[];

  constructor() { }

  ngOnInit() {}

}
