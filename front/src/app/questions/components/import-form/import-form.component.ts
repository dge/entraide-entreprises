import {Component, Input, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {QuestionsService} from '../../shared/services/questions.service';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-import-form',
  templateUrl: './import-form.component.html',
  styleUrls: ['./import-form.component.scss'],
})
export class ImportFormComponent implements OnInit {
  @Input() type: 'import_questions' | 'import_thematics';
  text: any;
  placeholderQuestion = `
  [
    {
      "thematic": "test",
      "title": "test",
      "message": "test",
      "answer": "test"
    }
  ]`;
  placeholderThematic = `
  [
    {
        "label": "Gestion, protection et financement de l’entreprise",
        "icon": "icon-questions",
        "key": "thematics.gestion"
    }
  ]`;

  constructor(private modalController: ModalController,
              private questionService: QuestionsService) {
  }

  ngOnInit() {
  }

  dismissModal(b: boolean) {
    this.modalController.dismiss(null);
  }

  save() {
    try {
      const json = JSON.parse(this.text);
      if (this.type === 'import_questions') {
        this.questionService.importQuestions(json).pipe(take(1)).subscribe();
      } else {
        this.questionService.importThematics(json).pipe(take(1)).subscribe();
      }
    } catch (e) {
      console.error(e);
    }
  }
}
