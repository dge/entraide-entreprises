import {Component, Input, OnInit} from '@angular/core';
import {Answer} from '../../shared/models/answer';

@Component({
  selector: 'app-answer-list',
  templateUrl: './answer-list.component.html',
  styleUrls: ['./answer-list.component.scss'],
})
export class AnswerListComponent implements OnInit {

  @Input() answerList: Answer[];
  constructor() { }

  ngOnInit() {}

}
