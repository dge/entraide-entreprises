import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {IonTextarea} from '@ionic/angular';
import {FieldEditorConfig} from '../../../shared/form/models/field-editor-config';

@Component({
  selector: 'app-footer-answer-question',
  templateUrl: './footer-answer-question.component.html',
  styleUrls: ['./footer-answer-question.component.scss'],
})
export class FooterAnswerQuestionComponent implements OnInit {

  @Input() submitLabel = 'Répondre';
  @Input() seeButtons: boolean;
  @Output() cancel: EventEmitter<void> = new EventEmitter<void>();
  @Output() save: EventEmitter<string> = new EventEmitter<string>();
  @ViewChild('textarea') textarea: IonTextarea;
  focus: boolean;
  showEditor = false;
  control: FormControl = new FormControl('', Validators.required);
  formGroup: FormGroup;
  field: FieldEditorConfig = {
    type: 'editor',
    name: 'editor',
    config: {
      placeholder: 'Répondre'
    }
  };


  constructor(private fb: FormBuilder) {
    this.formGroup = fb.group({
      editor: this.control
    });
  }

  ngOnInit() {
    this.field.config.placeholder = this.submitLabel;
  }

  onBlur(): void {
    setTimeout(() => {
      this.focus = false;
    }, 0);
  }

  onFocus(): void {
    this.focus = true;
  }

  setFocus(): void {
    this.focus = true;
    this.showEditor = true;
    this.textarea.setFocus();
    setTimeout(() => {
    }, 500);
  }

  saveMessage(): void {
    this.save.emit(this.control.value);
    this.control.setValue(null);
    this.showEditor = false;
  }

  cancelMessage() {
    this.cancel.emit();
    this.control.setValue(null);
    this.showEditor = false;
  }
}
