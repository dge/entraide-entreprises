import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {Thematique} from '../../shared/models/thematique';

@Component({
  selector: 'app-theme-list-item',
  templateUrl: './theme-list-item.component.html',
  styleUrls: ['./theme-list-item.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ThemeListItemComponent implements OnInit {
  @Input() theme: Thematique;

  constructor() {
  }

  ngOnInit() {
  }

}
