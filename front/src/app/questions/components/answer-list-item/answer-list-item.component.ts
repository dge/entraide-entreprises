import {Component, Input, OnInit} from '@angular/core';
import {Answer} from '../../shared/models/answer';

@Component({
  selector: 'app-answer-list-item',
  templateUrl: './answer-list-item.component.html',
  styleUrls: ['./answer-list-item.component.scss'],
})
export class AnswerListItemComponent implements OnInit {

  @Input() answer: Answer;
  @Input() index: number;
  constructor() { }

  ngOnInit() {}

}
