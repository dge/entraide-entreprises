import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {QuestionsRoutingModule} from './questions-routing.module';
import {SharedModule} from '../shared/shared.module';
import { StoreModule } from '@ngrx/store';
import * as fromQuestions from './shared/store';
import { EffectsModule } from '@ngrx/effects';
import { QuestionsEffects } from './shared/store/questions.effects';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    QuestionsRoutingModule,
    SharedModule,
    StoreModule.forFeature(fromQuestions.questionsFeatureKey, fromQuestions.reducer, { metaReducers: fromQuestions.metaReducers }),
    EffectsModule.forFeature([QuestionsEffects])
  ]
})
export class QuestionsModule {
}
