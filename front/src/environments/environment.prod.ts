export const environment = {
  production: true,
  apiUrl: '/api',
  useDialogflow: true,
  timerNotification: 5 * 60000,
  appVersion: require('../../package.json').version
};
