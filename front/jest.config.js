const { pathsToModuleNameMapper } = require('ts-jest/utils');
const { compilerOptions } = require('./tsconfig');

module.exports = {
  preset: 'jest-preset-angular',
  roots: ['<rootDir>/src/'],
  testMatch: ['**/+(*.)+(spec).+(ts)'],
  setupFilesAfterEnv: ['jest-extended', '<rootDir>/src/test.ts'],
  verbose: true,
  collectCoverage: true,
  coverageReporters: ['json', 'text', 'clover', 'html'],
  coverageDirectory: 'coverage/entraide-entreprise',
  moduleNameMapper: pathsToModuleNameMapper(compilerOptions.paths || {}, {
    prefix: '<rootDir>/'
  })
};
