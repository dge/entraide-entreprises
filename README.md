## Présentation générale du système 
Entraide Entreprises est une plateforme numérique, en construction, qui permet aux dirigeants, prioritairement de TPE, de poser leurs questions, de façon anonyme ou publique selon leur choix, et d’obtenir des réponses de pairs et d’experts classées par vote des utilisateurs. Les questions et réponses sont accessibles à tous les utilisateurs de la plateforme, dans une logique totalement ouverte.
Entraide Entreprise est une application web progressive.
## Finalité du système
L’outil Entraide Entreprise doit :
1. Permettre aux dirigeants d’entreprise d’obtenir des réponses à leur question
2. Permettre aux différents acteurs d’échanger plus facilement
3. Permettre de suivre des contributeurs ou des thématiques précises