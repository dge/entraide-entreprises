import { Document } from "mongoose";

export interface IContact extends Document {
  from: any;
  to: any;
  createdAt: Date
}
