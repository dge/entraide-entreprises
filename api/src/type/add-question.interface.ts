import {Document} from "mongoose";

export interface IAddQuestion extends Document {
  thematic: string;
  title: string;
  message: string;
  answer: string;
}
