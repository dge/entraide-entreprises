import {Document} from "mongoose";

export interface IHtmlPage extends Document {
    code: string;
    titre: string;
    contenu: string;
    createdAt: Date;
}
