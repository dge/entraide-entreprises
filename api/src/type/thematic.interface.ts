import { Document } from "mongoose";
import {IUser} from './user.interface';

export interface IThematic extends Document {
  label: string;
  icon: string;
  key: string;
  dialogflowName?: string;
  followers?: (string) [];
  default?: boolean;
  active?: boolean;
  createdAt?: Date;
  updatedAt?: Date;
  order?: number;
}
