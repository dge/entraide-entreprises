import { Document } from "mongoose";
import {IThematic} from './thematic.interface';

export interface ISecretCode extends Document {
  user: string;
  token: string;
  type: string;
  createdAt: Date;
}
