import {Document} from "mongoose";

export interface IAnswer extends Document {
  message: string | null;
  author: any;
  parent: any;
  answers: IAnswer[];
  question: any;
  likes: string[];
  anonyme: boolean;
  valid: boolean;
  moderate: boolean;
  alert: boolean;
  ip: string;
}
