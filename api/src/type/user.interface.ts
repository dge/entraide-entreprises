import {Document} from "mongoose";

export interface IUser extends Document {
  username: string;
  email: string;
  password: string;
  roles: string[];
  isVerified: boolean;
  isCompleted: boolean;
  anonyme: boolean;
  allowContact: boolean;
  followers: any[];
  firstName: string;
  lastName: string;
  dateOfBirth?: Date;
  profilePicture?: string;
  businessCreation?: boolean;
  entity: any;
  entities?: {
    id: string;
    name: string;
    siret: string;
    url: string;
    address: string;
    addressBis: string;
    codePostal: string;
    city: string;
    actif: boolean;
  }[];
  job?: string;
  experiences?: {
    name: string;
    startDate: string;
    endDate: string;
    url: string;
    description: string;
  }[];
  training?: {
    name: string;
    startDate: string;
    endDate: string;
    url: string;
    description: string;
  }[];
  networks?: {
    facebook: string;
    twitter: string;
    instagram: string;
    linkedin: string;
  }
  notification: {
    myquestion_answer_appli: boolean;
    myquestion_answer_email: boolean;
    myanswer_comment_appli: boolean;
    myanswer_comment_email: boolean;
    thematic_createquestion_appli: boolean;
    thematic_answerquestion_appli: boolean;
    thematic_createquestion_email: boolean;
    thematic_answerquestion_email: boolean;
    user_createquestion_appli: boolean;
    user_answerquestion_appli: boolean;
    user_createquestion_email: boolean;
    user_answerquestion_email: boolean;
  }
}
