import {Document} from "mongoose";

export interface IContactUser extends Document {
  user: string;
  message: string;
  name: string;
  email: string;
}
