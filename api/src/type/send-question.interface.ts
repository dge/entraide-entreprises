import {Document} from "mongoose";

export interface ISendQuestion extends Document {
  question: string;
  email: string;
  message: string;
}
