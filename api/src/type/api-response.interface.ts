export class ApiResponseInterface {
  error: boolean;
  statusCode: number;
  data: any;
  errors: any;

  constructor(error: boolean, statusCode: number, data: any, errors?: any) {
    this.error = error;
    this.statusCode = statusCode;
    this.data = data;
    this.errors = errors
  }
}
