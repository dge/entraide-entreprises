import { Document } from "mongoose";

export interface IQuestion extends Document {
  label: string;
  message: string;
  author: any;
  thematic: any;
  likes: string[];
  isFaq: boolean;
  isUrgent: boolean;
  anonyme: boolean;
  valid: boolean;
  ip: string;
}
