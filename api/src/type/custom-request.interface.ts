import { Request } from "express"
import {IUser} from './user.interface';
// @ts-ignore
export interface ICustomRequest extends Request {
  user: IUser
}
