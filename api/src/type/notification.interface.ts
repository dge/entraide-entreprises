import { Document } from "mongoose";
import {IThematic} from './thematic.interface';

export interface INotification extends Document {
  user: string;
  type: string;
  message: string;
  question: string;
}
