import {QuestionService} from '../service/question.service';
import {inject} from 'inversify';
import {Controller} from './controller';
import {controller, httpGet, httpPost, interfaces, queryParam, request, response} from 'inversify-express-utils';
import {Request, Response} from 'express';
import {Logger} from 'tslog';
import {IAddQuestion} from '../type/add-question.interface';
import {authMiddlewareFactory} from '../common/auth-middleware';
import FileMiddleware from '../common/file-middleware';
import {ICustomRequest} from '../type/custom-request.interface';
import {ISendQuestion} from '../type/send-question.interface';
import fs from 'fs';
import Path from 'path';

@controller("/api/question")
export class QuestionController extends Controller implements interfaces.Controller {

    constructor(@inject("QuestionService") protected service: QuestionService,
                @inject("Logger") protected log: Logger) {
        super(service, log);
    }

    @httpGet('/init')
    async init(@request() req: Request, @response() res: Response) {
        this.log.debug('init');
        return res.status(200).send(await this.service.init());
    }

    @httpGet('/thematic/:id')
    async getAllByThematics(@request() req: Request, @response() res: Response,
                            @queryParam("page") page: string,
                            @queryParam("limit") limit: string,
                            @queryParam("faq") faq: boolean,
                            @queryParam("urgent") urgent: boolean) {
        const {id} = req.params;
        return res.status(200).send(await this.service.getAllByThematics(id, parseInt(page, 10), parseInt(limit, 10), faq, urgent));
    }

    @httpGet('/favorite')
    async getFavoriteQuestions(@request() req: Request, @response() res: Response) {
        return res.status(200).send(await this.service.getFavoriteQuestions());
    }

    @httpPost('/likeQuestion')
    async likeQuestion(@request() req: Request, @response() res: Response) {
        this.log.debug('like');
        const result = await this.service.likeQuestion(req.body);
        if (result.error) {
            return res.status(result.statusCode).send(result);
        }
        return res.status(201).send(result);
    }

    @httpPost('/similar')
    async findSimilarQuestions(@request() req: Request, @response() res: Response) {
        return res.status(200).send(await this.service.findSimilarQuestions(req.body.question));
    }

    @httpGet('/askedQuestions/:id', authMiddlewareFactory)
    async askedQuestions(@request() req: ICustomRequest, @response() res: Response) {
        const {id} = req.params;
        const user = req.user;
        return res.status(200).send(await this.service.askedQuestions(id, user.username));
    }

    @httpGet('/answeredQuestions/:id', authMiddlewareFactory)
    async answeredQuestions(@request() req: ICustomRequest, @response() res: Response) {
        const {id} = req.params;
        const user = req.user;
        return res.status(200).send(await this.service.answeredQuestions(id, user.username));
    }

    @httpPost('/insertQuestionList', authMiddlewareFactory)
    async insertListQuestion(@request() req: ICustomRequest, @response() res: Response) {
        const user = req.user;
        const result = await this.service.insertListQuestion(req.body as IAddQuestion[], user.username);
        if (result.error) {
            return res.status(result.statusCode).send(result);
        }
        return res.status(200).send(result);
    }

    @httpGet('/notValidQuestions', authMiddlewareFactory)
    async notValidQuestions(@request() req: ICustomRequest, @response() res: Response,
                            @queryParam("page") page: string,
                            @queryParam("limit") limit: string) {
        return res.status(200).send(await this.service.notValidQuestions(parseInt(page, 10), parseInt(limit, 10)));
    }

    @httpGet('/validate/:id', authMiddlewareFactory)
    async validate(@request() req: ICustomRequest, @response() res: Response) {
        const {id} = req.params;
        return res.status(200).send(await this.service.validate(id));
    }

    @httpPost('/send', authMiddlewareFactory)
    async send(@request() req: ICustomRequest, @response() res: Response) {
        const user = req.user;
        const {id} = req.params;
        return res.status(200).send(await this.service.sendQuestion(req.body as ISendQuestion, user.username));
    }

    @httpPost('/upload-file', authMiddlewareFactory, FileMiddleware.memoryLoader.single('file'))
    async importFile(@request() req: any, @response() res: Response) {
        const user = req.user;
        return res.status(200).send(await this.service.importFile(req.file, user.username));
    }

    @httpGet('/count-moderation-action')
    async countModerationAction(@request() req: any, @response() res: Response) {
        return res.status(200).send(await this.service.countModerationAction());
    }

    @httpGet('/download-import-file')
    async downloadImportFile(@request() req: any, @response() res: Response) {
        const fileName = process.env.IMPORT_QUESTION_FILENAME || '';
        const fileURL = process.env.IMPORT_QUESTION_DIRECTORY || '';
        const buff = fs.readFileSync(Path.join(fileURL,fileName));
        const base64data = buff.toString('base64');
        return res.status(200).send({
            name: fileName,
            type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            content: base64data
        });
    }

}
