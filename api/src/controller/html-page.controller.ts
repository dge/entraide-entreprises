import {controller, httpGet, interfaces, request, response} from "inversify-express-utils";
import {inject} from "inversify";
import {Controller} from './controller';
import {Request, Response} from 'express';
import {Logger} from 'tslog';
import {HtmlPageService} from '../service/html-page.service';


@controller("/api/html-pages")
export class HtmlPageController extends Controller implements interfaces.Controller {

    constructor(@inject("HtmlPageService") protected service: HtmlPageService,
                @inject("Logger") protected log: Logger) {
        super(service, log);
    }

    @httpGet('/findbycode/:code')
    async getAllByQuestion(@request() req: Request, @response() res: Response) {
        const {code} = req.params;
        return res.status(200).send(await this.service.findByCode(code));
    }

}
