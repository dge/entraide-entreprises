import {
  controller,
  httpGet,
  httpPost,
  httpPut,
  interfaces,
  queryParam,
  request,
  response
} from "inversify-express-utils";
import {inject} from "inversify";
import {Controller} from './controller';
import {UserService} from '../service/user.service';
import {Request, Response} from 'express';
import bcrypt from 'bcryptjs';
import {Logger} from 'tslog';
import {ICustomRequest} from '../type/custom-request.interface';
import {authMiddlewareFactory} from '../common/auth-middleware';
import {IContactUser} from '../type/contact-user.interface';


@controller("/api/user")
export class UserController extends Controller implements interfaces.Controller {

  constructor(@inject("UserService") protected service: UserService,
              @inject("Logger") protected log: Logger) {
    super(service, log);
  }

  @httpPost('/checkDuplicateUser')
  async checkDuplicateUser(@request() req: Request, @response() res: Response) {
    return res.status(200).send(await this.service.checkDuplicateUser(req.body.usernameOrEmail));
  }

  @httpPost('/signup')
  async insert(@request() req: Request, @response() res: Response) {
    this.log.debug(req.body);
    const user = req.body;
    user.password = bcrypt.hashSync(user.password, 8);
    const result = await this.service.insert(user);
    if (result.error) {
      return res.status(result.statusCode).send(result);
    }
    return res.status(201).send(result);
  }

  @httpPost('/signin')
  async signin(@request() req: Request, @response() res: Response) {
    this.log.debug(req.body);
    const result = await this.service.signin(req.body.username, req.body.password);
    if (result.error) {
      return res.status(result.statusCode).send(result);
    }
    return res.status(201).send(result);
  }

  @httpPost('/confirmation')
  async confirmation(@request() req: Request, @response() res: Response) {
    const result = await this.service.confirmation(req.body.userId, req.body.token);
    if (result.error) {
      return res.status(result.statusCode).send(result);
    }
    return res.status(200).send(result);
  }

  @httpPost('/verify-reset-link')
  async verifyResetLink(@request() req: Request, @response() res: Response) {
    const result = await this.service.verifyResetLink(req.body.userId, req.body.token);
    if (result.error) {
      return res.status(result.statusCode).send(result);
    }
    return res.status(200).send(result);
  }

  @httpPost('/resendVerificationEmail')
  async resendVerificationEmail(@request() req: Request, @response() res: Response) {
    const result = await this.service.resendVerificationEmail(req.body.email);
    if (result.error) {
      return res.status(result.statusCode).send(result);
    }
    return res.status(200).send(result);
  }

  @httpPost('/askResetPassword')
  async askResetPassword(@request() req: Request, @response() res: Response) {
    const result = await this.service.askResetPassword(req.body.email);
    if (result.error) {
      return res.status(result.statusCode).send(result);
    }
    return res.status(200).send(result);
  }

  @httpPost('/resetPassword')
  async resetPassword(@request() req: Request, @response() res: Response) {
    const password = bcrypt.hashSync(req.body.password, 8);
    const result = await this.service.resetPassword(req.body.userId, password, req.body.token);
    if (result.error) {
      return res.status(result.statusCode).send(result);
    }
    return res.status(200).send(result);
  }

  @httpGet('/me', authMiddlewareFactory)
  async getMe(@request() req: ICustomRequest, @response() res: Response) {
    const user = req.user;
    res.status(200).send(await this.service.getMe(user.username));
  }

  @httpPut('/me', authMiddlewareFactory)
  async updateMe(@request() req: ICustomRequest, @response() res: Response) {
    const user = req.body;
    res.status(200).send(await this.service.update(user.id, user));
  }

  @httpPost('/changeMode', authMiddlewareFactory)
  async changeMode(@request() req: ICustomRequest, @response() res: Response) {
    const user = req.user;
    res.status(200).send(await this.service.changeMode(user.username));
  }

  @httpPost('/changeContact', authMiddlewareFactory)
  async changeContact(@request() req: ICustomRequest, @response() res: Response) {
    const user = req.user;
    res.status(200).send(await this.service.changeContact(user.username));
  }

  @httpPost('/likeUser')
  async likeUser(@request() req: Request, @response() res: Response) {
    const result = await this.service.likeUser(req.body);
    if (result.error) {
      return res.status(result.statusCode).send(result);
    }
    return res.status(201).send(result);
  }

  @httpGet('/followed/:username')
  async followedUsers(@request() req: Request, @response() res: Response) {
    const {username} = req.params;
    return res.status(200).send(await this.service.followedUsers(username));
  }

  @httpGet('/all')
  async getAllByThematics(@request() req: Request, @response() res: Response,
                          @queryParam("page") page: string,
                          @queryParam("limit") limit: string,
                          @queryParam("search") search: string) {
    this.log.debug(req.query.search);
    this.log.debug(req.query.search, page, limit);
    return res.status(200).send(await this.service.getAllUser('' + req.query.search, parseInt(page, 10), parseInt(limit, 10)));
  }

  @httpPut('/completeInscription/:id')
  async completeInscription(@request() req: Request, @response() res: Response) {
    const {id} = req.params;
    const result = await this.service.completeInscription(id, req.body);
    if (result.error) {
      return res.status(result.statusCode).send(result);
    }
    return res.status(201).send(result);
  }

  @httpGet('/changeEntity/:entityId', authMiddlewareFactory)
  async changeEntity(@request() req: ICustomRequest, @response() res: Response) {
    const {entityId} = req.params;
    const user = req.user;
    return res.status(200).send(await this.service.changeEntity(user.username, entityId));
  }

  @httpGet('/deleteAccount', authMiddlewareFactory)
  async deleteAccount(@request() req: ICustomRequest, @response() res: Response) {
    const user = req.user;
    return res.status(200).send(await this.service.deleteAccount(user.username));
  }

  @httpPost('/contact', authMiddlewareFactory)
  async send(@request() req: ICustomRequest, @response() res: Response) {
    const user = req.user;
    return res.status(200).send(await this.service.contactUser(req.body as IContactUser, user.username));
  }


}
