import {httpDelete, httpGet, httpPost, httpPut, interfaces, request, response} from 'inversify-express-utils';
import {Request, Response} from 'express';
import {Service} from '../service/service';
import {injectable} from 'inversify';
import {Logger} from 'tslog';


@injectable()
export class Controller {

  constructor(protected service: Service, protected log: Logger) {
  }

  @httpGet('')
  async getAll(@request() req: Request, @response() res: Response) {
    this.log.debug('getAll');
    return res.status(200).send(await this.service.getAll(req.query));
  }

  @httpGet('/item/:id')
  async getSpecific(@request() req: Request, @response() res: Response) {
    const {id} = req.params;
    this.log.debug('getSpecific : ' + id);
    return res.status(res.statusCode).send(await this.service.getSpecific(id));
  }

  @httpPost('')
  async insert(@request() req: Request, @response() res: Response) {
    this.log.debug('insert : ', req.body);
    const header = req.header('x-ip') || ' ';
    const ip = header.split(' ')[1];
    this.log.debug('ip : ', ip);
    const result = await this.service.insert(req.body, ip);
    if (result.error) {
      return res.status(result.statusCode).send(result)
    }
    return res.status(201).send(result);
  }

  @httpPut('/item/:id')
  async update(@request() req: Request, @response() res: Response) {
    const {id} = req.params;
    this.log.debug('update : ' + id, req.body);
    const result = await this.service.update(id, req.body);
    return res.status(result.statusCode).send(result);
  }

  @httpDelete('/item/:id')
  async delete(@request() req: Request, @response() res: Response) {
    const {id} = req.params;
    this.log.debug('delete : ' + id);
    const result = await this.service.delete(id);
    return res.status(result.statusCode).send(result);
  }
}
