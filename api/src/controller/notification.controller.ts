import {controller, httpDelete, httpGet, interfaces, request, response} from "inversify-express-utils";
import {inject} from "inversify";
import {Logger} from 'tslog';
import {NotificationService} from '../service/notification.service';
import {Response} from 'express';
import {authMiddlewareFactory} from '../common/auth-middleware';
import {ICustomRequest} from '../type/custom-request.interface';


@controller("/api/notifications")
export class NotificationController implements interfaces.Controller {

  constructor(@inject("NotificationService") protected service: NotificationService,
              @inject("Logger") protected log: Logger) {
  }

  @httpGet('', authMiddlewareFactory)
  async getAll(@request() req: ICustomRequest, @response() res: Response) {
    this.log.debug('getAll');
    const user = req.user;
    return res.status(200).send(await this.service.getAll(user.username));
  }

  @httpDelete('', authMiddlewareFactory)
  async deleteAll(@request() req: ICustomRequest, @response() res: Response) {
    this.log.debug('deleteAll');
    const user = req.user;
    return res.status(200).send(await this.service.deleteAll(user.username));
  }

  @httpDelete('/:id', authMiddlewareFactory)
  async delete(@request() req: ICustomRequest, @response() res: Response) {
    this.log.debug('delete');
    const {id} = req.params;
    return res.status(200).send(await this.service.delete(id));
  }


}
