import {controller, httpGet, httpPost, interfaces, queryParam, request, response} from "inversify-express-utils";
import {inject} from "inversify";
import {Controller} from './controller';
import {AnswerService} from '../service/answer.service';
import {Request, Response} from 'express';
import {Logger} from 'tslog';
import {authMiddlewareFactory} from '../common/auth-middleware';
import {ICustomRequest} from '../type/custom-request.interface';


@controller("/api/answer")
export class AnswerController extends Controller implements interfaces.Controller {

  constructor(@inject("AnswerService") protected service: AnswerService,
              @inject("Logger") protected log: Logger) {
    super(service, log);
  }

  @httpGet('/question/:id')
  async getAllByQuestion(@request() req: Request, @response() res: Response) {
    const {id} = req.params;
    return res.status(200).send(await this.service.getAllByQuestion(id));
  }

  @httpPost('/likeAnswer')
  async likeAnswer(@request() req: Request, @response() res: Response) {
    const result = await this.service.likeAnswer(req.body);
    if (result.error) {
      return res.status(result.statusCode).send(result);
    }
    return res.status(201).send(result);
  }

  @httpGet('/notValidAnswers', authMiddlewareFactory)
  async notValidAnswers(@request() req: ICustomRequest, @response() res: Response,
                        @queryParam("page") page: string,
                        @queryParam("limit") limit: string) {
    return res.status(200).send(await this.service.notValidAnswers(parseInt(page, 10), parseInt(limit, 10)));
  }

  @httpGet('/alertAnswers', authMiddlewareFactory)
  async alertAnswers(@request() req: ICustomRequest, @response() res: Response,
                        @queryParam("page") page: string,
                        @queryParam("limit") limit: string) {
    return res.status(200).send(await this.service.alertAnswers(parseInt(page, 10), parseInt(limit, 10)));
  }

  @httpGet('/moderate/:id', authMiddlewareFactory)
  async moderate(@request() req: ICustomRequest, @response() res: Response) {
    const {id} = req.params;
    return res.status(200).send(await this.service.moderate(id));
  }

  @httpGet('/validate/:id', authMiddlewareFactory)
  async validate(@request() req: ICustomRequest, @response() res: Response) {
    const {id} = req.params;
    return res.status(200).send(await this.service.validate(id));
  }

  @httpGet('/alert/:id', authMiddlewareFactory)
  async alert(@request() req: ICustomRequest, @response() res: Response) {
    const {id} = req.params;
    return res.status(200).send(await this.service.alert(id));
  }

  @httpGet('/delete/:id', authMiddlewareFactory)
  async delete(@request() req: ICustomRequest, @response() res: Response) {
    const {id} = req.params;
    const user = req.user;
    return res.status(200).send(await this.service.deleteMyAnswer(id, user.username));
  }

}
