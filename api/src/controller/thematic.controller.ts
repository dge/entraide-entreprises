import {ThematicService} from '../service/thematic.service';
import {controller, httpGet, httpPost, interfaces, request, response} from "inversify-express-utils";
import {inject} from "inversify";
import {Controller} from './controller';
import {Logger} from 'tslog';
import {Request, Response} from 'express';
import {authMiddlewareFactory} from '../common/auth-middleware';
import {ICustomRequest} from '../type/custom-request.interface';


@controller("/api/thematic")
export class ThematicController extends Controller implements interfaces.Controller {

  constructor(@inject("ThematicService") protected service: ThematicService,
              @inject("Logger") protected log: Logger) {
    super(service, log);
  }

  @httpGet('/actives')
  async getAllActive(@request() req: Request, @response() res: Response) {
    const result = await this.service.getAllActive();
    if (result.error) {
      return res.status(result.statusCode).send(result);
    }
    return res.status(200).send(result);
  }

  @httpPost('/findQuestionThematic')
  async findNewQuestionThematic(@request() req: Request, @response() res: Response) {
    const result = await this.service.findNewQuestionThematic(req.body.question);
    if (result.error) {
      return res.status(result.statusCode).send(result);
    }
    return res.status(200).send(result);
  }

  @httpPost('/insertThematicList', authMiddlewareFactory)
  async insertThematicList(@request() req: ICustomRequest, @response() res: Response) {
    const user = req.user;
    const result = await this.service.insertListThematic(req.body);
    if (result.error) {
      return res.status(result.statusCode).send(result);
    }
    return res.status(200).send(result);
  }

  @httpPost('/likeThematic')
  async likeUser(@request() req: Request, @response() res: Response) {
    const result = await this.service.likeThematic(req.body);
    if (result.error) {
      return res.status(result.statusCode).send(result);
    }
    return res.status(201).send(result);
  }

}
