import {model, Schema} from "mongoose";
import {IAnswer} from '../type/answer.interface';

const schema = new Schema({
  message: {
    type: String,
    required: false
  },
  author: {
    type: Schema.Types.ObjectId,
    ref: 'user',
    required: false
  },
  parent: {
    type: Schema.Types.ObjectId,
    default: null,
    ref: 'answer'
  },
  question: {
    type: Schema.Types.ObjectId,
    ref: 'question',
    required: false
  },
  likes: [{
    type: Schema.Types.ObjectId,
    ref: 'user'
  }],
  anonyme: Boolean,
  valid: Boolean,
  moderate: Boolean,
  alert: Boolean,
  entity: String,
  ip: String
}, {
  timestamps: true
});

schema.virtual('answers', {
  ref: 'answer',
  localField: '_id',
  foreignField: 'parent'
});

const Answer = model<IAnswer>('answer', schema, 'answer');

export default Answer;
