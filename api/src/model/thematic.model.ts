import { Schema, model } from "mongoose";
import {IThematic} from '../type/thematic.interface';

const schema = new Schema({
  label: {
    type: String,
    required: true,
  },
  icon: {
    type: String,
    required: false,
  },
  key: {
    type: String,
    required: false
  },
  dialogflowName: {
    type: String,
    required: false
  },
  followers: [{
    type: Schema.Types.ObjectId,
    ref: 'user',
    required: true
  }],
  default: {
    type: Boolean,
    default: false
  },
  active: {
    type: Boolean,
    default: true
  },
  order: Number
}, {
  timestamps: true
});

schema.virtual('questionCount', {
  ref: 'question',
  localField: '_id',
  foreignField: 'thematic',
  count: true // Set `count: true` on the virtual
});

const Thematic = model<IThematic>("thematic", schema, 'thematic');

export default Thematic;
