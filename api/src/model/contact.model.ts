import {model, Schema} from "mongoose";
import {IContact} from '../type/contact.interface';

const schema = new Schema({
    from: {
        type: Schema.Types.ObjectId,
        ref: 'user',
        required: true
    },
    to: {
        type: Schema.Types.ObjectId,
        ref: 'user',
        required: true
    }
}, {
    timestamps: true
});

const Contact = model<IContact>('contact', schema, 'contact');

export default Contact;
