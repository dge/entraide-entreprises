import {model, Schema} from "mongoose";
import {IUser} from '../type/user.interface';
import {INotification} from '../type/notification.interface';


const schema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'user',
    required: true
  },
  type: {
    type: String,
    required: true
  },
  message: {
    type: String,
    required: true
  },
  question: {
    type: Schema.Types.ObjectId,
    ref: 'question'
  },
}, {
  timestamps: true
});


const Notification = model<INotification>('notification', schema, 'notification');

export default Notification;
