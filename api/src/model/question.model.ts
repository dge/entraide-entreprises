import {model, Schema} from "mongoose";
import {IQuestion} from '../type/question.interface';

const schema = new Schema({
  label: {
    type: String,
    required: true,
    index: true
  },
  message: {
    type: String,
    required: false
  },
  author: {
    type: Schema.Types.ObjectId,
    ref: 'user',
    required: false
  },
  thematic: {
    type: Schema.Types.ObjectId,
    ref: 'thematic',
    required: true
  },
  likes: [{
    type: Schema.Types.ObjectId,
    ref: 'user',
    required: true
  }],
  isFaq: Boolean,
  isUrgent: Boolean,
  anonyme: Boolean,
  valid: Boolean,
  entity: String,
  ip: String
}, {
  timestamps: true
});

schema.virtual('answerCount', {
  ref: 'answer',
  localField: '_id',
  foreignField: 'question',
  count: true // Set `count: true` on the virtual
});

schema.virtual('answers', {
  ref: 'answer',
  localField: '_id',
  foreignField: 'question'
});
schema.index({ label: 'text' })

const Question = model<IQuestion>('question', schema, 'question');

export default Question;
