import {model, Schema} from "mongoose";
import {IQuestion} from '../type/question.interface';
import {IUser} from '../type/user.interface';
import {ISecretCode} from '../type/secret-code.interface';

const schema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'user',
    required: true
  },
  token: {
    type: String,
    required: true
  },
  type: {
    type: String,
    required: true
  },
  createdAt: {
    type: Date,
    default: Date.now(),
    required: true
  },
});
// expires: 36000,

const SecretCode = model<ISecretCode>('secretCode', schema, 'secretCode');

export default SecretCode;
