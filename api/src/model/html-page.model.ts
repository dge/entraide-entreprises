import {model, Schema} from "mongoose";
import {IAnswer} from '../type/answer.interface';
import {IHtmlPage} from '../type/html-page.interface';

const schema = new Schema({
  code: {
    type: String,
    required: true
  },
  titre: {
    type: String,
    required: true
  },
  contenu: {
    type: String,
    required: true
  },
}, {
  timestamps: true
});


const HtmlPage = model<IHtmlPage>('htmlPage', schema, 'htmlPage');

export default HtmlPage;
