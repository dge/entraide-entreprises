import {model, Schema} from "mongoose";
import {IUser} from '../type/user.interface';


const schema = new Schema({
  firstName: {
    type: String,
    required: true
  },
  lastName: {
    type: String,
    required: true
  },
  username: {
    type: String,
    required: true,
    unique: true
  },
  roles: [String],
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  dateOfBirth: Date,
  profilePicture: String,
  job: String,
  businessCreation: Boolean,
  entity: {
    name: String,
    siret: String,
    url: String,
    address: String,
    addressBis: String,
    codePostal: String,
    city: String,
    actif: Boolean
  },
  entities: [{
    name: String,
    siret: String,
    url: String,
    address: String,
    addressBis: String,
    codePostal: String,
    city: String,
    actif: Boolean
  }],
  anonyme: {
    type: Boolean,
    default: false
  },
  isVerified: {
    type: Boolean,
    default: false
  },
  isCompleted: {
    type: Boolean,
    default: false
  },
  allowContact: {
    type: Boolean,
    default: false
  },
  followers: [{
    type: Schema.Types.ObjectId,
    ref: 'user',
    required: true
  }],
  experiences: [{
    name: String,
    startDate: String,
    endDate: String,
    url: String,
    description: String
  }],
  training: [{
    name: String,
    startDate: String,
    endDate: String,
    url: String,
    description: String
  }],
  networks: {
    facebook: String,
    twitter: String,
    instagram: String,
    linkedin: String
  },
  notification: {
    type: {
      myquestion_answer_appli: Boolean,
      myquestion_answer_email: Boolean,
      myanswer_comment_appli: Boolean,
      myanswer_comment_email: Boolean,
      thematic_createquestion_appli: Boolean,
      thematic_answerquestion_appli: Boolean,
      thematic_createquestion_email: Boolean,
      thematic_answerquestion_email: Boolean,
      user_createquestion_appli: Boolean,
      user_answerquestion_appli: Boolean,
      user_createquestion_email: Boolean,
      user_answerquestion_email: Boolean,
    }, default: {
      myquestion_answer_appli: true,
      myquestion_answer_email: true,
      myanswer_comment_appli: true,
      myanswer_comment_email: true,
      thematic_createquestion_appli: true,
      thematic_answerquestion_appli: true,
      thematic_createquestion_email: true,
      thematic_answerquestion_email: true,
      user_createquestion_appli: true,
      user_answerquestion_appli: true,
      user_createquestion_email: true,
      user_answerquestion_email: true,
    }
  }
}, {
  timestamps: true
});

/*schema.set('toJSON', {
  transform: function (doc, ret, options) {
    ret.id = ret._id;
    delete ret._id;
    delete ret.__v;
    delete ret.password;
  }
});*/

schema.virtual('questionCount', {
  ref: 'question',
  localField: '_id',
  foreignField: 'author',
  count: true // Set `count: true` on the virtual
});

schema.virtual('answerCount', {
  ref: 'answer',
  localField: '_id',
  foreignField: 'author',
  count: true // Set `count: true` on the virtual
});


const User = model<IUser>('user', schema, 'user');

export default User;
