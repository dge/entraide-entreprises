import {inject, injectable} from 'inversify';
import dialogflow from '@google-cloud/dialogflow';
import {v4 as uuidv4} from 'uuid';
import {Logger} from 'tslog';

@injectable()
class DialogflowService {

  constructor(@inject("Logger") protected log: Logger) {
  }

  async searchThematic(text = '') {
    this.log.debug(text);
    if (process.env.DIALOGFLOW_THEMATIC_CREDENTIAL) {
      // A unique identifier for the given session
      const sessionId = uuidv4();
      // Create a new session
      const sessionClient = new dialogflow.SessionsClient({
        keyFilename: process.env.DIALOGFLOW_THEMATIC_CREDENTIAL
      });
      // @ts-ignore
      const sessionPath = sessionClient.projectAgentSessionPath(process.env.DIALOGFLOW_THEMATIC_AGENT_SESSION_PATH, sessionId);
      const request = {
        session: sessionPath,
        queryInput: {
          text: {
            text,
            languageCode: process.env.DIALOGFLOW_LANGUAGE,
          },
        },
      };
      this.log.debug(request);
      let response: any;
      try {
        [response] = await sessionClient.detectIntent(request);
        this.log.debug(response);
      } catch (e) {
        this.log.error(e);
      }
      return response.queryResult && response.queryResult.intent ? response.queryResult.intent.displayName : '';
    } else {
      return '';
    }
  }

  async insertThematic(label: string) {
    if (process.env.DIALOGFLOW_THEMATIC_CREDENTIAL) {
      try {
        const sessionId = uuidv4();
        const intentsClient = new dialogflow.IntentsClient({
          keyFilename: process.env.DIALOGFLOW_THEMATIC_CREDENTIAL
        });
        // @ts-ignore
        const [response] = await intentsClient.createIntent({
          parent: 'projects/' + process.env.DIALOGFLOW_THEMATIC_AGENT_SESSION_PATH + '/agent',
          intent: {
            displayName: label
          },
          languageCode: process.env.DIALOGFLOW_LANGUAGE
        });
        this.log.debug(response);
        return response.name;
      } catch (e) {
        this.log.error(e);
      }
    } else {
      return '';
    }
  }

  async updateThematic(name: string, labelList: string[]) {
    if (process.env.DIALOGFLOW_THEMATIC_CREDENTIAL) {
      try {
        const sessionId = uuidv4();
        const intentsClient = new dialogflow.IntentsClient({
          keyFilename: process.env.DIALOGFLOW_THEMATIC_CREDENTIAL
        });
        // @ts-ignore
        const [response] = await intentsClient.getIntent({
          name,
          languageCode: process.env.DIALOGFLOW_LANGUAGE,
          intentView: 'INTENT_VIEW_FULL'
        });
        this.log.debug(response);
        if (!response.trainingPhrases) {
          response.trainingPhrases = [];
        }
        for (const label of labelList) {
          response.trainingPhrases.push({parts: [{text: label}], type: 'EXAMPLE'});
        }
        intentsClient.updateIntent({intent: response});
        return response.name;
      } catch (e) {
        this.log.error(e);
      }
    } else {
      return '';
    }
  }

  async listThematic() {
    if (process.env.DIALOGFLOW_THEMATIC_CREDENTIAL) {
      try {
        const sessionId = uuidv4();
        const intentsClient = new dialogflow.IntentsClient({
          keyFilename: process.env.DIALOGFLOW_THEMATIC_CREDENTIAL
        });
        // @ts-ignore
        const [response] = await intentsClient.listIntents({
          parent: 'projects/' + process.env.DIALOGFLOW_THEMATIC_AGENT_SESSION_PATH + '/agent',
          languageCode: process.env.DIALOGFLOW_LANGUAGE
        });
        this.log.debug(response);
        return response;
      } catch (e) {
        this.log.error(e);
      }
    } else {
      return null;
    }
  }

  async searchQuestion(text = '') {
    this.log.debug(text);
    if (process.env.DIALOGFLOW_QUESTION_CREDENTIAL) {
      // A unique identifier for the given session
      const sessionId = uuidv4();
      // Create a new session
      const sessionClient = new dialogflow.SessionsClient({
        keyFilename: process.env.DIALOGFLOW_QUESTION_CREDENTIAL
      });
      // @ts-ignore
      const sessionPath = sessionClient.projectAgentSessionPath(process.env.DIALOGFLOW_QUESTION_AGENT_SESSION_PATH, sessionId);
      // The text query request.
      const request = {
        session: sessionPath,
        queryInput: {
          text: {
            text,
            languageCode: process.env.DIALOGFLOW_LANGUAGE,
          },
        },
      };
      this.log.debug(request);
      let response: any;
      try {
        [response] = await sessionClient.detectIntent(request);
        this.log.debug(response);
      } catch (e) {
        this.log.error(e);
      }
      return response && response.queryResult && response.queryResult.intent ? response.queryResult.intent.displayName : '';
    } else {
      return '';
    }
  }

  async createIntent(text: string) {
    if (process.env.DIALOGFLOW_QUESTION_CREDENTIAL) {
      try {
        const sessionId = uuidv4();
        const intentsClient = new dialogflow.IntentsClient({
          keyFilename: process.env.DIALOGFLOW_QUESTION_CREDENTIAL
        });
        // @ts-ignore
        const [response] = await intentsClient.createIntent({
          parent: 'projects/' + process.env.DIALOGFLOW_QUESTION_AGENT_SESSION_PATH + '/agent',
          intent: {
            displayName: text,
            trainingPhrases: [{parts: [{text}]}]
          },
          languageCode: process.env.DIALOGFLOW_LANGUAGE
        });
        this.log.debug(response);
      } catch (e) {
        this.log.error(e);
      }
    } else {
      return null;
    }
  }

}

export {DialogflowService};
