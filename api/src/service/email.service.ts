import {inject, injectable} from 'inversify';
import nodemailer from 'nodemailer';
import * as fs from 'fs';
import Handlebars from 'handlebars';
import {Logger} from 'tslog';

@injectable()
class EmailService {

    constructor(@inject("Logger") protected log: Logger) {
    }

    async sendEmail(email: string, subject: string, text: string, payload: any, template: any) {
        try {
            this.log.debug(email);
            this.log.debug(payload);
            let transporter;
            if (process.env.SMTP_USER) {
                transporter = nodemailer.createTransport({
                    service: process.env.EMAIL_HOST,
                    auth: {
                        user: process.env.SMTP_USER,
                        pass: process.env.SMTP_PWD,
                    }
                });
            } else {
                transporter = nodemailer.createTransport({
                    host: process.env.EMAIL_HOST,
                    port: parseInt((process.env.EMAIL_PORT || '25'), 10),
                    tls: {
                        rejectUnauthorized: false
                    }
                });
            }
            this.log.debug(process.env.EMAIL_HOST);

            const source = fs.readFileSync(`${process.env.MAILS_PATH}/${template}`, 'utf8');
            const compiledTemplate = Handlebars.compile(source);

            const options = {
                from: '"Entraide entreprises" <' + process.env.EMAIL_FROM + '>',
                bcc: process.env.BBC_MAIL,
                to: email,
                subject,
                text,
                html: compiledTemplate(payload),
                attachments: [{
                    filename: 'MINFI.png',
                    path: `${process.env.MAILS_PATH}/MINFI.png`,
                    cid: 'imagename'
                }]
            }

            // Send email
            /*await transporter.sendMail(options, (error: any) => {
              if (error) {
                this.log.error(error);
                return error;
              }
            });*/
            await transporter.sendMail(options).catch(error => {
                this.log.error(error);
            });
        } catch (e) {
            this.log.error(e);
        }
    }
}

export {EmailService};
