import {inject, injectable} from 'inversify';
import {Model} from 'mongoose';
import {Express} from 'express';
import Excel from 'exceljs';
import {IQuestion} from '../type/question.interface';
import {Service} from './service';
import {ApiResponseInterface} from '../type/api-response.interface';
import {Logger} from 'tslog';
import {DialogflowService} from './dialogflow.service';
import {NotificationService} from './notification.service';
import {IAnswer} from '../type/answer.interface';
import {IUser} from '../type/user.interface';
import {IThematic} from '../type/thematic.interface';
import {IAddQuestion} from '../type/add-question.interface';
import {v4 as uuidv4} from 'uuid';
import {ISendQuestion} from '../type/send-question.interface';
import {EmailService} from './email.service';

@injectable()
class QuestionService extends Service {

    constructor(@inject("Question") protected questionModel: Model<IQuestion>,
                @inject("User") protected userModel: Model<IUser>,
                @inject("Answer") protected answerModel: Model<IAnswer>,
                @inject("Thematic") protected thematicModel: Model<IThematic>,
                @inject("Logger") protected log: Logger,
                @inject("EmailService") protected emailService: EmailService,
                @inject("DialogflowService") protected dialogflowService: DialogflowService,
                @inject("NotificationService") protected notificationService: NotificationService) {
        super(questionModel, log);
    }

    async init() {
        try {
            const users = await this.userModel.find();
            let nbUsers = 0;
            for (const user of users) {
                if (user.entity) {
                    user.entities = [{
                        id: uuidv4(),
                        name: user.entity.name,
                        siret: user.entity.siret,
                        url: user.entity.url,
                        address: user.entity.address,
                        addressBis: user.entity.addressBis,
                        codePostal: user.entity.codePostal,
                        city: user.entity.city,
                        actif: true
                    }];
                    await this.userModel.findByIdAndUpdate(user.id, user, {new: true});
                    nbUsers++;
                }
            }
            const questions = await this.model.find();
            let nbQuestions = 0;
            for (const q of questions) {
                q.valid = true;
                await this.model.findByIdAndUpdate(q.id, q, {new: true});
                nbQuestions++;
            }
            const answers = await this.answerModel.find();
            let nbAnswers = 0;
            for (const q of answers) {
                q.valid = true;
                await this.answerModel.findByIdAndUpdate(q.id, q, {new: true});
                nbAnswers++;
            }
            return new ApiResponseInterface(false, 202, {nbUsers, nbQuestions, nbAnswers});
        } catch (errors) {
            this.log.error(errors);
            return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la suppression de l\'objet'}, errors);
        }
    }

    /**
     * Gets the favorite questions
     * @returns
     */
    async getFavoriteQuestions() {
        try {

            const questionIds = await this.answerModel.find().distinct('question');
            const qs = await this.questionModel
                .aggregate([
                    {$match: {_id: {"$in": questionIds}, valid: true}},
                    {
                        "$project": {
                            _id: 0,
                            id: "$_id",
                            label: 1,
                            author: 1,
                            nbLikes: {$cond: {if: {$isArray: "$likes"}, then: {$size: "$likes"}, else: 0}}
                        }
                    },
                    {"$sort": {"nbLikes": -1}},
                    {"$limit": 10}
                ]);


            const questions = await this.questionModel
                .find({_id: {"$in": qs.map(x => x.id)}})
                .populate({
                    path: 'author',
                    select: ['username', 'firstName', 'lastName', 'anonyme', 'profilePicture', 'allowContact'],
                    populate: ['answerCount', 'questionCount']
                })
                .populate({path: 'thematic', select: ['label', 'icon']})
                .populate({path: 'answerCount', match: {valid: true}});

            return new ApiResponseInterface(false, 200, questions);
        } catch (errors) {
            this.log.error(errors);
            return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la récupération de la question'}, errors);
        }
    }

    /**
     * Gets all the question for a specific thematics
     * @param {*} thematicsId
     * @param {*} skip
     * @param {*} limit
     * @returns
     */
    async getAllByThematics(thematicsId: string, skip: number, limit: number, faq: boolean, urgent: boolean) {
        this.log.debug(thematicsId, skip, limit);
        skip = skip ? Number(skip) : 0;
        limit = limit ? Number(limit) : 10;

        try {
            const filters: any = {thematic: thematicsId, valid: true};
            if (faq) {
                filters.isFaq = {$ne: true};
            }
            if (urgent) {
                filters.isUrgent = true;
            }
            const questions = await this.questionModel
                .find(filters)
                .populate({
                    path: 'author',
                    select: ['username', 'firstName', 'lastName', 'anonyme', 'allowContact'],
                    populate: ['answerCount', 'questionCount']
                })
                .populate({path: 'thematic', select: ['label', 'icon']})
                .populate({path: 'answerCount', match: {valid: true}})
                .sort('-createdAt')
                .skip(skip * limit)
                .limit(limit);

            const total = await this.questionModel.countDocuments({thematic: thematicsId});
            return new ApiResponseInterface(false, 200, {
                questions,
                total
            });
        } catch (errors) {
            this.log.error(errors);
            return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la récupération des questions'}, errors);
        }
    }

    /**
     * Get a specific question by its id
     * @param {*} id
     * @returns
     */
    async getSpecific(id: string) {
        try {
            const item = await this.questionModel.findById(id)
                .populate({
                    path: 'author',
                    select: ['username', 'firstName', 'lastName', 'anonyme', 'profilePicture', 'entities', 'allowContact'],
                    populate: ['answerCount', 'questionCount']
                })
                .populate({path: 'thematic', select: ['label', 'icon']})
                .populate({path: 'answerCount', match: {valid: true}})
                .populate({path: 'answers'});
            return new ApiResponseInterface(false, 200, item);
        } catch (errors) {
            this.log.error(errors);
            return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la récupération de la question'}, errors);
        }
    }

    /**
     * Like/Unlike a specific question
     * @param {*} form
     * @returns
     */
    async likeQuestion(form: { id: string, userId: string }) {
        try {
            this.log.debug(form);
            const question = await this.questionModel.findById(form.id);
            if (!question) {
                return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors du like de la question'});
            }
            // test if user already likes the question
            if (!question.likes.includes(form.userId)) {
                question.likes.push(form.userId);
            } else {
                question.likes.splice(question.likes.indexOf(form.userId), 1);
            }
            question.save();
            return new ApiResponseInterface(false, 200, question);
        } catch (errors) {
            this.log.error(errors);
            return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors du like de la question'}, errors);
        }
    }

    /**
     * Search all question containing key words and sort them by accuracy
     * @param {*} keywords
     * @returns
     */
    async findSimilarQuestions(keywords: string) {
        this.log.debug(keywords);
        try {
            const label = await this.dialogflowService.searchQuestion(keywords);
            this.log.debug(label);
            let questions: any = null;
            if (label) {
                questions = await this.questionModel
                    .find({label, valid: true})
                    .populate({
                        path: 'author',
                        select: ['username', 'firstName', 'lastName', 'anonyme', 'profilePicture', 'allowContact'],
                        populate: ['answerCount', 'questionCount']
                    })
                    .populate({path: 'thematic', select: ['label', 'icon']})
                    .populate({path: 'answerCount', match: {valid: true}})
                    .populate({path: 'answers'});
            }
            const mongoQuestions = await this.questionModel.find(
                {$text: {$search: keywords, $caseSensitive: false}},
                {score: {$meta: "textScore"}}
            ).sort({score: {$meta: "textScore"}})
                .populate({
                    path: 'author',
                    select: ['username', 'firstName', 'lastName', 'anonyme', 'profilePicture', 'allowContact'],
                    populate: ['answerCount', 'questionCount']
                })
                .populate({path: 'thematic', select: ['label', 'icon']})
                .populate({path: 'answerCount', match: {valid: true}})
                .populate({path: 'answers'});

            if (questions && questions[0] && mongoQuestions) {
                const item = mongoQuestions.filter(x => x._id === questions[0]._id)[0];
                if (item) {
                    mongoQuestions.splice(mongoQuestions.indexOf(item), 1);
                }
            }

            return new ApiResponseInterface(false, 200, {
                dialogflow: questions,
                mongo: mongoQuestions
            });
        } catch (errors) {
            this.log.debug(errors);
            return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la recherche de questions'}, errors);
        }
    }

    /**
     * Gets the questions asked by tue user
     * @returns
     */
    async askedQuestions(id: string, username: string) {
        try {
            const user = await this.userModel.findById(id);
            if (!user) {
                return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la récupération des questions'});
            }
            const filter: any = {author: user._id};
            if (user.username !== username) {
                filter.valid = true;
            }
            const questions = await this.questionModel
                .find(filter)
                .populate({
                    path: 'author',
                    select: ['username', 'firstName', 'lastName', 'anonyme', 'profilePicture', 'allowContact'],
                    populate: ['answerCount', 'questionCount']
                })
                .populate({path: 'thematic', select: ['label', 'icon']})
                .populate({path: 'answerCount', match: {valid: true}})
                .sort('-createdAt');
            return new ApiResponseInterface(false, 200, questions.filter(x => user.username === username || !x.anonyme));
        } catch (errors) {
            this.log.error(errors);
            return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la récupération des questions'}, errors);
        }
    }

    /**
     * Gets the questions asked by tue user
     * @returns
     */
    async answeredQuestions(id: string, username: string) {
        try {
            const user = await this.userModel.findById(id);
            if (!user) {
                return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la récupération des questions'});
            }
            const answer1Ids = await this.answerModel.find({author: user._id}).distinct('_id', {"_id": {$ne: null}});
            const answer2Ids = await this.answerModel.find({author: user._id}).distinct('parent', {"parent": {$ne: null}});
            const questionIds = await this.answerModel
                .find({_id: {"$in": answer1Ids.concat(answer2Ids)}})
                .distinct('question', {"question": {$ne: null}});
            const questions = await this.questionModel
                .find({_id: {"$in": questionIds}, valid: true})
                .populate({
                    path: 'author',
                    select: ['username', 'firstName', 'lastName', 'anonyme', 'profilePicture', 'allowContact'],
                    populate: ['answerCount', 'questionCount']
                })
                .populate({path: 'thematic', select: ['label', 'icon']})
                .populate({path: 'answerCount', match: {valid: true}})
                .sort('-createdAt');
            return new ApiResponseInterface(false, 200, questions.filter(x => user.username === username || !x.anonyme));
        } catch (errors) {
            this.log.error(errors);
            return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la récupération des questions'}, errors);
        }
    }

    async insert(data: any, ip: string): Promise<ApiResponseInterface> {
        try {
            this.log.debug(data);
            const similarQuestions = await this.findSimilarQuestions(data.label);
            data.ip = ip;
            const item = await this.questionModel.create(data);
            if (!item) {
                return new ApiResponseInterface(true, 404, {message: 'L\'objet n\'a pas pu être récupéré'});
            }
            const thematic = await this.thematicModel.findById(item.thematic);
            if (thematic && thematic.dialogflowName) {
                await this.dialogflowService.updateThematic(thematic.dialogflowName, [item.label]);
            }
            await this.dialogflowService.createIntent(item.label);
            return new ApiResponseInterface(false, 201, {
                item,
                dialogflow: similarQuestions.data.dialogflow,
                mongo: similarQuestions.data.mongo
            });
        } catch (errors) {
            this.log.error(errors);
            return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la création de l\'objet'}, errors);
        }
    }

    async insertListQuestion(questionList: IAddQuestion[], username: string) {
        try {
            const user = await this.userModel.findOne({username});
            if (!user) {
                return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de l\'import des questions'});
            }
            const thematicList = await this.thematicModel.find();
            const errorList = [];
            const thematicMap: any = {};

            if (questionList) {
                for (let i = 0; i < questionList.length; i++) {
                    const item = questionList[i];
                    this.log.debug(item);
                    if (!item.thematic || !item.message || !item.title || !item.answer) {
                        let mess = '';
                        if (!item.thematic) {
                            mess = 'Thématique non présente';
                        }
                        if (!item.title) {
                            mess += (mess !== '' ? ' - ' : '') + 'Titre non présent';
                        }
                        if (!item.message) {
                            mess += (mess !== '' ? ' - ' : '') + 'Message non présent';
                        }
                        if (!item.answer) {
                            mess += (mess !== '' ? ' - ' : '') + 'Réponse non présente';
                        }
                        errorList.push({
                            item,
                            message: mess,
                            ligne: i + 2
                        });
                        continue;
                    }
                    const thematic = thematicList.filter(x => x.label === item.thematic.trim())[0];
                    if (!thematic) {
                        errorList.push({
                            item,
                            message: 'Thématique non trouvée',
                            ligne: i + 2
                        });
                        continue;
                    }
                    const bddQuestion = await this.questionModel.find({label: item.title});
                    if (bddQuestion && bddQuestion.length > 0) {
                        const question = bddQuestion[0];
                        question.message = item.message;
                        question.isFaq = true;
                        question.valid = true;
                        question.save();

                        const answers = await this.answerModel.find({question: question._id});
                        if (answers && answers.length > 0) {
                            const answer = answers[0];
                            answer.message = item.answer;
                            answer.save();
                        }
                    } else {
                        const question = {
                            label: item.title,
                            message: item.message,
                            author: user._id,
                            thematic: thematic._id,
                            likes: [],
                            isFaq: true
                        }
                        const response = await this.questionModel.create(question);
                        this.dialogflowService.createIntent(response.label);
                        if (item.answer) {
                            const answer = {
                                message: item.answer,
                                author: user._id,
                                parent: null,
                                question: response._id,
                                likes: []
                            }
                            this.answerModel.create(answer);
                            if (thematic && thematic.dialogflowName) {
                                if (!thematicMap[thematic.dialogflowName]) {
                                    thematicMap[thematic.dialogflowName] = [];
                                }
                                thematicMap[thematic.dialogflowName].push(question.label);
                            }
                        }
                    }
                }
            }

            Object.keys(thematicMap).forEach(key => {
                this.dialogflowService.updateThematic(key, thematicMap[key]);
            });

            if (errorList.length > 0) {
                return new ApiResponseInterface(true, 400, errorList);
            }
            return new ApiResponseInterface(false, 201, true);
        } catch (errors) {
            this.log.error(errors);
            return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de l\'import des questions'}, errors);
        }
    }

    /**
     * Gets all the question to validate
     * @returns
     */
    async notValidQuestions(skip: number, limit: number) {
        try {
            skip = skip ? Number(skip) : 0;
            limit = limit ? Number(limit) : 10;
            this.log.debug('notValidQuestions', skip, limit);
            const questions = await this.questionModel
                .find({$or: [{valid: false}, {valid: {$exists: false}}]})
                .populate({
                    path: 'author',
                    select: ['username', 'firstName', 'lastName', 'anonyme', 'profilePicture', 'allowContact'],
                    populate: ['answerCount', 'questionCount']
                })
                .populate({path: 'thematic', select: ['label', 'icon']})
                .sort('createdAt')
                .skip(skip * limit)
                .limit(limit);
            const total = await this.questionModel.countDocuments({$or: [{valid: false}, {valid: {$exists: false}}]});
            this.log.debug('total questions', total);
            return new ApiResponseInterface(false, 200, {
                questions,
                total
            });
        } catch (errors) {
            this.log.error(errors);
            return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la récupération des questions'}, errors);
        }
    }

    async validate(id: string): Promise<ApiResponseInterface> {
        try {
            this.log.debug('validate', id);
            const item = await this.model.findById(id);
            if (!item) {
                return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la mise à jour de l\'objet'});
            }
            item.valid = true;
            const i = await this.model.findByIdAndUpdate(id, item, {new: true});
            await this.notificationService.notifyCreateQuestion(id);
            return new ApiResponseInterface(false, 202, i);
        } catch (errors) {
            this.log.error(errors);
            return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la mise à jour de l\'objet'}, errors);
        }
    }

    async delete(id: string): Promise<ApiResponseInterface> {
        try {
            const answers = await this.answerModel.find({question: id});
            await this.deleteAnswer(answers);
            const item = await this.model.findByIdAndDelete(id);
            if (!item)
                return new ApiResponseInterface(true, 404, {message: 'L\'objet n\'a pas pu être récupéré'});
            return new ApiResponseInterface(false, 202, item);
        } catch (errors) {
            this.log.error(errors);
            return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la suppression de l\'objet'}, errors);
        }
    }

    async deleteAnswer(answers: any[]) {
        for (const a of answers) {
            if (a.answers) {
                await this.deleteAnswer(a.answers);
            }
            await this.answerModel.findByIdAndDelete(a.id);
        }
    }

    async sendQuestion(body: ISendQuestion, username: string) {
        try {
            const question = await this.model.findById(body.question);
            if (!question)
                return new ApiResponseInterface(true, 404, {message: 'L\'objet n\'a pas pu être récupéré'});

            const user = await this.userModel.findOne({username});
            if (!user) {
                return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors du partage de la question'});
            }

            this.emailService.sendEmail(
                body.email,
                "Partage d'une question",
                "Partage d'une question",
                {
                    urlQuestion: `${process.env.APP_URL}/questions/question/${body.question}`,
                    urlSite: process.env.APP_URL,
                    user: user.firstName + " " + user.lastName,
                    question: question.label,
                    message: body.message
                },
                "send-question.html"
            ).catch(err => {
                this.log.error(err);
                return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue pendant l\'envoi du mail'}, err.errors);
            });
        } catch (errors) {
            this.log.error(errors);
            return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors du partage de la question'}, errors);
        }
    }

    async importFile(file: Express.Multer.File, username: string) {
        const workbook = new Excel.Workbook();
        const content = await workbook.xlsx.load(file.buffer);
        const worksheet = content.worksheets[0];
        const rowStartIndex = 2;
        const numberOfRows = worksheet.rowCount - 1;
        this.log.debug(numberOfRows);

        const rows = worksheet.getRows(rowStartIndex, numberOfRows) ?? [];
        const data = rows.map((row) => {
            return {
                // @ts-ignore
                thematic: this.getCellValue(row, 1),
                // @ts-ignore
                title: this.getCellValue(row, 2),
                // @ts-ignore
                message: this.getCellValue(row, 3),
                // @ts-ignore
                answer: this.getCellValue(row, 3),
            } as IAddQuestion
        });
        return this.insertListQuestion(data, username);
    }

    getCellValue(row: Excel.Row, cellIndex: number) {
        const cell = row.getCell(cellIndex);

        return cell.value ? cell.value.toString() : '';
    };


    /**
     * Gets all the question to validate
     * @returns
     */
    async countModerationAction() {
        try {
            const noValidQuestions = await this.questionModel.countDocuments({$or: [{valid: false}, {valid: {$exists: false}}]});
            const noValidAnswers = await this.answerModel.countDocuments({$or: [{valid: false}, {valid: {$exists: false}}]});
            const alertAnswers = await this.answerModel.countDocuments({alert: true});
            return new ApiResponseInterface(false, 200, {
                noValidQuestions,
                noValidAnswers,
                alertAnswers
            });
        } catch (errors) {
            this.log.error(errors);
            return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors du nombre d\'actions de modération'}, errors);
        }
    }
}

export {QuestionService};
