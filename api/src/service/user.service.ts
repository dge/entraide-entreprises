import {inject, injectable} from 'inversify';
import {Model} from 'mongoose';
import {Service} from './service';
import {ApiResponseInterface} from '../type/api-response.interface';
import {IUser} from '../type/user.interface';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import crypto from 'crypto';
import {ISecretCode} from '../type/secret-code.interface';
import {EmailService} from './email.service';
import {Logger} from 'tslog';
import {v4 as uuidv4} from 'uuid';
import {User} from '../../../front/src/app/store/models/user';
import {IQuestion} from '../type/question.interface';
import {IAnswer} from '../type/answer.interface';
import {INotification} from '../type/notification.interface';
import {IContactUser} from '../type/contact-user.interface';
import {IContact} from '../type/contact.interface';

@injectable()
class UserService extends Service {

    constructor(@inject("User") protected userModel: Model<IUser>,
                @inject("Notification") protected notifModel: Model<INotification>,
                @inject("Question") protected questionModel: Model<IQuestion>,
                @inject("Contact") protected contactModel: Model<IContact>,
                @inject("Answer") protected answerModel: Model<IAnswer>,
                @inject("SecretCode") protected secretCodeModel: Model<ISecretCode>,
                @inject("EmailService") protected emailService: EmailService,
                @inject("Logger") protected log: Logger) {
        super(userModel, log);
    }

    async checkDuplicateUser(usernameOrEmail: string) {
        try {
            const users = await this.userModel.find({
                $or: [
                    {username: {$regex: new RegExp(`^${usernameOrEmail.trim()}$`, 'i')}},
                    {email: {$regex: new RegExp(`^${usernameOrEmail.trim()}$`, 'i')}}
                ]
            });
            return new ApiResponseInterface(users.length > 0, users.length === 0 ? 200 : 400,
                users.length === 0 ? 'OK' : 'Le nom d\'utilisateur ou l\'adresse email est déjà utilisé');
        } catch (errors) {
            this.log.error(errors);
            return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors du test utilisateur'}, errors);
        }
    }

    async insert(user: IUser) {
        const checkUsername = await this.checkDuplicateUser(user.username);
        const checkEmail = await this.checkDuplicateUser(user.email);
        if (checkUsername.error || checkEmail.error) {
            return new ApiResponseInterface(true, 400, 'Le nom d\'utilisateur ou l\'adresse email est déjà utilisé');
        }
        try {
            const item = await this.userModel.create(user);
            const bool = await this.createVerifyAccountSecretCode(item);
            if (!bool) {
                return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de l\'envoi du mail'});
            }
            return new ApiResponseInterface(false, 201, item, null);
        } catch (error: any) {
            this.log.error(error);
            return new ApiResponseInterface(true, 500,
                {message: 'Une erreur est survenue pendant la création du compte'}, error.errors);
        }
    }

    async signin(usernameOrEmail: string, password: string) {
        try {
            const user = await this.userModel.findOne({
                $or: [
                    {username: {$regex: new RegExp(`^${usernameOrEmail.trim()}$`, 'i')}},
                    {email: {$regex: new RegExp(`^${usernameOrEmail.trim()}$`, 'i')}}
                ]
            });
            if (!user) {
                return new ApiResponseInterface(true, 401, {message: 'Le couple identifiant/mot de passe n\'existe pas'});
            } else {
                const passwordIsValid = bcrypt.compareSync(password, user.password);
                if (!passwordIsValid) {
                    return new ApiResponseInterface(true, 401, {message: 'Le couple identifiant/mot de passe n\'existe pas'});
                }
            }
            if (!user.isVerified || !user.isCompleted) {
                return new ApiResponseInterface(true, 401, {message: 'Le compte n\'a pas encore été validé'});
            }
            return new ApiResponseInterface(false, 200, this.gettoken(user));
        } catch (errors) {
            this.log.error(errors);
            return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue pendant le login'}, errors);
        }
    }

    gettoken(user: IUser) {
        const SECRET = process.env.TOKEN_KEY || "token-entraide-secret-key";
        const token = jwt.sign({username: user.username}, SECRET, {
            expiresIn: 86400 // 24 hours
        });
        return {
            user: {
                id: user._id,
                username: user.username,
                firstName: user.firstName,
                lastName: user.lastName,
                profilePicture: user.profilePicture,
                roles: user.roles,
                anonyme: user.anonyme,
                entities: user.entities,
                email: user.email
            },
            accessToken: token
        };
    }

    async confirmation(userId: string, token: string) {
        try {
            this.log.debug(userId);
            this.log.debug(token);
            const user = await this.userModel.findById(userId);
            if (!user) {
                return new ApiResponseInterface(true, 400, {code: 'NOT_FOUND', message: 'Le compte n\'existe pas'});
            }
            user.isVerified = true;
            user.save();
            return new ApiResponseInterface(false, 200, true);

            /*
            else if (user.isVerified) {
              return new ApiResponseInterface(true, 400, {
                code: 'ALREADY_CHECK',
                message: 'L\'adresse mail de l\'utilisateur a déjà été vérifiée'
              });
            }
            const secretCode = await this.secretCodeModel.findOne({user: userId, token});
            if (secretCode && secretCode.type === 'emailVerification') {
              // Update user to set its status to verified
              user.isVerified = true;
              user.save();
              // Remove the secret code
              secretCode.remove();
              return new ApiResponseInterface(false, 200, true);
            }
            return new ApiResponseInterface(true, 400, {code: 'EXPIRE', message: 'La vérification a expiré'});
            */
        } catch (errors) {
            this.log.error(errors);
            return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue pendant la confirmation de l\'adresse mail'}, errors);
        }
    }

    async verifyResetLink(userId: string, token: string) {
        try {
            this.log.debug(userId);
            this.log.debug(token);
            const user = await this.userModel.findById(userId);
            if (!user) {
                return new ApiResponseInterface(true, 400, {code: 'NOT_FOUND', message: 'Le compte n\'existe pas'});
            }
            const secretCode = await this.secretCodeModel.findOne({user: userId, token});
            if (secretCode && secretCode.type === 'passwordReset') {
                return new ApiResponseInterface(false, 200, true);
            }
            return new ApiResponseInterface(true, 400, {code: 'EXPIRE', message: 'Le lien a expiré'});
        } catch (errors) {
            this.log.error(errors);
            return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue pendant la vérification du lien'}, errors);
        }
    }

    async resendVerificationEmail(email: string) {
        try {
            this.log.debug(email);
            const user = await this.userModel.findOne({
                $or:
                    [
                        {username: {$regex: new RegExp(`^${email.trim()}$`, 'i')}},
                        {email: {$regex: new RegExp(`^${email.trim()}$`, 'i')}}
                    ]
            });
            if (user && !user.isVerified) {
                // First check if a secret code is pending for this user
                const oldSecretCode = await this.secretCodeModel.findOne({user: user.id, type: 'emailVerification'});
                const bool = await this.createVerifyAccountSecretCode(user);
                if (!bool) {
                    return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de l\'envoi du mail'});
                }
                // Remove the old secret code
                if (oldSecretCode) {
                    oldSecretCode.remove();
                }
            }
            return new ApiResponseInterface(false, 200, true, null);
        } catch (errors) {
            this.log.error(errors);
            return new ApiResponseInterface(true, 500, {message: `Une erreur est survenue pendant la vérification de l\'email ${email}`}, errors);
        }
    }

    async getSpecific(id: string) {
        try {
            this.log.debug(id);
            const user = await this.userModel.findById(id);
            return new ApiResponseInterface(false, 200, user);
        } catch (errors) {
            this.log.error(errors);
            return new ApiResponseInterface(true, 500, {message: 'Une erreur est suvenue pendant la récupération de l\'utilisateur'}, errors);
        }
    }

    async getMe(username: string) {
        try {
            this.log.debug(username);
            const user = await this.userModel.findOne({username});
            return new ApiResponseInterface(false, 200, user);
        } catch (errors) {
            this.log.error(errors);
            return new ApiResponseInterface(true, 500, {message: 'Une erreur est suvenue pendant la récupération de l\'utilisateur'}, errors);
        }
    }

    /** User ask to reset his password */
    async askResetPassword(email: string) {
        try {
            this.log.debug(email);
            const user = await this.userModel.findOne({
                $or:
                    [
                        {username: {$regex: new RegExp(`^${email.trim()}$`, 'i')}},
                        {email: {$regex: new RegExp(`^${email.trim()}$`, 'i')}}
                    ]
            });
            if (!user) {
                return new ApiResponseInterface(false, 201, true);
            }
            // Create a secret code for the new user
            const code = await this.secretCodeModel.create({
                user: user._id,
                type: 'passwordReset',
                token: crypto.randomBytes(40).toString('hex')
            });
            this.emailService.sendEmail(
                user.email,
                "Réinitialiser votre mot de passe sur Entraide Entreprises",
                "Pour réinitialiser votre mot de passe, merci de cliquer sur le lien ci-dessous",
                {
                    urlPasswordReset: `${process.env.APP_URL}/utilisateur/reinitialiser-mot-de-passe?userId=${user._id}&token=${code.token}`,
                    urlSite: process.env.APP_URL
                },
                "reset-password.html"
            ).catch(err => {
                this.log.error(err);
                return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue pendant l\'envoi du mail'}, err.errors);
            });
            return new ApiResponseInterface(false, 201, true);
        } catch (errors) {
            this.log.error(errors);
            return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue pendant la demande de réinitialisation du mot de passe'}, errors);
        }
    }

    /** Actually reset the password for a specific user */
    async resetPassword(userId: string, password: string, token: string) {
        try {
            const user = await this.userModel.findById(userId);
            if (!user) {
                return new ApiResponseInterface(true, 400, {code: 'NOT_FOUND', message: 'Le compte n\'existe pas'});
            }
            const secretCode = await this.secretCodeModel.findOne({user: userId, token});
            if (secretCode && secretCode.type === 'passwordReset') {
                // Update user to set its status to verified
                user.password = password;
                user.isVerified = true;
                user.save();
                // Remove the secret code
                secretCode.remove();
                return new ApiResponseInterface(false, 200, true);
            }
            return new ApiResponseInterface(true, 400, {code: 'EXPIRE', message: 'La vérification a expiré'});
        } catch (errors) {
            this.log.error(errors);
            return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la réinitialisation du mot de passe'}, errors);
        }
    }

    /**
     * Like/Unlike a specific user
     * @param {*} form
     * @returns
     */
    async likeUser(form: { id: string, userId: string }) {
        this.log.debug('likeUser', form);
        try {
            const user = await this.userModel.findById(form.id);
            if (!user) {
                return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors du like de l\'utilisateur'});
            }
            // test if user already likes the question
            if (!user.followers) {
                user.followers = [];
            }
            if (!user.followers.includes(form.userId)) {
                user.followers.push(form.userId);
            } else {
                user.followers.splice(user.followers.indexOf(form.userId), 1);
            }
            user.save();
            return new ApiResponseInterface(false, 200, user);
        } catch (errors) {
            this.log.error(errors);
            return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors du like de la question'}, errors);
        }
    }

    /**
     * change mode of a user
     * @param {*} username
     * @returns
     */
    async changeMode(username: string) {
        this.log.debug('changeMode', username);
        try {
            const user = await this.userModel.findOne({username});
            if (!user) {
                return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors du like de l\'utilisateur'});
            }
            user.anonyme = !user.anonyme;
            user.save();
            return await new ApiResponseInterface(false, 200, user);
        } catch (errors) {
            this.log.error(errors);
            return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors du changement de mode'}, errors);
        }
    }

    /**
     * change contact of a user
     * @param {*} username
     * @returns
     */
    async changeContact(username: string) {
        this.log.debug('changeContact', username);
        try {
            const user = await this.userModel.findOne({username});
            if (!user) {
                return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors du like de l\'utilisateur'});
            }
            user.allowContact = !!!user.allowContact;
            this.log.debug(user.allowContact);
            user.save();
            return await new ApiResponseInterface(false, 200, user);
        } catch (errors) {
            this.log.error(errors);
            return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors du changement de mode'}, errors);
        }
    }

    /**
     * Like/Unlike a user
     * @param {*} username
     * @returns
     */
    async followedUsers(username: string) {
        this.log.debug('followedUsers', username);
        try {
            const user = await this.userModel.findOne({username});
            if (!user) {
                return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors du like de l\'utilisateur'});
            }
            const users = await this.userModel.find({'followers': user.id},
                ['username', 'followers', 'lastName', 'firstName', 'profilePicture', 'anonyme'])
                .populate({path: 'questionCount'})
                .populate({path: 'answerCount'});
            return new ApiResponseInterface(false, 200, users);
        } catch (errors) {
            this.log.error(errors);
            return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la récupération des utilisateurs'}, errors);
        }
    }

    async createVerifyAccountSecretCode(user: IUser): Promise<boolean> {
        try {
            this.log.debug(user);
            // Create a secret code for the new user
            const code = await this.secretCodeModel.create({
                user: user._id,
                type: 'emailVerification',
                token: crypto.randomBytes(40).toString('hex')
            })

            this.emailService.sendEmail(
                user.email,
                "Vérifiez votre compte sur Entraide Entreprises",
                "Pour confirmer votre adresse mail, merci de cliquer sur le lien ci-dessous",
                {
                    urlVerification: `${process.env.APP_URL}/utilisateur/verifier-compte?userId=${user._id}&token=${code.token}`,
                    urlSite: process.env.APP_URL
                },
                "account-verification.html"
            ).catch(err => {
                this.log.error(err);
                return false;
            });
            return true;
        } catch (e) {
            this.log.error(e);
            return false;
        }
    }

    async getAllUser(search: string, skip: number, limit: number) {
        this.log.debug(search, skip, limit);
        skip = skip ? Number(skip) : 0;
        limit = limit ? Number(limit) : 10;

        try {
            let filter: any = {'anonyme': false};
            if (search && search !== 'undefined') {
                filter = {
                    $and: [
                        {'anonyme': false},
                        {
                            $or: [
                                {'firstName': {$regex: '.*' + search + '.*'}},
                                {'lastName': {$regex: '.*' + search + '.*'}},
                                {'email': {$regex: '.*' + search + '.*'}}
                            ]
                        },
                    ]
                };
            }
            const users = await this.userModel.find(filter,
                ['username', 'followers', 'lastName', 'firstName', 'profilePicture', 'anonyme', 'job'])
                .populate({path: 'questionCount', match: {valid: true}})
                .populate({path: 'answerCount'})
                .skip(skip * limit)
                .limit(limit);

            const total = await this.userModel.countDocuments(filter);
            return new ApiResponseInterface(false, 200, {
                users,
                total
            });
        } catch (errors) {
            this.log.error(errors);
            return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la récupération des questions'}, errors);
        }
    }

    async completeInscription(id: string, body: any) {
        this.log.debug('completeInscription', body);
        try {
            const user = await this.userModel.findById(id);
            if (!user) {
                return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue l\'inscription'});
            }
            user.isCompleted = true;
            user.businessCreation = body.businessCreation;
            user.job = body.job;
            user.entities = [body.entity];
            user.save();
            return new ApiResponseInterface(false, 200, user);
        } catch (errors) {
            this.log.error(errors);
            return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors du like de la question'}, errors);
        }
    }

    async update(id: string, data: any): Promise<ApiResponseInterface> {
        try {
            if (data.entities) {
                for (const i of data.entities) {
                    if (i.id == null) {
                        i.id = uuidv4();
                    }
                }
            }
            const item = await this.model.findByIdAndUpdate(id, data, {new: true});
            return new ApiResponseInterface(false, 202, item);
        } catch (errors) {
            this.log.error(errors);
            return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la mise à jour de l\'objet'}, errors);
        }
    }

    async changeEntity(username: string, entityId: string) {
        try {
            this.log.debug(username, entityId);
            const user = await this.userModel.findOne({username});
            if (!user) {
                return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue pendant la modification de l\'utilisateur'});
            }
            if (user.entities) {
                for (const e of user.entities) {
                    e.actif = e.id === entityId;
                    this.log.debug(e.id, e.name, e.actif);
                }
            }
            return new ApiResponseInterface(false, 200, this.gettoken(user));
        } catch (errors) {
            this.log.error(errors);
            return new ApiResponseInterface(true, 500, {message: 'Une erreur est suvenue pendant la modification de l\'utilisateur'}, errors);
        }
    }

    /**
     * Like/Unlike a user
     * @param {*} username
     * @returns
     */
    async deleteAccount(username: string) {
        this.log.debug('deleteAccount', username);
        try {
            const user = await this.userModel.findOne({username});
            if (!user) {
                return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors du like de l\'utilisateur'});
            }
            const questions = await this.questionModel.find({author: user._id});
            for (const q of questions) {
                q.author = null;
                await this.questionModel.findByIdAndUpdate(q._id, q, {new: true});
            }
            const answers = await this.answerModel.find({author: user._id});
            for (const q of answers) {
                q.author = null;
                await this.answerModel.findByIdAndUpdate(q._id, q, {new: true});
            }
            const notifs = await this.notifModel.find({user: user._id});
            for (const q of notifs) {
                await this.answerModel.findByIdAndDelete(q._id);
            }
            const codes = await this.secretCodeModel.find({user: user._id});
            for (const q of codes) {
                await this.answerModel.findByIdAndDelete(q._id);
            }
            await this.userModel.findByIdAndDelete(user._id);
            return new ApiResponseInterface(false, 200, "ok");
        } catch (errors) {
            this.log.error(errors);
            return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la récupération des utilisateurs'}, errors);
        }
    }

    async contactUser(body: IContactUser, username: string) {
        try {

            this.log.debug(body.user);
            const contact = await this.userModel.findOne({_id: body.user});
            if (!contact) {
                return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de l\'import des questions'});
            }
            this.log.debug(username);
            const user = await this.userModel.findOne({username});
            if (!user) {
                return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de l\'import des questions'});
            }

            // Check nb contacts entre les 2
            const contacts = await this.contactModel.find({
                $and: [
                    {from: user._id},
                    {to: contact._id}
                ]
            });
            if (contacts) {
                let nb = 0;
                const nbJours = parseInt((process.env.CONTACT_NB_JOURS || '30'), 10);
                const nbContact = parseInt((process.env.CONTACT_NB_CONTACT || '5'), 10);
                for (const c of contacts) {
                    const date = new Date();
                    date.setDate(date.getDate() - nbJours);
                    if (c.createdAt > date) {
                        nb++;
                    }
                }
                if (nb > nbContact) {
                    return new ApiResponseInterface(true, 500, {message: 'Vous avez déjà envoyé plusieurs messages à cette personne'});
                }
            }

            // Check nb contacts du demandeur
            const userContacts = await this.contactModel.find({
                $and: [
                    {from: user._id}
                ]
            });
            if (userContacts) {
                let nb = 0;
                const nbJours = parseInt((process.env.CONTACT_NB_JOURS_USER || '30'), 10);
                const nbContact = parseInt((process.env.CONTACT_NB_CONTACT_USER || '30'), 10);
                for (const c of contacts) {
                    const date = new Date();
                    date.setDate(date.getDate() - nbJours);
                    if (c.createdAt > date) {
                        nb++;
                    }
                }
                if (nb > nbContact) {
                    return new ApiResponseInterface(true, 500, {message: 'Vous avez atteint votre quotat de demande de contact'});
                }
            }

            // Envoi du mail au contact
            await this.emailService.sendEmail(
                contact.email,
                "Demande de contact",
                "Demande de contact",
                {
                    urlSite: process.env.APP_URL,
                    email: body.email,
                    user: body.name,
                    message: body.message
                },
                "contact-user.html"
            ).catch(err => {
                this.log.error(err);
                return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue pendant l\'envoi du mail'}, err.errors);
            });

            // Envoi de la copie au demandeur
            if (contact.anonyme) {
                // Si contact anonyme
                await this.emailService.sendEmail(
                    body.email,
                    "Demande de contact",
                    "Demande de contact",
                    {
                        urlSite: process.env.APP_URL,
                        message: body.message
                    },
                    "contact-user-copy-anonyme.html"
                ).catch(err => {
                    this.log.error(err);
                    return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue pendant l\'envoi du mail'}, err.errors);
                });
            } else {
                // Si contact non anonyme
                await this.emailService.sendEmail(
                    body.email,
                    "Demande de contact",
                    "Demande de contact",
                    {
                        urlSite: process.env.APP_URL,
                        user: contact.firstName + " " + contact.lastName,
                        message: body.message
                    },
                    "contact-user-copy.html"
                ).catch(err => {
                    this.log.error(err);
                    return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue pendant l\'envoi du mail'}, err.errors);
                });
            }
            await this.contactModel.create({
                from: user._id,
                to: contact._id
            });
        } catch (errors) {
            this.log.error(errors);
            return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la demande de contact'}, errors);
        }
    }
}

export {UserService};
