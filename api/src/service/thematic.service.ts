import Thematic from '../model/thematic.model';
import {IThematic} from '../type/thematic.interface';
import {ApiResponseInterface} from '../type/api-response.interface';
import {inject, injectable} from "inversify";
import {Model} from 'mongoose';
import {Service} from './service';
import {Logger} from 'tslog';
import {DialogflowService} from './dialogflow.service';

@injectable()
class ThematicService extends Service {

    constructor(@inject("Thematic") protected model: Model<IThematic>,
                @inject("Logger") protected log: Logger,
                @inject("DialogflowService") protected dialogflowService: DialogflowService) {
        super(model, log);
    }

    async getAll(query: any) {
        try {
            const items = await this.model.find().populate({path: 'questionCount', match: {valid: true}});
            return new ApiResponseInterface(false, 200, items)
        } catch (errors) {
            return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la récupération des thématiques'}, errors)
        }
    }

    async getAllActive() {
        try {
            const items = await this.model.find({active: true}).populate({path: 'questionCount', match: {valid: true}});
            return new ApiResponseInterface(false, 200, items)
        } catch (errors) {
            return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la récupération des thématiques'}, errors)
        }
    }

    async getSpecific(id: string) {
        try {
            const item = await this.model.findById(id).populate({path: 'questionCount', match: {valid: true}});
            return new ApiResponseInterface(false, 200, item)
        } catch (errors) {
            return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la récupération de la thématique'}, errors)
        }
    }

    async findNewQuestionThematic(text: string) {
        try {
            const thematicKey: any = await this.dialogflowService.searchThematic(text);
            this.log.debug(thematicKey);
            const thematic = await this.model.findOne({key: thematicKey});
            this.log.debug(thematic);
            return new ApiResponseInterface(true, 200, thematic ? [thematic] : []);
        } catch (errors) {
            return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la récupération de la thématique'}, errors);
        }
    }

    async insertListThematic(list: IThematic[]) {
        try {
            const intents = await this.dialogflowService.listThematic();
            for (const thematic of list) {
                let dialogflowName;
                const i = intents ? intents.filter(x => x && x.displayName === thematic.key)[0] : null;
                if (i) {
                    dialogflowName = i.name;
                } else {
                    dialogflowName = await this.dialogflowService.insertThematic(thematic.key);
                }
                thematic.dialogflowName = dialogflowName ? dialogflowName : '';

                super.insert(thematic, '');
            }
            return new ApiResponseInterface(true, 200, true);
        } catch (errors) {
            return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la récupération de la thématique'}, errors);
        }
    }

    /**
     * Like/Unlike a specific thematic
     * @param {*} form
     * @returns
     */
    async likeThematic(form: { id: string, userId: string }) {
        this.log.debug('likeThematic', form);
        try {
            const thematic = await this.model.findById(form.id);
            if (!thematic) {
                return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors du like de l\'utilisateur'});
            }
            this.log.debug(thematic);
            if (!thematic.followers) {
                thematic.followers = [];
            }
            if (!thematic.followers.includes(form.userId)) {
                thematic.followers.push(form.userId);
            } else {
                thematic.followers.splice(thematic.followers.indexOf(form.userId), 1);
            }
            thematic.save();
            return new ApiResponseInterface(false, 200, thematic);
        } catch (errors) {
            this.log.error(errors);
            return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors du like de la question'}, errors);
        }
    }

    /*
    async find(): Promise<ApiResponse> {
      try {
        log('service');
        const items = await this.model.find(); // .populate('questionCount');
        return new ApiResponse(false, 200, items);
      } catch (error) {
        return new ApiResponse(true, 200, 'error');
      }
    }

     */
}

export {ThematicService};
