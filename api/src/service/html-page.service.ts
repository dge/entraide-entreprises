import {inject, injectable} from 'inversify';
import {Model} from 'mongoose';
import {Service} from './service';
import {ApiResponseInterface} from '../type/api-response.interface';
import {IAnswer} from '../type/answer.interface';
import {Logger} from 'tslog';
import {NotificationService} from './notification.service';
import {IQuestion} from '../type/question.interface';
import {IUser} from '../type/user.interface';
import {IHtmlPage} from '../type/html-page.interface';

@injectable()
class HtmlPageService extends Service {

  constructor(@inject("HtmlPage") protected model: Model<IHtmlPage>,
              @inject("Logger") protected log: Logger) {
    super(model, log);
  }


  async findByCode(code: string): Promise<ApiResponseInterface> {
    try {
      const items = await this.model.find({code});
      return new ApiResponseInterface(false, 200, items);
    } catch (error) {
      this.log.error(error);
      return new ApiResponseInterface(true, 200, 'error');
    }
  }


}

export {HtmlPageService};
