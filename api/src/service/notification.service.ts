import {inject, injectable} from 'inversify';
import {Model} from 'mongoose';
import {Service} from './service';
import {EmailService} from './email.service';
import {Logger} from 'tslog';
import {INotification} from '../type/notification.interface';
import {IQuestion} from '../type/question.interface';
import {IUser} from '../type/user.interface';
import {IAnswer} from '../type/answer.interface';
import {ApiResponseInterface} from '../type/api-response.interface';

@injectable()
class NotificationService extends Service {

  constructor(@inject("Notification") protected model: Model<INotification>,
              @inject("User") protected userModel: Model<IUser>,
              @inject("Question") protected questionModel: Model<IQuestion>,
              @inject("Answer") protected answerModel: Model<IAnswer>,
              @inject("EmailService") protected emailService: EmailService,
              @inject("Logger") protected log: Logger) {
    super(model, log);
  }


  async getAll(username: string): Promise<ApiResponseInterface> {
    try {
      this.log.debug(username);
      const user = await this.userModel.findOne({username});
      if (!user) {
        return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la récupération des questions'});
      }
      const items = await this.model.find({user: user._id});
      return new ApiResponseInterface(false, 200, items);
    } catch (errors) {
      this.log.error(errors);
      return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la récupération des objets'}, errors);
    }
  }

  async deleteAll(username: string) {
    try {
      const user = await this.userModel.findOne({username});
      if (!user) {
        return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la suppression des notifications'});
      }
      await this.model.deleteMany({user: user._id});
      return new ApiResponseInterface(false, 200, true);
    } catch (errors) {
      this.log.error(errors);
      return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la suppression des notifications'}, errors);
    }
  }

  async notifyCreateQuestion(questionId: string) {
    this.log.debug(questionId);
    const question: IQuestion | null = await this.questionModel.findById(questionId)
      .populate({
        path: 'author',
        select: ['username', 'firstName', 'lastName', 'anonyme', 'followers']
      })
      .populate({
        path: 'thematic',
        select: ['label', 'followers']
      }) as IQuestion;

    if (!question) {
      return;
    }
    let userIds: string[] = [];
    if (question.author.followers) {
      userIds = question.author.followers;
    }
    if (question.thematic.followers) {
      for (const user of question.thematic.followers) {
        if (userIds.indexOf(user) === -1 && user !== question.author._id) {
          userIds.push(user);
        }
      }
    }
    this.log.debug(userIds);

    if (userIds.length > 0) {
      const users = await this.userModel.find({_id: {"$in": userIds}});
      this.log.debug(users);
      const auth = question.author.anonyme ? question.author.username : question.author.firstName + ' ' + question.author.lastName
      const message = `${auth} a posé la question <em>${question.label}</em> sur la thématique <em>${question.thematic.label}</em>`;
      this.log.debug(message);
      for (const u of users) {
        const notification = {
          user: u._id,
          message,
          type: 'creationQuestion',
          question: questionId
        }
        this.log.debug(u.email);

        // Notif Appli
        if (((u as IUser).notification
          && (u as IUser).notification.thematic_createquestion_appli
          && question.thematic.followers
          && question.thematic.followers.indexOf(u._id) !== -1) ||
          ((u as IUser).notification
            && (u as IUser).notification.user_createquestion_appli
            && question.author.followers
            && question.author.followers.indexOf(u._id) !== -1)) {
          this.log.debug('add notif');
          await this.model.create(notification);
        }

        // Notif Mail
        if (((u as IUser).notification
          && (u as IUser).notification.thematic_createquestion_email
          && question.thematic.followers
          && question.thematic.followers.indexOf(u._id) !== -1) ||
          ((u as IUser).notification
            && (u as IUser).notification.user_createquestion_email
            && question.author.followers
            && question.author.followers.indexOf(u._id) !== -1)) {
          this.log.debug('send mail');
          await this.sendNotification((u as IUser).email, notification as INotification);
        }
      }
    }
  }

  async notifyAnswerQuestion(answerId: string) {
    this.log.debug('notifyAnswerQuestion', answerId);
    const answer: IAnswer | null = await this.answerModel.findById(answerId)
      .populate({
        path: 'author',
        select: ['username', 'firstName', 'lastName', 'anonyme', 'followers']
      })
      .populate({
        path: 'question',
        select: ['author', 'label'],
        populate: {path: 'thematic', select: ['label']}
      }) as IAnswer;

    if (!answer) {
      return;
    }
    const userIds: string[] = answer.author.followers;
    userIds.push((answer.question as IQuestion).author);
    const users = await this.userModel.find({_id: {"$in": userIds}});
    this.log.debug(users);

    const auth = answer.author.anonyme ? answer.author.username : answer.author.firstName + ' ' + answer.author.lastName
    const message = `${auth} a répondu à la question <em>${answer.question.label}</em> sur la thématique <em>${answer.question.thematic.label}</em>`;
    this.log.debug(message);
    for (const u of users) {
      const notification = {
        user: u._id,
        message,
        type: 'reponseQuestion',
        question: answer.question.id
      }
      this.log.debug(u.email);

      // Notif followers thematic, followers auteur response, auteur question
      // Notif Appli
      if (((u as IUser).notification
        && (u as IUser).notification.thematic_answerquestion_appli
        && answer.question.thematic.followers
        && answer.question.thematic.followers.indexOf(u._id) !== -1) ||
        ((u as IUser).notification
          && (u as IUser).notification.user_answerquestion_appli
          && answer.author.followers
          && answer.author.followers.indexOf(u._id) !== -1) ||
        ((u as IUser).notification
          && (u as IUser).notification.myquestion_answer_appli
          && answer.question.author._id === u._id)) {
        this.log.debug('add notif');
        await this.model.create(notification);
      }

      // Notif Mail
      if (((u as IUser).notification
        && (u as IUser).notification.thematic_answerquestion_email
        && answer.question.thematic.followers
        && answer.question.thematic.followers.indexOf(u._id) !== -1) ||
        ((u as IUser).notification
          && (u as IUser).notification.user_answerquestion_email
          && answer.author.followers
          && answer.author.followers.indexOf(u._id) !== -1) ||
        ((u as IUser).notification
          && (u as IUser).notification.myquestion_answer_email
          && answer.question.author._id === u._id)) {
        this.log.debug('send mail');
        await this.sendNotification((u as IUser).email, notification as INotification);
      }
    }
  }

  async notifyCommentAnswer(answerId: string) {
    this.log.debug('notifyCommentAnswer', answerId);
    const answer: IAnswer | null = await this.answerModel.findById(answerId)
      .populate({
        path: 'author',
        select: ['username', 'firstName', 'lastName', 'anonyme', 'followers']
      })
      .populate({
        path: 'parent',
        select: ['author', 'label'],
        populate: {
          path: 'question',
          select: ['author', 'label'],
          populate: {
            path: 'thematic',
            select: ['label']
          }
        }
      }) as IAnswer;

    if (!answer) {
      return;
    }
    const userIds: string[] = answer.author.followers;
    userIds.push((answer.parent.question as IQuestion).author);
    const users = await this.userModel.find({_id: {"$in": userIds}});
    this.log.debug(users);

    const auth = answer.author.anonyme ? answer.author.username : answer.author.firstName + ' ' + answer.author.lastName
    const message = `${auth} a commenté une réponse de la question <em>${answer.question.label}</em> sur la thématique <em>${answer.question.thematic.label}</em>`;
    this.log.debug(message);
    for (const u of users) {
      const notification = {
        user: u._id,
        message,
        type: 'commentReponse',
        question: answer.parent.question.id
      }
      this.log.debug(u.email);

      // Notif followers thematic, followers auteur response, auteur question
      // Notif Appli
      if (((u as IUser).notification
        && (u as IUser).notification.thematic_answerquestion_appli
        && answer.question.thematic.followers
        && answer.question.thematic.followers.indexOf(u._id) !== -1) ||
        ((u as IUser).notification
          && (u as IUser).notification.user_answerquestion_appli
          && answer.author.followers
          && answer.author.followers.indexOf(u._id) !== -1) ||
        ((u as IUser).notification
          && (u as IUser).notification.myanswer_comment_appli
          && answer.parent.author._id === u._id) ||
        ((u as IUser).notification
          && (u as IUser).notification.myquestion_answer_appli
          && answer.parent.question.author._id === u._id)) {
        this.log.debug('add notif');
        await this.model.create(notification);
      }

      // Notif Mail
      if (((u as IUser).notification
        && (u as IUser).notification.thematic_answerquestion_email
        && answer.question.thematic.followers
        && answer.question.thematic.followers.indexOf(u._id) !== -1) ||
        ((u as IUser).notification
          && (u as IUser).notification.user_answerquestion_email
          && answer.author.followers
          && answer.author.followers.indexOf(u._id) !== -1) ||
        ((u as IUser).notification
          && (u as IUser).notification.myquestion_answer_email
          && answer.question.author._id === u._id) ||
        ((u as IUser).notification
          && (u as IUser).notification.myquestion_answer_email
          && answer.question.author._id === u._id)) {
        this.log.debug('send mail');
        await this.sendNotification((u as IUser).email, notification as INotification);
      }
    }
  }

  async sendNotification(email: string, notification: INotification): Promise<boolean> {
    try {
      this.emailService.sendEmail(
        email,
        "Notification sur Entraide Entreprises",
        "Une notification a été créée",
        {
          notification: notification.message
            .replace(new RegExp('<em>', 'g'), '')
            .replace(new RegExp('</em>', 'g'), ''),
          urlQuestion: `${process.env.APP_URL}/questions/question/${notification.question}`,
          urlSite: process.env.APP_URL
        },
        "notification.html"
      ).catch(err => {
        this.log.error(err);
        return false;
      });
      return true;
    } catch (e) {
      this.log.error(e);
      return false;
    }
  }

}

export {NotificationService};
