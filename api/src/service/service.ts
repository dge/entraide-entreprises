import {injectable} from 'inversify';
import mongoose, {Model} from 'mongoose';
import {ApiResponseInterface} from '../type/api-response.interface';
import {Logger} from 'tslog';

@injectable()
export class Service {

  constructor(protected model: Model<any>, protected log: Logger) {
  }

  async find(): Promise<ApiResponseInterface> {
    try {
      const items = await this.model.find();
      return new ApiResponseInterface(false, 200, items);
    } catch (error) {
      this.log.error(error);
      return new ApiResponseInterface(true, 200, 'error');
    }
  }

  async getAll(query: any): Promise<ApiResponseInterface> {
    let {skip, limit} = query;
    skip = skip ? Number(skip) : 0;
    limit = limit ? Number(limit) : 10;
    delete query.skip;
    delete query.limit;
    if (query._id) {
      try {
        query._id = new mongoose.mongo.ObjectId(query._id);
      } catch (error) {
        this.log.debug("not able to generate mongoose id with content", query._id);
      }
    }
    try {
      const items = await this.model
        .find(query)
        .skip(skip * limit)
        .limit(limit);

      return new ApiResponseInterface(false, 200, items);
    } catch (errors) {
      this.log.error(errors);
      return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la récupération des objets'}, errors);
    }
  }

  async getSpecific(id: any): Promise<ApiResponseInterface> {
    try {
      const item = await this.model.findById(id);
      return new ApiResponseInterface(false, 200, item);
    } catch (errors) {
      this.log.error(errors);
      return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la récupération de l\'objet'}, errors);
    }
  }

  async insert(data: any, ip: string): Promise<ApiResponseInterface> {
    try {
      this.log.debug(data);
      const item = await this.model.create(data);
      if (!item) {
        return new ApiResponseInterface(true, 404, {message: 'L\'objet n\'a pas pu être récupéré'});
      }
      return new ApiResponseInterface(false, 201, item);
    } catch (errors) {
      this.log.error(errors);
      return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la création de l\'objet'}, errors);
    }
  }

  async update(id: string, data: any): Promise<ApiResponseInterface> {
    try {
      const item = await this.model.findByIdAndUpdate(id, data, {new: true});
      return new ApiResponseInterface(false, 202, item);
    } catch (errors) {
      this.log.error(errors);
      return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la mise à jour de l\'objet'}, errors);
    }
  }

  async delete(id: string): Promise<ApiResponseInterface> {
    try {
      const item = await this.model.findByIdAndDelete(id);
      if (!item)
        return new ApiResponseInterface(true, 404, {message: 'L\'objet n\'a pas pu être récupéré'});
      return new ApiResponseInterface(false, 202, item);
    } catch (errors) {
      this.log.error(errors);
      return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la suppression de l\'objet'}, errors);
    }
  }
}
