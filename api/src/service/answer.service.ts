import {inject, injectable} from 'inversify';
import {Model} from 'mongoose';
import {Service} from './service';
import {ApiResponseInterface} from '../type/api-response.interface';
import {IAnswer} from '../type/answer.interface';
import {Logger} from 'tslog';
import {NotificationService} from './notification.service';
import {IQuestion} from '../type/question.interface';
import {IUser} from '../type/user.interface';

@injectable()
class AnswerService extends Service {

  constructor(@inject("Answer") protected model: Model<IAnswer>,
              @inject("User") protected userModel: Model<IUser>,
              @inject("Question") protected questionModel: Model<IQuestion>,
              @inject("Logger") protected log: Logger,
              @inject("NotificationService") protected notificationService: NotificationService) {
    super(model, log);
  }

  async getAllByQuestion(questionId: string) {
    try {
      const answers = await this.model
        .find({question: questionId})
        .populate({
          path: 'answers', populate: {
            path: 'author',
            select: ['username', 'firstName', 'lastName', 'anonyme', 'profilePicture', 'entities', 'allowContact'],
            populate: ['answerCount', 'questionCount']
          }
        })
        .populate({
          path: 'author',
          select: ['username', 'firstName', 'lastName', 'anonyme', 'profilePicture', 'entities', 'allowContact'],
          populate: ['answerCount', 'questionCount']
        });
      return new ApiResponseInterface(false, 200, answers);
    } catch (errors) {
      return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la récupération des réponses'}, errors);
    }
  }

  async getSpecific(id: string) {
    try {
      const item = await this.model.findById(id)
        .populate({
          path: 'author',
          select: ['username', 'firstName', 'lastName', 'anonyme', 'profilePicture', 'entities', 'allowContact'],
          populate: ['answerCount', 'questionCount']
        });
      return new ApiResponseInterface(false, 200, item);
    } catch (errors) {
      return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la récupération de la réponse'}, errors);
    }
  }

  async insert(data: IAnswer, ip: string) {
    try {
      data.ip = ip;
      const resp = await super.insert(data, ip);
      if (data.question) {
        const item = await this.questionModel.findById(data.question);
        if (item && item.isUrgent) {
          item.isUrgent = false;
          item.save();
        }
      }
      return this.getSpecific(resp.data.id);
    } catch (error) {
      return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la création de la réponse'}, error);
    }
  }

  /**
   * Like/Unlike a specific answer
   * @param {*} id
   * @returns
   */
  async likeAnswer(form: { id: string, userId: string }) {
    try {
      const answer = await this.model.findById(form.id);
      if (!answer) {
        return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors du like de la réponse'});
      }
      // test if user already likes the question
      if (!answer.likes.includes(form.userId)) {
        answer.likes.push(form.userId);
      } else {
        answer.likes.splice(answer.likes.indexOf(form.userId), 1);
      }
      answer.save();
      return new ApiResponseInterface(false, 200, answer);
    } catch (errors) {
      return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors du like de la réponse'}, errors);
    }
  }

  async notValidAnswers(skip: number, limit: number) {
    try {
      skip = skip ? Number(skip) : 0;
      limit = limit ? Number(limit) : 10;
      this.log.debug('notValidAnswers', skip, limit);
      const answers = await this.model
        .find({$or: [{valid: false}, {valid: {$exists: false}}]})
        .populate({
          path: 'answers', populate: {
            path: 'author',
            select: ['username', 'firstName', 'lastName', 'anonyme', 'profilePicture', 'allowContact'],
            populate: ['answerCount', 'questionCount']
          }
        })
        .populate({
          path: 'author',
          select: ['username', 'firstName', 'lastName', 'anonyme', 'profilePicture', 'allowContact'],
          populate: ['answerCount', 'questionCount']
        })
        .sort('createdAt')
        .skip(skip * limit)
        .limit(limit);
      const total = await this.model.countDocuments({$or: [{valid: false}, {valid: {$exists: false}}]});
      this.log.debug('total answer', total);
      return new ApiResponseInterface(false, 200, {
        answers,
        total
      });
    } catch (errors) {
      return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la récupération des réponses'}, errors);
    }
  }

  async alertAnswers(skip: number, limit: number) {
    try {
      skip = skip ? Number(skip) : 0;
      limit = limit ? Number(limit) : 10;
      this.log.debug('alertAnswers', skip, limit);
      const answers = await this.model
        .find({alert: true})
        .populate({
          path: 'answers', populate: {
            path: 'author',
            select: ['username', 'firstName', 'lastName', 'anonyme', 'profilePicture', 'allowContact'],
            populate: ['answerCount', 'questionCount']
          }
        })
        .populate({
          path: 'author',
          select: ['username', 'firstName', 'lastName', 'anonyme', 'profilePicture', 'allowContact'],
          populate: ['answerCount', 'questionCount']
        })
        .sort('createdAt')
        .skip(skip * limit)
        .limit(limit);
      const total = await this.model.countDocuments({alert: true});
      this.log.debug('total answer', total);
      return new ApiResponseInterface(false, 200, {
        answers,
        total
      });
    } catch (errors) {
      return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la récupération des réponses'}, errors);
    }
  }

  async moderate(id: string): Promise<ApiResponseInterface> {
    try {
      this.log.debug('moderate answer', id);
      const item = await this.model.findById(id);
      if (!item) {
        return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la mise à jour de l\'objet'});
      }
      item.message = '';
      item.moderate = true;
      item.valid = true;
      item.alert = false;
      await this.model.findByIdAndUpdate(id, item, {new: true});
      return new ApiResponseInterface(false, 200, item);
    } catch (errors) {
      this.log.error(errors);
      return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la mise à jour de l\'objet'}, errors);
    }
  }

  async validate(id: string): Promise<ApiResponseInterface> {
    try {
      this.log.debug('validate answer', id);
      const item = await this.model.findById(id);
      if (!item) {
        return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la mise à jour de l\'objet'});
      }
      item.valid = true;
      item.alert = false;
      await this.model.findByIdAndUpdate(id, item, {new: true});
      if (item.parent) {
        await this.notificationService.notifyCommentAnswer(id);
      } else {
        await this.notificationService.notifyAnswerQuestion(id);
      }
      return new ApiResponseInterface(false, 200, item);
    } catch (errors) {
      this.log.error(errors);
      return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la mise à jour de l\'objet'}, errors);
    }
  }

  async alert(id: string): Promise<ApiResponseInterface> {
    try {
      this.log.debug('alert answer', id);
      const item = await this.model.findById(id);
      if (!item) {
        return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la mise à jour de l\'objet'});
      }
      item.alert = true;
      await this.model.findByIdAndUpdate(id, item, {new: true});
      return new ApiResponseInterface(false, 200, item);
    } catch (errors) {
      this.log.error(errors);
      return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la mise à jour de l\'objet'}, errors);
    }
  }

  async deleteMyAnswer(id: string, username: string): Promise<ApiResponseInterface> {
    try {
      this.log.debug('alert answer', id);
      const user = await this.userModel.findOne({username});
      if (!user) {
        return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la mise à jour de l\'objet'});
      }
      const item = await this.model.findById(id);
      if (!item || item.author.toString() !== user._id.toString()) {
        return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la mise à jour de l\'objet'});
      }
      item.message = null;
      await this.model.findByIdAndUpdate(id, item, {new: true});
      return new ApiResponseInterface(false, 200, item);
    } catch (errors) {
      this.log.error(errors);
      return new ApiResponseInterface(true, 500, {message: 'Une erreur est survenue lors de la mise à jour de l\'objet'}, errors);
    }
  }


}

export {AnswerService};
