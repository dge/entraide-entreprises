import {NextFunction, Response} from "express";
import jwt from 'jsonwebtoken';
import {ICustomRequest} from '../type/custom-request.interface';

export function authMiddlewareFactory(req: ICustomRequest, res: Response, next: NextFunction) {
  const SECRET = process.env.TOKEN_KEY || "token-entraide-secret-key";
  const authHeader = req.headers.authorization;
  if (authHeader) {
    const token = authHeader.split(' ')[1];
    jwt.verify(token, SECRET, (err: any, user: any) => {
      if (err) {
        return res.sendStatus(403);
      }
      req.user = user;
      next();
    });
  } else {
    res.sendStatus(401);
  }
}
/*
function authMiddlewareFactory() {
  return (req: ICustomRequest, res: Response, next: NextFunction) => {
    (async () => {
      const SECRET = process.env.TOKEN_KEY || "token-entraide-secret-key";
      const authHeader = req.headers.authorization;
      if (authHeader) {
        const token = authHeader.split(' ')[1];
        jwt.verify(token, SECRET, (err: any, user: any) => {
          if (err) {
            return res.sendStatus(403);
          }
          req.user = user;
          next();
        });
      } else {
        res.sendStatus(401);
      }
    })();
  };
}

const authMiddleware = authMiddlewareFactory();

export { authMiddleware };
*/
