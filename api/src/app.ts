import "reflect-metadata";
import cors from "cors";
import express from "express";
import mongoose, {Model} from "mongoose";
import dotenv from "dotenv";
import {ThematicService} from './service/thematic.service';
import {Container} from 'inversify';
import {InversifyExpressServer} from 'inversify-express-utils';
import {Service} from './service/service';
import {IThematic} from './type/thematic.interface';
import Thematic from './model/thematic.model';
import Question from './model/question.model';
import {IQuestion} from './type/question.interface';
import User from './model/user.model';
import {IUser} from './type/user.interface';
import Answer from './model/answer.model';
import {IAnswer} from './type/answer.interface';
import {Logger} from "tslog";
import {ISecretCode} from './type/secret-code.interface';
import SecretCode from './model/secret-code.model';
import Notification from './model/notification.model';
import {TLogLevelName} from 'tslog/src/interfaces';
import {QuestionService} from './service/question.service';
import {AnswerService} from './service/answer.service';
import {UserService} from './service/user.service';
import {EmailService} from './service/email.service';
import {DialogflowService} from './service/dialogflow.service';
import {INotification} from './type/notification.interface';
import {NotificationService} from './service/notification.service';


// declare metadata by @controller annotation
import "./controller/thematic.controller";
import "./controller/answer.controller";
import "./controller/question.controller";
import "./controller/user.controller";
import "./controller/notification.controller";
import "./controller/html-page.controller";
import "./controller/controller";
import {IContact} from './type/contact.interface';
import Contact from './model/contact.model';
import {IHtmlPage} from './type/html-page.interface';
import HtmlPage from './model/html-page.model';
import {HtmlPageService} from './service/html-page.service';

class App {
  public app: express.Application;
  log: Logger;

  constructor() {
    dotenv.config();
    this.app = express();
    const logLevel: TLogLevelName = (process.env.MIN_LOG_LEVEL as TLogLevelName) || 'info';
    this.log = new Logger({name: "myLogger", overwriteConsole: true, minLevel: logLevel});
    this.setConfig();
    this.setMongoConfig();
  }

  private setConfig() {
    this.log.debug("set config");
    // set up container
    const container = new Container();

    // set up bindings
    container.bind<ThematicService>('ThematicService').to(ThematicService);
    container.bind<QuestionService>('QuestionService').to(QuestionService);
    container.bind<AnswerService>('AnswerService').to(AnswerService);
    container.bind<UserService>('UserService').to(UserService);
    container.bind<EmailService>('EmailService').to(EmailService);
    container.bind<HtmlPageService>('HtmlPageService').to(HtmlPageService);
    container.bind<Service>('Service').to(Service);
    container.bind<DialogflowService>('DialogflowService').to(DialogflowService);
    container.bind<NotificationService>('NotificationService').to(NotificationService);
    container.bind<Model<IThematic>>('Thematic').toConstantValue(Thematic);
    container.bind<Model<IQuestion>>('Question').toConstantValue(Question);
    container.bind<Model<IAnswer>>('Answer').toConstantValue(Answer);
    container.bind<Model<IUser>>('User').toConstantValue(User);
    container.bind<Model<ISecretCode>>('SecretCode').toConstantValue(SecretCode);
    container.bind<Model<INotification>>('Notification').toConstantValue(Notification);
    container.bind<Model<IContact>>('Contact').toConstantValue(Contact);
    container.bind<Model<IHtmlPage>>('HtmlPage').toConstantValue(HtmlPage);
    container.bind<Logger>('Logger').toConstantValue(this.log);

    // create server
    const server = new InversifyExpressServer(container);
    server.setConfig((app) => {
      this.log.debug("set server config");
      app.use(cors());
      app.use(express.json({limit: '50mb'}));
      app.use(express.urlencoded({limit: '50mb', extended: true}));
      app.use((req, res, next) => {
        res.header(
          "Access-Control-Allow-Headers",
          "Origin, Content-Type, Accept"
        );
        next();
      });
      app.set('trust proxy', true);
    });
    this.app = server.build();
  }

  private setMongoConfig() {
    this.log.debug("set mongo config");
    mongoose.Promise = global.Promise;
    const url = process.env.MONGODB_URI || `mongodb://localhost:27017/ENTRAIDE_ENTREPRISE`;
    mongoose.connect(url, {
      useNewUrlParser: true,
      useCreateIndex: true,
      useUnifiedTopology: true,
      useFindAndModify: false
    }, () => {
      this.log.debug("connected to database");
    })
    mongoose.set("toJSON", {
      virtuals: true,
      transform: (_: any, converted: any) => {
        converted.id = converted._id;
        delete converted._id;
        delete converted.__v;
        if(converted.email) {
          delete converted.email;
        }
        if(converted.password) {
          delete converted.password;
        }
      },
    });
  }
}

export default new App().app;
