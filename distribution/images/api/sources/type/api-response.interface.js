"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiResponseInterface = void 0;
var ApiResponseInterface = /** @class */ (function () {
    function ApiResponseInterface(error, statusCode, data, errors) {
        this.error = error;
        this.statusCode = statusCode;
        this.data = data;
        this.errors = errors;
    }
    return ApiResponseInterface;
}());
exports.ApiResponseInterface = ApiResponseInterface;
//# sourceMappingURL=api-response.interface.js.map