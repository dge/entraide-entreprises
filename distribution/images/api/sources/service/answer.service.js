"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AnswerService = void 0;
var inversify_1 = require("inversify");
var mongoose_1 = require("mongoose");
var service_1 = require("./service");
var api_response_interface_1 = require("../type/api-response.interface");
var tslog_1 = require("tslog");
var notification_service_1 = require("./notification.service");
var AnswerService = /** @class */ (function (_super) {
    __extends(AnswerService, _super);
    function AnswerService(model, userModel, questionModel, log, notificationService) {
        var _this = _super.call(this, model, log) || this;
        _this.model = model;
        _this.userModel = userModel;
        _this.questionModel = questionModel;
        _this.log = log;
        _this.notificationService = notificationService;
        return _this;
    }
    AnswerService.prototype.getAllByQuestion = function (questionId) {
        return __awaiter(this, void 0, void 0, function () {
            var answers, errors_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.model
                                .find({ question: questionId })
                                .populate({
                                path: 'answers', populate: {
                                    path: 'author',
                                    select: ['username', 'firstName', 'lastName', 'anonyme', 'profilePicture', 'entities', 'allowContact'],
                                    populate: ['answerCount', 'questionCount']
                                }
                            })
                                .populate({
                                path: 'author',
                                select: ['username', 'firstName', 'lastName', 'anonyme', 'profilePicture', 'entities', 'allowContact'],
                                populate: ['answerCount', 'questionCount']
                            })];
                    case 1:
                        answers = _a.sent();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 200, answers)];
                    case 2:
                        errors_1 = _a.sent();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la récupération des réponses' }, errors_1)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AnswerService.prototype.getSpecific = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var item, errors_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.model.findById(id)
                                .populate({
                                path: 'author',
                                select: ['username', 'firstName', 'lastName', 'anonyme', 'profilePicture', 'entities', 'allowContact'],
                                populate: ['answerCount', 'questionCount']
                            })];
                    case 1:
                        item = _a.sent();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 200, item)];
                    case 2:
                        errors_2 = _a.sent();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la récupération de la réponse' }, errors_2)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AnswerService.prototype.insert = function (data, ip) {
        return __awaiter(this, void 0, void 0, function () {
            var resp, item, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 4, , 5]);
                        data.ip = ip;
                        return [4 /*yield*/, _super.prototype.insert.call(this, data, ip)];
                    case 1:
                        resp = _a.sent();
                        if (!data.question) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.questionModel.findById(data.question)];
                    case 2:
                        item = _a.sent();
                        if (item && item.isUrgent) {
                            item.isUrgent = false;
                            item.save();
                        }
                        _a.label = 3;
                    case 3: return [2 /*return*/, this.getSpecific(resp.data.id)];
                    case 4:
                        error_1 = _a.sent();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la création de la réponse' }, error_1)];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Like/Unlike a specific answer
     * @param {*} id
     * @returns
     */
    AnswerService.prototype.likeAnswer = function (form) {
        return __awaiter(this, void 0, void 0, function () {
            var answer, errors_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.model.findById(form.id)];
                    case 1:
                        answer = _a.sent();
                        if (!answer) {
                            return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors du like de la réponse' })];
                        }
                        // test if user already likes the question
                        if (!answer.likes.includes(form.userId)) {
                            answer.likes.push(form.userId);
                        }
                        else {
                            answer.likes.splice(answer.likes.indexOf(form.userId), 1);
                        }
                        answer.save();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 200, answer)];
                    case 2:
                        errors_3 = _a.sent();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors du like de la réponse' }, errors_3)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AnswerService.prototype.notValidAnswers = function (skip, limit) {
        return __awaiter(this, void 0, void 0, function () {
            var answers, total, errors_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        skip = skip ? Number(skip) : 0;
                        limit = limit ? Number(limit) : 10;
                        this.log.debug('notValidAnswers', skip, limit);
                        return [4 /*yield*/, this.model
                                .find({ $or: [{ valid: false }, { valid: { $exists: false } }] })
                                .populate({
                                path: 'answers', populate: {
                                    path: 'author',
                                    select: ['username', 'firstName', 'lastName', 'anonyme', 'profilePicture', 'allowContact'],
                                    populate: ['answerCount', 'questionCount']
                                }
                            })
                                .populate({
                                path: 'author',
                                select: ['username', 'firstName', 'lastName', 'anonyme', 'profilePicture', 'allowContact'],
                                populate: ['answerCount', 'questionCount']
                            })
                                .sort('createdAt')
                                .skip(skip * limit)
                                .limit(limit)];
                    case 1:
                        answers = _a.sent();
                        return [4 /*yield*/, this.model.countDocuments({ $or: [{ valid: false }, { valid: { $exists: false } }] })];
                    case 2:
                        total = _a.sent();
                        this.log.debug('total answer', total);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 200, {
                                answers: answers,
                                total: total
                            })];
                    case 3:
                        errors_4 = _a.sent();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la récupération des réponses' }, errors_4)];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    AnswerService.prototype.alertAnswers = function (skip, limit) {
        return __awaiter(this, void 0, void 0, function () {
            var answers, total, errors_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        skip = skip ? Number(skip) : 0;
                        limit = limit ? Number(limit) : 10;
                        this.log.debug('alertAnswers', skip, limit);
                        return [4 /*yield*/, this.model
                                .find({ alert: true })
                                .populate({
                                path: 'answers', populate: {
                                    path: 'author',
                                    select: ['username', 'firstName', 'lastName', 'anonyme', 'profilePicture', 'allowContact'],
                                    populate: ['answerCount', 'questionCount']
                                }
                            })
                                .populate({
                                path: 'author',
                                select: ['username', 'firstName', 'lastName', 'anonyme', 'profilePicture', 'allowContact'],
                                populate: ['answerCount', 'questionCount']
                            })
                                .sort('createdAt')
                                .skip(skip * limit)
                                .limit(limit)];
                    case 1:
                        answers = _a.sent();
                        return [4 /*yield*/, this.model.countDocuments({ alert: true })];
                    case 2:
                        total = _a.sent();
                        this.log.debug('total answer', total);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 200, {
                                answers: answers,
                                total: total
                            })];
                    case 3:
                        errors_5 = _a.sent();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la récupération des réponses' }, errors_5)];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    AnswerService.prototype.moderate = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var item, errors_6;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        this.log.debug('moderate answer', id);
                        return [4 /*yield*/, this.model.findById(id)];
                    case 1:
                        item = _a.sent();
                        if (!item) {
                            return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la mise à jour de l\'objet' })];
                        }
                        item.message = '';
                        item.moderate = true;
                        item.valid = true;
                        item.alert = false;
                        return [4 /*yield*/, this.model.findByIdAndUpdate(id, item, { new: true })];
                    case 2:
                        _a.sent();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 200, item)];
                    case 3:
                        errors_6 = _a.sent();
                        this.log.error(errors_6);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la mise à jour de l\'objet' }, errors_6)];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    AnswerService.prototype.validate = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var item, errors_7;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 7, , 8]);
                        this.log.debug('validate answer', id);
                        return [4 /*yield*/, this.model.findById(id)];
                    case 1:
                        item = _a.sent();
                        if (!item) {
                            return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la mise à jour de l\'objet' })];
                        }
                        item.valid = true;
                        item.alert = false;
                        return [4 /*yield*/, this.model.findByIdAndUpdate(id, item, { new: true })];
                    case 2:
                        _a.sent();
                        if (!item.parent) return [3 /*break*/, 4];
                        return [4 /*yield*/, this.notificationService.notifyCommentAnswer(id)];
                    case 3:
                        _a.sent();
                        return [3 /*break*/, 6];
                    case 4: return [4 /*yield*/, this.notificationService.notifyAnswerQuestion(id)];
                    case 5:
                        _a.sent();
                        _a.label = 6;
                    case 6: return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 200, item)];
                    case 7:
                        errors_7 = _a.sent();
                        this.log.error(errors_7);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la mise à jour de l\'objet' }, errors_7)];
                    case 8: return [2 /*return*/];
                }
            });
        });
    };
    AnswerService.prototype.alert = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var item, errors_8;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        this.log.debug('alert answer', id);
                        return [4 /*yield*/, this.model.findById(id)];
                    case 1:
                        item = _a.sent();
                        if (!item) {
                            return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la mise à jour de l\'objet' })];
                        }
                        item.alert = true;
                        return [4 /*yield*/, this.model.findByIdAndUpdate(id, item, { new: true })];
                    case 2:
                        _a.sent();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 200, item)];
                    case 3:
                        errors_8 = _a.sent();
                        this.log.error(errors_8);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la mise à jour de l\'objet' }, errors_8)];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    AnswerService.prototype.deleteMyAnswer = function (id, username) {
        return __awaiter(this, void 0, void 0, function () {
            var user, item, errors_9;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 4, , 5]);
                        this.log.debug('alert answer', id);
                        return [4 /*yield*/, this.userModel.findOne({ username: username })];
                    case 1:
                        user = _a.sent();
                        if (!user) {
                            return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la mise à jour de l\'objet' })];
                        }
                        return [4 /*yield*/, this.model.findById(id)];
                    case 2:
                        item = _a.sent();
                        if (!item || item.author.toString() !== user._id.toString()) {
                            return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la mise à jour de l\'objet' })];
                        }
                        item.message = null;
                        return [4 /*yield*/, this.model.findByIdAndUpdate(id, item, { new: true })];
                    case 3:
                        _a.sent();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 200, item)];
                    case 4:
                        errors_9 = _a.sent();
                        this.log.error(errors_9);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la mise à jour de l\'objet' }, errors_9)];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    AnswerService = __decorate([
        inversify_1.injectable(),
        __param(0, inversify_1.inject("Answer")),
        __param(1, inversify_1.inject("User")),
        __param(2, inversify_1.inject("Question")),
        __param(3, inversify_1.inject("Logger")),
        __param(4, inversify_1.inject("NotificationService")),
        __metadata("design:paramtypes", [mongoose_1.Model,
            mongoose_1.Model,
            mongoose_1.Model,
            tslog_1.Logger,
            notification_service_1.NotificationService])
    ], AnswerService);
    return AnswerService;
}(service_1.Service));
exports.AnswerService = AnswerService;
//# sourceMappingURL=answer.service.js.map