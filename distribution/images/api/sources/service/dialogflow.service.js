"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DialogflowService = void 0;
var inversify_1 = require("inversify");
var dialogflow_1 = __importDefault(require("@google-cloud/dialogflow"));
var uuid_1 = require("uuid");
var tslog_1 = require("tslog");
var DialogflowService = /** @class */ (function () {
    function DialogflowService(log) {
        this.log = log;
    }
    DialogflowService.prototype.searchThematic = function (text) {
        if (text === void 0) { text = ''; }
        return __awaiter(this, void 0, void 0, function () {
            var sessionId, sessionClient, sessionPath, request, response, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.log.debug(text);
                        if (!process.env.DIALOGFLOW_THEMATIC_CREDENTIAL) return [3 /*break*/, 5];
                        sessionId = uuid_1.v4();
                        sessionClient = new dialogflow_1.default.SessionsClient({
                            keyFilename: process.env.DIALOGFLOW_THEMATIC_CREDENTIAL
                        });
                        sessionPath = sessionClient.projectAgentSessionPath(process.env.DIALOGFLOW_THEMATIC_AGENT_SESSION_PATH, sessionId);
                        request = {
                            session: sessionPath,
                            queryInput: {
                                text: {
                                    text: text,
                                    languageCode: process.env.DIALOGFLOW_LANGUAGE,
                                },
                            },
                        };
                        this.log.debug(request);
                        response = void 0;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, sessionClient.detectIntent(request)];
                    case 2:
                        response = (_a.sent())[0];
                        this.log.debug(response);
                        return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        this.log.error(e_1);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/, response.queryResult && response.queryResult.intent ? response.queryResult.intent.displayName : ''];
                    case 5: return [2 /*return*/, ''];
                }
            });
        });
    };
    DialogflowService.prototype.insertThematic = function (label) {
        return __awaiter(this, void 0, void 0, function () {
            var sessionId, intentsClient, response, e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!process.env.DIALOGFLOW_THEMATIC_CREDENTIAL) return [3 /*break*/, 5];
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        sessionId = uuid_1.v4();
                        intentsClient = new dialogflow_1.default.IntentsClient({
                            keyFilename: process.env.DIALOGFLOW_THEMATIC_CREDENTIAL
                        });
                        return [4 /*yield*/, intentsClient.createIntent({
                                parent: 'projects/' + process.env.DIALOGFLOW_THEMATIC_AGENT_SESSION_PATH + '/agent',
                                intent: {
                                    displayName: label
                                },
                                languageCode: process.env.DIALOGFLOW_LANGUAGE
                            })];
                    case 2:
                        response = (_a.sent())[0];
                        this.log.debug(response);
                        return [2 /*return*/, response.name];
                    case 3:
                        e_2 = _a.sent();
                        this.log.error(e_2);
                        return [3 /*break*/, 4];
                    case 4: return [3 /*break*/, 6];
                    case 5: return [2 /*return*/, ''];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    DialogflowService.prototype.updateThematic = function (name, labelList) {
        return __awaiter(this, void 0, void 0, function () {
            var sessionId, intentsClient, response, _i, labelList_1, label, e_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!process.env.DIALOGFLOW_THEMATIC_CREDENTIAL) return [3 /*break*/, 5];
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        sessionId = uuid_1.v4();
                        intentsClient = new dialogflow_1.default.IntentsClient({
                            keyFilename: process.env.DIALOGFLOW_THEMATIC_CREDENTIAL
                        });
                        return [4 /*yield*/, intentsClient.getIntent({
                                name: name,
                                languageCode: process.env.DIALOGFLOW_LANGUAGE,
                                intentView: 'INTENT_VIEW_FULL'
                            })];
                    case 2:
                        response = (_a.sent())[0];
                        this.log.debug(response);
                        if (!response.trainingPhrases) {
                            response.trainingPhrases = [];
                        }
                        for (_i = 0, labelList_1 = labelList; _i < labelList_1.length; _i++) {
                            label = labelList_1[_i];
                            response.trainingPhrases.push({ parts: [{ text: label }], type: 'EXAMPLE' });
                        }
                        intentsClient.updateIntent({ intent: response });
                        return [2 /*return*/, response.name];
                    case 3:
                        e_3 = _a.sent();
                        this.log.error(e_3);
                        return [3 /*break*/, 4];
                    case 4: return [3 /*break*/, 6];
                    case 5: return [2 /*return*/, ''];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    DialogflowService.prototype.listThematic = function () {
        return __awaiter(this, void 0, void 0, function () {
            var sessionId, intentsClient, response, e_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!process.env.DIALOGFLOW_THEMATIC_CREDENTIAL) return [3 /*break*/, 5];
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        sessionId = uuid_1.v4();
                        intentsClient = new dialogflow_1.default.IntentsClient({
                            keyFilename: process.env.DIALOGFLOW_THEMATIC_CREDENTIAL
                        });
                        return [4 /*yield*/, intentsClient.listIntents({
                                parent: 'projects/' + process.env.DIALOGFLOW_THEMATIC_AGENT_SESSION_PATH + '/agent',
                                languageCode: process.env.DIALOGFLOW_LANGUAGE
                            })];
                    case 2:
                        response = (_a.sent())[0];
                        this.log.debug(response);
                        return [2 /*return*/, response];
                    case 3:
                        e_4 = _a.sent();
                        this.log.error(e_4);
                        return [3 /*break*/, 4];
                    case 4: return [3 /*break*/, 6];
                    case 5: return [2 /*return*/, null];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    DialogflowService.prototype.searchQuestion = function (text) {
        if (text === void 0) { text = ''; }
        return __awaiter(this, void 0, void 0, function () {
            var sessionId, sessionClient, sessionPath, request, response, e_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.log.debug(text);
                        if (!process.env.DIALOGFLOW_QUESTION_CREDENTIAL) return [3 /*break*/, 5];
                        sessionId = uuid_1.v4();
                        sessionClient = new dialogflow_1.default.SessionsClient({
                            keyFilename: process.env.DIALOGFLOW_QUESTION_CREDENTIAL
                        });
                        sessionPath = sessionClient.projectAgentSessionPath(process.env.DIALOGFLOW_QUESTION_AGENT_SESSION_PATH, sessionId);
                        request = {
                            session: sessionPath,
                            queryInput: {
                                text: {
                                    text: text,
                                    languageCode: process.env.DIALOGFLOW_LANGUAGE,
                                },
                            },
                        };
                        this.log.debug(request);
                        response = void 0;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, sessionClient.detectIntent(request)];
                    case 2:
                        response = (_a.sent())[0];
                        this.log.debug(response);
                        return [3 /*break*/, 4];
                    case 3:
                        e_5 = _a.sent();
                        this.log.error(e_5);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/, response && response.queryResult && response.queryResult.intent ? response.queryResult.intent.displayName : ''];
                    case 5: return [2 /*return*/, ''];
                }
            });
        });
    };
    DialogflowService.prototype.createIntent = function (text) {
        return __awaiter(this, void 0, void 0, function () {
            var sessionId, intentsClient, response, e_6;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!process.env.DIALOGFLOW_QUESTION_CREDENTIAL) return [3 /*break*/, 5];
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        sessionId = uuid_1.v4();
                        intentsClient = new dialogflow_1.default.IntentsClient({
                            keyFilename: process.env.DIALOGFLOW_QUESTION_CREDENTIAL
                        });
                        return [4 /*yield*/, intentsClient.createIntent({
                                parent: 'projects/' + process.env.DIALOGFLOW_QUESTION_AGENT_SESSION_PATH + '/agent',
                                intent: {
                                    displayName: text,
                                    trainingPhrases: [{ parts: [{ text: text }] }]
                                },
                                languageCode: process.env.DIALOGFLOW_LANGUAGE
                            })];
                    case 2:
                        response = (_a.sent())[0];
                        this.log.debug(response);
                        return [3 /*break*/, 4];
                    case 3:
                        e_6 = _a.sent();
                        this.log.error(e_6);
                        return [3 /*break*/, 4];
                    case 4: return [3 /*break*/, 6];
                    case 5: return [2 /*return*/, null];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    DialogflowService = __decorate([
        inversify_1.injectable(),
        __param(0, inversify_1.inject("Logger")),
        __metadata("design:paramtypes", [tslog_1.Logger])
    ], DialogflowService);
    return DialogflowService;
}());
exports.DialogflowService = DialogflowService;
//# sourceMappingURL=dialogflow.service.js.map