"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ThematicService = void 0;
var api_response_interface_1 = require("../type/api-response.interface");
var inversify_1 = require("inversify");
var mongoose_1 = require("mongoose");
var service_1 = require("./service");
var tslog_1 = require("tslog");
var dialogflow_service_1 = require("./dialogflow.service");
var ThematicService = /** @class */ (function (_super) {
    __extends(ThematicService, _super);
    function ThematicService(model, log, dialogflowService) {
        var _this = _super.call(this, model, log) || this;
        _this.model = model;
        _this.log = log;
        _this.dialogflowService = dialogflowService;
        return _this;
    }
    ThematicService.prototype.getAll = function (query) {
        return __awaiter(this, void 0, void 0, function () {
            var items, errors_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.model.find().populate({ path: 'questionCount', match: { valid: true } })];
                    case 1:
                        items = _a.sent();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 200, items)];
                    case 2:
                        errors_1 = _a.sent();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la récupération des thématiques' }, errors_1)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ThematicService.prototype.getAllActive = function () {
        return __awaiter(this, void 0, void 0, function () {
            var items, errors_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.model.find({ active: true }).populate({ path: 'questionCount', match: { valid: true } })];
                    case 1:
                        items = _a.sent();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 200, items)];
                    case 2:
                        errors_2 = _a.sent();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la récupération des thématiques' }, errors_2)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ThematicService.prototype.getSpecific = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var item, errors_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.model.findById(id).populate({ path: 'questionCount', match: { valid: true } })];
                    case 1:
                        item = _a.sent();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 200, item)];
                    case 2:
                        errors_3 = _a.sent();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la récupération de la thématique' }, errors_3)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ThematicService.prototype.findNewQuestionThematic = function (text) {
        return __awaiter(this, void 0, void 0, function () {
            var thematicKey, thematic, errors_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        return [4 /*yield*/, this.dialogflowService.searchThematic(text)];
                    case 1:
                        thematicKey = _a.sent();
                        this.log.debug(thematicKey);
                        return [4 /*yield*/, this.model.findOne({ key: thematicKey })];
                    case 2:
                        thematic = _a.sent();
                        this.log.debug(thematic);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 200, thematic ? [thematic] : [])];
                    case 3:
                        errors_4 = _a.sent();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la récupération de la thématique' }, errors_4)];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    ThematicService.prototype.insertListThematic = function (list) {
        return __awaiter(this, void 0, void 0, function () {
            var intents, _loop_1, this_1, _i, list_1, thematic, errors_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 6, , 7]);
                        return [4 /*yield*/, this.dialogflowService.listThematic()];
                    case 1:
                        intents = _a.sent();
                        _loop_1 = function (thematic) {
                            var dialogflowName, i;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        dialogflowName = void 0;
                                        i = intents ? intents.filter(function (x) { return x && x.displayName === thematic.key; })[0] : null;
                                        if (!i) return [3 /*break*/, 1];
                                        dialogflowName = i.name;
                                        return [3 /*break*/, 3];
                                    case 1: return [4 /*yield*/, this_1.dialogflowService.insertThematic(thematic.key)];
                                    case 2:
                                        dialogflowName = _b.sent();
                                        _b.label = 3;
                                    case 3:
                                        thematic.dialogflowName = dialogflowName ? dialogflowName : '';
                                        _super.prototype.insert.call(this_1, thematic, '');
                                        return [2 /*return*/];
                                }
                            });
                        };
                        this_1 = this;
                        _i = 0, list_1 = list;
                        _a.label = 2;
                    case 2:
                        if (!(_i < list_1.length)) return [3 /*break*/, 5];
                        thematic = list_1[_i];
                        return [5 /*yield**/, _loop_1(thematic)];
                    case 3:
                        _a.sent();
                        _a.label = 4;
                    case 4:
                        _i++;
                        return [3 /*break*/, 2];
                    case 5: return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 200, true)];
                    case 6:
                        errors_5 = _a.sent();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la récupération de la thématique' }, errors_5)];
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Like/Unlike a specific thematic
     * @param {*} form
     * @returns
     */
    ThematicService.prototype.likeThematic = function (form) {
        return __awaiter(this, void 0, void 0, function () {
            var thematic, errors_6;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.log.debug('likeThematic', form);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.model.findById(form.id)];
                    case 2:
                        thematic = _a.sent();
                        if (!thematic) {
                            return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors du like de l\'utilisateur' })];
                        }
                        this.log.debug(thematic);
                        if (!thematic.followers) {
                            thematic.followers = [];
                        }
                        if (!thematic.followers.includes(form.userId)) {
                            thematic.followers.push(form.userId);
                        }
                        else {
                            thematic.followers.splice(thematic.followers.indexOf(form.userId), 1);
                        }
                        thematic.save();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 200, thematic)];
                    case 3:
                        errors_6 = _a.sent();
                        this.log.error(errors_6);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors du like de la question' }, errors_6)];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    ThematicService = __decorate([
        inversify_1.injectable(),
        __param(0, inversify_1.inject("Thematic")),
        __param(1, inversify_1.inject("Logger")),
        __param(2, inversify_1.inject("DialogflowService")),
        __metadata("design:paramtypes", [mongoose_1.Model,
            tslog_1.Logger,
            dialogflow_service_1.DialogflowService])
    ], ThematicService);
    return ThematicService;
}(service_1.Service));
exports.ThematicService = ThematicService;
//# sourceMappingURL=thematic.service.js.map