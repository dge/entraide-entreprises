"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.QuestionService = void 0;
var inversify_1 = require("inversify");
var mongoose_1 = require("mongoose");
var exceljs_1 = __importDefault(require("exceljs"));
var service_1 = require("./service");
var api_response_interface_1 = require("../type/api-response.interface");
var tslog_1 = require("tslog");
var dialogflow_service_1 = require("./dialogflow.service");
var notification_service_1 = require("./notification.service");
var uuid_1 = require("uuid");
var email_service_1 = require("./email.service");
var QuestionService = /** @class */ (function (_super) {
    __extends(QuestionService, _super);
    function QuestionService(questionModel, userModel, answerModel, thematicModel, log, emailService, dialogflowService, notificationService) {
        var _this = _super.call(this, questionModel, log) || this;
        _this.questionModel = questionModel;
        _this.userModel = userModel;
        _this.answerModel = answerModel;
        _this.thematicModel = thematicModel;
        _this.log = log;
        _this.emailService = emailService;
        _this.dialogflowService = dialogflowService;
        _this.notificationService = notificationService;
        return _this;
    }
    QuestionService.prototype.init = function () {
        return __awaiter(this, void 0, void 0, function () {
            var users, nbUsers, _i, users_1, user, questions, nbQuestions, _a, questions_1, q, answers, nbAnswers, _b, answers_1, q, errors_1;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _c.trys.push([0, 16, , 17]);
                        return [4 /*yield*/, this.userModel.find()];
                    case 1:
                        users = _c.sent();
                        nbUsers = 0;
                        _i = 0, users_1 = users;
                        _c.label = 2;
                    case 2:
                        if (!(_i < users_1.length)) return [3 /*break*/, 5];
                        user = users_1[_i];
                        if (!user.entity) return [3 /*break*/, 4];
                        user.entities = [{
                                id: uuid_1.v4(),
                                name: user.entity.name,
                                siret: user.entity.siret,
                                url: user.entity.url,
                                address: user.entity.address,
                                addressBis: user.entity.addressBis,
                                codePostal: user.entity.codePostal,
                                city: user.entity.city,
                                actif: true
                            }];
                        return [4 /*yield*/, this.userModel.findByIdAndUpdate(user.id, user, { new: true })];
                    case 3:
                        _c.sent();
                        nbUsers++;
                        _c.label = 4;
                    case 4:
                        _i++;
                        return [3 /*break*/, 2];
                    case 5: return [4 /*yield*/, this.model.find()];
                    case 6:
                        questions = _c.sent();
                        nbQuestions = 0;
                        _a = 0, questions_1 = questions;
                        _c.label = 7;
                    case 7:
                        if (!(_a < questions_1.length)) return [3 /*break*/, 10];
                        q = questions_1[_a];
                        q.valid = true;
                        return [4 /*yield*/, this.model.findByIdAndUpdate(q.id, q, { new: true })];
                    case 8:
                        _c.sent();
                        nbQuestions++;
                        _c.label = 9;
                    case 9:
                        _a++;
                        return [3 /*break*/, 7];
                    case 10: return [4 /*yield*/, this.answerModel.find()];
                    case 11:
                        answers = _c.sent();
                        nbAnswers = 0;
                        _b = 0, answers_1 = answers;
                        _c.label = 12;
                    case 12:
                        if (!(_b < answers_1.length)) return [3 /*break*/, 15];
                        q = answers_1[_b];
                        q.valid = true;
                        return [4 /*yield*/, this.answerModel.findByIdAndUpdate(q.id, q, { new: true })];
                    case 13:
                        _c.sent();
                        nbAnswers++;
                        _c.label = 14;
                    case 14:
                        _b++;
                        return [3 /*break*/, 12];
                    case 15: return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 202, { nbUsers: nbUsers, nbQuestions: nbQuestions, nbAnswers: nbAnswers })];
                    case 16:
                        errors_1 = _c.sent();
                        this.log.error(errors_1);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la suppression de l\'objet' }, errors_1)];
                    case 17: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Gets the favorite questions
     * @returns
     */
    QuestionService.prototype.getFavoriteQuestions = function () {
        return __awaiter(this, void 0, void 0, function () {
            var questionIds, qs_1, questions, errors_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 4, , 5]);
                        return [4 /*yield*/, this.answerModel.find().distinct('question')];
                    case 1:
                        questionIds = _a.sent();
                        return [4 /*yield*/, this.questionModel
                                .aggregate([
                                { $match: { _id: { "$in": questionIds }, valid: true } },
                                {
                                    "$project": {
                                        _id: 0,
                                        id: "$_id",
                                        label: 1,
                                        author: 1,
                                        nbLikes: { $cond: { if: { $isArray: "$likes" }, then: { $size: "$likes" }, else: 0 } }
                                    }
                                },
                                { "$sort": { "nbLikes": -1 } },
                                { "$limit": 10 }
                            ])];
                    case 2:
                        qs_1 = _a.sent();
                        return [4 /*yield*/, this.questionModel
                                .find({ _id: { "$in": qs_1.map(function (x) { return x.id; }) } })
                                .populate({
                                path: 'author',
                                select: ['username', 'firstName', 'lastName', 'anonyme', 'profilePicture', 'allowContact'],
                                populate: ['answerCount', 'questionCount']
                            })
                                .populate({ path: 'thematic', select: ['label', 'icon'] })
                                .populate({ path: 'answerCount', match: { valid: true } })];
                    case 3:
                        questions = _a.sent();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 200, questions)];
                    case 4:
                        errors_2 = _a.sent();
                        this.log.error(errors_2);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la récupération de la question' }, errors_2)];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Gets all the question for a specific thematics
     * @param {*} thematicsId
     * @param {*} skip
     * @param {*} limit
     * @returns
     */
    QuestionService.prototype.getAllByThematics = function (thematicsId, skip, limit, faq, urgent) {
        return __awaiter(this, void 0, void 0, function () {
            var filters, questions, total, errors_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.log.debug(thematicsId, skip, limit);
                        skip = skip ? Number(skip) : 0;
                        limit = limit ? Number(limit) : 10;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 4, , 5]);
                        filters = { thematic: thematicsId, valid: true };
                        if (faq) {
                            filters.isFaq = { $ne: true };
                        }
                        if (urgent) {
                            filters.isUrgent = true;
                        }
                        return [4 /*yield*/, this.questionModel
                                .find(filters)
                                .populate({
                                path: 'author',
                                select: ['username', 'firstName', 'lastName', 'anonyme', 'profilePicture', 'allowContact'],
                                populate: ['answerCount', 'questionCount']
                            })
                                .populate({ path: 'thematic', select: ['label', 'icon'] })
                                .populate({ path: 'answerCount', match: { valid: true } })
                                .sort('-createdAt')
                                .skip(skip * limit)
                                .limit(limit)];
                    case 2:
                        questions = _a.sent();
                        return [4 /*yield*/, this.questionModel.countDocuments({ thematic: thematicsId })];
                    case 3:
                        total = _a.sent();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 200, {
                                questions: questions,
                                total: total
                            })];
                    case 4:
                        errors_3 = _a.sent();
                        this.log.error(errors_3);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la récupération des questions' }, errors_3)];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Get a specific question by its id
     * @param {*} id
     * @returns
     */
    QuestionService.prototype.getSpecific = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var item, errors_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.questionModel.findById(id)
                                .populate({
                                path: 'author',
                                select: ['username', 'firstName', 'lastName', 'anonyme', 'profilePicture', 'entities', 'allowContact'],
                                populate: ['answerCount', 'questionCount']
                            })
                                .populate({ path: 'thematic', select: ['label', 'icon'] })
                                .populate({ path: 'answerCount', match: { valid: true } })
                                .populate({ path: 'answers' })];
                    case 1:
                        item = _a.sent();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 200, item)];
                    case 2:
                        errors_4 = _a.sent();
                        this.log.error(errors_4);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la récupération de la question' }, errors_4)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Like/Unlike a specific question
     * @param {*} form
     * @returns
     */
    QuestionService.prototype.likeQuestion = function (form) {
        return __awaiter(this, void 0, void 0, function () {
            var question, errors_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.log.debug(form);
                        return [4 /*yield*/, this.questionModel.findById(form.id)];
                    case 1:
                        question = _a.sent();
                        if (!question) {
                            return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors du like de la question' })];
                        }
                        // test if user already likes the question
                        if (!question.likes.includes(form.userId)) {
                            question.likes.push(form.userId);
                        }
                        else {
                            question.likes.splice(question.likes.indexOf(form.userId), 1);
                        }
                        question.save();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 200, question)];
                    case 2:
                        errors_5 = _a.sent();
                        this.log.error(errors_5);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors du like de la question' }, errors_5)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Search all question containing key words and sort them by accuracy
     * @param {*} keywords
     * @returns
     */
    QuestionService.prototype.findSimilarQuestions = function (keywords) {
        return __awaiter(this, void 0, void 0, function () {
            var label, questions_2, mongoQuestions, item, errors_6;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.log.debug(keywords);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 6, , 7]);
                        return [4 /*yield*/, this.dialogflowService.searchQuestion(keywords)];
                    case 2:
                        label = _a.sent();
                        this.log.debug(label);
                        questions_2 = null;
                        if (!label) return [3 /*break*/, 4];
                        return [4 /*yield*/, this.questionModel
                                .find({ label: label, valid: true })
                                .populate({
                                path: 'author',
                                select: ['username', 'firstName', 'lastName', 'anonyme', 'profilePicture', 'allowContact'],
                                populate: ['answerCount', 'questionCount']
                            })
                                .populate({ path: 'thematic', select: ['label', 'icon'] })
                                .populate({ path: 'answerCount', match: { valid: true } })
                                .populate({ path: 'answers' })];
                    case 3:
                        questions_2 = _a.sent();
                        _a.label = 4;
                    case 4: return [4 /*yield*/, this.questionModel.find({ $text: { $search: keywords, $caseSensitive: false } }, { score: { $meta: "textScore" } }).sort({ score: { $meta: "textScore" } })
                            .populate({
                            path: 'author',
                            select: ['username', 'firstName', 'lastName', 'anonyme', 'profilePicture', 'allowContact'],
                            populate: ['answerCount', 'questionCount']
                        })
                            .populate({ path: 'thematic', select: ['label', 'icon'] })
                            .populate({ path: 'answerCount', match: { valid: true } })
                            .populate({ path: 'answers' })];
                    case 5:
                        mongoQuestions = _a.sent();
                        if (questions_2 && questions_2[0] && mongoQuestions) {
                            item = mongoQuestions.filter(function (x) { return x._id === questions_2[0]._id; })[0];
                            if (item) {
                                mongoQuestions.splice(mongoQuestions.indexOf(item), 1);
                            }
                        }
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 200, {
                                dialogflow: questions_2,
                                mongo: mongoQuestions
                            })];
                    case 6:
                        errors_6 = _a.sent();
                        this.log.debug(errors_6);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la recherche de questions' }, errors_6)];
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Gets the questions asked by tue user
     * @returns
     */
    QuestionService.prototype.askedQuestions = function (id, username) {
        return __awaiter(this, void 0, void 0, function () {
            var user_1, filter, questions, errors_7;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        return [4 /*yield*/, this.userModel.findById(id)];
                    case 1:
                        user_1 = _a.sent();
                        if (!user_1) {
                            return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la récupération des questions' })];
                        }
                        filter = { author: user_1._id };
                        if (user_1.username !== username) {
                            filter.valid = true;
                        }
                        return [4 /*yield*/, this.questionModel
                                .find(filter)
                                .populate({
                                path: 'author',
                                select: ['username', 'firstName', 'lastName', 'anonyme', 'profilePicture', 'allowContact'],
                                populate: ['answerCount', 'questionCount']
                            })
                                .populate({ path: 'thematic', select: ['label', 'icon'] })
                                .populate({ path: 'answerCount', match: { valid: true } })
                                .sort('-createdAt')];
                    case 2:
                        questions = _a.sent();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 200, questions.filter(function (x) { return user_1.username === username || !x.anonyme; }))];
                    case 3:
                        errors_7 = _a.sent();
                        this.log.error(errors_7);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la récupération des questions' }, errors_7)];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Gets the questions asked by tue user
     * @returns
     */
    QuestionService.prototype.answeredQuestions = function (id, username) {
        return __awaiter(this, void 0, void 0, function () {
            var user_2, answer1Ids, answer2Ids, questionIds, questions, errors_8;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 6, , 7]);
                        return [4 /*yield*/, this.userModel.findById(id)];
                    case 1:
                        user_2 = _a.sent();
                        if (!user_2) {
                            return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la récupération des questions' })];
                        }
                        return [4 /*yield*/, this.answerModel.find({ author: user_2._id }).distinct('_id', { "_id": { $ne: null } })];
                    case 2:
                        answer1Ids = _a.sent();
                        return [4 /*yield*/, this.answerModel.find({ author: user_2._id }).distinct('parent', { "parent": { $ne: null } })];
                    case 3:
                        answer2Ids = _a.sent();
                        return [4 /*yield*/, this.answerModel
                                .find({ _id: { "$in": answer1Ids.concat(answer2Ids) } })
                                .distinct('question', { "question": { $ne: null } })];
                    case 4:
                        questionIds = _a.sent();
                        return [4 /*yield*/, this.questionModel
                                .find({ _id: { "$in": questionIds }, valid: true })
                                .populate({
                                path: 'author',
                                select: ['username', 'firstName', 'lastName', 'anonyme', 'profilePicture', 'allowContact'],
                                populate: ['answerCount', 'questionCount']
                            })
                                .populate({ path: 'thematic', select: ['label', 'icon'] })
                                .populate({ path: 'answerCount', match: { valid: true } })
                                .sort('-createdAt')];
                    case 5:
                        questions = _a.sent();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 200, questions.filter(function (x) { return user_2.username === username || !x.anonyme; }))];
                    case 6:
                        errors_8 = _a.sent();
                        this.log.error(errors_8);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la récupération des questions' }, errors_8)];
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    QuestionService.prototype.insert = function (data, ip) {
        return __awaiter(this, void 0, void 0, function () {
            var similarQuestions, item, thematic, errors_9;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 7, , 8]);
                        this.log.debug(data);
                        return [4 /*yield*/, this.findSimilarQuestions(data.label)];
                    case 1:
                        similarQuestions = _a.sent();
                        data.ip = ip;
                        return [4 /*yield*/, this.questionModel.create(data)];
                    case 2:
                        item = _a.sent();
                        if (!item) {
                            return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 404, { message: 'L\'objet n\'a pas pu être récupéré' })];
                        }
                        return [4 /*yield*/, this.thematicModel.findById(item.thematic)];
                    case 3:
                        thematic = _a.sent();
                        if (!(thematic && thematic.dialogflowName)) return [3 /*break*/, 5];
                        return [4 /*yield*/, this.dialogflowService.updateThematic(thematic.dialogflowName, [item.label])];
                    case 4:
                        _a.sent();
                        _a.label = 5;
                    case 5: return [4 /*yield*/, this.dialogflowService.createIntent(item.label)];
                    case 6:
                        _a.sent();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 201, {
                                item: item,
                                dialogflow: similarQuestions.data.dialogflow,
                                mongo: similarQuestions.data.mongo
                            })];
                    case 7:
                        errors_9 = _a.sent();
                        this.log.error(errors_9);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la création de l\'objet' }, errors_9)];
                    case 8: return [2 /*return*/];
                }
            });
        });
    };
    QuestionService.prototype.insertListQuestion = function (questionList, username) {
        return __awaiter(this, void 0, void 0, function () {
            var user, thematicList, errorList, thematicMap_1, _loop_1, this_1, i, errors_10;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 7, , 8]);
                        return [4 /*yield*/, this.userModel.findOne({ username: username })];
                    case 1:
                        user = _a.sent();
                        if (!user) {
                            return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de l\'import des questions' })];
                        }
                        return [4 /*yield*/, this.thematicModel.find()];
                    case 2:
                        thematicList = _a.sent();
                        errorList = [];
                        thematicMap_1 = {};
                        if (!questionList) return [3 /*break*/, 6];
                        _loop_1 = function (i) {
                            var item, mess, thematic, bddQuestion, question, answers, answer, question, response, answer;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        item = questionList[i];
                                        this_1.log.debug(item);
                                        if (!item.thematic || !item.message || !item.title || !item.answer) {
                                            mess = '';
                                            if (!item.thematic) {
                                                mess = 'Thématique non présente';
                                            }
                                            if (!item.title) {
                                                mess += (mess !== '' ? ' - ' : '') + 'Titre non présent';
                                            }
                                            if (!item.message) {
                                                mess += (mess !== '' ? ' - ' : '') + 'Message non présent';
                                            }
                                            if (!item.answer) {
                                                mess += (mess !== '' ? ' - ' : '') + 'Réponse non présente';
                                            }
                                            errorList.push({
                                                item: item,
                                                message: mess,
                                                ligne: i + 2
                                            });
                                            return [2 /*return*/, "continue"];
                                        }
                                        thematic = thematicList.filter(function (x) { return x.label === item.thematic.trim(); })[0];
                                        if (!thematic) {
                                            errorList.push({
                                                item: item,
                                                message: 'Thématique non trouvée',
                                                ligne: i + 2
                                            });
                                            return [2 /*return*/, "continue"];
                                        }
                                        return [4 /*yield*/, this_1.questionModel.find({ label: item.title })];
                                    case 1:
                                        bddQuestion = _b.sent();
                                        if (!(bddQuestion && bddQuestion.length > 0)) return [3 /*break*/, 3];
                                        question = bddQuestion[0];
                                        question.message = item.message;
                                        question.isFaq = true;
                                        question.valid = true;
                                        question.save();
                                        return [4 /*yield*/, this_1.answerModel.find({ question: question._id })];
                                    case 2:
                                        answers = _b.sent();
                                        if (answers && answers.length > 0) {
                                            answer = answers[0];
                                            answer.message = item.answer;
                                            answer.save();
                                        }
                                        return [3 /*break*/, 5];
                                    case 3:
                                        question = {
                                            label: item.title,
                                            message: item.message,
                                            author: user._id,
                                            thematic: thematic._id,
                                            likes: [],
                                            isFaq: true
                                        };
                                        return [4 /*yield*/, this_1.questionModel.create(question)];
                                    case 4:
                                        response = _b.sent();
                                        this_1.dialogflowService.createIntent(response.label);
                                        if (item.answer) {
                                            answer = {
                                                message: item.answer,
                                                author: user._id,
                                                parent: null,
                                                question: response._id,
                                                likes: []
                                            };
                                            this_1.answerModel.create(answer);
                                            if (thematic && thematic.dialogflowName) {
                                                if (!thematicMap_1[thematic.dialogflowName]) {
                                                    thematicMap_1[thematic.dialogflowName] = [];
                                                }
                                                thematicMap_1[thematic.dialogflowName].push(question.label);
                                            }
                                        }
                                        _b.label = 5;
                                    case 5: return [2 /*return*/];
                                }
                            });
                        };
                        this_1 = this;
                        i = 0;
                        _a.label = 3;
                    case 3:
                        if (!(i < questionList.length)) return [3 /*break*/, 6];
                        return [5 /*yield**/, _loop_1(i)];
                    case 4:
                        _a.sent();
                        _a.label = 5;
                    case 5:
                        i++;
                        return [3 /*break*/, 3];
                    case 6:
                        Object.keys(thematicMap_1).forEach(function (key) {
                            _this.dialogflowService.updateThematic(key, thematicMap_1[key]);
                        });
                        if (errorList.length > 0) {
                            return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 400, errorList)];
                        }
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 201, true)];
                    case 7:
                        errors_10 = _a.sent();
                        this.log.error(errors_10);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de l\'import des questions' }, errors_10)];
                    case 8: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Gets all the question to validate
     * @returns
     */
    QuestionService.prototype.notValidQuestions = function (skip, limit) {
        return __awaiter(this, void 0, void 0, function () {
            var questions, total, errors_11;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        skip = skip ? Number(skip) : 0;
                        limit = limit ? Number(limit) : 10;
                        this.log.debug('notValidQuestions', skip, limit);
                        return [4 /*yield*/, this.questionModel
                                .find({ $or: [{ valid: false }, { valid: { $exists: false } }] })
                                .populate({
                                path: 'author',
                                select: ['username', 'firstName', 'lastName', 'anonyme', 'profilePicture', 'allowContact'],
                                populate: ['answerCount', 'questionCount']
                            })
                                .populate({ path: 'thematic', select: ['label', 'icon'] })
                                .sort('createdAt')
                                .skip(skip * limit)
                                .limit(limit)];
                    case 1:
                        questions = _a.sent();
                        return [4 /*yield*/, this.questionModel.countDocuments({ $or: [{ valid: false }, { valid: { $exists: false } }] })];
                    case 2:
                        total = _a.sent();
                        this.log.debug('total questions', total);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 200, {
                                questions: questions,
                                total: total
                            })];
                    case 3:
                        errors_11 = _a.sent();
                        this.log.error(errors_11);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la récupération des questions' }, errors_11)];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    QuestionService.prototype.validate = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var item, i, errors_12;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 4, , 5]);
                        this.log.debug('validate', id);
                        return [4 /*yield*/, this.model.findById(id)];
                    case 1:
                        item = _a.sent();
                        if (!item) {
                            return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la mise à jour de l\'objet' })];
                        }
                        item.valid = true;
                        return [4 /*yield*/, this.model.findByIdAndUpdate(id, item, { new: true })];
                    case 2:
                        i = _a.sent();
                        return [4 /*yield*/, this.notificationService.notifyCreateQuestion(id)];
                    case 3:
                        _a.sent();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 202, i)];
                    case 4:
                        errors_12 = _a.sent();
                        this.log.error(errors_12);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la mise à jour de l\'objet' }, errors_12)];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    QuestionService.prototype.delete = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var answers, item, errors_13;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 4, , 5]);
                        return [4 /*yield*/, this.answerModel.find({ question: id })];
                    case 1:
                        answers = _a.sent();
                        return [4 /*yield*/, this.deleteAnswer(answers)];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, this.model.findByIdAndDelete(id)];
                    case 3:
                        item = _a.sent();
                        if (!item)
                            return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 404, { message: 'L\'objet n\'a pas pu être récupéré' })];
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 202, item)];
                    case 4:
                        errors_13 = _a.sent();
                        this.log.error(errors_13);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la suppression de l\'objet' }, errors_13)];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    QuestionService.prototype.deleteAnswer = function (answers) {
        return __awaiter(this, void 0, void 0, function () {
            var _i, answers_2, a;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _i = 0, answers_2 = answers;
                        _a.label = 1;
                    case 1:
                        if (!(_i < answers_2.length)) return [3 /*break*/, 6];
                        a = answers_2[_i];
                        if (!a.answers) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.deleteAnswer(a.answers)];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3: return [4 /*yield*/, this.answerModel.findByIdAndDelete(a.id)];
                    case 4:
                        _a.sent();
                        _a.label = 5;
                    case 5:
                        _i++;
                        return [3 /*break*/, 1];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    QuestionService.prototype.sendQuestion = function (body, username) {
        return __awaiter(this, void 0, void 0, function () {
            var question, user, errors_14;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        return [4 /*yield*/, this.model.findById(body.question)];
                    case 1:
                        question = _a.sent();
                        if (!question)
                            return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 404, { message: 'L\'objet n\'a pas pu être récupéré' })];
                        return [4 /*yield*/, this.userModel.findOne({ username: username })];
                    case 2:
                        user = _a.sent();
                        if (!user) {
                            return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors du partage de la question' })];
                        }
                        this.emailService.sendEmail(body.email, "Partage d'une question", "Partage d'une question", {
                            urlQuestion: process.env.APP_URL + "/questions/question/" + body.question,
                            urlSite: process.env.APP_URL,
                            user: user.firstName + " " + user.lastName,
                            question: question.label,
                            message: body.message
                        }, "send-question.html").catch(function (err) {
                            _this.log.error(err);
                            return new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue pendant l\'envoi du mail' }, err.errors);
                        });
                        return [3 /*break*/, 4];
                    case 3:
                        errors_14 = _a.sent();
                        this.log.error(errors_14);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors du partage de la question' }, errors_14)];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    QuestionService.prototype.importFile = function (file, username) {
        var _a;
        return __awaiter(this, void 0, void 0, function () {
            var workbook, content, worksheet, rowStartIndex, numberOfRows, rows, data;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        workbook = new exceljs_1.default.Workbook();
                        return [4 /*yield*/, workbook.xlsx.load(file.buffer)];
                    case 1:
                        content = _b.sent();
                        worksheet = content.worksheets[0];
                        rowStartIndex = 2;
                        numberOfRows = worksheet.rowCount - 1;
                        this.log.debug(numberOfRows);
                        rows = (_a = worksheet.getRows(rowStartIndex, numberOfRows)) !== null && _a !== void 0 ? _a : [];
                        data = rows.map(function (row) {
                            return {
                                // @ts-ignore
                                thematic: _this.getCellValue(row, 1),
                                // @ts-ignore
                                title: _this.getCellValue(row, 2),
                                // @ts-ignore
                                message: _this.getCellValue(row, 3),
                                // @ts-ignore
                                answer: _this.getCellValue(row, 3),
                            };
                        });
                        return [2 /*return*/, this.insertListQuestion(data, username)];
                }
            });
        });
    };
    QuestionService.prototype.getCellValue = function (row, cellIndex) {
        var cell = row.getCell(cellIndex);
        return cell.value ? cell.value.toString() : '';
    };
    ;
    /**
     * Gets all the question to validate
     * @returns
     */
    QuestionService.prototype.countModerationAction = function () {
        return __awaiter(this, void 0, void 0, function () {
            var noValidQuestions, noValidAnswers, alertAnswers, errors_15;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 4, , 5]);
                        return [4 /*yield*/, this.questionModel.countDocuments({ $or: [{ valid: false }, { valid: { $exists: false } }] })];
                    case 1:
                        noValidQuestions = _a.sent();
                        return [4 /*yield*/, this.answerModel.countDocuments({ $or: [{ valid: false }, { valid: { $exists: false } }] })];
                    case 2:
                        noValidAnswers = _a.sent();
                        return [4 /*yield*/, this.answerModel.countDocuments({ alert: true })];
                    case 3:
                        alertAnswers = _a.sent();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 200, {
                                noValidQuestions: noValidQuestions,
                                noValidAnswers: noValidAnswers,
                                alertAnswers: alertAnswers
                            })];
                    case 4:
                        errors_15 = _a.sent();
                        this.log.error(errors_15);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors du nombre d\'actions de modération' }, errors_15)];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    QuestionService = __decorate([
        inversify_1.injectable(),
        __param(0, inversify_1.inject("Question")),
        __param(1, inversify_1.inject("User")),
        __param(2, inversify_1.inject("Answer")),
        __param(3, inversify_1.inject("Thematic")),
        __param(4, inversify_1.inject("Logger")),
        __param(5, inversify_1.inject("EmailService")),
        __param(6, inversify_1.inject("DialogflowService")),
        __param(7, inversify_1.inject("NotificationService")),
        __metadata("design:paramtypes", [mongoose_1.Model,
            mongoose_1.Model,
            mongoose_1.Model,
            mongoose_1.Model,
            tslog_1.Logger,
            email_service_1.EmailService,
            dialogflow_service_1.DialogflowService,
            notification_service_1.NotificationService])
    ], QuestionService);
    return QuestionService;
}(service_1.Service));
exports.QuestionService = QuestionService;
//# sourceMappingURL=question.service.js.map