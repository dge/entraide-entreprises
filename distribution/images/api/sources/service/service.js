"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Service = void 0;
var inversify_1 = require("inversify");
var mongoose_1 = __importStar(require("mongoose"));
var api_response_interface_1 = require("../type/api-response.interface");
var tslog_1 = require("tslog");
var Service = /** @class */ (function () {
    function Service(model, log) {
        this.model = model;
        this.log = log;
    }
    Service.prototype.find = function () {
        return __awaiter(this, void 0, void 0, function () {
            var items, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.model.find()];
                    case 1:
                        items = _a.sent();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 200, items)];
                    case 2:
                        error_1 = _a.sent();
                        this.log.error(error_1);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 200, 'error')];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    Service.prototype.getAll = function (query) {
        return __awaiter(this, void 0, void 0, function () {
            var skip, limit, items, errors_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        skip = query.skip, limit = query.limit;
                        skip = skip ? Number(skip) : 0;
                        limit = limit ? Number(limit) : 10;
                        delete query.skip;
                        delete query.limit;
                        if (query._id) {
                            try {
                                query._id = new mongoose_1.default.mongo.ObjectId(query._id);
                            }
                            catch (error) {
                                this.log.debug("not able to generate mongoose id with content", query._id);
                            }
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.model
                                .find(query)
                                .skip(skip * limit)
                                .limit(limit)];
                    case 2:
                        items = _a.sent();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 200, items)];
                    case 3:
                        errors_1 = _a.sent();
                        this.log.error(errors_1);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la récupération des objets' }, errors_1)];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    Service.prototype.getSpecific = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var item, errors_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.model.findById(id)];
                    case 1:
                        item = _a.sent();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 200, item)];
                    case 2:
                        errors_2 = _a.sent();
                        this.log.error(errors_2);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la récupération de l\'objet' }, errors_2)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    Service.prototype.insert = function (data, ip) {
        return __awaiter(this, void 0, void 0, function () {
            var item, errors_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.log.debug(data);
                        return [4 /*yield*/, this.model.create(data)];
                    case 1:
                        item = _a.sent();
                        if (!item) {
                            return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 404, { message: 'L\'objet n\'a pas pu être récupéré' })];
                        }
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 201, item)];
                    case 2:
                        errors_3 = _a.sent();
                        this.log.error(errors_3);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la création de l\'objet' }, errors_3)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    Service.prototype.update = function (id, data) {
        return __awaiter(this, void 0, void 0, function () {
            var item, errors_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.model.findByIdAndUpdate(id, data, { new: true })];
                    case 1:
                        item = _a.sent();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 202, item)];
                    case 2:
                        errors_4 = _a.sent();
                        this.log.error(errors_4);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la mise à jour de l\'objet' }, errors_4)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    Service.prototype.delete = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var item, errors_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.model.findByIdAndDelete(id)];
                    case 1:
                        item = _a.sent();
                        if (!item)
                            return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 404, { message: 'L\'objet n\'a pas pu être récupéré' })];
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 202, item)];
                    case 2:
                        errors_5 = _a.sent();
                        this.log.error(errors_5);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la suppression de l\'objet' }, errors_5)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    Service = __decorate([
        inversify_1.injectable(),
        __metadata("design:paramtypes", [mongoose_1.Model, tslog_1.Logger])
    ], Service);
    return Service;
}());
exports.Service = Service;
//# sourceMappingURL=service.js.map