"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
var inversify_1 = require("inversify");
var mongoose_1 = require("mongoose");
var service_1 = require("./service");
var api_response_interface_1 = require("../type/api-response.interface");
var bcryptjs_1 = __importDefault(require("bcryptjs"));
var jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
var crypto_1 = __importDefault(require("crypto"));
var email_service_1 = require("./email.service");
var tslog_1 = require("tslog");
var uuid_1 = require("uuid");
var UserService = /** @class */ (function (_super) {
    __extends(UserService, _super);
    function UserService(userModel, notifModel, questionModel, contactModel, answerModel, secretCodeModel, emailService, log) {
        var _this = _super.call(this, userModel, log) || this;
        _this.userModel = userModel;
        _this.notifModel = notifModel;
        _this.questionModel = questionModel;
        _this.contactModel = contactModel;
        _this.answerModel = answerModel;
        _this.secretCodeModel = secretCodeModel;
        _this.emailService = emailService;
        _this.log = log;
        return _this;
    }
    UserService.prototype.checkDuplicateUser = function (usernameOrEmail) {
        return __awaiter(this, void 0, void 0, function () {
            var users, errors_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.userModel.find({
                                $or: [
                                    { username: { $regex: new RegExp("^" + usernameOrEmail.trim() + "$", 'i') } },
                                    { email: { $regex: new RegExp("^" + usernameOrEmail.trim() + "$", 'i') } }
                                ]
                            })];
                    case 1:
                        users = _a.sent();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(users.length > 0, users.length === 0 ? 200 : 400, users.length === 0 ? 'OK' : 'Le nom d\'utilisateur ou l\'adresse email est déjà utilisé')];
                    case 2:
                        errors_1 = _a.sent();
                        this.log.error(errors_1);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors du test utilisateur' }, errors_1)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    UserService.prototype.insert = function (user) {
        return __awaiter(this, void 0, void 0, function () {
            var checkUsername, checkEmail, item, bool, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.checkDuplicateUser(user.username)];
                    case 1:
                        checkUsername = _a.sent();
                        return [4 /*yield*/, this.checkDuplicateUser(user.email)];
                    case 2:
                        checkEmail = _a.sent();
                        if (checkUsername.error || checkEmail.error) {
                            return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 400, 'Le nom d\'utilisateur ou l\'adresse email est déjà utilisé')];
                        }
                        _a.label = 3;
                    case 3:
                        _a.trys.push([3, 6, , 7]);
                        return [4 /*yield*/, this.userModel.create(user)];
                    case 4:
                        item = _a.sent();
                        return [4 /*yield*/, this.createVerifyAccountSecretCode(item)];
                    case 5:
                        bool = _a.sent();
                        if (!bool) {
                            return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de l\'envoi du mail' })];
                        }
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 201, item, null)];
                    case 6:
                        error_1 = _a.sent();
                        this.log.error(error_1);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue pendant la création du compte' }, error_1.errors)];
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    UserService.prototype.signin = function (usernameOrEmail, password) {
        return __awaiter(this, void 0, void 0, function () {
            var user, passwordIsValid, errors_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.userModel.findOne({
                                $or: [
                                    { username: { $regex: new RegExp("^" + usernameOrEmail.trim() + "$", 'i') } },
                                    { email: { $regex: new RegExp("^" + usernameOrEmail.trim() + "$", 'i') } }
                                ]
                            })];
                    case 1:
                        user = _a.sent();
                        if (!user) {
                            return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 401, { message: 'Le couple identifiant/mot de passe n\'existe pas' })];
                        }
                        else {
                            passwordIsValid = bcryptjs_1.default.compareSync(password, user.password);
                            if (!passwordIsValid) {
                                return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 401, { message: 'Le couple identifiant/mot de passe n\'existe pas' })];
                            }
                        }
                        if (!user.isVerified || !user.isCompleted) {
                            return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 401, { message: 'Le compte n\'a pas encore été validé' })];
                        }
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 200, this.gettoken(user))];
                    case 2:
                        errors_2 = _a.sent();
                        this.log.error(errors_2);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue pendant le login' }, errors_2)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    UserService.prototype.gettoken = function (user) {
        var SECRET = process.env.TOKEN_KEY || "token-entraide-secret-key";
        var token = jsonwebtoken_1.default.sign({ username: user.username }, SECRET, {
            expiresIn: 86400 // 24 hours
        });
        return {
            user: {
                id: user._id,
                username: user.username,
                firstName: user.firstName,
                lastName: user.lastName,
                profilePicture: user.profilePicture,
                roles: user.roles,
                anonyme: user.anonyme,
                entities: user.entities,
                email: user.email
            },
            accessToken: token
        };
    };
    UserService.prototype.confirmation = function (userId, token) {
        return __awaiter(this, void 0, void 0, function () {
            var user, errors_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.log.debug(userId);
                        this.log.debug(token);
                        return [4 /*yield*/, this.userModel.findById(userId)];
                    case 1:
                        user = _a.sent();
                        if (!user) {
                            return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 400, { code: 'NOT_FOUND', message: 'Le compte n\'existe pas' })];
                        }
                        user.isVerified = true;
                        user.save();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 200, true)];
                    case 2:
                        errors_3 = _a.sent();
                        this.log.error(errors_3);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue pendant la confirmation de l\'adresse mail' }, errors_3)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    UserService.prototype.verifyResetLink = function (userId, token) {
        return __awaiter(this, void 0, void 0, function () {
            var user, secretCode, errors_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        this.log.debug(userId);
                        this.log.debug(token);
                        return [4 /*yield*/, this.userModel.findById(userId)];
                    case 1:
                        user = _a.sent();
                        if (!user) {
                            return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 400, { code: 'NOT_FOUND', message: 'Le compte n\'existe pas' })];
                        }
                        return [4 /*yield*/, this.secretCodeModel.findOne({ user: userId, token: token })];
                    case 2:
                        secretCode = _a.sent();
                        if (secretCode && secretCode.type === 'passwordReset') {
                            return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 200, true)];
                        }
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 400, { code: 'EXPIRE', message: 'Le lien a expiré' })];
                    case 3:
                        errors_4 = _a.sent();
                        this.log.error(errors_4);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue pendant la vérification du lien' }, errors_4)];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    UserService.prototype.resendVerificationEmail = function (email) {
        return __awaiter(this, void 0, void 0, function () {
            var user, oldSecretCode, bool, errors_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 5, , 6]);
                        this.log.debug(email);
                        return [4 /*yield*/, this.userModel.findOne({
                                $or: [
                                    { username: { $regex: new RegExp("^" + email.trim() + "$", 'i') } },
                                    { email: { $regex: new RegExp("^" + email.trim() + "$", 'i') } }
                                ]
                            })];
                    case 1:
                        user = _a.sent();
                        if (!(user && !user.isVerified)) return [3 /*break*/, 4];
                        return [4 /*yield*/, this.secretCodeModel.findOne({ user: user.id, type: 'emailVerification' })];
                    case 2:
                        oldSecretCode = _a.sent();
                        return [4 /*yield*/, this.createVerifyAccountSecretCode(user)];
                    case 3:
                        bool = _a.sent();
                        if (!bool) {
                            return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de l\'envoi du mail' })];
                        }
                        // Remove the old secret code
                        if (oldSecretCode) {
                            oldSecretCode.remove();
                        }
                        _a.label = 4;
                    case 4: return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 200, true, null)];
                    case 5:
                        errors_5 = _a.sent();
                        this.log.error(errors_5);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: "Une erreur est survenue pendant la v\u00E9rification de l'email " + email }, errors_5)];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    UserService.prototype.getSpecific = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var user, errors_6;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.log.debug(id);
                        return [4 /*yield*/, this.userModel.findById(id)];
                    case 1:
                        user = _a.sent();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 200, user)];
                    case 2:
                        errors_6 = _a.sent();
                        this.log.error(errors_6);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est suvenue pendant la récupération de l\'utilisateur' }, errors_6)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    UserService.prototype.getMe = function (username) {
        return __awaiter(this, void 0, void 0, function () {
            var user, errors_7;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.log.debug(username);
                        return [4 /*yield*/, this.userModel.findOne({ username: username })];
                    case 1:
                        user = _a.sent();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 200, user)];
                    case 2:
                        errors_7 = _a.sent();
                        this.log.error(errors_7);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est suvenue pendant la récupération de l\'utilisateur' }, errors_7)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /** User ask to reset his password */
    UserService.prototype.askResetPassword = function (email) {
        return __awaiter(this, void 0, void 0, function () {
            var user, code, errors_8;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        this.log.debug(email);
                        return [4 /*yield*/, this.userModel.findOne({
                                $or: [
                                    { username: { $regex: new RegExp("^" + email.trim() + "$", 'i') } },
                                    { email: { $regex: new RegExp("^" + email.trim() + "$", 'i') } }
                                ]
                            })];
                    case 1:
                        user = _a.sent();
                        if (!user) {
                            return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 201, true)];
                        }
                        return [4 /*yield*/, this.secretCodeModel.create({
                                user: user._id,
                                type: 'passwordReset',
                                token: crypto_1.default.randomBytes(40).toString('hex')
                            })];
                    case 2:
                        code = _a.sent();
                        this.emailService.sendEmail(user.email, "Réinitialiser votre mot de passe sur Entraide Entreprises", "Pour réinitialiser votre mot de passe, merci de cliquer sur le lien ci-dessous", {
                            urlPasswordReset: process.env.APP_URL + "/utilisateur/reinitialiser-mot-de-passe?userId=" + user._id + "&token=" + code.token,
                            urlSite: process.env.APP_URL
                        }, "reset-password.html").catch(function (err) {
                            _this.log.error(err);
                            return new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue pendant l\'envoi du mail' }, err.errors);
                        });
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 201, true)];
                    case 3:
                        errors_8 = _a.sent();
                        this.log.error(errors_8);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue pendant la demande de réinitialisation du mot de passe' }, errors_8)];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    /** Actually reset the password for a specific user */
    UserService.prototype.resetPassword = function (userId, password, token) {
        return __awaiter(this, void 0, void 0, function () {
            var user, secretCode, errors_9;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        return [4 /*yield*/, this.userModel.findById(userId)];
                    case 1:
                        user = _a.sent();
                        if (!user) {
                            return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 400, { code: 'NOT_FOUND', message: 'Le compte n\'existe pas' })];
                        }
                        return [4 /*yield*/, this.secretCodeModel.findOne({ user: userId, token: token })];
                    case 2:
                        secretCode = _a.sent();
                        if (secretCode && secretCode.type === 'passwordReset') {
                            // Update user to set its status to verified
                            user.password = password;
                            user.isVerified = true;
                            user.save();
                            // Remove the secret code
                            secretCode.remove();
                            return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 200, true)];
                        }
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 400, { code: 'EXPIRE', message: 'La vérification a expiré' })];
                    case 3:
                        errors_9 = _a.sent();
                        this.log.error(errors_9);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la réinitialisation du mot de passe' }, errors_9)];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Like/Unlike a specific user
     * @param {*} form
     * @returns
     */
    UserService.prototype.likeUser = function (form) {
        return __awaiter(this, void 0, void 0, function () {
            var user, errors_10;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.log.debug('likeUser', form);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.userModel.findById(form.id)];
                    case 2:
                        user = _a.sent();
                        if (!user) {
                            return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors du like de l\'utilisateur' })];
                        }
                        // test if user already likes the question
                        if (!user.followers) {
                            user.followers = [];
                        }
                        if (!user.followers.includes(form.userId)) {
                            user.followers.push(form.userId);
                        }
                        else {
                            user.followers.splice(user.followers.indexOf(form.userId), 1);
                        }
                        user.save();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 200, user)];
                    case 3:
                        errors_10 = _a.sent();
                        this.log.error(errors_10);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors du like de la question' }, errors_10)];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * change mode of a user
     * @param {*} username
     * @returns
     */
    UserService.prototype.changeMode = function (username) {
        return __awaiter(this, void 0, void 0, function () {
            var user, errors_11;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.log.debug('changeMode', username);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 4, , 5]);
                        return [4 /*yield*/, this.userModel.findOne({ username: username })];
                    case 2:
                        user = _a.sent();
                        if (!user) {
                            return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors du like de l\'utilisateur' })];
                        }
                        user.anonyme = !user.anonyme;
                        user.save();
                        return [4 /*yield*/, new api_response_interface_1.ApiResponseInterface(false, 200, user)];
                    case 3: return [2 /*return*/, _a.sent()];
                    case 4:
                        errors_11 = _a.sent();
                        this.log.error(errors_11);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors du changement de mode' }, errors_11)];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * change contact of a user
     * @param {*} username
     * @returns
     */
    UserService.prototype.changeContact = function (username) {
        return __awaiter(this, void 0, void 0, function () {
            var user, errors_12;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.log.debug('changeContact', username);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 4, , 5]);
                        return [4 /*yield*/, this.userModel.findOne({ username: username })];
                    case 2:
                        user = _a.sent();
                        if (!user) {
                            return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors du like de l\'utilisateur' })];
                        }
                        user.allowContact = !!!user.allowContact;
                        this.log.debug(user.allowContact);
                        user.save();
                        return [4 /*yield*/, new api_response_interface_1.ApiResponseInterface(false, 200, user)];
                    case 3: return [2 /*return*/, _a.sent()];
                    case 4:
                        errors_12 = _a.sent();
                        this.log.error(errors_12);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors du changement de mode' }, errors_12)];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Like/Unlike a user
     * @param {*} username
     * @returns
     */
    UserService.prototype.followedUsers = function (username) {
        return __awaiter(this, void 0, void 0, function () {
            var user, users, errors_13;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.log.debug('followedUsers', username);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 4, , 5]);
                        return [4 /*yield*/, this.userModel.findOne({ username: username })];
                    case 2:
                        user = _a.sent();
                        if (!user) {
                            return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors du like de l\'utilisateur' })];
                        }
                        return [4 /*yield*/, this.userModel.find({ 'followers': user.id }, ['username', 'followers', 'lastName', 'firstName', 'profilePicture', 'anonyme'])
                                .populate({ path: 'questionCount' })
                                .populate({ path: 'answerCount' })];
                    case 3:
                        users = _a.sent();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 200, users)];
                    case 4:
                        errors_13 = _a.sent();
                        this.log.error(errors_13);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la récupération des utilisateurs' }, errors_13)];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    UserService.prototype.createVerifyAccountSecretCode = function (user) {
        return __awaiter(this, void 0, void 0, function () {
            var code, e_1;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.log.debug(user);
                        return [4 /*yield*/, this.secretCodeModel.create({
                                user: user._id,
                                type: 'emailVerification',
                                token: crypto_1.default.randomBytes(40).toString('hex')
                            })];
                    case 1:
                        code = _a.sent();
                        this.emailService.sendEmail(user.email, "Vérifiez votre compte sur Entraide Entreprises", "Pour confirmer votre adresse mail, merci de cliquer sur le lien ci-dessous", {
                            urlVerification: process.env.APP_URL + "/utilisateur/verifier-compte?userId=" + user._id + "&token=" + code.token,
                            urlSite: process.env.APP_URL
                        }, "account-verification.html").catch(function (err) {
                            _this.log.error(err);
                            return false;
                        });
                        return [2 /*return*/, true];
                    case 2:
                        e_1 = _a.sent();
                        this.log.error(e_1);
                        return [2 /*return*/, false];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    UserService.prototype.getAllUser = function (search, skip, limit) {
        return __awaiter(this, void 0, void 0, function () {
            var filter, users, total, errors_14;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.log.debug(search, skip, limit);
                        skip = skip ? Number(skip) : 0;
                        limit = limit ? Number(limit) : 10;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 4, , 5]);
                        filter = { 'anonyme': false };
                        if (search && search !== 'undefined') {
                            filter = {
                                $and: [
                                    { 'anonyme': false },
                                    {
                                        $or: [
                                            { 'firstName': { $regex: '.*' + search + '.*' } },
                                            { 'lastName': { $regex: '.*' + search + '.*' } },
                                            { 'email': { $regex: '.*' + search + '.*' } }
                                        ]
                                    },
                                ]
                            };
                        }
                        return [4 /*yield*/, this.userModel.find(filter, ['username', 'followers', 'lastName', 'firstName', 'profilePicture', 'anonyme', 'job'])
                                .populate({ path: 'questionCount', match: { valid: true } })
                                .populate({ path: 'answerCount' })
                                .skip(skip * limit)
                                .limit(limit)];
                    case 2:
                        users = _a.sent();
                        return [4 /*yield*/, this.userModel.countDocuments(filter)];
                    case 3:
                        total = _a.sent();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 200, {
                                users: users,
                                total: total
                            })];
                    case 4:
                        errors_14 = _a.sent();
                        this.log.error(errors_14);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la récupération des questions' }, errors_14)];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    UserService.prototype.completeInscription = function (id, body) {
        return __awaiter(this, void 0, void 0, function () {
            var user, errors_15;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.log.debug('completeInscription', body);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.userModel.findById(id)];
                    case 2:
                        user = _a.sent();
                        if (!user) {
                            return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue l\'inscription' })];
                        }
                        user.isCompleted = true;
                        user.businessCreation = body.businessCreation;
                        user.job = body.job;
                        user.entities = [body.entity];
                        user.save();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 200, user)];
                    case 3:
                        errors_15 = _a.sent();
                        this.log.error(errors_15);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors du like de la question' }, errors_15)];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    UserService.prototype.update = function (id, data) {
        return __awaiter(this, void 0, void 0, function () {
            var _i, _a, i, item, errors_16;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        if (data.entities) {
                            for (_i = 0, _a = data.entities; _i < _a.length; _i++) {
                                i = _a[_i];
                                if (i.id == null) {
                                    i.id = uuid_1.v4();
                                }
                            }
                        }
                        return [4 /*yield*/, this.model.findByIdAndUpdate(id, data, { new: true })];
                    case 1:
                        item = _b.sent();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 202, item)];
                    case 2:
                        errors_16 = _b.sent();
                        this.log.error(errors_16);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la mise à jour de l\'objet' }, errors_16)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    UserService.prototype.changeEntity = function (username, entityId) {
        return __awaiter(this, void 0, void 0, function () {
            var user, _i, _a, e, errors_17;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        this.log.debug(username, entityId);
                        return [4 /*yield*/, this.userModel.findOne({ username: username })];
                    case 1:
                        user = _b.sent();
                        if (!user) {
                            return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue pendant la modification de l\'utilisateur' })];
                        }
                        if (user.entities) {
                            for (_i = 0, _a = user.entities; _i < _a.length; _i++) {
                                e = _a[_i];
                                e.actif = e.id === entityId;
                                this.log.debug(e.id, e.name, e.actif);
                            }
                        }
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 200, this.gettoken(user))];
                    case 2:
                        errors_17 = _b.sent();
                        this.log.error(errors_17);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est suvenue pendant la modification de l\'utilisateur' }, errors_17)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Like/Unlike a user
     * @param {*} username
     * @returns
     */
    UserService.prototype.deleteAccount = function (username) {
        return __awaiter(this, void 0, void 0, function () {
            var user, questions, _i, questions_1, q, answers, _a, answers_1, q, notifs, _b, notifs_1, q, codes, _c, codes_1, q, errors_18;
            return __generator(this, function (_d) {
                switch (_d.label) {
                    case 0:
                        this.log.debug('deleteAccount', username);
                        _d.label = 1;
                    case 1:
                        _d.trys.push([1, 24, , 25]);
                        return [4 /*yield*/, this.userModel.findOne({ username: username })];
                    case 2:
                        user = _d.sent();
                        if (!user) {
                            return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors du like de l\'utilisateur' })];
                        }
                        return [4 /*yield*/, this.questionModel.find({ author: user._id })];
                    case 3:
                        questions = _d.sent();
                        _i = 0, questions_1 = questions;
                        _d.label = 4;
                    case 4:
                        if (!(_i < questions_1.length)) return [3 /*break*/, 7];
                        q = questions_1[_i];
                        q.author = null;
                        return [4 /*yield*/, this.questionModel.findByIdAndUpdate(q._id, q, { new: true })];
                    case 5:
                        _d.sent();
                        _d.label = 6;
                    case 6:
                        _i++;
                        return [3 /*break*/, 4];
                    case 7: return [4 /*yield*/, this.answerModel.find({ author: user._id })];
                    case 8:
                        answers = _d.sent();
                        _a = 0, answers_1 = answers;
                        _d.label = 9;
                    case 9:
                        if (!(_a < answers_1.length)) return [3 /*break*/, 12];
                        q = answers_1[_a];
                        q.author = null;
                        return [4 /*yield*/, this.answerModel.findByIdAndUpdate(q._id, q, { new: true })];
                    case 10:
                        _d.sent();
                        _d.label = 11;
                    case 11:
                        _a++;
                        return [3 /*break*/, 9];
                    case 12: return [4 /*yield*/, this.notifModel.find({ user: user._id })];
                    case 13:
                        notifs = _d.sent();
                        _b = 0, notifs_1 = notifs;
                        _d.label = 14;
                    case 14:
                        if (!(_b < notifs_1.length)) return [3 /*break*/, 17];
                        q = notifs_1[_b];
                        return [4 /*yield*/, this.answerModel.findByIdAndDelete(q._id)];
                    case 15:
                        _d.sent();
                        _d.label = 16;
                    case 16:
                        _b++;
                        return [3 /*break*/, 14];
                    case 17: return [4 /*yield*/, this.secretCodeModel.find({ user: user._id })];
                    case 18:
                        codes = _d.sent();
                        _c = 0, codes_1 = codes;
                        _d.label = 19;
                    case 19:
                        if (!(_c < codes_1.length)) return [3 /*break*/, 22];
                        q = codes_1[_c];
                        return [4 /*yield*/, this.answerModel.findByIdAndDelete(q._id)];
                    case 20:
                        _d.sent();
                        _d.label = 21;
                    case 21:
                        _c++;
                        return [3 /*break*/, 19];
                    case 22: return [4 /*yield*/, this.userModel.findByIdAndDelete(user._id)];
                    case 23:
                        _d.sent();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 200, "ok")];
                    case 24:
                        errors_18 = _d.sent();
                        this.log.error(errors_18);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la récupération des utilisateurs' }, errors_18)];
                    case 25: return [2 /*return*/];
                }
            });
        });
    };
    UserService.prototype.contactUser = function (body, username) {
        return __awaiter(this, void 0, void 0, function () {
            var contact, user, contacts, nb, nbJours, nbContact, _i, contacts_1, c, date, userContacts, nb, nbJours, nbContact, _a, contacts_2, c, date, errors_19;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 11, , 12]);
                        this.log.debug(body.user);
                        return [4 /*yield*/, this.userModel.findOne({ _id: body.user })];
                    case 1:
                        contact = _b.sent();
                        if (!contact) {
                            return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de l\'import des questions' })];
                        }
                        this.log.debug(username);
                        return [4 /*yield*/, this.userModel.findOne({ username: username })];
                    case 2:
                        user = _b.sent();
                        if (!user) {
                            return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de l\'import des questions' })];
                        }
                        return [4 /*yield*/, this.contactModel.find({
                                $and: [
                                    { from: user._id },
                                    { to: contact._id }
                                ]
                            })];
                    case 3:
                        contacts = _b.sent();
                        if (contacts) {
                            nb = 0;
                            nbJours = parseInt((process.env.CONTACT_NB_JOURS || '30'), 10);
                            nbContact = parseInt((process.env.CONTACT_NB_CONTACT || '5'), 10);
                            for (_i = 0, contacts_1 = contacts; _i < contacts_1.length; _i++) {
                                c = contacts_1[_i];
                                date = new Date();
                                date.setDate(date.getDate() - nbJours);
                                if (c.createdAt > date) {
                                    nb++;
                                }
                            }
                            if (nb > nbContact) {
                                return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Vous avez déjà envoyé plusieurs messages à cette personne' })];
                            }
                        }
                        return [4 /*yield*/, this.contactModel.find({
                                $and: [
                                    { from: user._id }
                                ]
                            })];
                    case 4:
                        userContacts = _b.sent();
                        if (userContacts) {
                            nb = 0;
                            nbJours = parseInt((process.env.CONTACT_NB_JOURS_USER || '30'), 10);
                            nbContact = parseInt((process.env.CONTACT_NB_CONTACT_USER || '30'), 10);
                            for (_a = 0, contacts_2 = contacts; _a < contacts_2.length; _a++) {
                                c = contacts_2[_a];
                                date = new Date();
                                date.setDate(date.getDate() - nbJours);
                                if (c.createdAt > date) {
                                    nb++;
                                }
                            }
                            if (nb > nbContact) {
                                return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Vous avez atteint votre quotat de demande de contact' })];
                            }
                        }
                        // Envoi du mail au contact
                        return [4 /*yield*/, this.emailService.sendEmail(contact.email, "Demande de contact", "Demande de contact", {
                                urlSite: process.env.APP_URL,
                                email: body.email,
                                user: body.name,
                                message: body.message
                            }, "contact-user.html").catch(function (err) {
                                _this.log.error(err);
                                return new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue pendant l\'envoi du mail' }, err.errors);
                            })];
                    case 5:
                        // Envoi du mail au contact
                        _b.sent();
                        if (!contact.anonyme) return [3 /*break*/, 7];
                        // Si contact anonyme
                        return [4 /*yield*/, this.emailService.sendEmail(body.email, "Demande de contact", "Demande de contact", {
                                urlSite: process.env.APP_URL,
                                message: body.message
                            }, "contact-user-copy-anonyme.html").catch(function (err) {
                                _this.log.error(err);
                                return new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue pendant l\'envoi du mail' }, err.errors);
                            })];
                    case 6:
                        // Si contact anonyme
                        _b.sent();
                        return [3 /*break*/, 9];
                    case 7: 
                    // Si contact non anonyme
                    return [4 /*yield*/, this.emailService.sendEmail(body.email, "Demande de contact", "Demande de contact", {
                            urlSite: process.env.APP_URL,
                            user: contact.firstName + " " + contact.lastName,
                            message: body.message
                        }, "contact-user-copy.html").catch(function (err) {
                            _this.log.error(err);
                            return new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue pendant l\'envoi du mail' }, err.errors);
                        })];
                    case 8:
                        // Si contact non anonyme
                        _b.sent();
                        _b.label = 9;
                    case 9: return [4 /*yield*/, this.contactModel.create({
                            from: user._id,
                            to: contact._id
                        })];
                    case 10:
                        _b.sent();
                        return [3 /*break*/, 12];
                    case 11:
                        errors_19 = _b.sent();
                        this.log.error(errors_19);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la demande de contact' }, errors_19)];
                    case 12: return [2 /*return*/];
                }
            });
        });
    };
    UserService = __decorate([
        inversify_1.injectable(),
        __param(0, inversify_1.inject("User")),
        __param(1, inversify_1.inject("Notification")),
        __param(2, inversify_1.inject("Question")),
        __param(3, inversify_1.inject("Contact")),
        __param(4, inversify_1.inject("Answer")),
        __param(5, inversify_1.inject("SecretCode")),
        __param(6, inversify_1.inject("EmailService")),
        __param(7, inversify_1.inject("Logger")),
        __metadata("design:paramtypes", [mongoose_1.Model,
            mongoose_1.Model,
            mongoose_1.Model,
            mongoose_1.Model,
            mongoose_1.Model,
            mongoose_1.Model,
            email_service_1.EmailService,
            tslog_1.Logger])
    ], UserService);
    return UserService;
}(service_1.Service));
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map