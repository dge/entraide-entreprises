"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NotificationService = void 0;
var inversify_1 = require("inversify");
var mongoose_1 = require("mongoose");
var service_1 = require("./service");
var email_service_1 = require("./email.service");
var tslog_1 = require("tslog");
var api_response_interface_1 = require("../type/api-response.interface");
var NotificationService = /** @class */ (function (_super) {
    __extends(NotificationService, _super);
    function NotificationService(model, userModel, questionModel, answerModel, emailService, log) {
        var _this = _super.call(this, model, log) || this;
        _this.model = model;
        _this.userModel = userModel;
        _this.questionModel = questionModel;
        _this.answerModel = answerModel;
        _this.emailService = emailService;
        _this.log = log;
        return _this;
    }
    NotificationService.prototype.getAll = function (username) {
        return __awaiter(this, void 0, void 0, function () {
            var user, items, errors_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        this.log.debug(username);
                        return [4 /*yield*/, this.userModel.findOne({ username: username })];
                    case 1:
                        user = _a.sent();
                        if (!user) {
                            return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la récupération des questions' })];
                        }
                        return [4 /*yield*/, this.model.find({ user: user._id })];
                    case 2:
                        items = _a.sent();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 200, items)];
                    case 3:
                        errors_1 = _a.sent();
                        this.log.error(errors_1);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la récupération des objets' }, errors_1)];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    NotificationService.prototype.deleteAll = function (username) {
        return __awaiter(this, void 0, void 0, function () {
            var user, errors_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        return [4 /*yield*/, this.userModel.findOne({ username: username })];
                    case 1:
                        user = _a.sent();
                        if (!user) {
                            return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la suppression des notifications' })];
                        }
                        return [4 /*yield*/, this.model.deleteMany({ user: user._id })];
                    case 2:
                        _a.sent();
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(false, 200, true)];
                    case 3:
                        errors_2 = _a.sent();
                        this.log.error(errors_2);
                        return [2 /*return*/, new api_response_interface_1.ApiResponseInterface(true, 500, { message: 'Une erreur est survenue lors de la suppression des notifications' }, errors_2)];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    NotificationService.prototype.notifyCreateQuestion = function (questionId) {
        return __awaiter(this, void 0, void 0, function () {
            var question, userIds, _i, _a, user, users, auth, message, _b, users_1, u, notification;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        this.log.debug(questionId);
                        return [4 /*yield*/, this.questionModel.findById(questionId)
                                .populate({
                                path: 'author',
                                select: ['username', 'firstName', 'lastName', 'anonyme', 'followers']
                            })
                                .populate({
                                path: 'thematic',
                                select: ['label', 'followers']
                            })];
                    case 1:
                        question = _c.sent();
                        if (!question) {
                            return [2 /*return*/];
                        }
                        userIds = [];
                        if (question.author.followers) {
                            userIds = question.author.followers;
                        }
                        if (question.thematic.followers) {
                            for (_i = 0, _a = question.thematic.followers; _i < _a.length; _i++) {
                                user = _a[_i];
                                if (userIds.indexOf(user) === -1 && user !== question.author._id) {
                                    userIds.push(user);
                                }
                            }
                        }
                        this.log.debug(userIds);
                        if (!(userIds.length > 0)) return [3 /*break*/, 8];
                        return [4 /*yield*/, this.userModel.find({ _id: { "$in": userIds } })];
                    case 2:
                        users = _c.sent();
                        this.log.debug(users);
                        auth = question.author.anonyme ? question.author.username : question.author.firstName + ' ' + question.author.lastName;
                        message = auth + " a pos\u00E9 la question <em>" + question.label + "</em> sur la th\u00E9matique <em>" + question.thematic.label + "</em>";
                        this.log.debug(message);
                        _b = 0, users_1 = users;
                        _c.label = 3;
                    case 3:
                        if (!(_b < users_1.length)) return [3 /*break*/, 8];
                        u = users_1[_b];
                        notification = {
                            user: u._id,
                            message: message,
                            type: 'creationQuestion',
                            question: questionId
                        };
                        this.log.debug(u.email);
                        if (!((u.notification
                            && u.notification.thematic_createquestion_appli
                            && question.thematic.followers
                            && question.thematic.followers.indexOf(u._id) !== -1) ||
                            (u.notification
                                && u.notification.user_createquestion_appli
                                && question.author.followers
                                && question.author.followers.indexOf(u._id) !== -1))) return [3 /*break*/, 5];
                        this.log.debug('add notif');
                        return [4 /*yield*/, this.model.create(notification)];
                    case 4:
                        _c.sent();
                        _c.label = 5;
                    case 5:
                        if (!((u.notification
                            && u.notification.thematic_createquestion_email
                            && question.thematic.followers
                            && question.thematic.followers.indexOf(u._id) !== -1) ||
                            (u.notification
                                && u.notification.user_createquestion_email
                                && question.author.followers
                                && question.author.followers.indexOf(u._id) !== -1))) return [3 /*break*/, 7];
                        this.log.debug('send mail');
                        return [4 /*yield*/, this.sendNotification(u.email, notification)];
                    case 6:
                        _c.sent();
                        _c.label = 7;
                    case 7:
                        _b++;
                        return [3 /*break*/, 3];
                    case 8: return [2 /*return*/];
                }
            });
        });
    };
    NotificationService.prototype.notifyAnswerQuestion = function (answerId) {
        return __awaiter(this, void 0, void 0, function () {
            var answer, userIds, users, auth, message, _i, users_2, u, notification;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.log.debug('notifyAnswerQuestion', answerId);
                        return [4 /*yield*/, this.answerModel.findById(answerId)
                                .populate({
                                path: 'author',
                                select: ['username', 'firstName', 'lastName', 'anonyme', 'followers']
                            })
                                .populate({
                                path: 'question',
                                select: ['author', 'label'],
                                populate: { path: 'thematic', select: ['label'] }
                            })];
                    case 1:
                        answer = _a.sent();
                        if (!answer) {
                            return [2 /*return*/];
                        }
                        userIds = answer.author.followers;
                        userIds.push(answer.question.author);
                        return [4 /*yield*/, this.userModel.find({ _id: { "$in": userIds } })];
                    case 2:
                        users = _a.sent();
                        this.log.debug(users);
                        auth = answer.author.anonyme ? answer.author.username : answer.author.firstName + ' ' + answer.author.lastName;
                        message = auth + " a r\u00E9pondu \u00E0 la question <em>" + answer.question.label + "</em> sur la th\u00E9matique <em>" + answer.question.thematic.label + "</em>";
                        this.log.debug(message);
                        _i = 0, users_2 = users;
                        _a.label = 3;
                    case 3:
                        if (!(_i < users_2.length)) return [3 /*break*/, 8];
                        u = users_2[_i];
                        notification = {
                            user: u._id,
                            message: message,
                            type: 'reponseQuestion',
                            question: answer.question.id
                        };
                        this.log.debug(u.email);
                        if (!((u.notification
                            && u.notification.thematic_answerquestion_appli
                            && answer.question.thematic.followers
                            && answer.question.thematic.followers.indexOf(u._id) !== -1) ||
                            (u.notification
                                && u.notification.user_answerquestion_appli
                                && answer.author.followers
                                && answer.author.followers.indexOf(u._id) !== -1) ||
                            (u.notification
                                && u.notification.myquestion_answer_appli
                                && answer.question.author._id === u._id))) return [3 /*break*/, 5];
                        this.log.debug('add notif');
                        return [4 /*yield*/, this.model.create(notification)];
                    case 4:
                        _a.sent();
                        _a.label = 5;
                    case 5:
                        if (!((u.notification
                            && u.notification.thematic_answerquestion_email
                            && answer.question.thematic.followers
                            && answer.question.thematic.followers.indexOf(u._id) !== -1) ||
                            (u.notification
                                && u.notification.user_answerquestion_email
                                && answer.author.followers
                                && answer.author.followers.indexOf(u._id) !== -1) ||
                            (u.notification
                                && u.notification.myquestion_answer_email
                                && answer.question.author._id === u._id))) return [3 /*break*/, 7];
                        this.log.debug('send mail');
                        return [4 /*yield*/, this.sendNotification(u.email, notification)];
                    case 6:
                        _a.sent();
                        _a.label = 7;
                    case 7:
                        _i++;
                        return [3 /*break*/, 3];
                    case 8: return [2 /*return*/];
                }
            });
        });
    };
    NotificationService.prototype.notifyCommentAnswer = function (answerId) {
        return __awaiter(this, void 0, void 0, function () {
            var answer, userIds, users, auth, message, _i, users_3, u, notification;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.log.debug('notifyCommentAnswer', answerId);
                        return [4 /*yield*/, this.answerModel.findById(answerId)
                                .populate({
                                path: 'author',
                                select: ['username', 'firstName', 'lastName', 'anonyme', 'followers']
                            })
                                .populate({
                                path: 'parent',
                                select: ['author', 'label'],
                                populate: {
                                    path: 'question',
                                    select: ['author', 'label'],
                                    populate: {
                                        path: 'thematic',
                                        select: ['label']
                                    }
                                }
                            })];
                    case 1:
                        answer = _a.sent();
                        if (!answer) {
                            return [2 /*return*/];
                        }
                        userIds = answer.author.followers;
                        userIds.push(answer.parent.question.author);
                        return [4 /*yield*/, this.userModel.find({ _id: { "$in": userIds } })];
                    case 2:
                        users = _a.sent();
                        this.log.debug(users);
                        auth = answer.author.anonyme ? answer.author.username : answer.author.firstName + ' ' + answer.author.lastName;
                        message = auth + " a comment\u00E9 une r\u00E9ponse de la question <em>" + answer.question.label + "</em> sur la th\u00E9matique <em>" + answer.question.thematic.label + "</em>";
                        this.log.debug(message);
                        _i = 0, users_3 = users;
                        _a.label = 3;
                    case 3:
                        if (!(_i < users_3.length)) return [3 /*break*/, 8];
                        u = users_3[_i];
                        notification = {
                            user: u._id,
                            message: message,
                            type: 'commentReponse',
                            question: answer.parent.question.id
                        };
                        this.log.debug(u.email);
                        if (!((u.notification
                            && u.notification.thematic_answerquestion_appli
                            && answer.question.thematic.followers
                            && answer.question.thematic.followers.indexOf(u._id) !== -1) ||
                            (u.notification
                                && u.notification.user_answerquestion_appli
                                && answer.author.followers
                                && answer.author.followers.indexOf(u._id) !== -1) ||
                            (u.notification
                                && u.notification.myanswer_comment_appli
                                && answer.parent.author._id === u._id) ||
                            (u.notification
                                && u.notification.myquestion_answer_appli
                                && answer.parent.question.author._id === u._id))) return [3 /*break*/, 5];
                        this.log.debug('add notif');
                        return [4 /*yield*/, this.model.create(notification)];
                    case 4:
                        _a.sent();
                        _a.label = 5;
                    case 5:
                        if (!((u.notification
                            && u.notification.thematic_answerquestion_email
                            && answer.question.thematic.followers
                            && answer.question.thematic.followers.indexOf(u._id) !== -1) ||
                            (u.notification
                                && u.notification.user_answerquestion_email
                                && answer.author.followers
                                && answer.author.followers.indexOf(u._id) !== -1) ||
                            (u.notification
                                && u.notification.myquestion_answer_email
                                && answer.question.author._id === u._id) ||
                            (u.notification
                                && u.notification.myquestion_answer_email
                                && answer.question.author._id === u._id))) return [3 /*break*/, 7];
                        this.log.debug('send mail');
                        return [4 /*yield*/, this.sendNotification(u.email, notification)];
                    case 6:
                        _a.sent();
                        _a.label = 7;
                    case 7:
                        _i++;
                        return [3 /*break*/, 3];
                    case 8: return [2 /*return*/];
                }
            });
        });
    };
    NotificationService.prototype.sendNotification = function (email, notification) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                try {
                    this.emailService.sendEmail(email, "Notification sur Entraide Entreprises", "Une notification a été créée", {
                        notification: notification.message
                            .replace(new RegExp('<em>', 'g'), '')
                            .replace(new RegExp('</em>', 'g'), ''),
                        urlQuestion: process.env.APP_URL + "/questions/question/" + notification.question,
                        urlSite: process.env.APP_URL
                    }, "notification.html").catch(function (err) {
                        _this.log.error(err);
                        return false;
                    });
                    return [2 /*return*/, true];
                }
                catch (e) {
                    this.log.error(e);
                    return [2 /*return*/, false];
                }
                return [2 /*return*/];
            });
        });
    };
    NotificationService = __decorate([
        inversify_1.injectable(),
        __param(0, inversify_1.inject("Notification")),
        __param(1, inversify_1.inject("User")),
        __param(2, inversify_1.inject("Question")),
        __param(3, inversify_1.inject("Answer")),
        __param(4, inversify_1.inject("EmailService")),
        __param(5, inversify_1.inject("Logger")),
        __metadata("design:paramtypes", [mongoose_1.Model,
            mongoose_1.Model,
            mongoose_1.Model,
            mongoose_1.Model,
            email_service_1.EmailService,
            tslog_1.Logger])
    ], NotificationService);
    return NotificationService;
}(service_1.Service));
exports.NotificationService = NotificationService;
//# sourceMappingURL=notification.service.js.map