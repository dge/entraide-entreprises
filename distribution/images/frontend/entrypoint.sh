#!/usr/bin/env sh

set -e

is_error="false" 
if [ -z $HTTPS_PORT ]; then 
    echo "[ERROR] HTTPS_PORT must be set"
    is_error="true"
fi 
if [ -z $BACKEND_API ]; then 
    echo "[ERROR] BACKEND_API must be set"
    is_error="true"
fi 
if [ -z $SERVER_HOSTNAME ]; then 
    echo "[ERROR] SERVER_HOSTNAME must be set"
    is_error="true" 
fi 
if [ -z $SSL_CERTIFICATE_CRT ]; then 
    echo "[ERROR] SSL_CERTIFICATE_CRT must be set"
    is_error="true" 
fi 
if [ -z $SSL_CERTIFICATE_KEY ]; then 
    echo "[ERROR] SSL_CERTIFICATE_KEY must be set"
    is_error="true" 
fi 
if [ -z $AUTH ]; then 
    echo "[ERROR] AUTH must be set"
    is_error="true" 
fi 
if [ "$is_error" == "true" ];  then 
    exit 1 
fi


if [ $AUTH == "true" ]; then 
    USER=`echo $USER | sed 's/ *$//g'`
    if  [ "$PASS_IS_ENCRYPT" == "true" ]; then 
        echo "$USER:$PASSWORD"  >> /etc/nginx/.htpasswd_ENTRAIDE
    else 
        echo "$USER:$(openssl passwd -apr1 $PASSWORD)"  >> /etc/nginx/.htpasswd_ENTRAIDE
    fi
    envsubst '$SERVER_HOSTNAME $HTTPS_PORT $BACKEND_API $SSL_CERTIFICATE_CRT $SSL_CERTIFICATE_KEY' < /etc/nginx/conf.d/site.conf.template > /etc/nginx/conf.d/default.conf
else
    envsubst '$SERVER_HOSTNAME $HTTPS_PORT $BACKEND_API $SSL_CERTIFICATE_CRT $SSL_CERTIFICATE_KEY' < /etc/nginx/conf.d/site_noauth.conf.template > /etc/nginx/conf.d/default.conf
fi

nginx -g 'daemon off;'
