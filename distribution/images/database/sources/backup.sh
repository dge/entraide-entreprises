#!/usr/bin/env bash
set -Eeo pipefail

DUMPDIR=/backup
dbname=MONGO_INITDB_DATABASE
DATE=$(date +%Y%m%d_%H%M%S)
NOW_MONTH=$(date +"%Y-%m")

AMAZON_S3_BUCKET="s3://${AWS_S3_BUCKET}/${ENV}/backup/${NOW_MONTH}/"
AMAZON_S3_BIN="aws"
AMAZON_S3_ENDPOINT="${AWS_S3_ENDPOINT}"
BACKUP_FULL_PATH=${DUMPDIR}/dump_${dbname}_${DATE}.gz
BACKUP_FULL_PATH_ENC=${DUMPDIR}/dump_${dbname}_${DATE}.gz.enc
KEY_PUB_PATH="/backup/dump.pub.pem"

backup_mongo(){
   mongodump  --gzip --archive=${BACKUP_FULL_PATH} --db=$dbname -u $MONGO_INITDB_USERNAME -p $MONGO_INITDB_PASSWORD
   openssl smime -encrypt -binary -aes256 -in ${BACKUP_FULL_PATH} -out  ${BACKUP_FULL_PATH_ENC}  -outform DER ${KEY_PUB_PATH}
   # To decrypt openssl smime -decrypt -in  ${BACKUP_FULL_PATH_ENC}  -binary -inform DEM -inkey PRIVATE_KEY -out db.gz
   rm ${BACKUP_FULL_PATH}
}

upload_s3(){
      ${AMAZON_S3_BIN} s3 cp ${BACKUP_FULL_PATH_ENC} ${AMAZON_S3_BUCKET} --endpoint ${AMAZON_S3_ENDPOINT}
}
backup_mongo
upload_s3


find ${DUMPDIR}/ -name 'dump_*' -mtime +30 -exec rm -f {} \; -print
