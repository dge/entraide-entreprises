#!/bin/bash
set -Eeuo pipefail
error=0
if [ -z $AWS_S3_ENDPOINT ]; then 
    echo "[ERROR] AWS_S3_ENDPOINT  must be set"
    error=1
fi
if [ -z $AWS_ACCESS_KEY_ID ]; then 
    echo "[ERROR] AWS_ACCESS_KEY_ID  must be set"
    error=1
fi
if [ -z $AWS_SECRET_ACCESS_KEY ]; then 
    echo "[ERROR] AWS_SECRET_ACCESS_KEY  must be set"
    error=1
fi
if [ -z $AWS_S3_BUCKET ]; then 
    echo "[ERROR] AWS_S3_BUCKET  must be set"
    error=1
fi
if [ -z $ENV ]; then 
    echo "[ERROR] ENV  must be set"
    error=1
fi
if  [[ $error -eq 1 ]] ; then  
    exit 1
fi
if [ -z  $MONGO_INITDB_DATABASE ]; then 
    echo "[ERROR] init database var ( MONGO_INITDB_DATABASE ) must be set"
    error=1
fi
echo -e "\033[0;34m [MONGO][INFO] set value \033[0m"
sed -i "s/MONGO_INITDB_DATABASE/$MONGO_INITDB_DATABASE/g" "/db-shell/backup.sh"
envsubst '$AWS_S3_ENDPOINT' < /root/.aws/config.tpl > /root/.aws/config
envsubst '$AWS_ACCESS_KEY_ID $AWS_SECRET_ACCESS_KEY' < /root/.aws/credentials.tpl > /root/.aws/credentials

#================================================
#
# EXECUTION DE L'ENTRYPOINT OFFICIEL.
#
#================================================
#/usr/local/bin/docker-entrypoint.sh $@
exec "$@"