
error=0
if [ -z  $MONGO_INITDB_DATABASE ]; then 
    echo "[ERROR] init database var ( MONGO_INITDB_DATABASE ) must be set"
    error=1
fi
if [ -z $MONGO_INITDB_ROOT_USERNAME ]; then 
    echo "[ERROR] root admin user var ( MONGO_INITDB_ROOT_USERNAME )  must be set"
    error=1
fi 
if [ -z $MONGO_INITDB_ROOT_PASSWORD ]; then 
    echo "[ERROR] root admin password var ( MONGO_INITDB_ROOT_PASSWORD )  must be set"
    error=1
fi
if [ -z $MONGO_INITDB_USERNAME ]; then 
    echo "[ERROR] User of initdb var( MONGO_INITDB_USERNAME )  must be set"
    error=1
fi
if [ -z $MONGO_INITDB_PASSWORD ]; then 
    echo "[ERROR] User password of initdb var( MONGO_INITDB_PASSWORD )  must be set"
    error=1
fi
if [ -z $AWS_S3_ENDPOINT ]; then 
    echo "[ERROR] AWS_S3_ENDPOINT  must be set"
    error=1
fi
if [ -z $AWS_ACCESS_KEY_ID ]; then 
    echo "[ERROR] AWS_ACCESS_KEY_ID  must be set"
    error=1
fi
if [ -z $AWS_SECRET_ACCESS_KEY ]; then 
    echo "[ERROR] AWS_SECRET_ACCESS_KEY  must be set"
    error=1
fi
if [ -z $AWS_S3_BUCKET ]; then 
    echo "[ERROR] AWS_S3_BUCKET  must be set"
    error=1
fi
if [ -z $ENV ]; then 
    echo "[ERROR] ENV  must be set"
    error=1
fi
if  [[ $error -eq 1 ]] ; then  
    exit 1
fi
#sed -i "s/MONGO_INITDB_DATABASE/$MONGO_INITDB_DATABASE/g" "/db-shell/backup.sh"
#envsubst '$AWS_S3_ENDPOINT' < /root/.aws/config.tpl > /root/.aws/config
#envsubst '$$AWS_ACCESS_KEY_ID $AWS_SECRET_ACCESS_KEY' < /root/.aws/credentials.tpl > /root/.aws/credentials


mongo -- "$MONGO_INITDB_DATABASE" <<EOF
    var rootUser = '$MONGO_INITDB_ROOT_USERNAME';
    var rootPassword = '$MONGO_INITDB_ROOT_PASSWORD';
    var admin = db.getSiblingDB('admin');
    admin.auth(rootUser, rootPassword);

    var user = '$MONGO_INITDB_USERNAME';
    var passwd = '$MONGO_INITDB_PASSWORD';
    db.createUser({user: user, pwd: passwd, roles: [ { role: "readWrite", db: "$MONGO_INITDB_DATABASE" }]});
EOF
