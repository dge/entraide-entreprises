# # Copie des certificats et des clés de chiffrement dans le volume correspondant 
#!/usr/bin/env bash

current_dir=`dirname "$0"` 
. $current_dir/host.env
date_container="$(date +%s)"
docker volume create ${NAME}_certificates_data
docker run -d --name dummy-${date_container} -v ${NAME}_certificates_data:/etc/nginx/certs nginx
docker cp ./certs dummy-${date_container}:/etc/nginx
docker stop dummy-${date_container}
docker rm dummy-${date_container}
