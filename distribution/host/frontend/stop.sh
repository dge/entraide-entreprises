#!/usr/bin/env bash
set -e

current_dir=`dirname "$0"`

# Sourcing des variables d'environnement :
. $current_dir/host.env

docker stop ${NAME} || true
docker rm ${NAME} || true