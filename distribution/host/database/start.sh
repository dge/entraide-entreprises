#!/usr/bin/env bash
set -e

current_dir=`dirname "$0"`

# Sourcing des variables d'environnement :
. $current_dir/host.env

# Gestion de l'utilisateur OS / CONTENEUR :
if [ "$(whoami)" != "${OS_USER}" ] ; then
    echo "Vous devez lancer le shell à l'aide de l'utilisateur ${OS_USER}"
    exit 1
fi

# Réseau docker :
[[ ! $(docker network ls) =~ ${NETWORK} ]] && docker network create ${NETWORK}

# Exécution du conteneur :
cd ${current_dir}

if [[ "$VERSION" == *-RC ]] ; then
    docker pull ${IMAGE}
fi

eval "echo \"$(cat app.env)\"" > .app.env.temp

# Creation des volumes si non existants.
docker volume create ${NAME}_data
docker volume create ${NAME}_backup

docker run -d \
    --restart unless-stopped \
    --name ${NAME} \
    --env-file=.app.env.temp \
    --network ${NETWORK} \
    -v ${NAME}_data:/data/db \
    -v ${NAME}_backup:/backup \
    ${IMAGE}

exit 0
