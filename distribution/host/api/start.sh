#!/usr/bin/env bash
set -e

current_dir=`dirname "$0"`
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
# Sourcing des variables d'environnement :
. $current_dir/host.env

# Gestion de l'utilisateur OS / CONTENEUR :
if [ "$(whoami)" != "${OS_USER}" ] ; then
    echo "Vous devez lancer le shell à l'aide de l'utilisateur ${OS_USER}"
    exit 1
fi

# Réseau docker :
[[ ! $(docker network ls) =~ ${NETWORK} ]] && docker network create ${NETWORK}

# Exécution du conteneur :
cd ${current_dir}

if [[ "$VERSION" == *-RC ]] ; then
    docker pull ${IMAGE}
fi

eval "echo \"$(cat app.env)\"" > .app.env.temp

# Creation des volumes si non existants.

docker run -d \
    --restart unless-stopped \
    --name ${NAME} \
    --env-file=.app.env.temp \
    --network ${NETWORK} \
    -v ${SCRIPT_DIR}/.app.env.temp:/usr/app/api/dist/api/src/.env \
    ${IMAGE}

exit 0
